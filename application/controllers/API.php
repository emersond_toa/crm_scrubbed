<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Manila');
header('Content-Type: application/json');

class API extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model('Models');
        $this->load->library('session');
        $this->load->database();
        $this->load->model('API_model');
    }

    public function revenue() {
        $revenue = $this->API_model->model_revenue();
        print json_encode($revenue->result(), JSON_PRETTY_PRINT);
    }

    public function revenue_api() {

        $this->form_validation->set_rules('year', 'year', 'required');
        $this->form_validation->set_rules('group', 'group', 'required');
        $this->form_validation->set_rules('type', 'type', 'required');

        if ($this->form_validation->run() === TRUE) {

            $year = $this->input->post('year');
            $group = $this->input->post('group');
            $type = $this->input->post('type');

            $result = [];
            for ($month = 1; $month <= 12; $month++) {
                $dateObj = DateTime::createFromFormat('!m', $month);
                $revenue = $this->API_model->model_revenue($month, $year, $group, $type);
                $result[] = ['labels' => $dateObj->format('M'), "TOTAL" => (isset($revenue->result()[0]->total_amount) ? $revenue->result()[0]->total_amount : 0)];
            }
            print json_encode($result, JSON_PRETTY_PRINT);
        } else {
            print json_encode(['message' => 'error'], JSON_PRETTY_PRINT);
        }
        //model_revenue($month,$year,$group,$type)
    }

    public function ex() {



        $this->form_validation->set_rules('year', 'year', 'required');
        $this->form_validation->set_rules('group', 'group', 'required');


        if ($this->form_validation->run() === TRUE) {

            $year = $this->input->post('year');
            $group = $this->input->post('group');


            $result = [];
            for ($month = 1; $month <= 12; $month++) {
                $dateObj = DateTime::createFromFormat('!m', $month);
                $revenue = $this->API_model->model_experience($month, "2020", "281,282", "qrc", "Yes");
                $result[] = ['labels' => $dateObj->format('M'), "TOTAL" => (isset($revenue->result()[0]->total_amount) ? $revenue->result()[0]->total_amount : 0)];
            }
            print json_encode($result, JSON_PRETTY_PRINT);
        } else {
            // print json_encode(['message' => 'error'], JSON_PRETTY_PRINT);
        }


        $result = [];
        for ($month = 1; $month <= 12; $month++) {
            $dateObj = DateTime::createFromFormat('!m', $month);
            $revenue = $this->API_model->model_experience(sprintf("%02d", $month), "2020", "281,282", "qrc", "Yes");
            $result[] = ['labels' => $dateObj->format('M'), "TOTAL" => (isset($revenue->result()[0]->total_count) ? $revenue->result()[0]->total_count : 0)];
        }
        print json_encode($result, JSON_PRETTY_PRINT);
    }

    public function test() {
        $year = 2020;
        $group = "18,19";
        $result = [];
        for ($month = 1; $month <= 12; $month++) {
            $key = "qrc";
            $value = "Yes";
            $dateObj = DateTime::createFromFormat('!m', $month);
            $data = $this->API_model->model_exprnc(sprintf("%02d", $month), $year, $group, $key, $value);
            $result[] = ['labels' => $dateObj->format('M'), "TOTAL" => (isset($data->result()[0]->value) ? $data->result()[0]->value : 0)];
        }
        print json_encode($result, JSON_PRETTY_PRINT);
    }
    public function test_sum() {
        $year = 2020; 
        $group = "18,19";
        $result = [];
        for ($month = 1; $month <= 12; $month++) {
            $key = "calls_made";
            $dateObj = DateTime::createFromFormat('!m', $month);
            $data = $this->API_model->model_exprnc_sum(sprintf("%02d", $month), $year, $group, $key);
            $result[] = ['labels' => $dateObj->format('M'), "TOTAL" => (isset($data->result()[0]->value) ? $data->result()[0]->value : 0)];
        }
        print json_encode($result, JSON_PRETTY_PRINT);
    }
}

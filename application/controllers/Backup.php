<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Manila');
header('Content-Type: application/json');

class Backup extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model('Models');
        $this->load->library('session');
        $this->load->database();
    }
    public function index() {
        exec("sudo mysqldump --user=admin --password=h3ll0123123!!! --host=localhost crm_database --result-file=/home/ubuntu/crm_backup/".date('m-d-y')."-backup.sql");
    }
}

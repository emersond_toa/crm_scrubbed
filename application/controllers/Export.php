<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
class Export extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->model('Models');
        $this->load->model('Payment_model');
        $this->load->database();
        $this->load->model('Company_model');
    }

    public function index() 
    {
        if (!$this->session->userdata('user_session')) {
            redirect('https://employeeportal.scrubbed.net/');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $data['title'] = 'Export Client';
        $data['clients'] = $this->Company_model->get_all_clients();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('CRM_Export_Client', $data);
        $this->load->view('templates/footer');
    }

    public function export_clients() {
        if (!$this->session->userdata('user_session')) {
            redirect('login');
        }
        $this->form_validation->set_rules('client[]', 'client[]', 'required');
        if ($this->form_validation->run() === TRUE) {
            $clients = $this->input->post('client');

            $data = $this->Company_model->export($clients);
            $filename = 'clients-' . date('mdYHis') . "-report" . ".xls";
            header("Content-Type: application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=\"$filename\"");
            $this->ExportFile($data);

            // $successMsg = 'Successfully exported!';
            // $this->session->set_userdata('success_msg', $successMsg);
            // redirect('clients/export');
        } else {
            $this->session->set_userdata('error_msg', 'All fields are required!');
            redirect('clients/export');
        }
    }

    function ExportFile($records) {
        $heading = false;
        if (!empty($records))
            foreach ($records as $row) {
                if (!$heading) {
                    // display field/column names as a first row
                    echo implode("\t", array_keys($row)) . "\n";
                    $heading = true;
                }
                echo implode("\t", array_values($row)) . "\n";
            }
        exit;
    }
}

<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: application/json');

class Type_of_Service extends CI_Controller 
{
    public function __construct() {
        parent::__construct();
        $this->load->model('TypeofService_model');
        $this->load->model('Company_model');
        $this->load->library('form_validation');
    }

    public function store() 
    {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('companyId', 'companyId', 'required');
            // $this->form_validation->set_rules('group', 'group', 'required');
            $this->form_validation->set_rules('typeofservice', 'typeofservice', 'required');
            $this->form_validation->set_rules('agreement', 'agreement', 'required');
            // $this->form_validation->set_rules('hourlymonthly', 'hourlymonthly', 'required');
            $this->form_validation->set_rules('startdate', 'startdate', 'required');
            // $this->form_validation->set_rules('enddate', 'enddate', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $data = [
                    'company_id' => $this->input->post('tosentity'),
                    // 'group_text' => $this->input->post('group'),
                    'service_id' => $this->input->post('typeofservice'),
                    'agreement_id' => $this->input->post('agreement'),
                    'onetimefee' => html_purify($this->input->post('onetimefee')),
                    'recurringfee' => html_purify($this->input->post('recurringfee')),
                    'hourly_monthly' => $this->input->post('hourlymonthly'),
                    'start_date' => $this->input->post('startdate'),
                    'end_date' => $this->input->post('enddate'),
                    'notes' => html_purify($this->input->post('notes')),
                ];
    
                $this->TypeofService_model->store($data);
                $this->Company_model->last_update($this->input->post('companyId'));
    
                print json_encode(['status' =>'ok']);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function client_services()
    {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $services = $this->TypeofService_model->client_services($this->input->post('id'));
                print json_encode($services);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function destroy() 
    {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $this->TypeofService_model->delete($this->input->post('id'));
                print json_encode(["status" => "ok"]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function show() 
    {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $service = $this->TypeofService_model->show($this->input->post('id'));
                print json_encode(["status" => "ok", 'data' => $service]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }

    }

    public function update() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('editid', 'editid', 'required');
            // $this->form_validation->set_rules('editgroup', 'editgroup', 'required');
            $this->form_validation->set_rules('edittypeofservice', 'edittypeofservice', 'required');
            $this->form_validation->set_rules('editagreement', 'editagreement', 'required');
            $this->form_validation->set_rules('edithourlymonthly', 'edithourlymonthly', 'required');
            $this->form_validation->set_rules('editservicestartdate', 'editservicestartdate', 'required');
            $this->form_validation->set_rules('editenddate', 'editenddate', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $data = [
                    'company_id' => $this->input->post('edittosentity'),
                    'group_text' => $this->input->post('editgroup'),
                    'service_id' => $this->input->post('edittypeofservice'),
                    'agreement_id' => $this->input->post('editagreement'),
                    'onetimefee' => html_purify($this->input->post('editonetimefee')),
                    'recurringfee' => html_purify($this->input->post('editrecurringfee')),
                    'hourly_monthly' => $this->input->post('edithourlymonthly'),
                    'start_date' => $this->input->post('editservicestartdate'),
                    'end_date' => $this->input->post('editenddate'),
                    'notes' => html_purify($this->input->post('editnotes')),
                ];
    
                $this->TypeofService_model->update($this->input->post('editid'), $data);
    
                print json_encode(['status' =>'ok']);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }

    }
}

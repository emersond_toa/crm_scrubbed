<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Manila');
header('Content-Type: application/json');

class Request extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model('Models');
        $this->load->library('session');
        $this->load->database();
        $this->load->model('Company_model');
    }

    public function index() {
        $result = $this->Models->leads_number(2);
        print json_encode($result->result(), JSON_PRETTY_PRINT);
    }

    public function logout() {
        $this->session->unset_userdata('user_session');
        $this->session->unset_userdata('user_position');
        $value = array("message" => "success");
        print json_encode($value);
    }

    public function contact_info() {
        $this->form_validation->set_rules('id', 'id', 'required');
        if ($this->form_validation->run()) {
            $result = $this->Models->contact_information($this->input->post('id'));
            print json_encode($result);
        }
    }

    public function login_crm_api() {
        $login = $this->Models->login_with_crm($this->uri->segment(3));
        if ($login->num_rows() > 0) {
            $this->session->set_userdata('user_session', $login->result()[0]->ID);
            $this->session->set_userdata('user_position', $login->result()[0]->crm_access);
            redirect(site_url());
        } else {
            
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function delete_added_cta() {
        $this->form_validation->set_rules('delete_client', 'delete_client', 'required');
        $this->form_validation->set_rules('delete_type', 'delete_type', 'required');
        if ($this->form_validation->run()) {
            if ($this->input->post('delete_client') === 'ACCOUNTING') {
                $delete = $this->input->post('delete_client');
                $this->Models->delete_client_acc($delete);
            } else {
                $delete = $this->input->post('delete_client');
                $this->Models->delete_client_tax($delete);
            }

            $value = array("message" => "success", "type" => "add", "text" => "Successfully deleted");
            print json_encode($value);
        }
    }

    public function delete_client() {
        $this->form_validation->set_rules('delete_client', 'delete_client', 'required');
        if ($this->form_validation->run()) {
            $this->Models->delete_client_id($this->input->post('delete_client'));
            $value = array("message" => "success", "type" => "add", "text" => "Successfully deleted");
            print json_encode($value);
        }
    }

    public function delete_service() {
        $this->form_validation->set_rules('delete_service', 'delete_service', 'required');
        if ($this->form_validation->run()) {
            $this->Models->delete_service_id($this->input->post('delete_service'));
            $value = array("message" => "success", "type" => "delete", "text" => "Successfully deleted");
            print json_encode($value);
        }
    }

    public function delete_service_doc() {
        $this->form_validation->set_rules('delete_service_doc', 'delete_service_doc', 'required');
        if ($this->form_validation->run()) {
            if (file_exists('/var/www/html/crm/uploads/' . $this->input->post('file_name'))) {
                unlink("/var/www/html/crm/uploads/" . $this->input->post('file_name'));
            }
            $this->Models->delete_service_doc_id($this->input->post('delete_service_doc'));
            $value = array("message" => "success", "type" => "delete", "text" => "Successfully deleted");
            print json_encode($value);
        }
    }

    public function delete_company_address() {
        $this->form_validation->set_rules('delete_company_address', 'delete_company_address', 'required');
        if ($this->form_validation->run()) {
            $this->Models->delete_company_address_id($this->input->post('delete_company_address'));
            $value = array("message" => "success", "type" => "delete", "text" => "Successfully deleted");
            print json_encode($value);
        }
    }

    public function delete_former_name() {
        $this->form_validation->set_rules('delete_former_name', 'delete_former_name', 'required');
        if ($this->form_validation->run()) {
            $this->Models->delete_former_name_id($this->input->post('delete_former_name'));
            $value = array("message" => "success", "type" => "delete", "text" => "Successfully deleted");
            print json_encode($value);
        }
    }

    public function delete_former_name_doc() {
        $this->form_validation->set_rules('delete_former_name_doc', 'delete_former_name_doc', 'required');
        if ($this->form_validation->run()) {
            if (file_exists('/var/www/html/crm/uploads/' . $this->input->post('file_name'))) {
                unlink("/var/www/html/crm/uploads/" . $this->input->post('file_name'));
            }
            $this->Models->delete_former_name_doc_id($this->input->post('delete_former_name_doc'));
            $value = array("message" => "success", "type" => "delete", "text" => "Successfully deleted");
            print json_encode($value);
        }
    }

    public function delete_client_doc() {
        $this->form_validation->set_rules('delete_client_doc', 'delete_client_doc', 'required');
        if ($this->form_validation->run()) {
            if (file_exists('/var/www/html/crm/uploads/' . $this->input->post('file_name'))) {
                unlink("/var/www/html/crm/uploads/" . $this->input->post('file_name'));
            }
            $this->Models->delete_client_doc_id($this->input->post('delete_client_doc'));
            $value = array("message" => "success", "type" => "delete", "text" => "Successfully deleted");
            print json_encode($value);
        }
    }

    public function update_client() {
        if ($this->session->userdata('user_session')) {
            
        }
        $this->form_validation->set_rules('Company', 'Company', 'required');
        $this->form_validation->set_rules('Type', 'Type', 'required');
        $this->form_validation->set_rules('Marketing_Status', 'Marketing_Status', 'required');
        $this->form_validation->set_rules('Date_Signed', 'Date_Signed', 'required');
        $this->form_validation->set_rules('Team', 'Team', 'required');
        $this->form_validation->set_rules('First', 'First', 'required');
        $this->form_validation->set_rules('Hourly', 'Hourly', 'required');
        $this->form_validation->set_rules('Monthly', 'Monthly', 'required');
        $this->form_validation->set_rules('Non_recurring', 'Non_recurring', 'required');
        $this->form_validation->set_rules('Industry', 'Industry', 'required');
        $this->form_validation->set_rules('Date_From', 'Date_From', 'required');
        $this->form_validation->set_rules('Date_To', 'Date_To', 'required');
        $this->form_validation->set_rules('Status', 'Status', 'required');
        if ($this->form_validation->run()) {
            $add = array(
                "Company" => $this->input->post('Company'),
                "Type" => $this->input->post('Type'),
                "Marketing_Status" => $this->input->post('Marketing_Status'),
                "Date_Signed" => $this->input->post('Date_Signed'),
                "Team" => $this->input->post('Team'),
                "First" => $this->input->post('First'),
                "Hourly" => $this->input->post('Hourly'),
                "Monthly" => $this->input->post('Monthly'),
                "Non_recurring" => $this->input->post('Non_recurring'),
                "Industry" => $this->input->post('Industry'),
                "Date_From" => $this->input->post('Date_From'),
                "Date_To" => $this->input->post('Date_To'),
                "Status" => $this->input->post('Status')
            );
            //$this->Models->add_client($add);
            $value = array("message" => "success", "type" => "add", "text" => "Successfully added");
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    public function add_new_client() {
        if (!$this->session->userdata('user_session')) {
            
        }
        $this->form_validation->set_rules('Company', 'Company', 'required');
        $this->form_validation->set_rules('Type', 'Type', 'required');
        $this->form_validation->set_rules('Marketing_Status', 'Marketing_Status', 'required');
        $this->form_validation->set_rules('Date_Signed', 'Date_Signed', 'required');
        $this->form_validation->set_rules('Team', 'Team', 'required');
        $this->form_validation->set_rules('First', 'First', 'required');
        $this->form_validation->set_rules('Hourly', 'Hourly', 'required');
        $this->form_validation->set_rules('Monthly', 'Monthly', 'required');
        $this->form_validation->set_rules('Non_recurring', 'Non_recurring', 'required');
        $this->form_validation->set_rules('Industry', 'Industry', 'required');
        $this->form_validation->set_rules('Date_From', 'Date_From', 'required');
        $this->form_validation->set_rules('Date_To', 'Date_To', 'required');
        $this->form_validation->set_rules('Status', 'Status', 'required');
        if ($this->form_validation->run()) {
            $add = array(
                "Company" => $this->input->post('Company'),
                "Type" => $this->input->post('Type'),
                "Marketing_Status" => $this->input->post('Marketing_Status'),
                "Date_Signed" => $this->input->post('Date_Signed'),
                "Team" => $this->input->post('Team'),
                "First" => $this->input->post('First'),
                "Hourly" => $this->input->post('Hourly'),
                "Monthly" => $this->input->post('Monthly'),
                "Non_recurring" => $this->input->post('Non_recurring'),
                "Industry" => $this->input->post('Industry'),
                "Date_From" => $this->input->post('Date_From'),
                "Date_To" => $this->input->post('Date_To'),
                "Status" => $this->input->post('Status')
            );
            $this->Models->add_client($add);
            $value = array("message" => "success", "type" => "add", "text" => "Successfully added");
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    function add_tax_form() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        $this->form_validation->set_rules('REF', 'REF', 'required');
        $this->form_validation->set_rules('What_does', 'What_does', 'required');

        $this->form_validation->set_rules('How_long_have', 'How_long_have', 'required');
        $this->form_validation->set_rules('Who_is_currently', 'Who_is_currently', 'required');
        $this->form_validation->set_rules('What_type_of_annual', 'What_type_of_annual', 'required');
        $this->form_validation->set_rules('Tax_What_year_was', 'Tax_What_year_was', 'required');

        $this->form_validation->set_rules('Are_you_required', 'What_is_your_current', 'required');
        $this->form_validation->set_rules('Are_you_required_sales', 'Are_you_required_sales', 'required');

        $Do_you_have_any = '';
        $Are_you_required = '';
        $Do_you_need = '';
        $Do_you_need_tax = '';
        if ($this->form_validation->run()) {
            $other = '';
            for ($x = 1; $x <= 5; $x++) {
                if ($this->input->post('Do_you_have_any' . $x)) {
                    $Do_you_have_any .= $this->input->post('Do_you_have_any' . $x) . ',';
                }
            }

            for ($x = 1; $x <= 7; $x++) {
                if ($this->input->post('Are_you_required' . $x)) {
                    $Are_you_required .= $this->input->post('Are_you_required' . $x) . ',';
                }
            }

            for ($x = 1; $x <= 9; $x++) {
                if ($this->input->post('Do_you_need' . $x)) {
                    $Do_you_need .= $this->input->post('Do_you_need' . $x) . ',';
                }
            }

            for ($x = 1; $x <= 9; $x++) {
                if ($this->input->post('Do_you_need_tax' . $x)) {
                    $Do_you_need_tax .= $this->input->post('Do_you_need_tax' . $x) . ',';
                }
            }


            $cna = array(
                "tax_type" => 'TAX',
                "CID" => $this->input->post('CID'),
                "REF_ID" => $this->input->post('REF'),
                "What_does" => $this->input->post('What_does'),
                "How_long_have" => $this->input->post('How_long_have'),
                "Tax_Who_is_currently" => $this->input->post('Who_is_currently'),
                "Tax_What_type_of" => $this->input->post('What_type_of_annual'),
                "Tax_What_year_was" => $this->input->post('Tax_What_year_was'),
                "Tax_Do_you_have" => $Do_you_have_any,
                "Tax_Are_you_currently" => $this->input->post('Are_you_currently'),
                "Tax_file_sales" => $this->input->post('Are_you_required_sales'),
                "Tax_file_local_business" => $other,
                "Tax_Do_you_need_tax_planning" => $Do_you_need,
                "Tax_Do_you_need_tax" => $Do_you_need_tax,
                "date_to_string" => strtotime('now')
            );

            $this->Models->add_crm_client_tax($cna);
            $value = array("message" => "success", "type" => "add", "text" => "Successfully added", "cid" => $this->input->post('CID'), "ref" => $this->input->post('CID'));
        } else {
            $value = array("message" => "error", "text" => "error test");
        }
        print json_encode($value);
    }

    public function client_tax_form() {

        $this->form_validation->set_rules('Contact_Person', 'Contact_Person', 'required');
        $this->form_validation->set_rules('Position_Title', 'Position_Title', 'required');
        $this->form_validation->set_rules('Email_Address', 'Email_Address', 'required');
        $this->form_validation->set_rules('Phone_Number', 'Phone_Number', 'required');
        $this->form_validation->set_rules('Company_Legal', 'Company_Legal', 'required');
        $this->form_validation->set_rules('Company_Full', 'Company_Full', 'required');
        $this->form_validation->set_rules('Company_Website', 'Company_Website', 'required');
        $this->form_validation->set_rules('What_does', 'What_does', 'required');

        $this->form_validation->set_rules('How_long_have', 'How_long_have', 'required');
        $this->form_validation->set_rules('Who_is_currently', 'Who_is_currently', 'required');
        $this->form_validation->set_rules('What_type_of_annual', 'What_type_of_annual', 'required');
        $this->form_validation->set_rules('Tax_What_year_was', 'Tax_What_year_was', 'required');

        $this->form_validation->set_rules('Are_you_required', 'What_is_your_current', 'required');
        $this->form_validation->set_rules('Are_you_required_sales', 'Are_you_required_sales', 'required');

        $Do_you_have_any = '';
        $Are_you_required = '';
        $Do_you_need = '';
        $Do_you_need_tax = '';
        if ($this->form_validation->run()) {
            $other = '';
            for ($x = 1; $x <= 5; $x++) {
                if ($this->input->post('Do_you_have_any' . $x)) {
                    $Do_you_have_any .= $this->input->post('Do_you_have_any' . $x) . ',';
                }
            }

            for ($x = 1; $x <= 7; $x++) {
                if ($this->input->post('Are_you_required' . $x)) {
                    $Are_you_required .= $this->input->post('Are_you_required' . $x) . ',';
                }
            }

            for ($x = 1; $x <= 9; $x++) {
                if ($this->input->post('Do_you_need' . $x)) {
                    $Do_you_need .= $this->input->post('Do_you_need' . $x) . ',';
                }
            }

            for ($x = 1; $x <= 9; $x++) {
                if ($this->input->post('Do_you_need_tax' . $x)) {
                    $Do_you_need_tax .= $this->input->post('Do_you_need_tax' . $x) . ',';
                }
            }

            $ref_id = strtotime('now');
            $add = array(
                "REF_ID" => $ref_id,
                "Contact_Person" => $this->input->post('Contact_Person'),
                "Position_Title" => $this->input->post('Position_Title'),
                "Email_Address" => $this->input->post('Email_Address'),
                "Phone_Number" => $this->input->post('Phone_Number'),
                "Company_Legal" => $this->input->post('Company_Legal'),
                "Company_Full" => $this->input->post('Company_Full'),
                "Company_Website" => $this->input->post('Company_Website'),
                "What_does" => $this->input->post('What_does'),
                "What_industry" => 'none',
                "How_long_have" => $this->input->post('How_long_have'),
                "How_is_the_company" => 'none',
                "Do_you_currently" => 'none',
                "What_is_your_current" => 'none',
                "Do_we_need_to_bring" => 'none',
                "Do_you_need_to_track" => 'none',
                "What_other_Scrubbed" => $other,
                "How_did_you" => 'none',
                "date_to_string" => strtotime('now'),
                "submitted_date" => date('Y-m-d')
            );

            $cna = array(
                "tax_type" => $this->input->post('form_type'),
                "REF_ID" => $ref_id,
                "What_does" => $this->input->post('What_does'),
                "How_long_have" => $this->input->post('How_long_have'),
                "Tax_Who_is_currently" => $this->input->post('Who_is_currently'),
                "Tax_What_type_of" => $this->input->post('What_type_of_annual'),
                "Tax_What_year_was" => $this->input->post('Tax_What_year_was'),
                "Tax_Do_you_have" => $Do_you_have_any,
                "Tax_Are_you_currently" => $this->input->post('Are_you_currently'),
                "Tax_file_sales" => $this->input->post('Are_you_required_sales'),
                "Tax_file_local_business" => $other,
                "Tax_Do_you_need_tax_planning" => $Do_you_need,
                "Tax_Do_you_need_tax" => $Do_you_need_tax,
                "date_to_string" => strtotime('now')
            );

            $this->Models->add_client($add);
            $this->Models->add_crm_client_tax($cna);

            $value = array("message" => "success", "type" => "add", "text" => "Successfully added");
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    public function client_website_form() {
        $this->form_validation->set_rules('Contact_Person', 'Contact_Person', 'required');
        $this->form_validation->set_rules('Position_Title', 'Position_Title', 'required');
        $this->form_validation->set_rules('Email_Address', 'Email_Address', 'required');
        $this->form_validation->set_rules('Phone_Number', 'Phone_Number', 'required');
        $this->form_validation->set_rules('Company_Legal', 'Company_Legal', 'required');
        $this->form_validation->set_rules('Company_Full', 'Company_Full', 'required');
        $this->form_validation->set_rules('Company_Website', 'Company_Website', 'required');
        $this->form_validation->set_rules('What_does', 'What_does', 'required');

        $this->form_validation->set_rules('What_industry', 'What_industry', 'required');
        $this->form_validation->set_rules('How_long_have', 'How_long_have', 'required');
        $this->form_validation->set_rules('How_is_the_company', 'How_is_the_company', 'required');
        $this->form_validation->set_rules('Do_you_currently', 'Do_you_currently', 'required');
        $this->form_validation->set_rules('What_is_your_current', 'What_is_your_current', 'required');
        $this->form_validation->set_rules('Do_we_need_to_bring', 'Do_we_need_to_bring', 'required');
        $this->form_validation->set_rules('Do_you_need_to_track', 'Do_you_need_to_track', 'required');

        if ($this->form_validation->run()) {
            $other = '';
            for ($x = 1; $x <= 7; $x++) {
                if ($this->input->post('What_other_Scrubbed' . $x)) {
                    $other .= $this->input->post('What_other_Scrubbed' . $x) . ',';
                }
            }
            $ref_id = strtotime('now');
            $add = array(
                "REF_ID" => $ref_id,
                "Contact_Person" => $this->input->post('Contact_Person'),
                "Position_Title" => $this->input->post('Position_Title'),
                "Email_Address" => $this->input->post('Email_Address'),
                "Phone_Number" => $this->input->post('Phone_Number'),
                "Company_Legal" => $this->input->post('Company_Legal'),
                "Company_Full" => $this->input->post('Company_Full'),
                "Company_Website" => $this->input->post('Company_Website'),
                "What_does" => $this->input->post('What_does'),
                "What_industry" => $this->input->post('What_industry'),
                "How_long_have" => $this->input->post('How_long_have'),
                "How_is_the_company" => $this->input->post('How_is_the_company'),
                "Do_you_currently" => $this->input->post('Do_you_currently'),
                "What_is_your_current" => $this->input->post('What_is_your_current'),
                "Do_we_need_to_bring" => $this->input->post('Do_we_need_to_bring'),
                "Do_you_need_to_track" => $this->input->post('Do_you_need_to_track'),
                "What_other_Scrubbed" => $other,
                "How_did_you" => $this->input->post('How_did_you'),
                "date_to_string" => strtotime('now'),
                "submitted_date" => date('Y-m-d')
            );

            $cna = array(
                "REF_ID" => $ref_id,
                "What_does" => $this->input->post('What_does'),
                "What_industry" => $this->input->post('What_industry'),
                "How_long_have" => $this->input->post('How_long_have'),
                "How_is_the_company" => $this->input->post('How_is_the_company'),
                "Do_you_currently" => $this->input->post('Do_you_currently'),
                "What_is_your_current" => $this->input->post('What_is_your_current'),
                "Do_we_need_to_bring" => $this->input->post('Do_we_need_to_bring'),
                "Do_you_need_to_track" => $this->input->post('Do_you_need_to_track'),
                "What_other_Scrubbed" => $other,
                "How_did_you" => $this->input->post('How_did_you'),
                "date_to_string" => strtotime('now'),
                "submitted_date" => date('Y-m-d'),
                "form_type" => $this->input->post('form_type'),
            );
            $this->Models->add_client($add);
            $this->Models->add_client_assesment($cna);
            $value = array("message" => "success", "type" => "add", "text" => "Successfully added");
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    function client_information_upload_doc() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        if ($this->form_validation->run() === TRUE) {
            $key_id = $this->input->post('CID');
            if (isset($_FILES['file'])) {
                $total = count($_FILES['file']['name']);
                $ext = array("doc", "docx", "ppt", "pptx", "xls", "xlsx", "pdf", "jpg", "jpeg", "JPG", "png", "gif");
                for ($i = 0; $i < $total; $i++) {
                    $fname = $_FILES['file']['name'][$i];
                    if (in_array(pathinfo($fname, PATHINFO_EXTENSION), $ext)) {
                        $tmpFilePath = $_FILES['file']['tmp_name'][$i];
                        move_uploaded_file($tmpFilePath, 'uploads/' . $key_id . '_' . $fname);
                        $array = array(
                            'CID' => $key_id,
                            'file_name' => $key_id . '_' . $_FILES['file']['name'][$i],
                            'date_uploaded' => date('m/d/y h:i:s A'),
                            'extension' => pathinfo($fname, PATHINFO_EXTENSION),
                        );
                        $this->Models->information_upload_doc($array);
                    }
                }
                $value = array(
                    "message" => "success",
                    "cid" => $key_id,
                    "text" => "Success!",
                    "type" => "info"
                );
            } else {
                $value = array(
                    "message" => "error",
                    "text" => "Error uploading",
                    "type" => "warning"
                );
            }
            print json_encode($value);
        }
    }

    public function add_cna_form() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        $this->form_validation->set_rules('REF', 'REF', 'required');
        $this->form_validation->set_rules('Add_What_does', 'Add_What_does', 'required');
        $this->form_validation->set_rules('Add_What_industry', 'Add_What_industry', 'required');
        $this->form_validation->set_rules('Add_How_long_have', 'Add_How_long_have', 'required');
        $this->form_validation->set_rules('Add_How_is_the_company', 'Add_How_is_the_company', 'required');
        $this->form_validation->set_rules('Add_Do_you_currently', 'Add_Do_you_currently', 'required');
        $this->form_validation->set_rules('Add_What_is_your_current', 'Add_What_is_your_current', 'required');
        $this->form_validation->set_rules('Add_Do_we_need_to_bring', 'Add_Do_we_need_to_bring', 'required');
        $this->form_validation->set_rules('Add_Do_you_need_to_track', 'Add_Do_you_need_to_track', 'required');
        $this->form_validation->set_rules('Add_How_did_you', 'Add_How_did_you', 'required');
        if ($this->form_validation->run()) {
            $other = '';
            for ($x = 1; $x <= 7; $x++) {
                if ($this->input->post('Add_What_other_Scrubbed' . $x)) {
                    $other .= $this->input->post('Add_What_other_Scrubbed' . $x) . ',';
                }
            }
            $other = rtrim($other, ",");
            $add = array(
                "REF_ID" => $this->input->post('REF'),
                "CID" => $this->input->post('CID'),
                "What_does" => $this->input->post('Add_What_does'),
                "What_industry" => $this->input->post('Add_What_industry'),
                "How_long_have" => $this->input->post('Add_How_long_have'),
                "How_is_the_company" => $this->input->post('Add_How_is_the_company'),
                "Do_you_currently" => $this->input->post('Add_Do_you_currently'),
                "What_is_your_current" => $this->input->post('Add_What_is_your_current'),
                "Do_we_need_to_bring" => $this->input->post('Add_Do_we_need_to_bring'),
                "Do_you_need_to_track" => $this->input->post('Add_Do_you_need_to_track'),
                "What_other_Scrubbed" => $other,
                "How_did_you" => $this->input->post('Add_How_did_you'),
                "date_to_string" => strtotime('now'),
                "submitted_date" => date('m/d/Y h:i:s A'),
                "form_type" => 'ACCOUNTING'
            );
            $this->Models->add_client_assesment($add);
            $value = array("message" => "success", "type" => "add", "text" => "Successfully added", "cid" => $this->input->post('CID'), "ref" => $this->input->post('CID'));
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    public function update_admin_client_form() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        $this->form_validation->set_rules('Contact_Person', 'Contact_Person', 'required');
        $this->form_validation->set_rules('Position_Title', 'Position_Title', 'required');
        $this->form_validation->set_rules('Email_Address', 'Email_Address', 'required');
        $this->form_validation->set_rules('Phone_Number', 'Phone_Number', 'required');
        $this->form_validation->set_rules('Company_Legal', 'Company_Legal', 'required');
        $this->form_validation->set_rules('Company_Full', 'Company_Full', 'required');
        $this->form_validation->set_rules('Company_Website', 'Company_Website', 'required');
        $this->form_validation->set_rules('What_does', 'What_does', 'required');
        $this->form_validation->set_rules('What_industry', 'What_industry', 'required');
        $this->form_validation->set_rules('How_long_have', 'How_long_have', 'required');
        $this->form_validation->set_rules('How_is_the_company', 'How_is_the_company', 'required');
        $this->form_validation->set_rules('Do_you_currently', 'Do_you_currently', 'required');
        $this->form_validation->set_rules('What_is_your_current', 'What_is_your_current', 'required');
        $this->form_validation->set_rules('Do_we_need_to_bring', 'Do_we_need_to_bring', 'required');
        $this->form_validation->set_rules('Do_you_need_to_track', 'Do_you_need_to_track', 'required');
        if ($this->form_validation->run()) {
            $other = '';
            for ($x = 1; $x <= 7; $x++) {
                if ($this->input->post('What_other_Scrubbed' . $x)) {
                    $other .= $this->input->post('What_other_Scrubbed' . $x) . ',';
                }
            }
            $other = rtrim($other, ",");
            $update = array(
                "Contact_Person" => $this->input->post('Contact_Person'),
                "Position_Title" => $this->input->post('Position_Title'),
                "Email_Address" => $this->input->post('Email_Address'),
                "Phone_Number" => $this->input->post('Phone_Number'),
                "Company_Legal" => $this->input->post('Company_Legal'),
                "Company_Full" => $this->input->post('Company_Full'),
                "Company_Website" => $this->input->post('Company_Website'),
                "What_does" => $this->input->post('What_does'),
                "What_industry" => $this->input->post('What_industry'),
                "How_long_have" => $this->input->post('How_long_have'),
                "How_is_the_company" => $this->input->post('How_is_the_company'),
                "Do_you_currently" => $this->input->post('Do_you_currently'),
                "What_is_your_current" => $this->input->post('What_is_your_current'),
                "Do_we_need_to_bring" => $this->input->post('Do_we_need_to_bring'),
                "Do_you_need_to_track" => $this->input->post('Do_you_need_to_track'),
                "What_other_Scrubbed" => $other,
                "How_did_you" => $this->input->post('How_did_you'),
                "date_to_string" => strtotime('now'),
                "submitted_date" => date('m/d/Y h:i:s A')
            );
            $this->Models->update_client($update, $this->input->post('CID'));
            $value = array("message" => "success", "type" => "update", "text" => "Successfully updated $other " . $this->input->post('CID'));
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    function update_tax() {
        $this->form_validation->set_rules('TID', 'TID', 'required');

        $this->form_validation->set_rules('Update_What_does', 'What_does', 'required');

        $this->form_validation->set_rules('How_long_have', 'How_long_have', 'required');
        $this->form_validation->set_rules('Who_is_currently', 'Who_is_currently', 'required');
        $this->form_validation->set_rules('What_type_of_annual', 'What_type_of_annual', 'required');
        $this->form_validation->set_rules('Tax_What_year_was', 'Tax_What_year_was', 'required');

        $this->form_validation->set_rules('Are_you_required', 'What_is_your_current', 'required');
        $this->form_validation->set_rules('Are_you_required_sales', 'Are_you_required_sales', 'required');

        $Do_you_have_any = '';
        $Are_you_required = '';
        $Do_you_need = '';
        $Do_you_need_tax = '';
        if ($this->form_validation->run()) {
            $other = '';
            for ($x = 1; $x <= 5; $x++) {
                if ($this->input->post('Do_you_have_any' . $x)) {
                    $Do_you_have_any .= $this->input->post('Do_you_have_any' . $x) . ',';
                }
            }

            for ($x = 1; $x <= 7; $x++) {
                if ($this->input->post('Are_you_required' . $x)) {
                    $Are_you_required .= $this->input->post('Are_you_required' . $x) . ',';
                }
            }

            for ($x = 1; $x <= 9; $x++) {
                if ($this->input->post('Do_you_need' . $x)) {
                    $Do_you_need .= $this->input->post('Do_you_need' . $x) . ',';
                }
            }

            for ($x = 1; $x <= 9; $x++) {
                if ($this->input->post('Do_you_need_tax' . $x)) {
                    $Do_you_need_tax .= $this->input->post('Do_you_need_tax' . $x) . ',';
                }
            }

            $tid = $this->input->post('TID');
            $tax = array(
                "What_does" => $this->input->post('What_does'),
                "How_long_have" => $this->input->post('How_long_have'),
                "Tax_Who_is_currently" => $this->input->post('Who_is_currently'),
                "Tax_What_type_of" => $this->input->post('What_type_of_annual'),
                "Tax_What_year_was" => $this->input->post('Tax_What_year_was'),
                "Tax_Do_you_have" => $Do_you_have_any,
                "Tax_Are_you_currently" => $this->input->post('Are_you_currently'),
                "Tax_file_sales" => $this->input->post('Are_you_required_sales'),
                "Tax_file_local_business" => $other,
                "Tax_Do_you_need_tax_planning" => $Do_you_need,
                "Tax_Do_you_need_tax" => $Do_you_need_tax,
            );
            $this->Models->update_client_tax($tid, $tax);
            $value = array("message" => "success", "type" => "update", "text" => "Successfully updated ", "payat" => $Do_you_need_tax);
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    public function update_accounting() {
        $this->form_validation->set_rules('CAID', 'CAID', 'required');
        $this->form_validation->set_rules('REF', 'REF', 'required');
        $this->form_validation->set_rules('Update_What_does', 'Update_What_does', 'required');
        $this->form_validation->set_rules('Update_What_industry', 'Update_What_industry', 'required');
        $this->form_validation->set_rules('Update_How_long_have', 'Update_How_long_have', 'required');
        $this->form_validation->set_rules('Update_How_is_the_company', 'Update_How_is_the_company', 'required');
        $this->form_validation->set_rules('Update_Do_you_currently', 'Update_Do_you_currently', 'required');
        $this->form_validation->set_rules('Update_What_is_your_current', 'Update_What_is_your_current', 'required');
        $this->form_validation->set_rules('Update_Do_we_need_to_bring', 'Update_Do_we_need_to_bring', 'required');
        $this->form_validation->set_rules('Update_Do_you_need_to_track', 'Update_Do_you_need_to_track', 'required');
        if ($this->form_validation->run()) {
            $other = '';
            for ($x = 1; $x <= 7; $x++) {
                if ($this->input->post('Update_What_other_Scrubbed' . $x)) {
                    $other .= $this->input->post('Update_What_other_Scrubbed' . $x) . ',';
                }
            }
            $other = rtrim($other, ",");
            $update = array(
                "What_does" => $this->input->post('Update_What_does'),
                "What_industry" => $this->input->post('Update_What_industry'),
                "How_long_have" => $this->input->post('Update_How_long_have'),
                "How_is_the_company" => $this->input->post('Update_How_is_the_company'),
                "Do_you_currently" => $this->input->post('Update_Do_you_currently'),
                "What_is_your_current" => $this->input->post('Update_What_is_your_current'),
                "Do_we_need_to_bring" => $this->input->post('Update_Do_we_need_to_bring'),
                "Do_you_need_to_track" => $this->input->post('Update_Do_you_need_to_track'),
                "What_other_Scrubbed" => $other,
                "How_did_you" => $this->input->post('Update_How_did_you'),
            );
            $this->Models->update_client_assesment($update, $this->input->post('CAID'));
            $value = array("message" => "success", "type" => "update", "text" => "Successfully updated $other " . $this->input->post('CAID'));
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    function uploaded_files() {
        $this->form_validation->set_rules('upload', 'upload', 'required');
        if ($this->form_validation->run()) {
            $show = $this->Models->uploaded_files_infomation($this->input->post('upload'));
            print json_encode($show->result());
        }
    }

    function former_uploaded_files() {
        $this->form_validation->set_rules('upload', 'upload', 'required');
        if ($this->form_validation->run()) {
            $show = $this->Models->former_uploaded_files_infomation($this->input->post('upload'));
            print json_encode($show->result());
        }
    }

    function type_of_services_uploaded_files() {
        $this->form_validation->set_rules('upload', 'upload', 'required');
        if ($this->form_validation->run()) {
            $show = $this->Models->type_of_service_uploaded_files_infomation($this->input->post('upload'));
            print json_encode($show->result());
        }
    }

    function show_client_info() {
        $this->form_validation->set_rules('cid', 'cid', 'required');
        if ($this->form_validation->run()) {
            $show = $this->Models->show_client_infomation($this->input->post('cid'));
            print json_encode($show->result());
        }
    }

    function add_former_name_info() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        $this->form_validation->set_rules('Date_Change', 'Date_Change', 'required');
        $this->form_validation->set_rules('Former_Name', 'Former_Name', 'required');
        $this->form_validation->set_rules('Reason', 'Reason', 'required');
        if ($this->form_validation->run()) {
            $array = array(
                'CID' => $this->input->post('CID'),
                'Date_Change' => $this->input->post('Date_Change'),
                'Former_Name' => $this->input->post('Former_Name'),
                'Reason' => $this->input->post('Reason'),
            );
            $this->Models->add_former_infomation($array);
            $show = $this->Models->show_added_former_infomation($this->input->post('CID'));
            print json_encode(array("type" => "add", "result" => $show->result()));
        } else {
            $array = array(
                'CID' => $this->input->post('CID'),
                'Date_Change' => $this->input->post('Date_Change'),
                'Former_Name' => $this->input->post('Former_Name'),
                'Reason' => $this->input->post('Reason'),
            );
            print json_encode($array);
        }
    }

    function update_former_name_info() {
        $this->form_validation->set_rules('ID', 'ID', 'required');
        $this->form_validation->set_rules('Date_Change', 'Date_Change', 'required');
        $this->form_validation->set_rules('Former_Name', 'Former_Name', 'required');
        $this->form_validation->set_rules('Reason', 'Reason', 'required');
        if ($this->form_validation->run()) {
            $array = array(
                'Date_Change' => $this->input->post('Date_Change'),
                'Former_Name' => $this->input->post('Former_Name'),
                'Reason' => $this->input->post('Reason'),
            );
            $this->Models->update_former_infomation($array, $this->input->post('ID'));
            print json_encode(array("type" => "update"));
        } else {
            $array = array(
                'CID' => $this->input->post('CID'),
                'Date_Change' => $this->input->post('Date_Change'),
                'Former_Name' => $this->input->post('Former_Name'),
                'Reason' => $this->input->post('Reason'),
            );
            print json_encode($array);
        }
    }

    function show_added_former_info() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        if ($this->form_validation->run()) {
            $show = $this->Models->show_added_former_infomation($this->input->post('CID'));
            print json_encode($show->result());
        }
    }

    function crm_former_name_upload_doc() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        if ($this->form_validation->run() === TRUE) {
            $key_id = $this->input->post('CID');
            if (isset($_FILES['file'])) {
                $total = count($_FILES['file']['name']);
                $ext = array("doc", "docx", "ppt", "pptx", "xls", "xlsx", "pdf", "jpg", "jpeg", "JPG", "png", "gif");
                for ($i = 0; $i < $total; $i++) {
                    $fname = $_FILES['file']['name'][$i];
                    if (in_array(pathinfo($fname, PATHINFO_EXTENSION), $ext)) {
                        $tmpFilePath = $_FILES['file']['tmp_name'][$i];
                        move_uploaded_file($tmpFilePath, 'uploads/' . $key_id . '_' . $fname);
                        $array = array(
                            'CID' => $key_id,
                            'file_name' => $key_id . '_' . $_FILES['file']['name'][$i],
                            'date_uploaded' => date('m/d/y h:i:s A'),
                            'extension' => pathinfo($fname, PATHINFO_EXTENSION),
                        );
                        $this->Models->former_information_upload_doc($array);
                    }
                }
                $value = array(
                    "message" => "success",
                    "cid" => $key_id,
                    "text" => "Success!",
                    "type" => "info"
                );
            } else {
                $value = array(
                    "message" => "error",
                    "text" => "Error uploading",
                    "type" => "warning"
                );
            }
            print json_encode($value);
        }
    }

    function crm_type_of_service_upload_doc() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        if ($this->form_validation->run() === TRUE) {
            $key_id = $this->input->post('CID');
            if (isset($_FILES['file'])) {
                $total = count($_FILES['file']['name']);
                $ext = array("doc", "docx", "ppt", "pptx", "xls", "xlsx", "pdf", "jpg", "jpeg", "JPG", "png", "gif");
                for ($i = 0; $i < $total; $i++) {
                    $fname = $_FILES['file']['name'][$i];
                    if (in_array(pathinfo($fname, PATHINFO_EXTENSION), $ext)) {
                        $tmpFilePath = $_FILES['file']['tmp_name'][$i];
                        move_uploaded_file($tmpFilePath, 'uploads/' . $key_id . '_' . $fname);
                        $array = array(
                            'CID' => $key_id,
                            'file_name' => $key_id . '_' . $_FILES['file']['name'][$i],
                            'date_uploaded' => date('m/d/y h:i:s A'),
                            'extension' => pathinfo($fname, PATHINFO_EXTENSION),
                        );
                        $this->Models->type_of_services_upload_doc($array);
                    }
                }
                $value = array(
                    "message" => "success",
                    "cid" => $key_id,
                    "text" => "Success!",
                    "type" => "info"
                );
            } else {
                $value = array(
                    "message" => "error",
                    "text" => "Error uploading",
                    "type" => "warning"
                );
            }
            print json_encode($value);
        }
    }

    function add_compny_address() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        $this->form_validation->set_rules('State', 'State', 'required');
        $this->form_validation->set_rules('City', 'City', 'required');
        $this->form_validation->set_rules('Zip_code', 'Zip_code', 'required');
        if ($this->form_validation->run()) {
            $array = array(
                'CID' => $this->input->post('CID'),
                'State' => $this->input->post('State'),
                'City' => $this->input->post('City'),
                'Zip_Code' => $this->input->post('Zip_code'),
            );
            $this->Models->add_company_address($array);
            $show = $this->Models->show_added_company_address($this->input->post('CID'));
            print json_encode(array("type" => "add", "result" => $show->result()));
        }
    }

    function show_added_compny_address() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        if ($this->form_validation->run()) {
            $show = $this->Models->show_added_company_address($this->input->post('CID'));
            print json_encode($show->result());
        }
    }

    function update_compny_address() {
        $this->form_validation->set_rules('ID', 'ID', 'required');
        $this->form_validation->set_rules('State', 'State', 'required');
        $this->form_validation->set_rules('City', 'City', 'required');
        $this->form_validation->set_rules('Zip_code', 'Zip_code', 'required');
        if ($this->form_validation->run()) {
            $array = array(
                'State' => $this->input->post('State'),
                'City' => $this->input->post('City'),
                'Zip_Code' => $this->input->post('Zip_code'),
            );
            $this->Models->update_company_address($array, $this->input->post('ID'));
            print json_encode(array("type" => "update"));
        }
    }

    function check_client() {
        $this->form_validation->set_rules('ID', 'ID', 'required');
        $this->form_validation->set_rules('check_data', 'check_data', 'required');
        if ($this->form_validation->run()) {
            $this->Models->client_check($this->input->post('ID'), array('client_check' => $this->input->post('check_data')));
            print json_encode(array("message" => "success"));
        }
    }

    function add_type_of_services() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        $this->form_validation->set_rules('Group', 'Group', 'required');
        $this->form_validation->set_rules('Type_of_service', 'Type_of_service', 'required');
        $this->form_validation->set_rules('Agreement', 'Agreement', 'required');
        $this->form_validation->set_rules('Date_from', 'Date_from', 'required');
        $this->form_validation->set_rules('Date_to', 'Date_to', 'required');
        $this->form_validation->set_rules('Hourly_Monthly', 'Hourly_Monthly', 'required');
        $this->form_validation->set_rules('Amount', 'Amount', 'required');
        if ($this->form_validation->run()) {
            $array = array(
                'CID' => $this->input->post('CID'),
                'group_text' => $this->input->post('Group'),
                'type_of_service' => $this->input->post('Type_of_service'),
                'agreement' => $this->input->post('Agreement'),
                'date_from' => $this->input->post('Date_from'),
                'date_to' => $this->input->post('Date_to'),
                'hourly_monthly' => $this->input->post('Hourly_Monthly'),
                'amount' => $this->input->post('Amount'),
            );
            $this->Models->add_type_of_services($array);
            $show = $this->Models->show_added_type_of_services($this->input->post('CID'));
            print json_encode(array("type" => "add", "result" => $show->result()));
        }
    }

    function update_type_of_services() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        $this->form_validation->set_rules('Group', 'Group', 'required');
        $this->form_validation->set_rules('Type_of_service', 'Type_of_service', 'required');
        $this->form_validation->set_rules('Agreement', 'Agreement', 'required');
        $this->form_validation->set_rules('Date_from', 'Date_from', 'required');
        $this->form_validation->set_rules('Date_to', 'Date_to', 'required');
        $this->form_validation->set_rules('Hourly_Monthly', 'Hourly_Monthly', 'required');
        $this->form_validation->set_rules('Amount', 'Amount', 'required');
        if ($this->form_validation->run()) {
            $array = array(
                'CID' => $this->input->post('CID'),
                'group_text' => $this->input->post('Group'),
                'type_of_service' => $this->input->post('Type_of_service'),
                'agreement' => $this->input->post('Agreement'),
                'date_from' => $this->input->post('Date_from'),
                'date_to' => $this->input->post('Date_to'),
                'hourly_monthly' => $this->input->post('Hourly_Monthly'),
                'amount' => $this->input->post('Amount'),
            );
            $this->Models->update_type_of_services($array, $this->input->post('ID'));
            print json_encode(array("type" => "update"));
        }
    }

    function show_added_type_of_services() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        if ($this->form_validation->run()) {
            $show = $this->Models->show_added_type_of_services($this->input->post('CID'));
            print json_encode($show->result());
        }
    }

    function add_reminder() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        $this->form_validation->set_rules('start_date', 'start_date', 'required');
        $this->form_validation->set_rules('end_date', 'end_date', 'required');
        $this->form_validation->set_rules('notify', 'notify', 'required');
        $this->form_validation->set_rules('what', 'what', 'required');
        $this->form_validation->set_rules('where', 'where', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        if ($this->form_validation->run()) {
            $array = array(
                'CID' => $this->input->post('CID'),
                'start_date' => $this->input->post('start_date'),
                'end_date' => $this->input->post('end_date'),
                'notify' => $this->input->post('notify'),
                'reminder_what' => $this->input->post('what'),
                'reminder_where' => $this->input->post('where'),
                'description' => $this->input->post('description'),
                'created_at' => date("Y-m-d H:i:s"),
            );
            $this->Models->add_reminder($array);
            $show = $this->Models->show_added_reminder($this->input->post('CID'));
            print json_encode(array("type" => "add", "result" => $show->result()));
        }
    }

    function update_reminder() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        $this->form_validation->set_rules('start_date', 'start_date', 'required');
        $this->form_validation->set_rules('end_date', 'end_date', 'required');
        $this->form_validation->set_rules('notify', 'notify', 'required');
        $this->form_validation->set_rules('what', 'what', 'required');
        $this->form_validation->set_rules('where', 'where', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        if ($this->form_validation->run()) {
            $array = array(
                'CID' => $this->input->post('CID'),
                'start_date' => $this->input->post('start_date'),
                'end_date' => $this->input->post('end_date'),
                'notify' => $this->input->post('notify'),
                'reminder_what' => $this->input->post('what'),
                'reminder_where' => $this->input->post('where'),
                'description' => $this->input->post('description'),
            );
            $this->Models->update_reminder($array, $this->input->post('ID'));
            print json_encode(array("type" => "update"));
        }
    }

    function show_added_reminder() {
        $this->form_validation->set_rules('CID', 'CID', 'required');
        if ($this->form_validation->run()) {
            $show = $this->Models->show_added_reminder($this->input->post('CID'));
            print json_encode($show->result());
        }
    }

    public function delete_reminder() {
        $this->form_validation->set_rules('delete_reminder', 'delete_reminder', 'required');
        if ($this->form_validation->run()) {
            $this->Models->delete_reminder_id($this->input->post('delete_reminder'));
            $value = array("message" => "success", "type" => "delete", "text" => "Successfully deleted");
            print json_encode($value);
        }
    }

    function leads_status() {
        $this->form_validation->set_rules('cid', 'cid', 'required');
        $this->form_validation->set_rules('status', 'status', 'required');
        if ($this->form_validation->run()) {
            $this->Models->leads_status_update($this->input->post('cid'), array('status' => $this->input->post('status')));
            print json_encode(array("message" => "success"));
        }
    }

    function get_leads_number($num, $year) {
        $numbr = $this->Models->leads_number($num, $year);
        return $numbr->num_rows();
    }

    function leads_number() {

        if ($this->session->userdata('user_session')) {
            $year = $this->input->post('year');
            $numbr = $this->Models->leads_number_crm($year);

            $num1 = $this->get_leads_number(1, $year);
            $num2 = $this->get_leads_number(2, $year);
            $num3 = $this->get_leads_number(3, $year);
            $num4 = $this->get_leads_number(4, $year);
            $num5 = $this->get_leads_number(5, $year);
            $num6 = $this->get_leads_number(6, $year);
            $num7 = $this->get_leads_number(7, $year);
            $num8 = $this->get_leads_number(8, $year);

            $data = array('data' => array(
                    'labels' =>
                    array(
                        0 => 'New Lead (' . $num1 . ')',
                        1 => 'Qualified Lead  (' . $num2 . ')',
                        2 => 'Called (' . $num3 . ')',
                        3 => 'Proposal (' . $num4 . ')',
                        4 => 'Agreement (' . $num5 . ')',
                        5 => 'Signed (' . $num6 . ')',
                        6 => 'Declined (' . $num7 . ')',
                        7 => 'Unqualified Lead (' . $num8 . ')',
                    ),
                    'datasets' =>
                    array(
                        0 =>
                        array(
                            'label' => 'Total ' . $numbr->num_rows(),
                            'data' =>
                            array(
                                0 => $num1,
                                1 => $num2,
                                2 => $num3,
                                3 => $num4,
                                4 => $num5,
                                5 => $num6,
                                6 => $num7,
                                7 => $num8,
                            ),
                            'backgroundColor' =>
                            array(
                                0 => '#105fa2',
                                1 => '#105fa2',
                                2 => '#105fa2',
                                3 => '#105fa2',
                                4 => '#105fa2',
                                5 => '#105fa2',
                                6 => '#105fa2',
                                7 => '#105fa2',
                            ),
                            'borderColor' =>
                            array(
                                0 => '#105fa2',
                                1 => '#105fa2',
                                2 => '#105fa2',
                                3 => '#105fa2',
                                4 => '#105fa2',
                                5 => '#105fa2',
                                6 => '#105fa2',
                                7 => '#105fa2',
                            ),
                            'borderWidth' => 1,
                        ),
                    ),
                )
            );
            print json_encode($data, JSON_PRETTY_PRINT);
        }
    }

    function get_cna_data() {
        $this->form_validation->set_rules('cid', 'cid', 'required');
        $this->form_validation->set_rules('cna', 'cna', 'required');
        if ($this->form_validation->run()) {
            $data = $this->Models->select_client_assesment($this->input->post('cid'), $this->input->post('cna'));
            print json_encode($data->result(), JSON_PRETTY_PRINT);
        }
    }

    function tax_ref() {
        $data = $this->Models->select_client_assesment(71, 1600905023);
        print json_encode($data->result(), JSON_PRETTY_PRINT);
    }

    function get_service_number($num, $year) {
        $numbr = $this->Models->service_number($num, $year);
        return $numbr->num_rows();
    }

    function service_line() {
        if ($this->session->userdata('user_session')) {
            $year = $this->input->post('year');

            $num1 = $this->get_service_number(1, $year);
            $num2 = $this->get_service_number(2, $year);
            $num3 = $this->get_service_number(3, $year);
            $num4 = $this->get_service_number(4, $year);
            $num5 = $this->get_service_number(5, $year);
            $num6 = $this->get_service_number(6, $year);
            $num7 = $this->get_service_number(7, $year);
            $num8 = $this->get_service_number(8, $year);
            $num9 = $this->get_service_number(9, $year);
            $num10 = $this->get_service_number(10, $year);
            $num11 = $this->get_service_number(11, $year);

            $result = $this->Models->service_select(); 
            
            $myarray = array();
            foreach ($result->result() as $value) {
                $myarray[] = $value->short_name;
            }
            
            $data = array(
                'labels' => $myarray, 
                'datasets' => array(array(
                        "label" => ($year ? "Year:" . $year : "Year:All"),
                        "backgroundColor" => "#105fa2",
                        "hoverBorderColor" => "#105fa2",
                        "data" => array($num1, $num2, $num3, $num4, $num5, $num6, $num7, $num8, $num9, $num10, $num11)
                    ))
            );
            print json_encode($data, JSON_PRETTY_PRINT);
        }
    }

    function get_industry_number($num, $year) {
        $numbr = $this->Models->industry_number($num, $year);
        return $numbr->num_rows();
    }

    function industry_lines() {


        if ($this->session->userdata('user_session')) {
            $year = $this->input->post('year');

            $num1 = $this->get_industry_number(1, $year);
            $num2 = $this->get_industry_number(2, $year);
            $num3 = $this->get_industry_number(3, $year);
            $num4 = $this->get_industry_number(4, $year);
            $num5 = $this->get_industry_number(5, $year);
            $num6 = $this->get_industry_number(6, $year);
            $num7 = $this->get_industry_number(7, $year);
            $num8 = $this->get_industry_number(8, $year);
            $num9 = $this->get_industry_number(9, $year);
            $num10 = $this->get_industry_number(10, $year);
            $num11 = $this->get_industry_number(11, $year);




            $data = array(
                'labels' => ["Biotech (" . $num1 . ")", "Cannabis (" . $num2 . ")", "Clean Technology and Renewable Energy (" . $num3 . ")", "Consumer Products and Retail (" . $num4 . ")", "Financial Services (" . $num5 . ")", "Individual or Family Office (" . $num6 . ")", "Information, Media and Entertainment (" . $num7 . ")", "Manufacturing and Distribution (" . $num8 . ")", "Professional and Business Services (" . $num9 . ")", "Real Estate (" . $num10 . ")", "Social Enterprise and NPO (" . $num11 . ")"],
                'datasets' => array(array(
                        "label" => ($year ? "Year:" . $year : "Year:All"),
                        "backgroundColor" => "#105fa2",
                        "hoverBorderColor" => "#105fa2",
                        "data" => array($num1, $num2, $num3, $num4, $num5, $num6, $num7, $num8, $num9, $num10, $num11)
                    ))
            );
            print json_encode($data, JSON_PRETTY_PRINT);
        }
    }

    function addmanualrevenue() {
        $this->form_validation->set_rules('invoiceno', 'invoiceno', 'required');
        $this->form_validation->set_rules('itemnameselect', 'itemnameselect', 'required');
        $this->form_validation->set_rules('client_id', 'client_id', 'required');

        if ($this->form_validation->run() === TRUE) {
            $clientnameselect = $this->input->post('clientnameselect');
            $invoiceno = $this->input->post('invoiceno');

            $invoicedate = $this->input->post('invoicedate');

            $itemnameselect = $this->input->post('itemnameselect');

            $total = $this->input->post('total');
            $groupid = $this->input->post('groupid');

            $sqlcx = "SELECT * FROM crm_xero_client_list_tbl where entity='" . $clientnameselect . "'";
            $querycx = $this->db->query($sqlcx);
            $executecx = $querycx->result();
            foreach ($executecx as $ramcx) {
                $cid = $ramcx->ID;
            }



//            $sql = "INSERT INTO crm_kpi_revenue_details_per_invoice_tbl(`customer_name`, `invoice_date`, `total`, `item_name`, `month`, `year`, `invoice_no`, `groupid`,"
//                    . " `clientid`, `due_date`, `due_amount`) values ('" . $clientnameselect . "', '" . $date . "', '" . $total . "', '" . $itemnameselect . "', "
//                    . " '" . $invoicemonth . "', '" . $invoiceyear . "', '" . $invoiceno . "', '" . $groupid . "', '" . $this->input->post('client_id') . "', '', '0')";
//
//            $this->db->query($sql);

            $data = array(
                "customer_name" => $clientnameselect,
                "invoice_date" => date('m/d/Y', strtotime($invoicedate)),
                "total" => $total,
                "item_name" => $itemnameselect,
                "month" => date('m', strtotime($invoicedate)),
                "year" => date('Y', strtotime($invoicedate)),
                "invoice_no" => $invoiceno,
                "groupid" => $groupid,
                "clientid" => $this->input->post('client_id'),
                "due_date" => '',
                "due_amount" => 0,
            );
            $this->db->insert('crm_kpi_revenue_details_per_invoice_tbl', $data);



//            $crm = $this->db->query("SELECT * FROM crm_kpi_revenue_details_per_invoice_tbl ORDER BY `ID` DESC LIMIT 1");
//            $data = array("message" => "success", "added" => $crm->result());
//            print json_encode($data);
//            



            $crm = $this->db->query("SELECT * FROM crm_kpi_revenue_details_per_invoice_tbl AS client_rev LEFT JOIN (SELECT * FROM intra.user_group) as intra_usergroup ON intra_usergroup.GID = client_rev.groupid  WHERE client_rev.clientid = '" . $this->input->post('client_id') . "' ORDER BY ID DESC LIMIT 1 ");
            $data = array("message" => "success", "added" => $crm->result());
            print json_encode($data);
        }
    }

    function addmanualar() {
        $this->form_validation->set_rules('invoiceno', 'invoiceno', 'required');
        $this->form_validation->set_rules('itemnameselect', 'itemnameselect', 'required');
        $this->form_validation->set_rules('client_id', 'client_id', 'required');

        if ($this->form_validation->run() === TRUE) {
            $clientnameselect = $this->input->post('clientnameselect');
            $invoiceno = $this->input->post('invoiceno');

            $invoicedate = $this->input->post('invoicedate');

            $itemnameselect = $this->input->post('itemnameselect');

            $total = $this->input->post('total');
            $groupid = $this->input->post('groupid');

            $sqlcx = "SELECT * FROM crm_xero_client_list_tbl where entity='" . $clientnameselect . "'";
            $querycx = $this->db->query($sqlcx);
            $executecx = $querycx->result();
            foreach ($executecx as $ramcx) {
                $cid = $ramcx->ID;
            }



//            $sql = "INSERT INTO crm_kpi_revenue_details_per_invoice_tbl(`customer_name`, `invoice_date`, `total`, `item_name`, `month`, `year`, `invoice_no`, `groupid`,"
//                    . " `clientid`, `due_date`, `due_amount`) values ('" . $clientnameselect . "', '" . $date . "', '" . $total . "', '" . $itemnameselect . "', "
//                    . " '" . $invoicemonth . "', '" . $invoiceyear . "', '" . $invoiceno . "', '" . $groupid . "', '" . $this->input->post('client_id') . "', '', '0')";
//
//            $this->db->query($sql);

            $data = array(
                "customer_name" => $clientnameselect,
                "invoice_date" => date('m/d/Y', strtotime($invoicedate)),
                "invoice_no" => $invoiceno,
                "item_name" => $itemnameselect,
                "groupid" => $groupid,
                "clientid" => $this->input->post('client_id'),
                "amount_due" => $total,
                "month" => date('m', strtotime($invoicedate)),
                "year" => date('Y', strtotime($invoicedate)),
                "locked" => 'Y',
            );
            $this->db->insert('crm_kpi_ar_details_tbl', $data);



//            $crm = $this->db->query("SELECT * FROM crm_kpi_revenue_details_per_invoice_tbl ORDER BY `ID` DESC LIMIT 1");
//            $data = array("message" => "success", "added" => $crm->result());
//            print json_encode($data);
//            



            $crm = $this->db->query("SELECT * FROM crm_kpi_ar_details_tbl AS client_rev LEFT JOIN (SELECT * FROM intra.user_group) as intra_usergroup ON intra_usergroup.GID = client_rev.groupid  WHERE client_rev.clientid = '" . $this->input->post('client_id') . "' ORDER BY ID DESC LIMIT 1 ");
            $data = array("message" => "success", "added" => $crm->result());
            print json_encode($data);
        }
    }

    function ar_select() {
        $this->form_validation->set_rules('client_id', 'client_id', 'required');
        if ($this->form_validation->run()) {
            $crm = $this->Company_model->get_ar($this->input->post('client_id'));
            $data = array("message" => "success", "added" => $crm->result());
            print json_encode($data);
        }
    }

    function transactions() {
        $this->form_validation->set_rules('client_id', 'client_id', 'required');
        if ($this->form_validation->run()) {
            $crm = $this->Company_model->get_transactions($this->input->post('client_id'));
            $data = array("message" => "success", "added" => $crm);
            print json_encode($data);
        }
    }

    function revenue_select() {
        $this->form_validation->set_rules('client_id', 'client_id', 'required');
        if ($this->form_validation->run()) {
            $crm = $this->db->query("SELECT *,SUM(total) AS merge_invoice FROM crm_kpi_revenue_details_per_invoice_tbl AS client_rev LEFT JOIN (SELECT * FROM intra.user_group) as intra_usergroup ON intra_usergroup.GID = client_rev.groupid  WHERE client_rev.clientid = '" . $this->input->post('client_id') . "' GROUP BY invoice_no ORDER BY client_rev.ID DESC ");
            $data = array("message" => "success", "added" => $crm->result());
            print json_encode($data);
        }
    }

    function revenue_drilldown_select() {
        $this->form_validation->set_rules('invoice', 'invoice', 'required');
        $this->form_validation->set_rules('clientid', 'clientid', 'required');
        if ($this->form_validation->run()) {
            $crm = $this->db->query("SELECT * FROM crm_kpi_revenue_details_per_invoice_tbl AS client_rev LEFT JOIN (SELECT * FROM intra.user_group) as intra_usergroup ON intra_usergroup.GID = client_rev.groupid  WHERE client_rev.invoice_no= '" . $this->input->post('invoice') . "' and client_rev.clientid= '" . $this->input->post('clientid') . "' ORDER BY client_rev.ID DESC ");
            $data = array("message" => "success", "added" => $crm->result());
            print json_encode($data);
        }
    }

    function ar_drilldown_select() {
        $this->form_validation->set_rules('invoice', 'invoice', 'required');
        $this->form_validation->set_rules('clientid', 'clientid', 'required');
        if ($this->form_validation->run()) {
            $crm = $this->Company_model->get_ar_invoice($this->input->post('invoice'), $this->input->post('clientid'));
            $data = array("message" => "success", "added" => $crm->result());
            print json_encode($data);
        }
    }

    function transactions_drilldown_select() {
        $this->form_validation->set_rules('invoice', 'invoice', 'required');
        $this->form_validation->set_rules('clientid', 'clientid', 'required');
        if ($this->form_validation->run()) {
            $invoices = $this->Company_model->get_transaction_invoices($this->input->post('invoice'), $this->input->post('clientid'));
            $data = array("message" => "success", "invoices" => $invoices->result());
            print json_encode($data);
        }
    }

    function delete_ar_manual() {
        $this->form_validation->set_rules('delete_ar_manual', 'delete_ar_manual', 'required');
        if ($this->form_validation->run()) {
            $this->db->where(array('ID' => $this->input->post('delete_ar_manual')));
            $this->db->delete('crm_kpi_revenue_details_per_invoice_tbl');
            $data = array("message" => "success", "text" => "AR has been deleted");
            print json_encode($data);
        }
    }

    function revenue_select_export() {
        header('Content-Type: text/html');
//        header("Content-Type: application/xls");
//        header("Content-Disposition: attachment; filename=export-".strtotime('now').".xls");
//        header("Pragma: no-cache");
//        header("Expires: 0");

        if ($this->uri->segment(3)) {
            $crm = $this->db->query("SELECT *,SUM(total) AS merge_invoice FROM crm_kpi_revenue_details_per_invoice_tbl AS client_rev LEFT JOIN (SELECT * FROM intra.user_group) as intra_usergroup ON intra_usergroup.GID = client_rev.groupid  WHERE client_rev.clientid = '" . $this->uri->segment(3) . "' GROUP BY invoice_no ORDER BY client_rev.ID DESC  ");
            $_html = '';
            $_html .= '<table border="1" cellspacing="0" bordercolor="#d7d7d7" width="900">';
            $_html .= '<thead>';
            $_html .= '<tr>';
            $_html .= '<th class="aligncenter">#</th>';
            $_html .= '<th class="aligncenter">Invoice Number </th>';
            $_html .= '<th class="aligncenter">Invoice Date </th>';
            $_html .= '<th class="aligncenter">Client Name</th>';
            $_html .= '<th class="aligncenter">Group</th>';
            $_html .= '<th class="text-right">Amount Due</th>';
            $_html .= '</tr>';
            $_html .= '</thead>';
            $_html .= '<tbody class="added-revenue">';
            $num = 0;
            foreach ($crm->result() as $value) {
                $_html .= '<tr>';
                $_html .= '<td class="count">' . $num++ . '</td>';
                $_html .= '<td>' . ($value->total != $value->merge_invoice ? "<a href='" . $value->invoice_no . "' class='text-primary'>" . $value->invoice_no . "</a>" : $value->invoice_no) . '</td>';
                $_html .= '<td>' . $value->invoice_date . '</td>';
                $_html .= '<td>' . $value->customer_name . '</td>';
                $_html .= '<td>' . $value->group_name . '</td>';
                $_html .= '<td class="text-right">' . $value->total . ' - ' . $value->merge_invoice . '</td>';
                $_html .= '</td>';
                $_html .= '</tr>';
            }

            $_html .= '</tbody>';
            $_html .= '</table>';
            echo $_html;
        }
    }

    function replace_special_char($text) {
        $search = array(
            ",",
            "`",
            " Inc.",
            " Inc.",
            " LLC",
            "."
        );
        $replace = array(
            "",
            "",
            "",
            "",
            "",
            "",
        );
        return str_replace($search, $replace, $text);
    }

    function name_replace() {
        
    }

    function billing_update_id() {
        $crm = $this->db->query("SELECT * FROM crm_companies WHERE account = entity ");
        //print json_encode($crm->result(), JSON_PRETTY_PRINT);
        foreach ($crm->result() as $value) {

            $this->db->query('UPDATE `crm_kpi_revenue_details_per_invoice_tbl` SET `clientid` = "' . $value->ID . '" WHERE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(customer_name,"`",""),",",""),".","")," Inc.","")," LLC","") = "' . $this->replace_special_char($value->account) . '" ');

            $this->db->query('UPDATE `crm_kpi_revenue_details_per_invoice_tbl` SET `clientid` = "' . $value->ID . '" WHERE customer_name = "' . ($value->account) . '" ');

            $this->db->query('UPDATE `crm_kpi_ar_details_tbl` SET `clientid` = "' . $value->ID . '" WHERE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(customer_name,"`",""),",",""),".","")," Inc.","")," LLC","") = "' . $this->replace_special_char($value->account) . '" ');

            $this->db->query('UPDATE `crm_kpi_ar_details_tbl` SET `clientid` = "' . $value->ID . '" WHERE customer_name = "' . $value->account . '" ');
        }
    }

    function client_experience() {
        $this->form_validation->set_rules('clientid', 'clientid', 'required');
        if ($this->form_validation->run()) {
            $array = array(
                "clientid" => $this->input->post('clientid'),
                "date" => strtotime($this->input->post('date')),
            );
            $data = $this->Models->crm_client_experience($array);
            print json_encode($data->result(), JSON_PRETTY_PRINT);
        } else {
            
        }
    }

    function update_experience() {
        $this->form_validation->set_rules('related_id', 'related_id', 'required');
        $this->form_validation->set_rules('data_id', 'data_id', 'required');
        $this->form_validation->set_rules('Service_Continuum', 'Service_Continuum', 'required|numeric');
        $this->form_validation->set_rules('Calls_made', 'Calls_made', 'required|numeric');
        $this->form_validation->set_rules('QRC', 'QRC', 'required');
        $this->form_validation->set_rules('Note', 'Note', 'required');
        $this->form_validation->set_rules('CI_log', 'CI_log', 'required');

        $this->form_validation->set_rules('Errors', 'Errors', 'required');
        $this->form_validation->set_rules('Client_satifaction', 'Client_satifaction', 'required');
        $this->form_validation->set_rules('Deadline_missed', 'Deadline_missed', 'required');
        $this->form_validation->set_rules('dateexp', 'dateexp', 'required');

        if ($this->form_validation->run()) {
            $array = array(
                "month" => date('m', strtotime($this->input->post('dateexp'))),
                "year" => date('Y', strtotime($this->input->post('dateexp'))),
                "ongoing_concern" => $this->input->post('Note'),
                "qrc" => $this->input->post('QRC'),
                "ci_log" => $this->input->post('CI_log'),
                "ci_log_note" => $this->input->post('ci_log_note'),
                "no_of_errors" => $this->input->post('Errors'),
                "no_errors_note" => $this->input->post('error_note'),
                "client_satifaction" => $this->input->post('Client_satifaction'),
                "calls_made" => $this->input->post('Calls_made'),
                "deadline_missed" => $this->input->post('Deadline_missed'),
                "service_continum" => $this->input->post('Service_Continuum'),
                "timestamp" => date('m/d/Y h:i:s', strtotime($this->input->post('dateexp'))),
            );
            $this->Models->crm_update_client_experience($this->input->post('related_id'), $this->input->post('data_id'), $array);
            print json_encode(array("message" => "success"), JSON_PRETTY_PRINT);
        }
    }

    function add_experience() {
        $this->form_validation->set_rules('related_id', 'related_id', 'required');
        $this->form_validation->set_rules('Service_Continuum', 'Service_Continuum', 'required|numeric');
        $this->form_validation->set_rules('Calls_made', 'Calls_made', 'required|numeric');
        $this->form_validation->set_rules('QRC', 'QRC', 'required');
        
        $this->form_validation->set_rules('Note', 'Note', 'required');
        $this->form_validation->set_rules('CI_log', 'CI_log', 'required');

        $this->form_validation->set_rules('Errors', 'Errors', 'required');
        $this->form_validation->set_rules('Client_satifaction', 'Client_satifaction', 'required');
        $this->form_validation->set_rules('Deadline_missed', 'Deadline_missed', 'required');
        $this->form_validation->set_rules('dateexp', 'dateexp', 'required');
        $this->form_validation->set_rules('client_id', 'client_id', 'required');

        if ($this->form_validation->run()) {
            
            $array = array(
                "clientid" => $this->input->post('client_id'),
                "month" => date('m', strtotime($this->input->post('dateexp'))),
                "year" => date('Y', strtotime($this->input->post('dateexp'))),
                "referenceid" => $this->input->post('related_id'),
                "ongoing_concern" => $this->input->post('Note'),
                "qrc" => $this->input->post('QRC'),
                "ci_log" => $this->input->post('CI_log'),
                "ci_log_note" => $this->input->post('ci_log_note'),
                "no_of_errors" => $this->input->post('Errors'),
                "no_errors_note" => $this->input->post('error_note'),
                "client_satifaction" => $this->input->post('Client_satifaction'),
                "calls_made" => $this->input->post('Calls_made'),
                "deadline_missed" => $this->input->post('Deadline_missed'),
                "service_continum" => $this->input->post('Service_Continuum'),
                "reviewed_by" => $this->session->userdata('user_session'),
                "generated_by" => $this->session->userdata('user_session'),
                "timestamp" => date('m/d/Y h:i:s', strtotime($this->input->post('dateexp'))),
            );
            $this->Models->crm_add_client_experience($array);
            $data = $this->Models->crm_select_client_experience($this->input->post('client_id'));
            print json_encode($data->result(), JSON_PRETTY_PRINT);
            
        }
    }

    function select_update_client_experience() {
        $this->form_validation->set_rules('data_id', 'data_id', 'required');
        $this->form_validation->set_rules('related_id', 'related_id', 'required');
        if ($this->form_validation->run()) {
            $data = array(
                "uid" => $this->input->post('data_id'),
                "referenceid" => $this->input->post('related_id'),
            );
            $result = $this->Models->crm_select_update_client_experience($data);
            print json_encode($result->result(), JSON_PRETTY_PRINT);
        }
    }

    function review_this_client_experience() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('review_undo', 'review_undo', 'required|numeric');
            $this->form_validation->set_rules('client_id', 'client_id', 'required|numeric');
            $this->form_validation->set_rules('related', 'related', 'required|numeric');
            $this->form_validation->set_rules('id', 'id', 'required|numeric');
            if ($this->form_validation->run()) {
                if ($this->input->post('review_undo')) {
                    $this->Models->crm_update_client_experience($this->input->post('id'),$this->input->post('client_id'), ["reviewed_by" => $this->session->userdata('user_session')]);
                } else {
                    $this->Models->crm_update_client_experience($this->input->post('id'),$this->input->post('client_id'), ["reviewed_by" => ""]);
                }

                $result = $this->Models->get_user_info($this->session->userdata('user_session'));
                print json_encode([
                    "name" => $result->first_name . " " . $result->last_name
                ]);
            }
        }
    }

    function delete_client_experience() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required|numeric');
            if ($this->form_validation->run()) {
                $result = $this->Models->crm_delete_client_experience($this->input->post('id'));
                $data = array("message" => "success", "text" => "AR has been deleted");
                print json_encode($data);
            }
        }
    }

    function upload_logo() {
        if ($this->session->userdata('user_session')) {
            $config['upload_path'] = './uploads/logos';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '1000';
            $config['max_width'] = '2024';
            $config['max_height'] = '2024';
            $config["file_name"] = $this->input->post('client_id') . ".jpg";

            if (file_exists('/var/www/html/crm/uploads/logos/' . $this->input->post('client_id') . ".jpg")) {
                unlink('./uploads/logos/' . $this->input->post('client_id') . ".jpg");
            }
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $msg = array('message' => 'error', 'text' => $this->upload->display_errors());
            } else {

                $msg = array('message' => 'success', 'text' => $this->upload->data(), 'clientid' => $this->input->post('client_id'));
            }
            print json_encode($msg);
        }
    }

    function company_logo() {
        if ($this->session->userdata('user_session')) {
            if ($this->uri->segment(3)) {
                header('Content-Type: image/jpeg');
                if (file_exists('/var/www/html/crm/uploads/logos/' . $this->uri->segment(3) . ".jpg")) {
                    echo file_get_contents(site_url('uploads/logos/' . $this->uri->segment(3) . ".jpg"));
                } else {
                    echo file_get_contents(site_url('uploads/logos/default.jpg'));
                }
            }
        }
    }

    /////////////////////////////////////
}

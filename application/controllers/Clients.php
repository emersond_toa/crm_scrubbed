<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->model('Models');
        $this->load->model('Client_model');
        $this->load->model('ClientStatus_model');
        $this->load->model('Status_model');
        $this->load->model('Form_model');
        $this->load->model('Company_model');
        $this->load->model('Contact_model');
        $this->load->model('Industry_model');
        $this->load->model('Service_model');
        $this->load->model('ReferralPartner_model');
        $this->load->model('LeadSource_model');
        $this->load->model('TerminationReason_model');
        $this->load->model('Agreement_model');
        $this->load->database();
    }

    public function index() {
        $data['title'] = 'Clients';
        $data['clientstatuses'] = $this->ClientStatus_model->all_status();
        $data['forms'] = $this->Form_model->all_forms();
        $data['users'] = $this->User_model->all_users();
        $data['companies'] = $this->Company_model->all_companies();
        $data['industries'] = $this->Industry_model->all_industries();
        $data['statuses'] = $this->Status_model->all_status();
        $data['services'] = $this->Service_model->all_services();
        $data['referralpartners'] = $this->ReferralPartner_model->all();
        $data['leadsources'] = $this->LeadSource_model->all();
        $data['reasons'] = $this->TerminationReason_model->all();
        $data['agreements'] = $this->Agreement_model->all();
        $data['work_groups'] = $this->Company_model->work_groups();

        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) 
        {
            $this->load->view('templates/sidebar');
            $this->load->view('clients/index');
            $this->load->view('clients/modals/view');
            $this->load->view('clients/modals/edit');
        } 
        else {
            redirect("https://employeeportal.scrubbed.net/");
        }
        $this->load->view('templates/footer');
    }

    // public function update()
    // {
    //     $this->form_validation->set_rules('fullname', 'fullname', 'required');
    //     $this->form_validation->set_rules('jobtitle', 'jobtitle', 'required');
    //     $this->form_validation->set_rules('email', 'email', 'required');
    //     $this->form_validation->set_rules('phone', 'phone', 'required');

    //     if ($this->form_validation->run() == FALSE) 
    //     {
    //         print json_encode(["status" => "error"]);
    //     }
    //     else
    //     {
    //         $data = [
    //             'fullname' => $this->input->post('fullname'),
    //             'jobtitle' => $this->input->post('jobtitle'),
    //             'email' => $this->input->post('email'),
    //             'phone' => $this->input->post('phone'),
    //             'clientstatus_id' => empty($this->input->post('clientstatus')) ? 1 : $this->input->post('clientstatus'),
    //             'assigned_id' => $this->input->post('assignedto'),
    //             'updated_at' => date('Y-m-d h:i:s A'),
    //         ];

    //         $this->Contact_model->update($this->input->post('contact_id'), $data);
        
    //         print json_encode(["status" => "ok"]);
            
    //     }
    // }

    public function get_allcompanies() {
        if ($this->session->userdata('user_session')) {
            $columns = [
                0 => 'name',
                1 => 'contact',
                2 => 'leadform',
                3 => 'industry',
                4 => 'services',
                5 => 'work_groups',
                6 => 'updated_at',
                7 => 'clientstatus',
                8 => 'actions',
            ];
    
            $limit = $this->input->post('length');
            $start = $this->input->post('start');
            $order = $columns[$this->input->post('order')[0]['column']];
    
            $dir = $this->input->post('order')[0]['dir'];
            $leadstatus = $this->input->post('leadstatus');
            $leadform = $this->input->post('leadform');
    
            $totalData = $this->Company_model->clientcount($leadstatus, $leadform);
    
            $totalFiltered = $totalData;
    
            if (empty($this->input->post('search')['value'])) {
                $companies = $this->Company_model->clientdatatable($limit, $start, $order, $dir, $leadstatus, $leadform);
            } else {
                $search = $this->input->post('search')['value'];
    
                $companies = $this->Company_model->clientdatatablesearch($limit, $start, $search, $order, $dir, $leadstatus, $leadform);
    
                // removed 
                $totalFiltered = $this->Company_model->client_contacts_search_count($search, $leadstatus, $leadform);
            }
    
            $data = array();
            if (!empty($companies)) {
                foreach ($companies as $company) {
                    $nestedData['name'] = '<a href="#" class="open-client-viewmodal" data-toggle="modal" data-target="#viewModal" data-id="' . $company->ID . '">' . $company->name . '</a>';
                    $nestedData['contact'] = $company->contact;
                    $nestedData['leadform'] = $company->leadform;
                    $nestedData['industry'] = $company->industry;
                    $nestedData['services'] = '';
                    $users = [];
                    foreach($company->assignedto as $ass) {
                        $users[] = $ass->first_name.' '.$ass->last_name;
                    }
                    $nestedData['assignedto'] = implode (", ", $users);
                    $services = [];
                    foreach($company->services as $service) {
                        $services[] = $service->name;
                    }
    
                    $work_groups = [];
                    foreach($company->work_groups as $work) {
                        $work_groups[] = $work->group_name;
                    }
                    $nestedData['work_groups'] = implode (", ", $work_groups);
    
                    $nestedData['services'] = implode (", ", $services);
                    $nestedData['lastupdate'] = date('m/d/Y h:i a', strtotime($company->updated_at));
                    $nestedData['clientstatus'] = $this->Company_model->clientstatus_badge($company->clientstatus);
                    $nestedData['actions'] = '<button class="btn btn-light btn-sm open-client-viewmodal action-button-sm" data-toggle="modal" data-target="#viewModal" data-id="' . $company->ID . '"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="View Client"></i></button><button class="btn mx-1 btn-light btn-sm open-client-editmodal action-button-sm" data-toggle="modal" data-target="#editModal" data-id="' . $company->ID . '"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit Client"></i></button><button class="btn btn-light btn-sm delete-client action-button-sm" data-id="' . $company->ID . '" data-toggle="tooltip" data-placement="top" title="Delete Client"><i class="fa fa-times"></i></button>';
    
                    $data[] = $nestedData;
                }
            }
    
            $json_data = [
                "draw" => intval($this->input->post('draw')),
                "recordsTotal" => intval($totalData),
                "recordsFiltered" => intval($totalFiltered), //totalFiltered
                "data" => $data
            ];
    
            echo json_encode($json_data);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    // public function get_allclients()
    // {
    //     $columns = [
    //         0 => 'company',
    //         1 => 'fullname',
    //         2 => 'service',
    //         3 => 'industry',
    //         4 => 'assignedto',
    //         5 => 'services',
    //         6 => 'lastupdate',
    //         7 => 'clientstatus',
    //         8 => 'actions',
            
    //     ];

    //     $limit = $this->input->post('length');
    //     $start = $this->input->post('start');
    //     $order = $columns[$this->input->post('order')[0]['column']];
    //     $leadstatus = $this->input->post('leadstatus');
    //     $dir = $this->input->post('order')[0]['dir'];
    //     $leadservice = $this->input->post('leadservice');

    //     $totalData = $this->Client_model->allclients_count($leadstatus, $leadservice);
            
    //     $totalFiltered = $totalData; 
            
    //     if(empty($this->input->post('search')['value']))
    //     {            
    //         $clients = $this->Client_model->allclients($limit,$start,$order,$dir, $leadstatus, $leadservice);
    //     }
    //     else {
    //         $search = $this->input->post('search')['value']; 

    //         $clients =  $this->Client_model->clients_search($limit,$start,$search,$order,$dir, $leadstatus, $leadservice);

    //         // removed 
    //         // $totalFiltered = $this->Client_model->clients_search_count($search);
    //     }

    //     $data = array();
    //     if(!empty($clients))
    //     {
    //         foreach ($clients as $client)
    //         {
    //             $nestedData['company'] = '<a href="#" class="btn btn-link open-client-viewmodal" data-toggle="modal" data-target="#viewModal" data-id="'. $client->id .'">'. $client->company .'</a>';
    //             $nestedData['fullname'] = $client->fullname;
    //             $nestedData['service'] = $client->service;
    //             $nestedData['industry'] = $client->industry;
    //             $nestedData['assignedto'] = $client->assignedto;
    //             $nestedData['services'] = '';
    //             $nestedData['lastupdate'] = '';
    //             $nestedData['clientstatus'] = $this->Client_model->clientstatus_badge($client->clientstatus);
    //             $nestedData['actions'] = '<button class="btn btn-light btn-sm open-client-viewmodal" data-toggle="modal" data-target="#viewModal" data-id="' . $client->id . '"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="View Client"></i></button><button class="btn mx-1 btn-light btn-sm open-client-editmodal" data-toggle="modal" data-target="#editModal" data-id="'. $client->id .'"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit Client"></i></button><button class="btn btn-light btn-sm delete-client" data-id="'. $client->id .'" data-toggle="tooltip" data-placement="top" title="Delete Client"><i class="fa fa-times"></i></button>';

    //             $data[] = $nestedData;

    //         }
    //     }
        
    //     $json_data = [
    //         "draw" => intval($this->input->post('draw')),  
    //         "recordsTotal" => intval($totalData),  
    //         "recordsFiltered" => intval($totalData), //totalFiltered
    //         "data" => $data   
    //     ];
            
    //     echo json_encode($json_data); 
    // }
}

<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: application/json');

class Entity extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Entity_model');
        $this->load->model('Models');
        $this->load->model('Company_model');
    }

    public function index() {
        
    }

    public function store() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('companyId', 'companyId', 'required');
            $this->form_validation->set_rules('entityname', 'entityname', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $company_id = $this->input->post('companyId');
                $company = $this->Company_model->show($company_id);
                $data = [
                    'account' => $company[0]->account,
                    'account_name' => $company[0]->account_name,
                    'entity' => html_purify($this->input->post('entityname')),
                ];
                $this->Entity_model->store($data);
                $this->Company_model->last_update($this->input->post('companyId'));
                print json_encode(['status' => 'ok']);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    function UserExperienceReviewer() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('uid', 'uid', 'required|numeric');
            $this->form_validation->set_rules('bind', 'bind', 'required|numeric');
            if ($this->form_validation->run()) {
                $u = $this->session->userdata('user_session');
                if (!$this->input->post('bind')) {
                    $this->Entity_model->model_entity_update($this->input->post('uid'), array('reviewed_by' => $u, 'reviewed_timestamp' => 'Reviewed Date<br>' . date('m/d/Y h:i:s A')));
                } else {
                    $this->Entity_model->model_entity_update($this->input->post('uid'), array('reviewed_by' => '', 'reviewed_timestamp' => ''));
                }
                $exist = $this->Entity_model->model_check_rewier($this->input->post('uid'));
                $value = array("status" => "success", "message" => "Success", "callback" => $exist->result());
            } else {
                $value = array("status" => "error", "message" => "Please refesh your browser");
            }
        } else {
            $value = array("status" => "error", "message" => "Please refesh your browser and login again");
        }
        print json_encode($value, JSON_PRETTY_PRINT);
    }

    function InsertUpdateSubjective() {
        if ($this->session->userdata('user_session')) {

            $month = date('m', strtotime('first day of last month'));
            $year = date('Y', strtotime('first day of last month'));

            $this->form_validation->set_rules('client_id', 'client_id', 'required|numeric');
            $this->form_validation->set_rules('entry_update', 'entry_update', 'required|numeric');
            $this->form_validation->set_rules('group_id', 'group_id', 'required');

            $this->form_validation->set_rules('Service_Continuum', 'Service_Continuum', 'required|numeric');
            $this->form_validation->set_rules('Calls_made', 'Calls_made', 'required|numeric');
            $this->form_validation->set_rules('QRC', 'QRC', 'required');

            $this->form_validation->set_rules('CI_log', 'CI_log', 'required');
            $this->form_validation->set_rules('Errors', 'Errors', 'required');

            $this->form_validation->set_rules('Deadline_missed', 'Deadline_missed', 'required');
            $this->form_validation->set_rules('Audit_Ready', 'Audit_Ready', 'required');

            if ($this->form_validation->run()) {
                if ($this->input->post('entry_update')) {
                    $array = array(
                        "month" => $month,
                        "year" => $year,
                        "groupid" => $this->input->post('group_id'),
                        "clientid" => $this->input->post('client_id'),
                        "ongoing_concern" => $this->input->post('Note'),
                        "qrc" => $this->input->post('QRC'),
                        "ci_log" => $this->input->post('CI_log'),
                        "ci_log_note" => $this->input->post('ci_log_note'),
                        "no_of_errors" => $this->input->post('Errors'),
                        "no_errors_note" => $this->input->post('error_note'),
                        "client_satifaction" => $this->input->post('Client_satifaction'),
                        "calls_made" => $this->input->post('Calls_made'),
                        "deadline_missed" => $this->input->post('Deadline_missed'),
                        "service_continum" => $this->input->post('Service_Continuum'),
                        "audit_ready" => $this->input->post('Audit_Ready'),
                        "timestamp" => date('m/d/Y h:i:s'),
                        "referenceid" => "refid-" . strtotime('now'),
                    );
                    $this->Entity_model->model_entity_update($this->input->post('data_id'), $array);
                    $exist = $this->Entity_model->check_if_exist_cx($month, $year, $this->input->post('group_id'), $this->input->post('client_id'));
                    $value = array("status" => "success", "message" => "Data has been successfully updated.", "type" => "update", "callback" => $exist->result());
                } else {
                    $array = array(
                        "month" => $month,
                        "year" => $year,
                        "groupid" => $this->input->post('group_id'),
                        "clientid" => $this->input->post('client_id'),
                        "ongoing_concern" => $this->input->post('Note'),
                        "qrc" => $this->input->post('QRC'),
                        "ci_log" => $this->input->post('CI_log'),
                        "ci_log_note" => $this->input->post('ci_log_note'),
                        "no_of_errors" => $this->input->post('Errors'),
                        "no_errors_note" => $this->input->post('error_note'),
                        "client_satifaction" => $this->input->post('Client_satifaction'),
                        "calls_made" => $this->input->post('Calls_made'),
                        "deadline_missed" => $this->input->post('Deadline_missed'),
                        "service_continum" => $this->input->post('Service_Continuum'),
                        "audit_ready" => $this->input->post('Audit_Ready'),
                        "timestamp" => date('m/d/Y h:i:s'),
                        "referenceid" => "refid-" . strtotime('now'),
                    );
                    $this->Entity_model->model_entity_insert($array);
                    $exist = $this->Entity_model->check_if_exist_cx($month, $year, $this->input->post('group_id'), $this->input->post('client_id'));
                    $value = array("status" => "success", "message" => "New data has been successfully added.", "type" => "add", "callback" => $exist->result());
                }
            } else {
                $value = array("status" => "error", "message" => "Required all inputs");
            }
        } else {
            $value = array("status" => "error", "message" => "Please refesh your browser and login again");
        }
        print json_encode($value, JSON_PRETTY_PRINT);
    }

    public function cx_entities() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required|numeric');
//            $this->form_validation->set_rules('month', 'month', 'required');
//            $this->form_validation->set_rules('year', 'year', 'required');
            $month = date('m', strtotime('first day of last month'));
            $year = date('Y', strtotime('first day of last month'));

            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $entities = $this->Entity_model->entity_cx($month, $year, $this->input->post('id'));
                if ($entities->num_rows()) {
                    if ($entities->row()->reviewed_by) {
                        $reviewer = $this->Entity_model->entity_cx_reviewed($entities->row()->reviewed_by);
                        $review = $reviewer->row()->fullname;
                    } else {
                        $review = "";
                    }
                    $entity = $entities->result();
                } else {
                    $entity = "";
                    $review = "";
                }

                print json_encode(["status" => "success", "update" => $entities->num_rows(), "data" => $entity, "reviewer" => $review, "user" => $this->session->userdata('user_session')]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function client_entities() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $entities = $this->Entity_model->client_entities($this->input->post('id'));
                print json_encode($entities);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function client_all_entities() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $entities = $this->Entity_model->client_all_entities($this->input->post('id'));
                print json_encode($entities);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function show() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $entity = $this->Entity_model->show($this->input->post('id'));
                print json_encode(["status" => "ok", 'data' => $entity]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function update() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('entityeditid', 'entityeditid', 'required');
            $this->form_validation->set_rules('entityeditname', 'entityeditname', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $data = [
                    'entity' => html_purify($this->input->post('entityeditname')),
                ];
                $this->Entity_model->update($this->input->post('entityeditid'), $data);

                print json_encode(['status' => 'ok']);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function destroy() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $this->Entity_model->delete($this->input->post('id'));
                print json_encode(["status" => "ok"]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

}

<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: application/json');

class Contacts extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->model('Models');
        $this->load->model('Contact_model');
        $this->load->model('Company_model');
        $this->load->library('form_validation');
        $this->load->database();
    }

    public function get_allcontacts() {
        $columns = [
            0 => 'company',
            1 => 'fullname',
            2 => 'service',
            3 => 'industry',
            4 => 'assignedto',
            5 => 'lastupdate',
            6 => 'status',
            7 => 'actions',
        ];

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];

        $dir = $this->input->post('order')[0]['dir'];
        $leadstatus = $this->input->post('leadstatus');
        $leadservice = $this->input->post('leadservice');

        $totalData = $this->Contact_model->allcontacts_count($leadstatus, $leadservice);

        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $contacts = $this->Contact_model->allcontacts($limit, $start, $order, $dir, $leadstatus, $leadservice);
        } else {
            $search = $this->input->post('search')['value'];

            $contacts = $this->Contact_model->contacts_search($limit, $start, $search, $order, $dir, $leadstatus, $leadservice);

            // removed 
            // $totalFiltered = $this->Contact_model->contacts_search_count($search);
        }

        $data = array();
        if (!empty($contacts)) {
            foreach ($contacts as $contact) {
                $nestedData['company'] = '<a href="#" class="btn btn-link open-funnel-viewmodal" data-toggle="modal" data-target="#viewModal" data-id="' . $contact->id . '">' . $contact->company . '</a>';
                $nestedData['fullname'] = $contact->fullname;
                $nestedData['service'] = $contact->service;
                $nestedData['industry'] = $contact->industry;
                $nestedData['assignedto'] = $contact->assignedto;
                $nestedData['lastupdate'] = '';
                $nestedData['status'] = $this->Contact_model->status_badge($contact->status);
                $nestedData['actions'] = '<button class="btn btn-light btn-sm open-funnel-viewmodal" data-toggle="modal" data-target="#viewModal" data-id="' . $contact->id . '"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="View Contact"></i></button><button class="btn mx-1 btn-light btn-sm open-funnel-editmodal" data-toggle="modal" data-target="#editModal" data-id="' . $contact->id . '"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit Contact"></i></button><button class="btn btn-light btn-sm mark-as-spam" data-id="' . $contact->id . '" data-toggle="tooltip" data-placement="top" title="Delete Contact"><i class="fa fa-times"></i></button>';

                $data[] = $nestedData;
            }
        }

        $json_data = [
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData), //totalFiltered
            "data" => $data
        ];

        echo json_encode($json_data);
    }

    public function get_contact() {
        $id = $this->input->post('id');
        $contacts = $this->Contact_model->get_contact($id);
        $json_data = [
            "data" => $contacts
        ];

        echo json_encode($json_data);
    }

    public function client_accounting_form() {

        
        $this->form_validation->set_rules('form_type', 'form_type', 'required');
        $this->form_validation->set_rules('Contact_Person', 'Contact_Person', 'required');
        $this->form_validation->set_rules('Position_Title', 'Position_Title', 'required');
        $this->form_validation->set_rules('Email_Address', 'Email_Address', 'required');
        $this->form_validation->set_rules('Phone_Number', 'Phone_Number', 'required');

        $this->form_validation->set_rules('Company_Legal', 'Company_Legal', 'required');
        $this->form_validation->set_rules('Company_Full', 'Company_Full', 'required');
        $this->form_validation->set_rules('Company_Website', 'Company_Website', 'required');

        $this->form_validation->set_rules('What_does', 'What_does', 'required');

        $html = '';

        if ($this->form_validation->run()) {
            $ref_id = strtotime('now') . rand(10, 99);
            $company = array(
                "ref_id" => $ref_id,
                "form_type" => html_purify($this->input->post('form_type')),
                "industry_id" => html_purify($this->input->post('What_industry')),
                "clientstatus_id" => 1,
                "status_id" => 1,
                "form_id" => 1,
                "account" => html_purify($this->input->post('Company_Legal')),
                "entity" => html_purify($this->input->post('Company_Legal')),
                "website" => html_purify($this->input->post('Company_Website')),
                "phone" => html_purify($this->input->post('Phone_Number')),
                "full_address" => html_purify($this->input->post('Company_Full')),
                "created_at" => date('Y-m-d H:i:s'),
            );

            $html .= "<strong>What does your company do?</strong><p>-" . $this->input->post('What_does') . "</p>";
            $html .= "<strong>How long have you been in operation?</strong><p>-" . $this->input->post('How_long_have') . "</p>";
            $html .= "<strong>How is the company currently funded?</strong><p>-" . $this->input->post('How_is_the_company') . "</p>";
            $html .= "<strong>Do you currently have accounting system in place?</strong><p>-" . $this->input->post('Do_you_currently') . "</p>";
            $html .= "<strong>What is your current average monthly company financial transactions?</strong><p>-" . $this->input->post('What_is_your_current') . "</p>";
            $html .= "<strong>Do we need to bring your books up to date?</strong><p>-" . $this->input->post('Do_we_need_to_bring') . "</p>";
            $html .= "<strong>Do you need to track expenses by department, by project, or by any other segment?</strong> <p>-" . $this->input->post('Do_you_need_to_track') . "</p>";
            $html .= "<strong>How did you find out about us?</strong><p>-" . $this->input->post('How_did_you') . "</p>";

            $other = '<strong>What other Scrubbed services are you interested in?</strong>';
            for ($x = 1; $x <= 7; $x++) {
                if ($this->input->post('What_other_Scrubbed' . $x)) {
                    $other .= '<p>-' . $this->input->post('What_other_Scrubbed' . $x) . '</p>';
                }
            }
            $html .= $other;

            $this->Contact_model->add_company($company);
            $company_id = $this->Contact_model->get_added_client($ref_id);
            /*
             * 
             * Service ID 
             * 1 Accounting
             * 2 CorFin
             * 3 Tax

             */
            if ($company_id->num_rows()) {
                $add = array(
                    "company_id" => $company_id->result()[0]->ID,
                    "is_active" => 1,
                    "fullname" => html_purify($this->input->post('Contact_Person')),
                    "email" => html_purify($this->input->post('Email_Address')),
                    "jobtitle" => html_purify($this->input->post('Position_Title')),
                    "phone" => html_purify($this->input->post('Phone_Number')),
                    "created_at" => date('Y-m-d H:i:s'),
                );
                $this->Contact_model->add_client($add);
            }
            $note = array(
                "company_id" => $company_id->result()[0]->ID,
                "title" => html_purify($this->input->post('form_type')),
                "content" => $html,
                "created_at" => date('Y-m-d H:i:s')
            );
            $this->Contact_model->add_notes($note);
            $value = array("message" => "success", "type" => "add", "text" => "Successfully added");
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    public function client_tax_form() {

        $this->form_validation->set_rules('form_type', 'form_type', 'required');
        $this->form_validation->set_rules('Contact_Person', 'Contact_Person', 'required');
        $this->form_validation->set_rules('Position_Title', 'Position_Title', 'required');
        $this->form_validation->set_rules('Email_Address', 'Email_Address', 'required');
        $this->form_validation->set_rules('Phone_Number', 'Phone_Number', 'required');

        $this->form_validation->set_rules('Company_Legal', 'Company_Legal', 'required');
        $this->form_validation->set_rules('Company_Full', 'Company_Full', 'required');
        $this->form_validation->set_rules('Company_Website', 'Company_Website', 'required');


        $this->form_validation->set_rules('What_states_are', 'What_states_are', 'required');
        $this->form_validation->set_rules('What_does', 'What_does', 'required');
        $this->form_validation->set_rules('How_long_have', 'How_long_have', 'required');
        $this->form_validation->set_rules('Who_is_currently', 'Who_is_currently', 'required');
        $this->form_validation->set_rules('What_type_of_annual', 'What_type_of_annual', 'required');
        $this->form_validation->set_rules('Tax_What_year_was', 'Tax_What_year_was', 'required');
        $this->form_validation->set_rules('Are_you_currently', 'Are_you_currently', 'required');
        $this->form_validation->set_rules('Are_you_required', 'What_is_your_current', 'required');
        $this->form_validation->set_rules('Are_you_required_sales', 'Are_you_required_sales', 'required');



        $html = '';
        $Do_you_have_any = '<strong>Do you have any delinquent tax years covering these taxes?</strong>' . "</p>";
        $Do_you_need = '<strong>Do you need tax planning & advisory services?</strong>' . "</p>";
        $Do_you_need_tax = '<strong>Do you need tax analysis services?</strong>' . "</p>";

        if ($this->form_validation->run()) {

            for ($x = 1; $x <= 5; $x++) {
                if ($this->input->post('Do_you_have_any' . $x)) {
                    $Do_you_have_any .= "-" . $this->input->post('Do_you_have_any' . $x) . "</p>";
                }
            }


            for ($x = 1; $x <= 9; $x++) {
                if ($this->input->post('Do_you_need' . $x)) {
                    $Do_you_need .= "-" . $this->input->post('Do_you_need' . $x) . "</p>";
                }
            }


            for ($x = 1; $x <= 9; $x++) {
                if ($this->input->post('Do_you_need_tax' . $x)) {
                    $Do_you_need_tax .= "-" . $this->input->post('Do_you_need_tax' . $x) . "</p>";
                }
            }

            $html .= "<strong>What states are you registered in?</strong><p>" . $this->input->post('What_states_are') . "</p>";
            $html .= "<strong>What does your company do?</strong><p>" . $this->input->post('What_does') . "</p>";
            $html .= "<strong>How long have you been in operation?</strong><p>" . $this->input->post('How_long_have') . "</p>";
            $html .= "<strong>Who is currently handling your company's tax compliance & filing?</strong><p>" . $this->input->post('Who_is_currently') . "</p>";
            $html .= "<strong>What type of annual income tax return do you file?</strong><p>" . $this->input->post('What_type_of_annual') . "</p>";
            $html .= "<strong>Are you currently under audit?</strong><p>" . $this->input->post('Are_you_currently') . "</p>";
            $html .= "<strong>What year was your last completed income tax return?</strong><p>" . $this->input->post('Tax_What_year_was') . "</p>";
            $html .= "<strong>Do you have any delinquent tax years covering these taxes?</strong><p>" . $this->input->post('Are_you_required') . "</p>";
            $html .= "<strong>What does your company do?</strong><p>" . $this->input->post('Are_you_required_sales') . "</p>";
            $html .= "<strong>Please indicate any specific concern that you have about your taxes?</strong><p>" . $this->input->post('Please_indicate') . "</p>";

            $html .= $Do_you_have_any;
            $html .= $Do_you_need;
            $html .= $Do_you_need_tax;


            $ref_id = strtotime('now') . rand(10, 99);
            $company = array(
                "ref_id" => $ref_id,
                "form_type" => html_purify($this->input->post('form_type')),
                "account" => html_purify($this->input->post('Company_Legal')),
                "entity" => html_purify($this->input->post('Company_Legal')),
                "status_id" => 1,
                "full_address" => html_purify($this->input->post('Company_Full')),
                "website" => html_purify($this->input->post('Company_Website')),
                "created_at" => date('Y-m-d H:i:s'),
            );
            /*
             * 
             * Service ID 
             * 1 Accounting
             * 2 CorFin
             * 3 Tax
             * 
             * 
             */

            $this->Contact_model->add_company($company);
            $company_id = $this->Contact_model->get_added_client($ref_id);

            if ($company_id->num_rows()) {
                $add = array(        
                    "fullname" => html_purify($this->input->post('Contact_Person')),
                    "jobtitle" => html_purify($this->input->post('Position_Title')),
                    "email" => html_purify($this->input->post('Email_Address')),
                    "phone" => html_purify($this->input->post('Phone_Number')),
//                    "status_id" => 1,
//                    "service_id" => 3,
//                    "clientstatus_id" => 1,
                    "company_id" => $company_id->result()[0]->ID,
                    "created_at" => date('Y-m-d H:i:s'),
                    "contact_id" => $company_id->result()[0]->ID,
                );
                $this->Contact_model->add_client($add);
            }
            $note = array(
                "company_id" => $company_id->result()[0]->ID,
                "title" => html_purify($this->input->post('form_type')),
                "content" => $html,
                "created_at" => date('Y-m-d H:i:s')
            );
            $this->Contact_model->add_notes($note);

            $value = array("message" => "success", "type" => "add", "text" => "Successfully added");
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    function client_free_consultation() {
        $this->form_validation->set_rules('form_type', 'form_type', 'required');
        $this->form_validation->set_rules('Company', 'Company', 'required');
        $this->form_validation->set_rules('Contact_Person', 'Contact_Person', 'required');
        $this->form_validation->set_rules('Email_Address', 'Email_Address', 'required');
        $this->form_validation->set_rules('Phone_Number', 'Phone_Number', 'required');
        $this->form_validation->set_rules('note', 'note', 'required');
        if ($this->form_validation->run()) {
            $ref_id = strtotime('now') . rand(10, 99);
            $company = array(
                "ref_id" => $ref_id,
                "form_type" => html_purify($this->input->post('form_type')),
                "industry_id" => 15,
                "account" => html_purify($this->input->post('Company')),
                "entity" => html_purify($this->input->post('Company')),
                "status_id" => 1,
                "clientstatus_id" => 1,
                "form_id" => html_purify($this->input->post('service_id')),
                "created_at" => date('Y-m-d H:i:s'),
            );
            $this->Contact_model->add_company($company);
            $company_id = $this->Contact_model->get_added_client($ref_id);
            /*
             *
             * Service ID
             * 1 Accounting
             * 2 CorFin
             * 3 Tax
             */
            if ($company_id->num_rows()) {
                $add = array(
                    "fullname" => html_purify($this->input->post('Contact_Person')),
                    "is_active" => 1,
                    "email" => html_purify($this->input->post('Email_Address')),
                    "phone" => html_purify($this->input->post('Phone_Number')),
                    "company_id" => $company_id->result()[0]->ID,
                    "created_at" => date('Y-m-d H:i:s'),
                );
                $this->Contact_model->add_client($add);
            }
            $note = array(
                "company_id" => $company_id->result()[0]->ID,
                "title" => html_purify($this->input->post('form_type')),
                "content" => html_purify($this->input->post('note')),
                "created_at" => date('Y-m-d H:i:s')
            );
            $this->Contact_model->add_notes($note);
            $value = array("message" => "success", "type" => "add", "text" => "Successfully added");
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    public function add_existing_company() {
        $this->form_validation->set_rules('company_id', 'company_id', 'required');
        $this->form_validation->set_rules('contact_id', 'contact_id', 'required');

        if ($this->form_validation->run() == FALSE) {
            print json_encode(["status" => "error"]);
        } else {
            $data = [
                'company_id' => $this->input->post('company_id'),
                'updated_at' => date('Y-m-d h:i:s A'),
            ];

            $this->Contact_model->add_existing_company($this->input->post('contact_id'), $data);
            $company = $this->Company_model->show($this->input->post('company_id'));
            echo json_encode(["status" => "ok", 'data' => $company]);
        }
    }

    public function remove_company() {
        $this->form_validation->set_rules('contact_id', 'contact_id', 'required');

        if ($this->form_validation->run() == FALSE) {
            print json_encode(["status" => "error"]);
        } else {
            $data = [
                'company_id' => null,
                'updated_at' => date('Y-m-d h:i:s A'),
            ];

            $this->Contact_model->remove_company($this->input->post('contact_id'), $data);

            echo json_encode(["status" => "ok"]);
        }
    }

    public function add_new_company() {
        $this->form_validation->set_rules('contact_id', 'contact_id', 'required');
        $this->form_validation->set_rules('companyname', 'companyname', 'required');

        if ($this->form_validation->run() == FALSE) {
            print json_encode(["status" => "error"]);
        } else {
            $data = [
                'name' => html_purify($this->input->post('companyname')),
                'website' => html_purify($this->input->post('website')),
                'phone' => html_purify($this->input->post('companyphone')),
                'industry_id' => html_purify($this->input->post('industry')),
                'full_address' => html_purify($this->input->post('address')),
            ];

            $company_id = $this->Company_model->store($data);

            $contact = [
                'company_id' => $company_id,
                'updated_at' => date('Y-m-d h:i:s A'),
            ];

            $this->Contact_model->add_existing_company($this->input->post('contact_id'), $contact);

            $return = [
                'status' => 'ok',
                'companyid' => $company_id,
                'companyname' => html_purify($this->input->post('companyname')),
            ];

            print json_encode($return);
        }
    }

    public function destroy() {
        $this->form_validation->set_rules('contact_id', 'contact_id', 'required');
        $id = $this->input->post('contact_id');

        if ($this->form_validation->run() == FALSE) {
            print json_encode(["status" => "error"]);
        } else {
            $this->Contact_model->delete_contact($id);
            print json_encode(['status' => 'ok']);
        }
    }

    public function conv() {
        $user = $this->Models->get_user_info(365);
        echo $user->first_name . ' ' . $user->last_name;
    }

    public function conversation() {
        $this->form_validation->set_rules('company_id', 'company_id', 'required|numeric');
        if ($this->form_validation->run()) {
            $array = [];
            $result = $this->Contact_model->select_conversation($this->input->post('company_id'));
            foreach ($result->result() as $value) {
                $user = $this->Models->get_user_info($value->userid);
                $array[] = [
                    "id" => $value->id,
                    "title" => $value->title,
                    "userid" => $user->first_name . ' ' . $user->last_name,
                    "content" => "$value->content",
                    "update_at" => "$value->update_at",
                ];
            }
            print json_encode($array, JSON_PRETTY_PRINT);
        }
    }

    public function add_conversation() {
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('contact_id', 'contact_id', 'required');
        $this->form_validation->set_rules('note', 'note', 'required');
        if ($this->form_validation->run()) {
            $data = [
                "title" => html_purify($this->input->post('title')),
                "userid" => $this->session->userdata('user_session'),
                "content" => html_purify($this->input->post('note')),
                "update_at" => date('Y-m-d h:i:s A'),
                "company_id" => $this->input->post('contact_id')
            ];
            $this->Contact_model->insert_conversation($data);

            $array = [];
            $result = $this->Contact_model->output_conversation($this->input->post('contact_id'));
            $this->Company_model->last_update($this->input->post('contact_id'));
            foreach ($result->result() as $value) {
                $user = $this->Models->get_user_info($value->userid);
                $array[] = [
                    "message" => 'success',
                    "id" => $value->id,
                    "title" => $value->title,
                    "userid" => $user->first_name . ' ' . $user->last_name,
                    "content" => "$value->content",
                    "update_at" => "$value->update_at",
                ];
            }
            print json_encode($array, JSON_PRETTY_PRINT);
        } else {
            $array = [
                "message" => 'error'
            ];
            print json_encode($array, JSON_PRETTY_PRINT);
        }
    }

    public function store() {
        $this->form_validation->set_rules('companyId', 'companyId', 'required');
        $this->form_validation->set_rules('contactaddfullname', 'contactaddfullname', 'required');
        $this->form_validation->set_rules('contactaddjobtitle', 'contactaddjobtitle', 'required');
        $this->form_validation->set_rules('contactaddemail', 'contactaddemail', 'required');
        $this->form_validation->set_rules('contactaddphone', 'contactaddphone', 'required');
        if ($this->form_validation->run() == FALSE) {
            print json_encode(["status" => "error"]);
        } else {
            $data = [
                'company_id' => $this->input->post('contactaddentity'),
                'fullname' => html_purify($this->input->post('contactaddfullname')),
                'jobtitle' => html_purify($this->input->post('contactaddjobtitle')),
                'email' => html_purify($this->input->post('contactaddemail')),
                'phone' => html_purify($this->input->post('contactaddphone')),
                'linkedin' => html_purify($this->input->post('contactaddlinkedin')),
                'skype' => html_purify($this->input->post('contactaddskype')),
            ];

            $this->Contact_model->store($data);
            $this->Company_model->last_update($this->input->post('companyId'));

            print json_encode(['status' => 'ok']);
        }
    }

    public function show() {
        $this->form_validation->set_rules('id', 'id', 'required');
        if ($this->form_validation->run() == FALSE) {
            print json_encode(["status" => "error"]);
        } else {
            $contact = $this->Contact_model->show($this->input->post('id'));
            print json_encode(["status" => "ok", 'data' => $contact->result()]);
        }
    }

    public function update() {
        $this->form_validation->set_rules('contacteditid', 'contacteditid', 'required');
        $this->form_validation->set_rules('contacteditfullname', 'contacteditfullname', 'required');
        $this->form_validation->set_rules('contacteditjobtitle', 'contacteditjobtitle', 'required');
        $this->form_validation->set_rules('contacteditemail', 'contacteditemail', 'required');
        $this->form_validation->set_rules('contacteditphone', 'contacteditphone', 'required');
        $this->form_validation->set_rules('contacteditstatus', 'contacteditstatus', 'required');

        if ($this->form_validation->run() == FALSE) {
            print json_encode(["status" => "error"]);
        } else {
            $data = [
                'company_id' => html_purify($this->input->post('contacteditentity')),
                'fullname' => html_purify($this->input->post('contacteditfullname')),
                'jobtitle' => html_purify($this->input->post('contacteditjobtitle')),
                'email' => html_purify($this->input->post('contacteditemail')),
                'phone' => html_purify($this->input->post('contacteditphone')),
                'is_active' => $this->input->post('contacteditstatus'),
                'linkedin' => html_purify($this->input->post('contacteditlinkedin')),
                'skype' => html_purify($this->input->post('contacteditskype')),
            ];

            $this->Contact_model->update($this->input->post('contacteditid'), $data);

            print json_encode(['status' => 'ok']);
        }
    }

    public function update_conversation() {
        $this->form_validation->set_rules('id', 'id', 'required');
        $this->form_validation->set_rules('notepad', 'notepad', 'required');
        if ($this->form_validation->run()) {
            $data = [
                "content" => html_purify($this->input->post('notepad')),
                "update_at" => date('Y-m-d h:i:s A'),
            ];
            $this->Contact_model->update_conversation($data, $this->input->post('id'));
            $array = [
                "message" => 'success',
            ];
            print json_encode($array, JSON_PRETTY_PRINT);
        }
    }

    public function delete_conversation() {
        $this->form_validation->set_rules('id', 'id', 'required');
        if ($this->form_validation->run()) {
            $this->Contact_model->delete_conversation($this->input->post('id'));
            $array = ["message" => 'success'];
            print json_encode($array, JSON_PRETTY_PRINT);
        }
    }

    public function edit_cna() {
        $this->form_validation->set_rules('id', 'id', 'required');
        $this->form_validation->set_rules('notepad', 'notepad', 'required');
        if ($this->form_validation->run()) {
            $data = [
                "content" => html_purify($this->input->post('notepad')),
                "updated_at" => date('Y-m-d h:i:s A'),
            ];
            $this->Contact_model->edit_conversation($data, $this->input->post('id'));
            $this->Company_model->last_update($this->input->post('id'));
            $array = ["message" => 'success'];
            print json_encode($array, JSON_PRETTY_PRINT);
        }
    }

}

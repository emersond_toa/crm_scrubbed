<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");

class Upload_billing extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->model('Models');
        $this->load->database();
    }

    public function index() {
        $data['title'] = 'Upload Billing';
        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) {
            $this->load->view('templates/sidebar', $data);
            $this->load->view('CRM_Upload_billing');
        } else {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('templates/footer');
    }

    public function upload_billing() {
        if ($this->session->userdata('user_session')) {
            if (isset($_FILES['file'])) {
                $fname = $_FILES['file']['name'][0];
                $ext = array("csv");
                if (in_array(pathinfo($fname, PATHINFO_EXTENSION), $ext)) {
                    $tmpFilePath = $_FILES['file']['tmp_name'][0];
                    move_uploaded_file($tmpFilePath, '/home/ubuntu/billing_uploads/billing.csv');
                    $file = fopen('/home/ubuntu/billing_uploads/billing.csv', 'r');
                    while (($line = fgetcsv($file)) !== FALSE) {
                        if ($line[0] != 'Invoice #') {
                            $data = array(
                                "Invoice" => $line[0],
                                "Date" => $line[1],
                                "Customer" => $line[2],
                                "Due_Date" => $line[3],
                                "Aging" => $line[4],
                                "Open_Balance" => $line[5],
                            );
                            $this->Models->temporary_aging_details($data);
                        }
                    }
                    fclose($file);
                    exec_shell("sudo chmod 777 /home/ubuntu/billing_uploads/ -R");
                    unlink('/home/ubuntu/billing_uploads/billing.csv');
                }
            }
        }
    }

    public function move_to_billing() {
        header('Content-Type: application/json');
        if ($this->session->userdata('user_session')) {
            $aging = $this->Models->temporary_aging_select_details();
            echo json_encode($aging->result(), JSON_PRETTY_PRINT);
            foreach ($aging->result() as $value) {
                
                echo $value->Invoice ."\n";
            }
        }
    }

}

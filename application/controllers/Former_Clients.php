<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');

class Former_Clients extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->model('Models');
        $this->load->model('Client_model');
        $this->load->model('ClientStatus_model');
        $this->load->model('Status_model');
        $this->load->model('Form_model');
        $this->load->model('Company_model');
        $this->load->model('Contact_model');
        $this->load->model('Industry_model');
        $this->load->model('Service_model');
        $this->load->model('ReferralPartner_model');
        $this->load->model('LeadSource_model');
        $this->load->model('TerminationReason_model');
        $this->load->model('Agreement_model');
        $this->load->database();
    }

    public function index() {
        $data['title'] = 'Former Clients';
        $data['clientstatuses'] = $this->ClientStatus_model->all_status();
        $data['forms'] = $this->Form_model->all_forms();
        $data['users'] = $this->User_model->all_users();
        $data['companies'] = $this->Company_model->all_companies();
        $data['industries'] = $this->Industry_model->all_industries();
        $data['statuses'] = $this->Status_model->all_status();
        $data['services'] = $this->Service_model->all_services();
        $data['referralpartners'] = $this->ReferralPartner_model->all();
        $data['leadsources'] = $this->LeadSource_model->all();
        $data['reasons'] = $this->TerminationReason_model->all();
        $data['agreements'] = $this->Agreement_model->all();
        $data['work_groups'] = $this->Company_model->work_groups();

        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) 
        {
            $this->load->view('templates/sidebar');
            $this->load->view('former_clients/index');
            $this->load->view('former_clients/modals/view');
            $this->load->view('former_clients/modals/edit');
        } 
        else {
            redirect("https://employeeportal.scrubbed.net/");
        }
        $this->load->view('templates/footer');
    }

    public function get_allcompanies() {
        if ($this->session->userdata('user_session')) {
            $columns = [
                0 => 'name',
                1 => 'contact',
                2 => 'leadform',
                3 => 'services',
                4 => 'industry',
                5 => 'work_groups',
                6 => 'updated_at',
                7 => 'termination_date',
                8 => 'clientstatus',
                9 => 'actions',
            ];
    
            $limit = $this->input->post('length');
            $start = $this->input->post('start');
            $order = $columns[$this->input->post('order')[0]['column']];
    
            $dir = $this->input->post('order')[0]['dir'];
            $leadstatus = $this->input->post('leadstatus');
            $leadform = $this->input->post('leadform');
    
            $totalData = $this->Company_model->former_clientcount($leadstatus, $leadform);
    
            $totalFiltered = $totalData;
    
            if (empty($this->input->post('search')['value'])) {
                $companies = $this->Company_model->former_clientdatatable($limit, $start, $order, $dir, $leadstatus, $leadform);
            } else {
                $search = $this->input->post('search')['value'];
    
                $companies = $this->Company_model->former_clientdatatablesearch($limit, $start, $search, $order, $dir, $leadstatus, $leadform);
    
                // removed 
                $totalFiltered = $this->Company_model->former_client_contacts_search_count($search, $leadstatus, $leadform);
            }
    
            $data = array();
            if (!empty($companies)) {
                foreach ($companies as $company) {
                    $nestedData['name'] = '<a href="#" class="open-former-client-viewmodal" data-toggle="modal" data-target="#viewModal" data-id="' . $company->ID . '">' . $company->name . '</a>';
                    $nestedData['contact'] = $company->contact;
                    $nestedData['leadform'] = $company->leadform;
                    $nestedData['industry'] = $company->industry;
                    $nestedData['services'] = '';
                    $users = [];
                    foreach($company->assignedto as $ass) {
                        $users[] = $ass->first_name.' '.$ass->last_name;
                    }
                    $nestedData['assignedto'] = implode (", ", $users);
                    $services = [];
                    foreach($company->services as $service) {
                        $services[] = $service->name;
                    }
                    $nestedData['services'] = implode (", ", $services);
    
                    $work_groups = [];
                    foreach($company->work_groups as $work) {
                        $work_groups[] = $work->group_name;
                    }
                    $nestedData['work_groups'] = implode (", ", $work_groups);
    
                    $nestedData['lastupdate'] = date('m/d/Y h:i a', strtotime($company->updated_at));
                    $nestedData['termination_date'] = date("m/d/Y", strtotime($company->termination_date));
                    $nestedData['clientstatus'] = $this->Company_model->clientstatus_badge($company->clientstatus);
                    $nestedData['actions'] = '<button class="btn btn-light btn-sm open-former-client-viewmodal" data-toggle="modal" data-target="#viewModal" data-id="' . $company->ID . '"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="View Client"></i></button><button class="btn mx-1 btn-light btn-sm open-former-client-editmodal" data-toggle="modal" data-target="#editModal" data-id="' . $company->ID . '"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit Client"></i></button><button class="btn btn-light btn-sm delete-former-client" data-id="' . $company->ID . '" data-toggle="tooltip" data-placement="top" title="Delete Client"><i class="fa fa-times"></i></button>';
    
                    $data[] = $nestedData;
                }
            }
    
            $json_data = [
                "draw" => intval($this->input->post('draw')),
                "recordsTotal" => intval($totalData),
                "recordsFiltered" => intval($totalFiltered), //totalFiltered
                "data" => $data
            ];
    
            echo json_encode($json_data);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }
}

<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');

class CPU_usage extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->model('Models');
        $this->load->database();
    }

    public function socket() {
        shell_exec("bash /home/ubuntu/node/start_socket.sh");
    }

    public function index() {
        $data['title'] = 'Settings';

        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) {
            $this->load->view('templates/sidebar');
            $this->load->view('CRM_Usage');
        } else {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('templates/footer');
    }

    public function get_device_information() {
        header('Content-Type: application/json');


        $disk = shell_exec('sudo df | grep /dev/sda2');
        $disk = preg_replace('/\s+/', ' ', $disk);
        $disk = explode(' ', $disk);
        $sqlpro = $this->db->query('show full PROCESSLIST');


        $total = (int) str_replace("\n", '', shell_exec("nproc"));
        $usage = str_replace("\n", '', shell_exec("cat /proc/loadavg | awk '{ print $1*100/" . $total . " }'"));

        $processor = array(
            "total" => 'Total CPU: ' . $total,
            "used" => round($usage, 2),
            "remaining" => round((100 - $usage), 2) . '%',
            "used_percent" => round($usage, 2) . '%',
            "remaining_percent" => round((100 - $usage), 2) . "%"
        );
        $ddisk = array(
            "total" => 'Total Disk: ' . number_format($disk[1] / 1000000, 2) . 'GB',
            "used" => number_format($disk[2] / 1000000, 2) . 'GB ',
            "remaining" => number_format($disk[3] / 1000000, 2) . 'GB',
            "used_percent" => $disk[4],
            "remaining_percent" => 100 - str_replace("%", "", $disk[4]) . "%"
        );
        $ram = shell_exec("free -m | grep Mem: | awk '{print $2,$3,$7}'");
        $random = explode(' ', $ram);
        $dram = array(
            "total" => $random[0],
            "used" => $random[1],
            "available" => str_replace("\n", '', $random[0] - $random[1]),
            "used_percent" => ROUND($random[1] / $random[0] * 100, 2),
            "available_percent" => ROUND(100 - ($random[1] / $random[0] * 100), 2),
        );
        $data = array(
            "random_access_memory" => $dram,
            "memory_card" => $ddisk,
            "processor" => $processor,
            "process_list" => $sqlpro->result()
        );
        print json_encode($data, JSON_PRETTY_PRINT);
    }

}

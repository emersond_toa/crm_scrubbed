<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
class Invoices extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->model('Models');
        $this->load->model('Invoice_model');
        $this->load->model('Payment_model');
        $this->load->model('Company_model');
        $this->load->model('Client_model');
        $this->load->database();
        // / Load file helper
        $this->load->helper('file');
        $this->load->library('csvimport');
    }

    public function index() 
    {
        $data = array();
        $data['title'] = 'Invoices';
        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) 
        {
            // Get messages from the session
            if($this->session->userdata('success_msg'))
            {
                $data['success_msg'] = $this->session->userdata('success_msg');
                $this->session->unset_userdata('success_msg');
            }
            if($this->session->userdata('error_msg'))
            {
                $data['error_msg'] = $this->session->userdata('error_msg');
                $this->session->unset_userdata('error_msg');
            }
            
            // Get rows
            $data['invoices'] = $this->Invoice_model->getRows();
            $data['invoices_pagination'] = $this->Invoice_model->invoices_pagination();
            $data['total'] = $this->Invoice_model->getTotalInvoices();
            $data['clients'] = $this->Client_model->getActiveClients();
            $data['work_groups'] = $this->Models->getWorkgroups();
            $data['line_items'] = $this->Models->getLineItems();
            $data['sources'] = [
                ['bill','bill.com'],
                ['xero','xero'],
            ];
            
            $this->load->view('templates/sidebar');
            $this->load->view('invoices/index', $data);
        } 
        else 
        {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('templates/footer');
    }

    public function import()
    {
        $this->form_validation->set_rules('file', 'CSV file', 'callback_file_check');

        if($this->form_validation->run() == true) {
            $file_data = $this->csvimport->get_array($_FILES["file"]["tmp_name"]);
            $data = [];
            foreach($file_data as $row)
            {
                $client_id = $row['Customer ID'] == '' ? null : $row['Customer ID'];
                $check_invoice = array(
                    'client_id' => $client_id,
                    'client_name' => $row['Customer Name'],
                    'group_id' => null,
                    'invoice_number' => $row['Invoice #'],
                    'credit_amount' => $row['Credit Amount'],
                    'line_item_name' => $row['Line Item Name'],
                    'line_item_amount' => $row['Line Item Amount'],
                    'total_line_items' => $row['Total Line Items'],
                    'is_included' => 0,
                    'source' => 'bill',
                    'payment_status' => $row['Payment Status'],
                    'transaction_type' => 'invoices',
                    'transaction_date' => date('Y-m-d h:i:s', strtotime($row['Transaction Date'])),
                    'created_at' => date('Y-m-d h:i:s', strtotime($row['Created'])),
                    'updated_at' => date('Y-m-d h:i:s', strtotime($row['Updated'])),
                );
                $data[] = $check_invoice;
            }
            if(!empty($data)) {
                $this->Invoice_model->insert($data);
                // $this->Invoice_model->update_invoice_group();
                $this->session->set_userdata('success_msg', 'Success!');
            } else {
                $this->session->set_userdata('error_msg', 'Empty Data!');
            }
        } else {
            $this->session->set_userdata('error_msg', 'Invalid file, please select only CSV file.');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function update_invoice_group($month, $year) 
    {
        $this->Invoice_model->update_invoice_group($month, $year);
        echo 'Ok';
    }

    public function not_included() 
    {
        $data = array();
        $data['title'] = 'Invoices';
        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) 
        {
            // Get messages from the session
            if($this->session->userdata('success_msg'))
            {
                $data['success_msg'] = $this->session->userdata('success_msg');
                $this->session->unset_userdata('success_msg');
            }
            if($this->session->userdata('error_msg'))
            {
                $data['error_msg'] = $this->session->userdata('error_msg');
                $this->session->unset_userdata('error_msg');
            }
            
            // Get rows
            $data['invoices'] = $this->Invoice_model->getRowsMissing();
            $data['invoices_pagination'] = $this->Invoice_model->missing_invoices_pagination();
            $data['total'] = $this->Invoice_model->getMissingTotalInvoices();
            $data['clients'] = $this->Client_model->getActiveClients();
            $data['work_groups'] = $this->Models->getWorkgroups();
            $data['line_items'] = $this->Models->getLineItems();
            $data['sources'] = [
                ['bill','bill.com'],
                ['xero','xero'],
            ];

            $this->load->view('templates/sidebar');
            $this->load->view('invoices/missing', $data);
        } 
        else 
        {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('templates/footer');
    }

    public function upload_invoices() 
    {
        $data = array();
        $data['title'] = 'Invoices';
        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) 
        {
            // Get messages from the session
            if($this->session->userdata('success_msg'))
            {
                $data['success_msg'] = $this->session->userdata('success_msg');
                $this->session->unset_userdata('success_msg');
            }
            if($this->session->userdata('error_msg'))
            {
                $data['error_msg'] = $this->session->userdata('error_msg');
                $this->session->unset_userdata('error_msg');
            }
            
            // Get rows
            $this->load->view('templates/sidebar');
            $this->load->view('invoices/upload', $data);
        } 
        else 
        {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('templates/footer');
    }
    
    /*
     * Callback function to check file value and type during validation
     */
    public function file_check($str){
        $allowed_mime_types = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != ""){
            $mime = get_mime_by_extension($_FILES['file']['name']);
            $fileAr = explode('.', $_FILES['file']['name']);
            $ext = end($fileAr);
            if(($ext == 'csv') && in_array($mime, $allowed_mime_types)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Please select only CSV file to upload.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please select a CSV file to upload.');
            return false;
        }
    }

    public function show($invoice) 
    {
        $data = array();
        $data['title'] = 'Invoices';
        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) 
        {
            // Get row
            $data['invoices'] = $this->Invoice_model->show($invoice);
            $data['payments'] = $this->Payment_model->show($invoice);

            $data['total_invoice'] = $this->Invoice_model->total($invoice)->line_item_amount;
            $data['total_payments'] =  $this->Payment_model->total($invoice)->line_item_amount;
            $data['total_balance'] = ($data['total_invoice'] - $data['total_payments']);

            $this->load->view('templates/sidebar');
            $this->load->view('invoices/show', $data);
        } else {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('templates/footer');
    }

    public function xero() 
    {
        $data = array();
        $data['title'] = 'Invoices';
        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) 
        {
            // Get messages from the session
            if($this->session->userdata('success_msg'))
            {
                $data['success_msg'] = $this->session->userdata('success_msg');
                $this->session->unset_userdata('success_msg');
            }
            if($this->session->userdata('error_msg'))
            {
                $data['error_msg'] = $this->session->userdata('error_msg');
                $this->session->unset_userdata('error_msg');
            }
            
            // Get rows
            $data['invoices'] = $this->Invoice_model->getRows();
            $data['invoices_pagination'] = $this->Invoice_model->invoices_pagination();
            $data['total'] = $this->Invoice_model->getTotalInvoices();


            $this->load->view('templates/sidebar');
            $this->load->view('invoices/xero', $data);
        } 
        else 
        {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('templates/footer');
    }

    public function exportXeroTemplate()
    { 
        if (!$this->session->userdata('user_session')) {
            redirect(site_url());
        }
        // file name 
        $filename = 'Xero_Scrubbed_Invoice_'.date('mdYHis').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; ");

        // file creation 
        $file = fopen('php://output', 'w');
      
        $header = array("Customer Name",
        "Invoice Number",
        "Invoice Date",
        "Line Item Name",
        "Line Item Amount"); 
        fputcsv($file, $header);
        fclose($file); 
        exit; 
    }

    public function add_revenue()
    {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('client', 'client', 'required');
            // $this->form_validation->set_rules('work_group', 'work_group', 'required');
            $this->form_validation->set_rules('line_item_name', 'line_item_name', 'required');
            $this->form_validation->set_rules('invoice_no', 'invoice_no', 'required');
            $this->form_validation->set_rules('line_item_amount', 'line_item_amount', 'required');
            $this->form_validation->set_rules('source', 'source', 'required');
            $this->form_validation->set_rules('transaction_date', 'transaction_date', 'required');
    
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $client = $this->Client_model->getClient($this->input->post('client'));
                $data = [
                    'client_id' => $this->input->post('client'),
                    'client_name' => $client->entity,
                    'group_id' => $this->input->post('work_group'),
                    'line_item_name' => $this->input->post('line_item_name'),
                    'invoice_number' => html_purify($this->input->post('invoice_no')),
                    'line_item_amount' => html_purify($this->input->post('line_item_amount')),
                    'source' => $this->input->post('source'),
                    "transaction_date" => $this->input->post('transaction_date'),
                    'transaction_type' => 'invoices',
                    'data_type' => 'manual',
                    'is_included' => $this->input->post('work_group') ? 1 : 0,
                ];
                $invoice = $this->Invoice_model->store($data);
                print json_encode(['status' => 'ok', 'invoice' => $invoice]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    } 

    public function add_ar()
    {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('invoice_no', 'Invoice Number', 'required');
            $this->form_validation->set_rules('invoice_date', 'Invoice Date', 'required');
            $this->form_validation->set_rules('customer_name', 'Customer Name', 'required');
            $this->form_validation->set_rules('amount_due', 'Amount Due', 'required');
            $this->form_validation->set_rules('groupid', 'Group Name', 'required');
            $this->form_validation->set_rules('item_name', 'Item Name', 'required');
    
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $filterDate =  $this->input->post('filterDate');
                $month = date('m', strtotime($filterDate));
                $year =  date('Y', strtotime($filterDate));

                $data = [
                    'customer_name' => $this->input->post('customer_name'),
                    'invoice_date' => date('m/d/Y', strtotime($this->input->post('invoice_date'))),
                    'invoice_no' => $this->input->post('invoice_no'),
                    'item_name' => $this->input->post('item_name'),
                    'groupid' => $this->input->post('groupid'),
                    'month' => $month,
                    'year' => $year,
                    'amount_due' => $this->input->post('amount_due'),
                ];
                $invoice = $this->Invoice_model->storeAr($data);
                print json_encode(['status' => 'ok', 'invoice' => $invoice]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    } 

    public function getAr() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $transaction = $this->Invoice_model->findAr($this->input->post('id'));
                print json_encode(["status" => "ok", 'data' => $transaction]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function updateAr() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('edit_ar_id', 'edit_ar_id', 'required');
            $this->form_validation->set_rules('edit_invoice_no', 'Invoice Number', 'required');
            $this->form_validation->set_rules('edit_invoice_date', 'Invoice Date', 'required');
            $this->form_validation->set_rules('edit_customer_name', 'Customer Name', 'required');
            $this->form_validation->set_rules('edit_amount_due', 'Amount Due', 'required');
            $this->form_validation->set_rules('edit_groupid', 'Group Name', 'required');
            $this->form_validation->set_rules('edit_item_name', 'Item Name', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $data = [
                    'ID' => $this->input->post('edit_ar_id'),
                    'customer_name' => $this->input->post('edit_customer_name'),
                    'invoice_date' => date('m/d/Y', strtotime($this->input->post('edit_invoice_date'))),
                    'invoice_no' => $this->input->post('edit_invoice_no'),
                    'item_name' => $this->input->post('edit_item_name'),
                    'groupid' => $this->input->post('edit_groupid'),
                    'amount_due' => $this->input->post('edit_amount_due'),
                ];

                $invoice = $this->Invoice_model->updateAr($data);
                print json_encode(['status' => 'ok', 'invoice' => $invoice]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function destroyAr() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $this->Invoice_model->deleteAr($this->input->post('id'));
                print json_encode(["status" => "ok"]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function getTransaction() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $transaction = $this->Invoice_model->find($this->input->post('id'));
                print json_encode(["status" => "ok", 'data' => $transaction]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function updateRevenue() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('edit_transaction_id', 'edit_transaction_id', 'required');
            $this->form_validation->set_rules('edit_client', 'edit_client', 'required');
            $this->form_validation->set_rules('edit_invoice_no', 'edit_invoice_no', 'required');
            // $this->form_validation->set_rules('edit_work_group', 'edit_work_group', 'required');
            $this->form_validation->set_rules('edit_line_item_amount', 'edit_line_item_amount', 'required');
            $this->form_validation->set_rules('edit_line_item_name', 'edit_line_item_name', 'required');
            $this->form_validation->set_rules('edit_source', 'edit_source', 'required');
            $this->form_validation->set_rules('edit_transaction_date', 'edit_transaction_date', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $client = $this->Client_model->getClient($this->input->post('edit_client'));
                $data = [
                    'id' => $this->input->post('edit_transaction_id'),
                    'client_id' => $this->input->post('edit_client'),
                    'client_name' => $client->entity,
                    'group_id' => $this->input->post('edit_work_group'),
                    'line_item_name' => $this->input->post('edit_line_item_name'),
                    'invoice_number' => html_purify($this->input->post('edit_invoice_no')),
                    'line_item_amount' => html_purify($this->input->post('edit_line_item_amount')),
                    'source' => $this->input->post('edit_source'),
                    "transaction_date" => $this->input->post('edit_transaction_date'),
                    'transaction_type' => 'invoices',
                    'data_type' => 'manual',
                    'is_included' => $this->input->post('edit_work_group') ? 1 : 0,
                ];

                $invoice = $this->Invoice_model->update($data);
                print json_encode(['status' => 'ok', 'invoice' => $invoice]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function destroyTransaction() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $this->Invoice_model->delete($this->input->post('id'));
                print json_encode(["status" => "ok"]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function work_groups() 
    {
        $data = array();
        $data['title'] = 'Work Groups';
        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) 
        {
            // Get messages from the session
            if($this->session->userdata('success_msg'))
            {
                $data['success_msg'] = $this->session->userdata('success_msg');
                $this->session->unset_userdata('success_msg');
            }
            if($this->session->userdata('error_msg'))
            {
                $data['error_msg'] = $this->session->userdata('error_msg');
                $this->session->unset_userdata('error_msg');
            }
           
            $this->load->view('templates/sidebar');
            $this->load->view('invoices/groups', $data);
        } 
        else 
        {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('templates/footer');
    }

    public function get_group_revenue_ar() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('month_year', 'month_year', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $revenues_ar = $this->Invoice_model->get_all_revenue_ar($this->input->post('month_year'));
                $data = [];
                $revenue_total = 0;
                $ar_total = 0;
                foreach($revenues_ar as $group) {
                    $revenue_total = $revenue_total + $group->revenue; 
                    $ar_total = $ar_total + $group->ar; 

                    $group->revenue = number_format($group->revenue, 2); 
                    $group->ar = number_format($group->ar, 2); 

                    $data[] = $group;
                }

                print json_encode(["status" => "ok" , "data" => $data, "revenue_total" => number_format($revenue_total, 2), "ar_total" => number_format($ar_total, 2)]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function get_group_revenue() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('group_id', 'group_id', 'required');
            $this->form_validation->set_rules('month_year', 'month_year', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $group_id = $this->input->post('group_id');
                $month_year = $this->input->post('month_year');

                $groups = $this->Invoice_model->get_group_revenue($group_id, $month_year);
                $data = [];
                $revenue_total = 0;
                foreach($groups as $group) {
                    $revenue_total = $revenue_total + $group->revenue; 
                    $group->revenue = number_format($group->revenue, 2); 
                    $data[] = $group;
                }

                print json_encode(["status" => "ok" , "data" => $data, "revenue_total" => number_format($revenue_total, 2)]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function get_group_ar() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('group_id', 'group_id', 'required');
            $this->form_validation->set_rules('month_year', 'month_year', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $group_id = $this->input->post('group_id');
                $month = date('m', strtotime($this->input->post('month_year')));
                $year = date('Y', strtotime($this->input->post('month_year')));

                $groups = $this->Invoice_model->get_group_ar($group_id, $month, $year);
                $data = [];
                $ar_total = 0;
                foreach($groups as $group) {
                    $ar_total = $ar_total + $group->ar; 
                    $group->ar = number_format($group->ar, 2); 
                    $data[] = $group;
                }

                print json_encode(["status" => "ok" , "data" => $data, "ar_total" => number_format($ar_total, 2)]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function AccountsReceivable() 
    {
        $data = array();
        $data['title'] = 'Invoices';
        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) 
        {
            // Get messages from the session
            if($this->session->userdata('success_msg'))
            {
                $data['success_msg'] = $this->session->userdata('success_msg');
                $this->session->unset_userdata('success_msg');
            }
            if($this->session->userdata('error_msg'))
            {
                $data['error_msg'] = $this->session->userdata('error_msg');
                $this->session->unset_userdata('error_msg');
            }
            
            $data['filterDate'] =  !empty($_GET['filterDate']) ? $_GET['filterDate'] : date('Y-m', strtotime("-1 month"));

            $data['ars'] = $this->Invoice_model->getArRows($data['filterDate']);
            $data['arsPagination'] = $this->Invoice_model->arPagination($data['filterDate']);
            $data['clients'] = $this->Client_model->getActiveClients();
            $data['work_groups'] = $this->Models->getWorkgroups();
            $data['line_items'] = $this->Models->getLineItems();
            $data['sources'] = [
                ['bill','bill.com'],
                ['xero','xero'],
            ];
            
            $this->load->view('templates/sidebar');
            $this->load->view('invoices/ar', $data);
        } 
        else 
        {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('templates/footer');
    }

    
}

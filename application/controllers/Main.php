<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->model('Models');
        $this->load->database();
    }

    public function index() {
        $data['title'] = 'CRM';

        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) {
            $this->load->view('templates/sidebar');
            $this->load->view('CRM_Content');
        } else {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('templates/footer');
    }

    public function leads() {
        $this->load->view('CRM_Header');
        if (!$this->session->userdata('user_session')) {
            $this->load->view('CRM_Navigation');
            $this->load->view('CRM_Sidebar');
            $this->load->view('CRM_Client_Form');
        } else {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('CRM_Footer'); 
    }

}

<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
class View_Tax extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->model('Models');
        $this->load->database();
    }

    public function index() {
        $this->load->view('CRM_View_Tax'); 
    }

}

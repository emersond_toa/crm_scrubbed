<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
class Transaction extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->model('Models');
        $this->load->model('Transaction_model');
        $this->load->database();
    }

    function client_transactions() {
        $this->form_validation->set_rules('client_id', 'client_id', 'required');
        if ($this->form_validation->run()) {
            $transactions = $this->Transaction_model->get_client_transactions($this->input->post('client_id'));
            print json_encode($transactions);
        }
    }
}

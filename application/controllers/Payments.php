<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");

class Payments extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->model('Models');
        $this->load->model('Payment_model');
        $this->load->model('Company_model');
        $this->load->database();
        // / Load file helper
        $this->load->helper('file');
        $this->load->library('csvimport');
    }

    public function index() {
        $data = array();
        $data['title'] = 'Payments';
        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) {
            // Get messages from the session
            if ($this->session->userdata('success_msg')) {
                $data['success_msg'] = $this->session->userdata('success_msg');
                $this->session->unset_userdata('success_msg');
            }
            if ($this->session->userdata('error_msg')) {
                $data['error_msg'] = $this->session->userdata('error_msg');
                $this->session->unset_userdata('error_msg');
            }

            // Get rows
            $data['payments'] = $this->Payment_model->getRows();
            $data['payments_pagination'] = $this->Payment_model->payments_pagination();
            $data['total'] = $this->Payment_model->getTotalPayments();

            $this->load->view('templates/sidebar');
            $this->load->view('payments/index', $data);
        } else {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('templates/footer');
    }

    public function AR_process() {

        $this->db->query("DELETE FROM crm_database.crm_temporary_ar");
        $result = $this->db->query("
            
                SELECT *,SUM(transaction.line_item_amount) AS line_item_amount_total FROM

                (SELECT *,FROM_UNIXTIME(UNIX_TIMESTAMP(transaction_date),'%Y') AS get_year,FROM_UNIXTIME(UNIX_TIMESTAMP(transaction_date),'%m') AS get_month

                FROM `crm_database`.`crm_transactions`) AS transaction

                WHERE is_included = '1' and transaction_type = 'invoices' and get_year = '2021' GROUP BY invoice_number ORDER BY ID DESC

            ");

        foreach ($result->result() as $value) {

            $payments = $this->db->query("
           
                    SELECT *,SUM(transaction.line_item_amount) AS line_item_amount_total  FROM

                    (SELECT *,FROM_UNIXTIME(UNIX_TIMESTAMP(transaction_date),'%Y') AS get_year,FROM_UNIXTIME(UNIX_TIMESTAMP(transaction_date),'%m') AS get_month

                    FROM `crm_database`.`crm_transactions`) AS transaction

                    WHERE is_included = '1' and transaction_type = 'payments' and get_year = '2021' and invoice_number = '{$value->invoice_number}'  ORDER BY ID DESC

                ");

            if ((isset($payments->row()->line_item_amount_total) ? $payments->row()->line_item_amount_total : false) === false) {
                if ($value->line_item_amount_total > 0) {
                    $data = [
                        "transaction_date" => $value->transaction_date,
                        "line_item_amount" => $value->line_item_amount_total,
                        "group_id" => $value->group_id,
                        "client_id" => $value->client_id,
                        "remaining_balance" => $value->line_item_amount_total,
                        "invoice_number" => $value->invoice_number
                    ];
                    $this->db->insert("crm_database.crm_temporary_ar", $data);
                }
            } else {
                $remaining = $value->line_item_amount_total - $payments->row()->line_item_amount_total;
                if ($remaining > 0) {
                    $data = [
                        "transaction_date" => $value->transaction_date,
                        "line_item_amount" => $value->line_item_amount_total,
                        "group_id" => $value->group_id,
                        "client_id" => $value->client_id,
                        "remaining_balance" => $remaining,
                        "invoice_number" => $value->invoice_number
                    ];
                    $this->db->insert("crm_database.crm_temporary_ar", $data);
                }
            }
        }
    }

    public function import(){
        $file_data = $this->csvimport->get_array($_FILES["file"]["tmp_name"]);
        $data = [];
        $this->db->delete('crm_transactions', array('transaction_type' => 'payments'));
        foreach($file_data as $row)
        {
            $client_id = $row['Customer ID'] == '' ? null : $row['Customer ID'];
            if($client_id == 504) {
                $client_id = 807;
            } 
            if($client_id == 528) {
                // set 528 Transaction Support id to 927 Moss Adams id 
                $client_id = 927;
            }  
            if($row['Customer Name'] == 'OSC2') {
                // one step closer
                $client_id = 809;
            }
            if($client_id == 261) {
                // Three Habitat Consulting	
                $client_id = 268;
            } 
            if($client_id == 50) {
                // Next Frontier Holding, Inc. and Related Entities	
                $client_id = 881;
            } 
            if(!$client_id) {
                $company = $this->Company_model->find($row['Customer Name']);
                if($company) {
                    $client_id = $company->ID; 
                }
            }

            $data[] = array(
                'client_id' => $client_id,
                'client_name' => $row['Customer Name'],
                'invoice_number' => $row['Invoice #'],
                'line_item_amount' => $row['Line Item Amount'],
                'total_line_items' => $row['Total Line Items'],
                'transaction_type' => 'payments',
                'source' => 'bill',
                'transaction_date' => date('Y-m-d h:i:s', strtotime($row['Transaction Date'])),
                'created_at' => date('Y-m-d h:i:s', strtotime($row['Created'])),
                'updated_at' => date('Y-m-d h:i:s', strtotime($row['Updated'])),
            );
        }
        $this->Payment_model->insert($data);
        shell_exec('sudo curl --silent "' . site_url('Payments/AR_process/AR_process_123123') . '" > /dev/null &');
        redirect('payments');
    }

    /*
     * Callback function to check file value and type during validation
     */

    public function file_check($str) {
        $allowed_mime_types = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
            $mime = get_mime_by_extension($_FILES['file']['name']);
            $fileAr = explode('.', $_FILES['file']['name']);
            $ext = end($fileAr);
            if (($ext == 'csv') && in_array($mime, $allowed_mime_types)) {
                return true;
            } else {
                $this->form_validation->set_message('file_check', 'Please select only CSV file to upload.');
                return false;
            }
        } else {
            $this->form_validation->set_message('file_check', 'Please select a CSV file to upload.');
            return false;
        }
    }

    public function upload() 
    {
        $data = array();
        $data['title'] = 'Payments';
        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) 
        {
            // Get messages from the session
            if($this->session->userdata('success_msg'))
            {
                $data['success_msg'] = $this->session->userdata('success_msg');
                $this->session->unset_userdata('success_msg');
            }
            if($this->session->userdata('error_msg'))
            {
                $data['error_msg'] = $this->session->userdata('error_msg');
                $this->session->unset_userdata('error_msg');
            }
            
            // Get rows
            $this->load->view('templates/sidebar');
            $this->load->view('payments/upload', $data);
        } 
        else 
        {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('templates/footer');
    }
}

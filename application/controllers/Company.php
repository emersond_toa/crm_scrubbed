<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: application/json');

class Company extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Company_model');
    }
 
    public function show() {
        if ($this->session->userdata('user_session')) {
            $company = $this->Company_model->show($this->input->post('id'));
            print json_encode(["status" => "ok", 'data' => $company]);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function update_funnel() 
    {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('editcompanyid', 'editcompanyid', 'required');
            $this->form_validation->set_rules('editname', 'editname', 'required');
            // $this->form_validation->set_rules('editwebsite', 'editwebsite', 'required');
            // $this->form_validation->set_rules('editphone', 'editphone', 'required');
            // $this->form_validation->set_rules('editaddress', 'editaddress', 'required');
            // $this->form_validation->set_rules('editindustry', 'editindustry', 'required');
            // $this->form_validation->set_rules('editstatus', 'editstatus', 'required');
    
            if ($this->form_validation->run() == FALSE) 
            {
                print json_encode(["status" => "error"]);
            }
            else
            {
                $data = [
                    'entity' => html_purify($this->input->post('editname')),
                    'website' => html_purify($this->input->post('editwebsite')),
                    'phone' => html_purify($this->input->post('editphone')),
                    'industry_id' => html_purify($this->input->post('editindustry')),
                    'full_address' => html_purify($this->input->post('editaddress')),
                    'status_id' => html_purify($this->input->post('editstatus')),
                    'clientstatus_id' => html_purify(($this->input->post('editstatus') == 6) ? 1 : null),
                    'referral_partner_id' => html_purify($this->input->post('editreferralpartner')),
                    'lead_source_id' => html_purify($this->input->post('editleadsource')),
                    'updated_at' => date('Y-m-d h:i:s A'),
                ];
    
                $tags = $this->input->post('editassignedto');
                $workgroup_tags = $this->input->post('editworkgroups');
                $this->Company_model->update($this->input->post('editcompanyid'), $data, $tags, html_purify($this->input->post('editname')), $workgroup_tags);
                $this->Company_model->last_update($this->input->post('editcompanyid'));
                $return = [
                    'status' =>'ok',
                ];
    
                print json_encode($return);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function update_client() 
    {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('editcompanyid', 'editcompanyid', 'required');
            $this->form_validation->set_rules('editname', 'editname', 'required');
            // $this->form_validation->set_rules('editwebsite', 'editwebsite', 'required');
            // $this->form_validation->set_rules('editphone', 'editphone', 'required');
            // $this->form_validation->set_rules('editaddress', 'editaddress', 'required');
            // $this->form_validation->set_rules('editindustry', 'editindustry', 'required');
            // $this->form_validation->set_rules('editstatus', 'editstatus', 'required');
    
            if ($this->form_validation->run() == FALSE) 
            {
                print json_encode(["status" => "error"]);
            }
            else
            {
                $data = [
                    'entity' => html_purify($this->input->post('editname')),
                    'website' => html_purify($this->input->post('editwebsite')),
                    'phone' => html_purify($this->input->post('editphone')),
                    'industry_id' => html_purify($this->input->post('editindustry')),
                    'full_address' => html_purify($this->input->post('editaddress')),
                    'status_id' => html_purify($this->input->post('editstatus')),
                    'clientstatus_id' => html_purify($this->input->post('editclientstatus')),
                    'referral_partner_id' => html_purify($this->input->post('editreferralpartner')),
                    'lead_source_id' => html_purify($this->input->post('editleadsource')),
                    'start_date' => html_purify($this->input->post('editstartdate') ? $this->input->post('editstartdate') : null),
                    'termination_date' => html_purify($this->input->post('editterminationdate') ? $this->input->post('editterminationdate') : null),
                    'termination_reason_id' => html_purify($this->input->post('editterminationreason')),
                    'updated_at' => date('Y-m-d h:i:s A'),
                ];
    
                $tags = $this->input->post('editassignedto');
                $workgroup_tags = $this->input->post('editworkgroups');
                $this->Company_model->update($this->input->post('editcompanyid'), $data, $tags, html_purify($this->input->post('editname')), $workgroup_tags);
    
                $this->Company_model->last_update($this->input->post('editcompanyid'));
                $return = [
                    'status' =>'ok',
                ];
    
                print json_encode($return);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
        
    }

    public function contacts()
    {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $contacts = $this->Company_model->contacts($this->input->post('id'));
                print json_encode($contacts->result());
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function destroy() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $this->Company_model->delete($this->input->post('id'));
                print json_encode(["status" => "ok"]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }

    }
}

<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: application/json');

class Reminder extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Reminder_model');
        $this->load->model('Company_model');
        $this->load->model('Models');
    }

    public function store() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('companyId', 'companyId', 'required');
            $this->form_validation->set_rules('start_date', 'start_date', 'required');
            // $this->form_validation->set_rules('notify', 'notify', 'required');
            $this->form_validation->set_rules('what', 'what', 'required');
            $this->form_validation->set_rules('where', 'where', 'required');
            $this->form_validation->set_rules('description', 'description', 'required');
    
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $data = [
                    'company_id' => $this->input->post('companyId'),
                    'start_date' => $this->input->post('start_date'),
                    'reminder_what' => html_purify($this->input->post('what')),
                    'reminder_where' => html_purify($this->input->post('where')),
                    'description' => html_purify($this->input->post('description')),
                ];
                $tags = $this->input->post('notify');
                $this->Reminder_model->store($data, $tags);
                $this->Company_model->last_update($this->input->post('companyId'));
                print json_encode(['status' => 'ok']);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function client_reminders() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $reminders = $this->Reminder_model->client_reminders($this->input->post('id'));
                print json_encode($reminders);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function show() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $reminder = $this->Reminder_model->show($this->input->post('id'));
                print json_encode(["status" => "ok", 'data' => $reminder]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function update() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('editreminderid', 'editreminderid', 'required');
            $this->form_validation->set_rules('editstart_date', 'editstart_date', 'required');
            // $this->form_validation->set_rules('editnotify', 'editnotify', 'required');
            $this->form_validation->set_rules('editwhat', 'editwhat', 'required');
            $this->form_validation->set_rules('editwhere', 'editwhere', 'required');
            $this->form_validation->set_rules('editdescription', 'editdescription', 'required');
    
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $data = [
                    'start_date' => $this->input->post('editstart_date'),
                    'reminder_what' => html_purify($this->input->post('editwhat')),
                    'reminder_where' => html_purify($this->input->post('editwhere')),
                    'description' => html_purify($this->input->post('editdescription')),
                ];
    
                $tags = $this->input->post('editnotify');
                $this->Reminder_model->update($this->input->post('editreminderid'), $data, $tags);
    
                print json_encode(['status' => 'ok']);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
        
    }

    public function destroy() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == FALSE) {
                print json_encode(["status" => "error"]);
            } else {
                $this->Reminder_model->delete($this->input->post('id'));
                print json_encode(["status" => "ok"]);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    function reminder_email($date = '', $where = '', $what = '', $description = '', $address = '') {
        if ($this->session->userdata('user_session')) {
            $_message = "";
            $_message .= "<html>";
            $_message .= "<head>";
            $_message .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>";
            $_message .= "<title>CRM Scrubbed.net " . date('Y') . "</title>";
            $_message .= "</head>";
            $_message .= '<div style="text-align:center;"><img src="' . site_url('images/final-logo.png') . '" alt="logo" align="center"></div>';
            $_message .= "<body bgcolor='#efefef' style='font-family: sans-serif;font-size:14px;color: #8c8c8c;'>";
            $_message .= '<div bgcolor="#FFFFFF" style="padding: 15px;background-color: #FFFFFF; max-width:900px; margin: 0 auto 0 auto; text-align:center;">';
            $_message .= '<table align="center" style="background-color:#FFF;width:100%;max-width:900px; border:1px solid #DDD;border-spacing: 0px;border-collapse: collapse; font-family:sans-serif;color: #8c8c8c;font-size: 14px;text-align:left;">';
            $_message .= '<tbody>' . "\n";
            $_message .= '<tr>' . "\n";
            $_message .= '<td style="padding:5px;border: 1px solid #DDD;background-color:#FFFFFF;">';
            $_message .= 'Date: ' . $date . '<br>' . "\n";
            $_message .= 'Where: ' . $where . '<br>' . "\n";
            $_message .= 'What: ' . $what . '<br>' . "\n";
            $_message .= '</td>' . "\n";
            $_message .= '</tr>' . "\n";
            $_message .= '<tr>' . "\n";
            $_message .= '<td  style="padding: 15px;border-top: 1px solid #DDD;background-color: #FFFFFF;">';
            $_message .= $description;
            $_message .= '</td>';
            $_message .= '</tr>';
            $_message .= '</tbody>';
            $_message .= '</table>';
            $_message .= '</div>';
            $_message .= '<div style="text-align:center"><div style="text-align:center;padding:15px;font-size:12px;color#c3c3c3;margin:15px;"><center>CRM Scrubbed.net ' . date('Y') . '</center></div></div>';
            $_message .= "</body>";
            $_message .= "</html>";

            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'scrubbed.ticketing@gmail.com',
                'smtp_pass' => 'hello123!!!',
                'mailtype' => 'html',
                'charset' => 'iso-8859-1'
            );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from('scrubbed.ticketing@gmail.com', 'CRM - no reply');
            $this->email->to($address);
            $this->email->subject('CRM Reminder');
            $this->email->message($_message);
            $this->email->send();
            return true;
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function reminder_cron() {
        if ($this->session->userdata('user_session')) {
            $reminder = $this->Reminder_model->reminder_crontab();
            foreach ($reminder->result() as $value) {
                if ((strtotime($value->start_date)-600) < (strtotime('now'))) {
                    $get_user = $this->Reminder_model->crm_reminder_users_crontab($value->id);
                    $email = [];
                    foreach ($get_user->result() as $user_value) {
                        $email[] = $this->Models->get_user_info($user_value->user_id)->email;
                    }
                    $this->reminder_email($value->start_date, $value->reminder_where, $value->reminder_what, $value->description, implode(",", $email));
                    $this->Reminder_model->update_crm_reminders($value->id, array("done" => 1));
                }
               file_put_contents('/var/www/html/crm/images/time.txt', (strtotime($value->start_date)+600) ."<". (strtotime('now'))."\n", FILE_APPEND);
            }
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }
}

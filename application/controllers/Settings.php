<?php

date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
class Settings extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->model('Models');
        $this->load->model('ReferralPartner_model');
        $this->load->model('LeadSource_model');
        $this->load->model('TerminationReason_model');
        $this->load->database();
    }

    public function index() 
    {
        $data['title'] = 'Settings';
        $data['referrals'] = $this->ReferralPartner_model->all();
        $data['sources'] = $this->LeadSource_model->all();
        $data['reasons'] = $this->TerminationReason_model->all();

        $this->load->view('templates/header', $data);
        if ($this->session->userdata('user_session')) 
        {
            $this->load->view('templates/sidebar');
            $this->load->view('CRM_Settings', $data);
        } else {
            redirect('https://employeeportal.scrubbed.net/');
        }
        $this->load->view('templates/footer');
    }

    public function get_referrals()
    {
        if ($this->session->userdata('user_session')) 
        {
            $result = $this->ReferralPartner_model->all();
            print json_encode($result);
        } else {
            print json_encode(["status" => "not authorized"]);
        } 
    }

    public function create_referral() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('name', 'name', 'required');
            if ($this->form_validation->run()) {
                $data = array(
                    "name" => html_purify($this->input->post('name')),
                );
                $referral = $this->ReferralPartner_model->create($data);
                $value = array("message" => "success", "text" => "Successfully added", "data" => $referral);
            } else {
                $value = array("message" => "error", "text" => "error");
            }
            print json_encode($value);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function delete_referral() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run()) {
                $id = $this->input->post('id');
                $this->ReferralPartner_model->delete($id);
                $value = array("message" => "success", "text" => "Successfully deleted");
            } else {
                $value = array("message" => "error", "text" => "error");
            }
            print json_encode($value);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function update_referral() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            $this->form_validation->set_rules('name', 'name', 'required');
            if ($this->form_validation->run()) {
                $data = array(
                    "id" => $this->input->post('id'),
                    "name" => html_purify($this->input->post('name')),
                );
                $this->ReferralPartner_model->update($data);
                $value = array("message" => "success", "text" => "Successfully updated");
            } else {
                $value = array("message" => "error", "text" => "error");
            }
            print json_encode($value);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function get_sources()
    {
        if ($this->session->userdata('user_session')) 
        {
            $result = $this->LeadSource_model->all();
            print json_encode($result);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function create_source() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('name', 'name', 'required');
            if ($this->form_validation->run()) {
                $data = array(
                    "name" => html_purify($this->input->post('name')),
                );
                $source = $this->LeadSource_model->create($data);
                $value = array("message" => "success", "text" => "Successfully added", "data" => $source);
            } else {
                $value = array("message" => "error", "text" => "error");
            }
            print json_encode($value);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function delete_source() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run()) {
                $id = $this->input->post('id');
                $this->LeadSource_model->delete($id);
                $value = array("message" => "success", "text" => "Successfully deleted");
            } else {
                $value = array("message" => "error", "text" => "error");
            }
            print json_encode($value);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function update_source() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            $this->form_validation->set_rules('name', 'name', 'required');
            if ($this->form_validation->run()) {
                $data = array(
                    "id" => $this->input->post('id'),
                    "name" => html_purify($this->input->post('name')),
                );
                $this->LeadSource_model->update($data);
                $value = array("message" => "success", "text" => "Successfully updated");
            } else {
                $value = array("message" => "error", "text" => "error");
            }
            print json_encode($value);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function get_terminationreasons()
    {
        if ($this->session->userdata('user_session')) 
        {
            $result = $this->TerminationReason_model->all();
            print json_encode($result);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function create_terminationreason() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('name', 'name', 'required');
            if ($this->form_validation->run()) {
                $data = array(
                    "name" => html_purify($this->input->post('name')),
                );
                $terminationreason = $this->TerminationReason_model->create($data);
                $value = array("message" => "success", "text" => "Successfully added", "data" => $terminationreason);
            } else {
                $value = array("message" => "error", "text" => "error");
            }
            print json_encode($value);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function delete_terminationreason() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run()) {
                $id = $this->input->post('id');
                $this->TerminationReason_model->delete($id);
                $value = array("message" => "success", "text" => "Successfully deleted");
            } else {
                $value = array("message" => "error", "text" => "error");
            }
            print json_encode($value);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }

    public function update_terminationreason() {
        if ($this->session->userdata('user_session')) {
            $this->form_validation->set_rules('id', 'id', 'required');
            $this->form_validation->set_rules('name', 'name', 'required');
            if ($this->form_validation->run()) {
                $data = array(
                    "id" => $this->input->post('id'),
                    "name" => html_purify($this->input->post('name')),
                );
                $this->TerminationReason_model->update($data);
                $value = array("message" => "success", "text" => "Successfully updated");
            } else {
                $value = array("message" => "error", "text" => "error");
            }
            print json_encode($value);
        } else {
            print json_encode(["status" => "not authorized"]);
        }
    }
}

<?php
class Service_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

    public function all_services()
    {
        $query = $this->db->get('crm_services');
        return $query->result();
    }
}
<?php
class User_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

    public function all_users()
    {
        $query = $this->db->query("SELECT * FROM intra.employee_info WHERE crm_access = '2' ");
        return $query->result();
    }

    public function auth_user($email)
    {
        $this->db->where('email', $email);
        $query = $this->db->get('intra.employee_info');
        return $query->row();
    }
}

<?php
class Payment_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
        // Set table name
        $this->table = 'crm_transactions';
	}

    function quotes($string) {
        $chars = array("'", '"', '\\');
        return str_replace($chars, '', $string);
    }

    function max_row() {
        if (!($this->uri->segment(4))) {
            return '100';
        } else if (is_numeric($this->uri->segment(4))) {
            return ltrim(rtrim($this->uri->segment(4)));
        } else {
            return '100';
        }
    }

    function page() {
        if (!$this->input->get('page')) {
            return '1';
        } else if (is_numeric($this->input->get('page'))) {
            return ltrim(rtrim($this->input->get('page')));
        } else {
            return '1';
        }
    }

    function keyword() {
        if (!($this->input->get('keyword'))) {
            return '';
        } else {
            return $this->quotes($this->input->get('keyword'));
        }
    }

    function getRows(){

        $setLimit = $this->max_row();
        $srh = explode(" ", $this->keyword());
        $pageLimit = ($this->page() * $setLimit) - ($setLimit);

        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "entity LIKE '%" . $field . "%'";
                $conditions[] = "group_name LIKE '%" . $field . "%'";
                $conditions[] = "invoice_number LIKE '%" . $field . "%'";
                $conditions[] = "transaction_date LIKE '%" . $field . "%'";
                $conditions[] = "line_item_amount LIKE '%" . $field . "%'";
                $conditions[] = "total_line_items LIKE '%" . $field . "%'";
                $conditions[] = "source LIKE '%" . $field . "%'";
            }
        }
        $_query = "SELECT crm_transactions.*, crm_companies.entity, intra.user_group.group_name FROM crm_transactions
                    LEFT JOIN crm_companies ON crm_companies.ID = crm_transactions.client_id
                    LEFT JOIN intra.user_group ON intra.user_group.GID = crm_transactions.group_id
                    WHERE crm_transactions.transaction_type = 'payments'
                    AND crm_transactions.is_included = 1
                    ";
        if (count($conditions) > 0) {
            $_query .= "AND (" . implode(' || ', $conditions) . ") ";
        }
        $clients = $this->db->query($_query . " ORDER BY transaction_date DESC LIMIT $pageLimit , $setLimit");
        return $clients->result();
    }

    function payments_pagination() {
        $page_url = base_url($this->uri->segment(1) . '/' . $this->uri->segment(2) . '');
        $per_page = $this->max_row();
        $page = $this->page();
        $srh = explode(" ", $this->keyword());
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "entity LIKE '%" . $field . "%'";
                $conditions[] = "group_name LIKE '%" . $field . "%'";
                $conditions[] = "invoice_number LIKE '%" . $field . "%'";
                $conditions[] = "transaction_date LIKE '%" . $field . "%'";
                $conditions[] = "line_item_amount LIKE '%" . $field . "%'";
                $conditions[] = "total_line_items LIKE '%" . $field . "%'";
                $conditions[] = "source LIKE '%" . $field . "%'";
            }
        }

        $_query = "SELECT crm_transactions.*, crm_companies.entity, intra.user_group.group_name FROM crm_transactions
                    LEFT JOIN crm_companies ON crm_companies.ID = crm_transactions.client_id
                    LEFT JOIN intra.user_group ON intra.user_group.GID = crm_transactions.group_id
                    WHERE crm_transactions.transaction_type = 'payments'
                    AND crm_transactions.is_included = 1
                    ";
        if (count($conditions) > 0) {
            $_query .= "AND (" . implode(' || ', $conditions) . ") ";
        }

        $query = $this->db->query($_query . " ORDER BY transaction_date DESC");

        $total = $query->num_rows();
        $adjacents = "2";
        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;
        $prev = $page - 1;
        $next = $page + 1;
        $setLastpage = ceil($total / $per_page);
        $lpm1 = $setLastpage - 1;
        $setPaginate = "";
        if ($setLastpage > 1) {
            $setPaginate .= "<ul class='pagination pagination-sm float-right'>";
            $setPaginate .= "<li class='disabled'><a>Page $page of $setLastpage</a></li>";
            if ($setLastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $setLastpage; $counter++) {
                    if ($counter == $page)
                        $setPaginate .= "<li class='active'><a>$counter</a></li>";
                    else
                        $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                }
            }
            elseif ($setLastpage > 5 + ($adjacents * 2)) {
                if ($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active btn-default'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                    $setPaginate .= "<li class='dot disabled'><a class='disabled'>...</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "'>$lpm1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>$setLastpage</a></li>";
                }
                elseif ($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                    $setPaginate .= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "'>1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "'>2</a></li>";
                    $setPaginate .= "<li class='dot disabled'><a>...</a></li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                    $setPaginate .= "<li class='dot disabled'><a>..</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "'>$lpm1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>$setLastpage</a></li>";
                }
                else {
                    $setPaginate .= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "'>1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "'>2</a></li>";
                    $setPaginate .= "<li class='dot disabled'><a>..</a></li>";
                    for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                }
            }
            if ($page < $counter - 1) {
                $setPaginate .= "<li><a href='{$page_url}?page=" . $next . "&keyword=" . $this->keyword() . "'>Next</a></li>";
                $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>Last</a></li>";
            } else {
                $setPaginate .= "<li class='disabled'><a class='current_page'>Next</a></li>";
                $setPaginate .= "<li class='disabled'><a class='current_page'>Last</a></li>";
            }
            $setPaginate .= "</ul>\n";
        }
        return $setPaginate;
    }

    public function insert($data) {
        return $this->db->insert_batch($this->table, $data); 
        // $this->db->insert($this->table, $data);
        // return $this->db->insert_id();
    }

    public function show($invoice) {
        $this->db->select('crm_transactions.*, crm_companies.entity, intra.user_group.group_name');
        $this->db->from($this->table);
        $this->db->join('crm_companies', 'crm_companies.ID = crm_transactions.client_id', 'left');
        $this->db->join('intra.user_group', 'intra.user_group.GID = crm_transactions.group_id', 'left');
        $this->db->where('invoice_number', $invoice);
        $this->db->where('transaction_type', 'payments');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function total($invoice) {
        $this->db->select_sum('line_item_amount');
        $this->db->from($this->table);
        $this->db->where('invoice_number', $invoice);
        $this->db->where('transaction_type', 'payments');
        $query = $this->db->get();
        return $query->row();
    }

    public function getTotalPayments()
    {
        $query = $this->db->query("SELECT * FROM crm_transactions WHERE is_included = 1 AND transaction_type = 'payments'");

        return $query->num_rows();
    }

    public function getMissingTotalPayments()
    {
        $query = $this->db->query("SELECT * FROM crm_transactions WHERE is_included = 0 AND transaction_type = 'payments'");

        return $query->num_rows();
    }
}
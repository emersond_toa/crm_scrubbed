<?php
class Status_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

    public function all_status()
    {
        $query = $this->db->get('crm_statuses');
        return $query->result();
    }
}

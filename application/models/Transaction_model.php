<?php
class Transaction_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
        $this->table = 'crm_transactions';
	}

    public function get_client_transactions($client_id)
    {
        $this->db->select('crm_transactions.*, crm_companies.entity, intra.user_group.group_name');
        $this->db->from($this->table);
        $this->db->join('crm_companies', 'crm_companies.ID = crm_transactions.client_id', 'left');
        $this->db->join('intra.user_group', 'intra.user_group.GID = crm_transactions.group_id', 'left');
        $this->db->where('client_id', $client_id);
        $query = $this->db->get();

        return $query->result();
    }
}
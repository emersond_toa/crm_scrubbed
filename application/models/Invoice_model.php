<?php
class Invoice_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
        // Set table name
        $this->table = 'crm_transactions';
	}

    function quotes($string) {
        $chars = array("'", '"', '\\');
        return str_replace($chars, '', $string);
    }

    function max_row() {
        if (!($this->uri->segment(4))) {
            return '100';
        } else if (is_numeric($this->uri->segment(4))) {
            return ltrim(rtrim($this->uri->segment(4)));
        } else {
            return '100';
        }
    }

    function page() {
        if (!$this->input->get('page')) {
            return '1';
        } else if (is_numeric($this->input->get('page'))) {
            return ltrim(rtrim($this->input->get('page')));
        } else {
            return '1';
        }
    }

    function keyword() {
        if (!($this->input->get('keyword'))) {
            return '';
        } else {
            return $this->quotes($this->input->get('keyword'));
        }
    }

    function getRows(){

        $setLimit = $this->max_row();
        $srh = explode(" ", $this->keyword());
        $pageLimit = ($this->page() * $setLimit) - ($setLimit);

        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                // $conditions[] = "ID LIKE '%" . $field . "%'";
                $conditions[] = "invoice_number LIKE '%" . $field . "%'";
                $conditions[] = "line_item_amount LIKE '%" . $field . "%'";
                $conditions[] = "line_item_name LIKE '%" . $field . "%'";
                $conditions[] = "entity LIKE '%" . $field . "%'";
                $conditions[] = "group_name LIKE '%" . $field . "%'";
                $conditions[] = "source LIKE '%" . $field . "%'";
            }
        }
        $_query = "SELECT crm_transactions.*, crm_companies.entity, intra.user_group.group_name FROM crm_transactions
                    LEFT JOIN crm_companies ON crm_companies.ID = crm_transactions.client_id
                    LEFT JOIN intra.user_group ON intra.user_group.GID = crm_transactions.group_id
                    WHERE crm_transactions.transaction_type = 'invoices'
                    AND crm_transactions.is_included = 1
                    ";
        if (count($conditions) > 0) {
            $_query .= "AND (" . implode(' || ', $conditions) . ") ";
        }
        $clients = $this->db->query($_query . " ORDER BY transaction_date DESC LIMIT $pageLimit , $setLimit");
        return $clients->result();
    }

    function invoices_pagination() {
        $page_url = base_url($this->uri->segment(1) . '/' . $this->uri->segment(2) . '');
        $per_page = $this->max_row();
        $page = $this->page();
        $srh = explode(" ", $this->keyword());
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                // $conditions[] = "ID LIKE '%" . $field . "%'";
                $conditions[] = "invoice_number LIKE '%" . $field . "%'";
                $conditions[] = "line_item_amount LIKE '%" . $field . "%'";
                $conditions[] = "line_item_name LIKE '%" . $field . "%'";
                $conditions[] = "entity LIKE '%" . $field . "%'";
                $conditions[] = "group_name LIKE '%" . $field . "%'";
                $conditions[] = "source LIKE '%" . $field . "%'";
            }
        }

        $_query = "SELECT crm_transactions.*, crm_companies.entity, intra.user_group.group_name FROM crm_transactions
                    LEFT JOIN crm_companies ON crm_companies.ID = crm_transactions.client_id
                    LEFT JOIN intra.user_group ON intra.user_group.GID = crm_transactions.group_id
                    WHERE crm_transactions.transaction_type = 'invoices'
                    AND crm_transactions.is_included = 1
                    ";
        if (count($conditions) > 0) {
            $_query .= "AND (" . implode(' || ', $conditions) . ") ";
        }

        $query = $this->db->query($_query . " ORDER BY transaction_date DESC");

        $total = $query->num_rows();
        $adjacents = "2";
        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;
        $prev = $page - 1;
        $next = $page + 1;
        $setLastpage = ceil($total / $per_page);
        $lpm1 = $setLastpage - 1;
        $setPaginate = "";
        if ($setLastpage > 1) {
            $setPaginate .= "<ul class='pagination pagination-sm float-right'>";
            $setPaginate .= "<li class='disabled'><a>Page $page of $setLastpage</a></li>";
            if ($setLastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $setLastpage; $counter++) {
                    if ($counter == $page)
                        $setPaginate .= "<li class='active'><a>$counter</a></li>";
                    else
                        $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                }
            }
            elseif ($setLastpage > 5 + ($adjacents * 2)) {
                if ($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active btn-default'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                    $setPaginate .= "<li class='dot disabled'><a class='disabled'>...</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "'>$lpm1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>$setLastpage</a></li>";
                }
                elseif ($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                    $setPaginate .= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "'>1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "'>2</a></li>";
                    $setPaginate .= "<li class='dot disabled'><a>...</a></li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                    $setPaginate .= "<li class='dot disabled'><a>..</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "'>$lpm1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>$setLastpage</a></li>";
                }
                else {
                    $setPaginate .= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "'>1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "'>2</a></li>";
                    $setPaginate .= "<li class='dot disabled'><a>..</a></li>";
                    for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                }
            }
            if ($page < $counter - 1) {
                $setPaginate .= "<li><a href='{$page_url}?page=" . $next . "&keyword=" . $this->keyword() . "'>Next</a></li>";
                $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>Last</a></li>";
            } else {
                $setPaginate .= "<li class='disabled'><a class='current_page'>Next</a></li>";
                $setPaginate .= "<li class='disabled'><a class='current_page'>Last</a></li>";
            }
            $setPaginate .= "</ul>\n";
        }
        return $setPaginate;
    }

    function getRowsMissing(){

        $setLimit = $this->max_row();
        $srh = explode(" ", $this->keyword());
        $pageLimit = ($this->page() * $setLimit) - ($setLimit);

        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "invoice_number LIKE '%" . $field . "%'";
                $conditions[] = "line_item_amount LIKE '%" . $field . "%'";
                $conditions[] = "line_item_name LIKE '%" . $field . "%'";
                $conditions[] = "entity LIKE '%" . $field . "%'";
                $conditions[] = "group_name LIKE '%" . $field . "%'";
                $conditions[] = "source LIKE '%" . $field . "%'";
            }
        }
        $_query = "SELECT crm_transactions.*, crm_companies.entity, intra.user_group.group_name FROM crm_transactions
                    LEFT JOIN crm_companies ON crm_companies.ID = crm_transactions.client_id
                    LEFT JOIN intra.user_group ON intra.user_group.GID = crm_transactions.group_id
                    WHERE crm_transactions.transaction_type = 'invoices'
                    AND YEAR(crm_transactions.transaction_date) >= 2021 
                    AND crm_transactions.is_included = 0
                    ";
        if (count($conditions) > 0) {
            $_query .= "AND (" . implode(' || ', $conditions) . ") ";
        }
        $clients = $this->db->query($_query . " ORDER BY transaction_date DESC LIMIT $pageLimit , $setLimit");
        return $clients->result();
    }

    function missing_invoices_pagination() {
        $page_url = base_url($this->uri->segment(1) . '/' . $this->uri->segment(2) . '');
        $per_page = $this->max_row();
        $page = $this->page();
        $srh = explode(" ", $this->keyword());
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "invoice_number LIKE '%" . $field . "%'";
                $conditions[] = "line_item_amount LIKE '%" . $field . "%'";
                $conditions[] = "line_item_name LIKE '%" . $field . "%'";
                $conditions[] = "entity LIKE '%" . $field . "%'";
                $conditions[] = "group_name LIKE '%" . $field . "%'";
                $conditions[] = "source LIKE '%" . $field . "%'";
            }
        }

        $_query = "SELECT crm_transactions.*, crm_companies.entity, intra.user_group.group_name FROM crm_transactions
                    LEFT JOIN crm_companies ON crm_companies.ID = crm_transactions.client_id
                    LEFT JOIN intra.user_group ON intra.user_group.GID = crm_transactions.group_id
                    WHERE crm_transactions.transaction_type = 'invoices'
                    AND YEAR(crm_transactions.transaction_date) >= 2021
                    AND crm_transactions.is_included = 0
                    ";
        if (count($conditions) > 0) {
            $_query .= "AND (" . implode(' || ', $conditions) . ") ";
        }

        $query = $this->db->query($_query . " ORDER BY transaction_date DESC");

        $total = $query->num_rows();
        $adjacents = "2";
        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;
        $prev = $page - 1;
        $next = $page + 1;
        $setLastpage = ceil($total / $per_page);
        $lpm1 = $setLastpage - 1;
        $setPaginate = "";
        if ($setLastpage > 1) {
            $setPaginate .= "<ul class='pagination pagination-sm float-right'>";
            $setPaginate .= "<li class='disabled'><a>Page $page of $setLastpage</a></li>";
            if ($setLastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $setLastpage; $counter++) {
                    if ($counter == $page)
                        $setPaginate .= "<li class='active'><a>$counter</a></li>";
                    else
                        $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                }
            }
            elseif ($setLastpage > 5 + ($adjacents * 2)) {
                if ($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active btn-default'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                    $setPaginate .= "<li class='dot disabled'><a class='disabled'>...</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "'>$lpm1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>$setLastpage</a></li>";
                }
                elseif ($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                    $setPaginate .= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "'>1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "'>2</a></li>";
                    $setPaginate .= "<li class='dot disabled'><a>...</a></li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                    $setPaginate .= "<li class='dot disabled'><a>..</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "'>$lpm1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>$setLastpage</a></li>";
                }
                else {
                    $setPaginate .= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "'>1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "'>2</a></li>";
                    $setPaginate .= "<li class='dot disabled'><a>..</a></li>";
                    for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                }
            }
            if ($page < $counter - 1) {
                $setPaginate .= "<li><a href='{$page_url}?page=" . $next . "&keyword=" . $this->keyword() . "'>Next</a></li>";
                $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>Last</a></li>";
            } else {
                $setPaginate .= "<li class='disabled'><a class='current_page'>Next</a></li>";
                $setPaginate .= "<li class='disabled'><a class='current_page'>Last</a></li>";
            }
            $setPaginate .= "</ul>\n";
        }
        return $setPaginate;
    }

    public function insert($data) {
        return $this->db->insert_batch($this->table, $data); 
        // $this->db->insert($this->table, $data);
        // return $this->db->insert_id();
    }

    public function show($invoice) {
        $this->db->select('crm_transactions.*, crm_companies.entity, intra.user_group.group_name');
        $this->db->from($this->table);
        $this->db->join('crm_companies', 'crm_companies.ID = crm_transactions.client_id', 'left');
        $this->db->join('intra.user_group', 'intra.user_group.GID = crm_transactions.group_id', 'left');
        $this->db->where('invoice_number', $invoice);
        $this->db->where('transaction_type', 'invoices');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function total($invoice) {
        $this->db->select_sum('line_item_amount');
        $this->db->from($this->table);
        $this->db->where('invoice_number', $invoice);
        $this->db->where('transaction_type', 'invoices');
        $query = $this->db->get();
        return $query->row();
    }

    public function getTotalInvoices()
    {
        $query = $this->db->query("SELECT * FROM crm_transactions WHERE is_included = 1 AND transaction_type = 'invoices'");

        return $query->num_rows();
    }

    public function getMissingTotalInvoices()
    {
        $query = $this->db->query("SELECT * FROM crm_transactions WHERE is_included = 0 AND transaction_type = 'invoices' AND YEAR(crm_transactions.transaction_date) >= 2021");

        return $query->num_rows();
    }

    public function update_invoice_group_old()
    {
        $this->db->query("UPDATE  
                            crm_transactions AS a
                            SET
                            a.group_id = IF(a.line_item_name LIKE '%Acctg & Bkkpg%',
                            IF(a.line_item_name LIKE '%Reim%', null, 
                                (SELECT b.groupid FROM intra.kpi_client_tbl AS b
                                LEFT JOIN intra.user_group AS c ON c.GID = b.groupid
                                WHERE b.month = MONTH(a.transaction_date)
                                AND b.year = YEAR(a.transaction_date)
                                AND b.clientid = a.client_id 
                                AND c.line_of_service='Acctg & Bkkpg' LIMIT 1)
                            ),

                            IF(a.line_item_name LIKE 'Tech Support%',
                            IF(a.line_item_name LIKE '%Reim%', null, 48),

                            IF(a.line_item_name LIKE 'Admin%',
                            IF(a.line_item_name LIKE '%Reim%', null, 64),

                            IF(a.line_item_name LIKE 'Tax%',
                            IF(a.line_item_name LIKE '%Reim%', null, 
                                38
                            ), 

                            IF(a.line_item_name LIKE 'Corp Fin%',
                            IF(a.line_item_name LIKE '%Reim%', null, 
                                28
                            ),

                            IF(a.line_item_name LIKE 'Corp. Fin%', 28,

                            IF(a.line_item_name LIKE 'Prof%',
                            IF(a.line_item_name LIKE '%Reim%', null, 
                                IF(a.line_item_name LIKE '%Val%', 36,
                                IF(a.line_item_name LIKE '%-Aud%', 33, 
                                IF(a.line_item_name LIKE '%Sox%', 34, 
                                IF(a.line_item_name LIKE '%TAS%', 35, 
                                IF(a.line_item_name LIKE '%Netsuite%', 48, 
                                IF(a.line_item_name LIKE '%Comm%', 52, 
                                IF(a.line_item_name LIKE '%DPC%', 54, 
                                IF(a.line_item_name LIKE '%Cybersec%', 60, 
                                IF(a.line_item_name LIKE '%TA%', 57, 
                                null))
                                )))))))
                            ),
                            null) 
                            ))))))
                            WHERE a.line_item_name IS NOT NULL
                            AND a.group_id IS NULL
                            AND a.transaction_type = 'invoices'
        ");

        // Internal Audit - SOX
        $this->db->query("UPDATE  
        crm_transactions AS a
        SET
        a.group_id = IF(a.line_item_name LIKE '%SOX-IA%',
        IF(a.line_item_name LIKE '%Reim%', null, 66),
        null) 
        WHERE a.line_item_name IS NOT NULL
        AND a.group_id IS NULL
        AND a.transaction_type = 'invoices'
        ");

        $this->db->query("UPDATE  
                            crm_transactions AS a
                            SET
                            a.group_id = IF(a.line_item_name LIKE '%Acctg & Bkkpg%',
                            IF(a.line_item_name LIKE '%Reim%', null, 
                                (SELECT b.groupid FROM intra.workspace_client_tbl AS b
                                LEFT JOIN intra.user_group AS c ON c.GID = b.groupid
                                WHERE b.clientid=a.client_id 
                                AND c.line_of_service='Acctg & Bkkpg' LIMIT 1)
                            ),
                            null) 
                            WHERE a.line_item_name IS NOT NULL
                            AND a.group_id IS NULL
                            AND a.transaction_type = 'invoices'
        ");

        // for Pro. services 
        $this->db->query("UPDATE  
        crm_transactions AS a
        SET
        -- // Briggs & Veselka Co.
        a.group_id = IF(a.client_id LIKE '886', 61,
            -- // Spiegel
            IF(a.client_id LIKE '228', 31,
                -- // ACS
                IF(a.client_id LIKE '498', 29,
                    -- // Porch & Associates LLC
                    IF(a.client_id LIKE '716', 50,
                        -- // Watson
                        IF(a.client_id LIKE '535', 32,
                            -- // Galleros 
                            IF(a.client_id LIKE '486', 30,
                                -- // Jaguar Health, Inc.	
                                IF(a.client_id LIKE '854', 54,
                                    -- // JCalifornia Residential Opportunity Fund
                                    IF(a.client_id LIKE '632', 47,
                                        -- // Rosario Angeles of JIT Business Consulting Inc
                                        IF(a.client_id LIKE '807', 48,
                                            -- // Galleros Koh LLP
                                            IF(a.client_id LIKE '536', 30,
                                                -- // OUM Co. LLP	
                                                IF(a.client_id LIKE '735', 51,
                                                    -- // Armanino LLP
                                                    IF(a.client_id LIKE '901', 67,
                                                        -- // UHY Advisors MidAtlantic MD, Inc.
                                                        IF(a.client_id LIKE '2380', 82,
                                                            -- // Crowe LLP
                                                            IF(a.client_id LIKE '2329', 84,
                                                                -- // UHY Advisors MO, Inc.
                                                                IF(a.client_id LIKE '2505', 82,
                                                                    -- // Elliott Davis, LLC
                                                                    IF(a.client_id LIKE '2310', 78,
                                                                        -- // Nexus Brands Group, Inc.
                                                                        IF(a.client_id LIKE '2481', 100,
                                                                            -- // Sightline Commercial Solutions, LLC
                                                                            IF(a.client_id LIKE '2465', 99,
                                                                                -- // Baker Tilly, LLP
                                                                                IF(a.client_id LIKE '2427', 95,
                                                                                    -- // Calibre CPA Group PLLC
                                                                                    IF(a.client_id LIKE '2510', 102,
                                                                                        -- // Val
                                                                                        IF(a.line_item_name LIKE '%Val%', 36,
                                                                                            null
                                                                                        )
                                                                                    )
                                                                                )
                                                                            )
                                                                        )
                                                                    )
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        ) 
        WHERE line_item_name LIKE 'Prof. Services%'
        AND group_id IS NULL
        AND transaction_type = 'invoices'");

        // Moss Adams - OFA
        $this->db->query("UPDATE  
        crm_transactions
        SET
        group_id = 62
        WHERE client_id = 927
        AND group_id IS NULL
        AND transaction_type = 'invoices'
        AND line_item_name LIKE 'Prof Services -OFA%'");

        // Briggs & Veselka Co. - OFA
        $this->db->query("UPDATE  
        crm_transactions
        SET
        group_id = 63
        WHERE client_id = 886
        AND group_id IS NULL
        AND transaction_type = 'invoices'
        AND line_item_name LIKE 'Prof Services-OFA%'");

        // Briggs & Veselka Co.
        $this->db->query("UPDATE  
        crm_transactions
        SET
        group_id = 61
        WHERE client_id = 886
        AND group_id IS NULL
        AND transaction_type = 'invoices'
        AND line_item_name LIKE 'Prof. Services%'");

        // CoolSys, Inc.
        $this->db->query("UPDATE  
        crm_transactions
        SET
        group_id = 76
        WHERE client_id IN (2164, 2248)
        AND transaction_type = 'invoices'");

        // Moss Adams - Technical Accounting
        $this->db->query("UPDATE  
        crm_transactions
        SET
        group_id = 69
        WHERE client_id = 927
        AND transaction_type = 'invoices'
        AND line_item_name LIKE '%TA-%'");

        // Armanino LLP
        $this->db->query("UPDATE  
        crm_transactions
        SET
        group_id = 74
        WHERE client_id = 901
        AND transaction_type = 'invoices'
        AND line_item_name LIKE '%IT-Audit%'");

        // Moss Adams
        $this->db->query("UPDATE  
        crm_transactions
        SET
        group_id = 79
        WHERE client_id = 528
        AND transaction_type = 'invoices'
        AND line_item_name LIKE '%IT-Audit%'");

        // Wave Computing Inc	
        // $this->db->query("UPDATE  
        // crm_transactions
        // SET
        // group_id = 22
        // WHERE client_id = 2141
        // AND group_id IS NULL
        // AND transaction_type = 'invoices'
        // AND line_item_name LIKE 'Acctg%'");

        // California Residential Opportunity Fund	
        // $this->db->query("UPDATE  
        // crm_transactions
        // SET
        // group_id = 47
        // WHERE client_id = 632
        // AND group_id IS NULL
        // AND transaction_type = 'invoices'
        // AND line_item_name LIKE 'Acctg%'");

        // // Slow Networks	
        // $this->db->query("UPDATE  
        // crm_transactions
        // SET
        // group_id = 21
        // WHERE client_id = 203
        // AND line_item_name LIKE 'Acctg%'");

        $this->db->query("UPDATE  
        crm_transactions AS a
        SET
        a.is_included = 0
        WHERE a.group_id IS NULL
        AND a.transaction_type = 'invoices'");

        $this->db->query("UPDATE  
        crm_transactions AS a
        SET
        a.is_included = 0
        WHERE a.line_item_name LIKE '%Others%'
        AND a.transaction_type = 'invoices'");

        // $this->db->query("DELETE  
        // FROM crm_transactions
        // WHERE transaction_date < '2020-12-30 00:00:00'
        // AND line_item_name LIKE '%Acctg%'
        // AND group_id IS NULL");

        // delete inactive clients
        // $this->db->query("DELETE  
        // FROM crm_transactions
        // WHERE client_id IN (2111)
        // OR client_name = 'Carl Kenneth Gigante'
        // OR client_name = 'Belieber Inc'");

    }

    public function update_invoice_group($month, $year)
    {
        // Acctg & Bkkpg
        $this->db->query("UPDATE  
                crm_transactions AS a
            SET
                a.group_id = IF(a.line_item_name LIKE '%Acctg & Bkkpg%' OR a.line_item_name LIKE '%Tech Support%' OR a.line_item_name LIKE '%Admin Services-EA%',
                    (
                        IF(a.line_item_name LIKE '%Reim%', null, 
                            (SELECT
                                b.groupid
                            FROM intra.kpi_client_tbl AS b
                            LEFT JOIN intra.user_group AS c ON c.GID = b.groupid
                            LEFT JOIN intra.crm_hubspot_tbl AS d ON (d.crm_id = b.clientid)
                            WHERE b.month = MONTH(a.transaction_date)
                            AND b.year = YEAR(a.transaction_date)
                            AND d.crm_id = a.client_id 
                            AND c.category = 'Accounting'
                            AND d.crm_id IS NOT NULL
                            LIMIT 1)
                        )
                    ),
                    IF(a.line_item_name LIKE '%Prof. Services - Non%'
                        OR a.line_item_name LIKE '%Prof. Services - Rec%'
                        OR a.line_item_name LIKE '%Prof. Services - Recurring%',
                        (
                            IF(a.line_item_name LIKE '%Reim%', null,
                                (SELECT
                                    b.groupid
                                FROM intra.kpi_client_tbl AS b
                                LEFT JOIN intra.user_group AS c ON c.GID = b.groupid
                                LEFT JOIN intra.crm_hubspot_tbl AS d ON (d.crm_id = b.clientid)
                                WHERE b.month = MONTH(a.transaction_date)
                                AND b.year = YEAR(a.transaction_date)
                                AND d.crm_id = a.client_id 
                                AND d.crm_id IS NOT NULL
                                AND c.category= 'External' 
                                LIMIT 1)
                            )
                        ),
                        -- External
                        IF (a.line_item_name LIKE '%Prof Services -Aud%' 
                            OR a.line_item_name LIKE '%Prof Serv -C&M-Audit%'
                            OR a.line_item_name LIKE '%Prof Services -DPC%',
                            IF(a.line_item_name LIKE '%Reim%', null,
                                113
                            ),
                            -- Outsourced
                            IF(a.line_item_name LIKE '%Prof Services -OFA%'
                                OR a.line_item_name LIKE '%Prof Services-OFA%',
                                IF(a.line_item_name LIKE '%Reim%', null,
                                    126
                                ),
                                -- TAS
                                IF(a.line_item_name LIKE '%Prof Services -TAS%'
                                    OR a.line_item_name LIKE '%Prof. Services -Val%'
                                    OR a.line_item_name LIKE '%Prof Services -Val%'
                                    OR a.line_item_name LIKE '%Prof Serv -Comm%'
                                    OR a.line_item_name LIKE '%Prof Services -Fin-Acctg%'
                                    OR a.line_item_name LIKE '%Prof Serv -Fin-Acctg%',
                                    IF(a.line_item_name LIKE '%Reim%', null,
                                        112
                                    ),
                                    -- RISK
                                    IF(line_item_name LIKE '%Prof Services -Sox-%'
                                        OR line_item_name LIKE '%Prof Serv - Cybersec%'
                                        OR line_item_name LIKE '%Prof Serv - HealthCare%'
                                        OR line_item_name LIKE '%Prof Serv  IT-Audit%'
                                        OR line_item_name LIKE '%Prof Services-SOX-IA%',
                                        IF(a.line_item_name LIKE '%Reim%', null,
                                            107
                                        ),
                                        -- TAG
                                        IF(line_item_name LIKE '%Prof Services-TA-%'
                                            OR line_item_name LIKE '%Prof Services-TA-%',
                                            IF(a.line_item_name LIKE '%Reim%', null,
                                                106
                                            ),
                                            -- CorFin
                                            IF(line_item_name LIKE '%Corp Fin%' OR line_item_name LIKE '%Corp. Fin%',
                                                IF(a.line_item_name LIKE '%Reim%', null,
                                                    103
                                                ),
                                                -- Tax
                                                IF(line_item_name LIKE '%Tax%',
                                                    IF(a.line_item_name LIKE '%Reim%', null,
                                                        111
                                                    ),
                                                    null
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            WHERE a.line_item_name IS NOT NULL
            AND a.group_id IS NULL
            AND a.transaction_type = 'invoices'
            AND MONTH(a.transaction_date) = $month
            AND YEAR(a.transaction_date) = $year
        ");

        // delete other line items 
        $this->db->query("DELETE  
        FROM crm_transactions
        WHERE line_item_name LIKE '%Reim%'
        OR line_item_name LIKE '%Accrued Comm%'
        OR line_item_name LIKE 'Marketing Exp'
        OR line_item_name LIKE 'Other Income' 
        OR line_item_name LIKE 'Coop-Marketing Support Fund' 
        OR line_item_name LIKE 'Customer Advances' 
        OR line_item_name LIKE 'Bank Charges' 
        OR line_item_name LIKE 'Financial projection' 
        OR line_item_name LIKE 'Professional Courtesy'
        OR line_item_name LIKE 'Consulting Services' 
        OR line_item_name LIKE 'Onboarding'
        OR line_item_name LIKE 'Staff Support'
        OR line_item_name LIKE 'Financial projection work'
        OR line_item_name LIKE 'Prof. Services - Advance'
        OR line_item_name LIKE 'Equity Warrants'
        OR line_item_name LIKE 'Setup Services'
        OR line_item_name LIKE 'Invoicing services'
        OR line_item_name LIKE 'Bad Debt Expense'
        OR line_item_name LIKE 'Tax comp supp services - Adv'
        AND is_included = 0
        AND transaction_type = 'invoices'
        AND MONTH(transaction_date) = $month
        AND YEAR(transaction_date) = $year");

        $this->db->query("UPDATE  
            crm_transactions
            SET
            is_included = 0, group_id = NULL
            WHERE line_item_name LIKE '%Others%'
            AND YEAR(transaction_date) = 2023 
            AND transaction_type = 'invoices'
            AND MONTH(transaction_date) = $month
            AND YEAR(transaction_date) = $year");

        $this->db->query("UPDATE crm_transactions SET is_included = 1
            WHERE is_included = 0
            AND group_id IS NOT NULL
            AND transaction_type = 'invoices'
            AND MONTH(transaction_date) = $month
            AND YEAR(transaction_date) = $year
        ");
    }

    public function check_ivoice($data)
    {
        $transaction_date = $data['transaction_date'];
        $client_id = $data['client_id'];
        $invoice_number = $data['invoice_number'];
        $line_item_name = $data['line_item_name'];
        $line_item_amount = $data['line_item_amount'];

        $test = $this->db->query("SELECT * FROM crm_transactions 
        WHERE transaction_date = '".$transaction_date."'
        AND client_id = '".$client_id."'
        AND invoice_number = '".$invoice_number."'
        AND line_item_name = '".$line_item_name."'
        AND line_item_amount = '".$line_item_amount."'
        ");
        // $this->db->where('client_id', $client_id);
        // $this->db->where('invoice_number', $invoice_number);
        // $this->db->where('line_item_name', $line_item_name);
        // $this->db->where('line_item_amount', $line_item_amount);
        // $this->db->where('transaction_date', $transaction_date);
        // $this->db->where('invoice_number', $data['invoice_number']);
        // $this->db->where('line_item_name', $data['line_item_name']);
        // $this->db->where('line_item_amount', $data['line_item_amount']);
        // $this->db->where('total_line_items', $data['total_line_items']);
        // $this->db->where('transaction_date', $data['transaction_date']);
        // $this->db->where('created_at', $data['created_at']);
        // $this->db->where('updated_at', $data['updated_at']);
        // $query = $this->db->get('crm_transactions');
        return $test->result(); 
    }

    public function store($data) 
    {
        $this->db->insert('crm_transactions', $data);

        $id = $this->db->insert_id();


        $this->db->select("a.*, b.group_name");
		$this->db->from('crm_transactions as a');
        $this->db->join('intra.user_group as b', 'b.GID = a.group_id', 'left');
		$this->db->where('a.id', $id);
        $q = $this->db->get()->row();
        $q->transaction_date = date('m/d/Y', strtotime($q->transaction_date)); 
        return $q;
    }

    public function storeAr($data) 
    {
        $this->db->insert('intra.kpi_ar_details_tbl', $data);

        $id = $this->db->insert_id();


        $this->db->select("a.*, b.group_name");
		$this->db->from('intra.kpi_ar_details_tbl as a');
        $this->db->join('intra.user_group as b', 'b.GID = a.groupid', 'left');
		$this->db->where('a.ID', $id);
        $q = $this->db->get()->row(); 
        return $q;
    }

    public function findAr($id) {
        $this->db->select('*');
        $this->db->from('intra.kpi_ar_details_tbl');
        $this->db->where('ID', $id);
        $query = $this->db->get()->row();
        $query->invoice_date = date('Y-m-d', strtotime($query->invoice_date)); 
        return $query;
    }

    public function updateAr($data) 
    {
        $this->db->where('ID', $data['ID']);
        $this->db->update('intra.kpi_ar_details_tbl', $data);

        $id = $data['ID'];


        $this->db->select("a.*, b.group_name");
		$this->db->from('intra.kpi_ar_details_tbl as a');
        $this->db->join('intra.user_group as b', 'b.GID = a.groupid', 'left');
		$this->db->where('a.ID', $id);
        $q = $this->db->get()->row();
        return $q;
    }

    public function deleteAr($id) {
        $this->db->where('ID', $id);
        $this->db->delete('intra.kpi_ar_details_tbl');
        return true;
    }

    public function find($id) {
        $this->db->select('*');
        $this->db->from('crm_transactions');
        $this->db->where('id', $id);
        $query = $this->db->get()->row();
        $query->transaction_date = date('Y-m-d', strtotime($query->transaction_date)); 
        return $query;
    }

    public function update($data) 
    {
        $this->db->where('id', $data['id']);
        $this->db->update('crm_transactions', $data);

        $id = $data['id'];


        $this->db->select("a.*, b.group_name");
		$this->db->from('crm_transactions as a');
        $this->db->join('intra.user_group as b', 'b.GID = a.group_id', 'left');
		$this->db->where('a.id', $id);
        $q = $this->db->get()->row();
        $q->transaction_date = date('m/d/Y', strtotime($q->transaction_date)); 
        return $q;
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('crm_transactions');
        return true;
    }

    public function get_all_revenue_ar($month_year) {
        $month = date('m', strtotime($month_year));
        $year = date('Y', strtotime($month_year));

        $data = $this->db->query("SELECT a.*, b.revenue, c.ar
                            FROM intra.user_group AS a
                            LEFT JOIN (SELECT *, SUM(line_item_amount) AS revenue 
                                        FROM crm_transactions 
                                        WHERE DATE_FORMAT(transaction_date,'%Y-%m') = '$month_year' 
                                        AND transaction_type = 'invoices' 
                                        AND is_included = 1
                                        GROUP BY group_id ) AS b 
                                        ON a.GID = b.group_id
                            LEFT JOIN (SELECT *, SUM(amount_due) AS ar  
                                        FROM intra.kpi_ar_details_tbl 
                                        WHERE `year` = '$year' 
                                        AND `month` = '$month'
                                        GROUP BY groupid ) AS c 
                                        ON a.GID = c.groupid
                            WHERE a.GID NOT IN (39, 40, 41, 42, 43, 44, 45, 49, 53, 55, 56)
                            GROUP BY a.GID
                            ORDER BY a.group_name
                        ");

        // $this->db->select("a.*, SUM(b.line_item_amount) as revenue, SUM(c.amount_due) as ar");
		// $this->db->from('intra.user_group as a');
        // $this->db->join('crm_transactions as b', 'a.GID = b.group_id', 'left');
        // $this->db->join('intra.kpi_ar_details_tbl as c', 'a.GID = c.groupid', 'left');
        // // $this->db->where('c.year', $year);
        // // $this->db->where('c.month', $month);
        // $this->db->where('DATE_FORMAT(b.transaction_date,"%Y-%m")', $month_year);
        // $this->db->where('b.transaction_type', 'invoices');
        // $this->db->where('b.is_included', 1);
        // $this->db->group_by('a.GID');
        return $data->result();
    }

    public function get_group_revenue($group_id, $month_year) {
        $this->db->select("*, SUM(line_item_amount) as revenue");
		$this->db->from('crm_transactions');
        $this->db->where('DATE_FORMAT(transaction_date,"%Y-%m")', $month_year);
        if($group_id != 0) {
            $this->db->where('group_id', $group_id);
        }
        $this->db->where('transaction_type', 'invoices');
        $this->db->where('is_included', 1);
        $this->db->group_by('invoice_number');
        $this->db->order_by('client_name', 1);
        return $this->db->get()->result();
    }

    public function get_group_ar($group_id, $month, $year) {
        $this->db->select("*, SUM(amount_due) as ar");
		$this->db->from('intra.kpi_ar_details_tbl');
        $this->db->where('year', $year);
        $this->db->where('month', $month);
        if($group_id != 0) {
            $this->db->where('groupid', $group_id);
        }
        $this->db->group_by('invoice_no');
        $this->db->order_by('customer_name', 1);
        return $this->db->get()->result();
    }

    function getArRows($filterDate){

        $month = date('m', strtotime($filterDate));
        $year =  date('Y', strtotime($filterDate));

        $setLimit = $this->max_row();
        $srh = explode(" ", $this->keyword());
        $pageLimit = ($this->page() * $setLimit) - ($setLimit);

        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "ikadt.ID LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.customer_name LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.invoice_date LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.invoice_no LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.item_name LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.month LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.year LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.amount_due LIKE '%" . $field . "%'";
                $conditions[] = "iug.group_name LIKE '%" . $field . "%'";
            }
        }
        $_query = "SELECT ikadt.*, iug.group_name FROM intra.kpi_ar_details_tbl AS ikadt
                    LEFT JOIN intra.user_group AS iug ON iug.GID = ikadt.groupid
                    WHERE ikadt.month = '$month'
                    AND ikadt.year = '$year'
                    ";
        if (count($conditions) > 0) {
            $_query .= "AND (" . implode(' || ', $conditions) . ") ";
        }
        $result = $this->db->query($_query . " ORDER BY ikadt.groupid ASC, ikadt.customer_name ASC LIMIT $pageLimit , $setLimit");
        return $result->result();
    }

    function arPagination($filterDate) {
        $month = date('m', strtotime($filterDate));
        $year =  date('Y', strtotime($filterDate));

        $page_url = base_url($this->uri->segment(1) . '/' . $this->uri->segment(2) . '');
        $per_page = $this->max_row();
        $page = $this->page();
        $srh = explode(" ", $this->keyword());
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "ikadt.ID LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.customer_name LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.invoice_date LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.invoice_no LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.item_name LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.month LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.year LIKE '%" . $field . "%'";
                $conditions[] = "ikadt.amount_due LIKE '%" . $field . "%'";
                $conditions[] = "iug.group_name LIKE '%" . $field . "%'";
            }
        }

        $_query = "SELECT ikadt.*, iug.group_name FROM intra.kpi_ar_details_tbl AS ikadt
                    LEFT JOIN intra.user_group AS iug ON iug.GID = ikadt.groupid
                    WHERE ikadt.month = $month
                    AND ikadt.year = $year
                    ";
        if (count($conditions) > 0) {
            $_query .= "AND (" . implode(' || ', $conditions) . ") ";
        }

        $query = $this->db->query($_query . " ORDER BY ikadt.groupid ASC, ikadt.customer_name ASC");

        $total = $query->num_rows();
        $adjacents = "2";
        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;
        $prev = $page - 1;
        $next = $page + 1;
        $setLastpage = ceil($total / $per_page);
        $lpm1 = $setLastpage - 1;
        $setPaginate = "";
        if ($setLastpage > 1) {
            $setPaginate .= "<ul class='pagination pagination-sm float-right'>";
            $setPaginate .= "<li class='disabled'><a>Page $page of $setLastpage</a></li>";
            if ($setLastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $setLastpage; $counter++) {
                    if ($counter == $page)
                        $setPaginate .= "<li class='active'><a>$counter</a></li>";
                    else
                        $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&filterDate=$filterDate&keyword=" . $this->keyword() . "'>$counter</a></li>";
                }
            }
            elseif ($setLastpage > 5 + ($adjacents * 2)) {
                if ($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active btn-default'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&filterDate=$filterDate&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                    $setPaginate .= "<li class='dot disabled'><a class='disabled'>...</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&filterDate=$filterDate&keyword=" . $this->keyword() . "'>$lpm1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&filterDate=$filterDate&keyword=" . $this->keyword() . "'>$setLastpage</a></li>";
                }
                elseif ($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                    $setPaginate .= "<li><a href='{$page_url}?page=1&filterDate=$filterDate&keyword=" . $this->keyword() . "'>1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=2&filterDate=$filterDate&keyword=" . $this->keyword() . "'>2</a></li>";
                    $setPaginate .= "<li class='dot disabled'><a>...</a></li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&filterDate=$filterDate&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                    $setPaginate .= "<li class='dot disabled'><a>..</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&filterDate=$filterDate&keyword=" . $this->keyword() . "'>$lpm1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&filterDate=$filterDate&keyword=" . $this->keyword() . "'>$setLastpage</a></li>";
                }
                else {
                    $setPaginate .= "<li><a href='{$page_url}?page=1&filterDate=$filterDate&keyword=" . $this->keyword() . "'>1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=2&filterDate=$filterDate&keyword=" . $this->keyword() . "'>2</a></li>";
                    $setPaginate .= "<li class='dot disabled'><a>..</a></li>";
                    for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&filterDate=$filterDate&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                }
            }
            if ($page < $counter - 1) {
                $setPaginate .= "<li><a href='{$page_url}?page=" . $next . "&filterDate=$filterDate&keyword=" . $this->keyword() . "'>Next</a></li>";
                $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&filterDate=$filterDate&keyword=" . $this->keyword() . "'>Last</a></li>";
            } else {
                $setPaginate .= "<li class='disabled'><a class='current_page'>Next</a></li>";
                $setPaginate .= "<li class='disabled'><a class='current_page'>Last</a></li>";
            }
            $setPaginate .= "</ul>\n";
        }
        return $setPaginate;
    }
}
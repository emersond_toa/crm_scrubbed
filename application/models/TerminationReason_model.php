<?php
class TerminationReason_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function all()
	{
        $query = $this->db->query("SELECT * FROM crm_termination_reasons");
        return $query->result();
	}

	public function create($data) 
    {
        $this->db->insert('crm_termination_reasons', $data);

        $id = $this->db->insert_id();
        $q = $this->db->get_where('crm_termination_reasons', array('id' => $id));
        return $q->row();
    }

    function delete($id) {
        $this->db->where(array('id' => $id));
        return $this->db->delete('crm_termination_reasons');
    }

    function update($data) {
        $this->db->where(array('id' => $data['id']));
        return $this->db->update('crm_termination_reasons', $data);
    }
}

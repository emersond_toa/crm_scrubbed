<?php

class Company_model extends CI_Model
{
	public function __construct()
	{
        $this->load->database();
        $this->column_search = array('name', 'leadform', 'industry', 'cstatus', 'contact');
        $this->load->model('Models');
	}

    public function all_companies()
    {
        $this->db->order_by('account');
        $query = $this->db->get('crm_companies');
        return $query->result();
    }

    public function show($id)
    {
        $query = $this->db->query("SELECT * FROM crm_companies WHERE ID = $id");
        $companies = $query->result();

        $data = [];
        foreach($companies as $company) {
            $company->users = '';
            $query2 = $this->db->query("SELECT intra.employee_info.* 
                FROM crm_company_user
                LEFT JOIN intra.employee_info ON crm_company_user.user_id = intra.employee_info.ID 
                WHERE crm_company_user.company_id = $company->ID
            ");
            $company->users = $query2->result();

            $company->work_groups = '';
            $query2 = $this->db->query("SELECT ugs.* 
                FROM intra.workspace_client_tbl as wsc 
                LEFT JOIN intra.user_group as ugs ON ugs.GID = wsc.groupid
                WHERE wsc.clientid = $company->ID
            ");
            $company->work_groups = $query2->result();
            $data[] = $company;
        }
        return $data;
    }

    public function update($id, $data, $tags, $entity, $workgroup_tags = null)
    {
        $check_parent = $this->db->query("SELECT * FROM crm_companies WHERE ID = $id AND (entity = account)")->row();
        if($check_parent){
            $query_entities = $this->db->query("UPDATE crm_companies SET account = '$entity', account_name = '$entity' WHERE account = '$check_parent->account'");
        }

        $this->db->where('ID', $id);
        $this->db->update('crm_companies', $data);

        // delete pivot 
        $this->db->where('company_id', $id);
        $this->db->delete('crm_company_user');

        
        if($tags) {
            $this->db->query("DELETE FROM crm_company_user where company_id='" . $id . "' ");
            $row_data = array(
                'company_id' => $id
            );
            foreach ($tags as $tag) {
                $row_data['user_id'] = $tag;
                $this->db->insert('crm_company_user', $row_data);
            }
        }

        if($workgroup_tags) {
            $this->db->query("DELETE FROM intra.workspace_client_tbl where clientid='" . $id . "' ");
            $row_data2 = array(
                'clientid' => $id
            );
            foreach ($workgroup_tags as $tag) {
                $row_data2['groupid'] = $tag;
                $this->db->insert('intra.workspace_client_tbl', $row_data2);
            }
        }
        return true;
    }

    public function clientstatus_badge($status)
    {   
		$result = '';				
		if($status == 'Current')
		{
			$result = '<span class="badge badge-success">'. $status .'</span>';
		}
		elseif($status == 'Past Due')
		{
			$result = '<span class="badge badge-primary">'. $status .'</span>';
		}
		elseif($status == 'Offboarding')
		{
			$result = '<span class="badge badge-secondary">'. $status .'</span>';
		}
		elseif($status == 'Former Client')
		{
			$result = '<span class="badge badge-info">'. $status .'</span>';
		}
		else
		{
			$result = '<span class="badge badge-light">'. $status .'</span>';
		}
    
        return $result;  

    }

    public function status_badge($status) {
        $result = '';
        if ($status == 'New Lead') {
            $result = '<span class="badge badge-warning">' . $status . '</span>';
        } elseif ($status == 'Called') {
            $result = '<span class="badge badge-dark">' . $status . '</span>';
        } elseif ($status == 'Proposal') {
            $result = '<span class="badge badge-secondary">' . $status . '</span>';
        } elseif ($status == 'Agreement') {
            $result = '<span class="badge badge-info">' . $status . '</span>';
        } elseif ($status == 'Signed') {
            $result = '<span class="badge badge-success">' . $status . '</span>';
        } elseif ($status == 'Declined') {
            $result = '<span class="badge badge-default">' . $status . '</span>';
        } elseif ($status == 'Qualified Lead') {
            $result = '<span class="badge badge-primary">' . $status . '</span>';
        } elseif ($status == 'Unqualified Lead') {
            $result = '<span class="badge badge-danger">' . $status . '</span>';
        } else {
            $result = '<span class="badge badge-light">' . $status . '</span>';
        }

        return $result;
    }

    public function funneldatatable($limit, $start, $col, $dir, $leadstatus, $leadform) {
        $_query = "";
        if (!empty($leadstatus)) {
            $a = array_diff($leadstatus, array(''));
            $b = implode(',', $a);
            $_query .= "AND com.status_id IN(".$b.")";
        }
        $query = $this->db->query("SELECT * FROM (SELECT com.*, com.account as name, con.fullname as contact, frm.name as leadform, ind.name as industry, sta.name as cstatus
                FROM crm_companies as com
                LEFT JOIN crm_contacts as con ON con.company_id = com.id   
                LEFT JOIN crm_forms as frm ON frm.id = com.form_id   
                LEFT JOIN crm_industries as ind ON ind.id = com.industry_id
                LEFT JOIN crm_statuses as sta ON sta.id = com.status_id  
                WHERE (com.account = com.entity)
                $_query) 
                AS data
                GROUP BY name
                ORDER BY $col $dir
                LIMIT $start, $limit
        ");

        if ($query->num_rows() > 0) {
            $data = [];
            foreach($query->result() as $company) {
                $company->assignedto = '';
                $query2 = $this->db->query("SELECT intra.employee_info.* 
                    FROM crm_company_user
                    LEFT JOIN intra.employee_info ON crm_company_user.user_id = intra.employee_info.ID 
                    WHERE crm_company_user.company_id = $company->ID
                ");
                $company->assignedto = $query2->result();

                $company->work_groups = '';
                $query3 = $this->db->query("SELECT ugs.* 
                    FROM intra.workspace_client_tbl as wsc 
                    LEFT JOIN intra.user_group as ugs ON ugs.GID = wsc.groupid
                    WHERE wsc.clientid = $company->ID
                ");
                $company->work_groups = $query3->result();
                $data[] = $company;
            }
            return $data;
        } else {
            return null;
        }
    }

    public function funnelcount($leadstatus, $leadform) {
        $_query = "";
        if (!empty($leadstatus)) {
            $a = array_diff($leadstatus, array(''));
            $b = implode(',', $a);
            $_query .= "AND com.status_id IN(".$b.")";
        }
        // if (!empty($leadform)) {
        //     $this->db->where_in('com.form_id', $leadform);
        // }
        $query = $this->db->query("SELECT *
            FROM crm_companies as com
            WHERE com.account = com.entity
            $_query
        ");

        return $query->num_rows();
    }

    public function funnel_contacts_search_count($search, $leadstatus, $leadform) {
        $_query = "";
        if (!empty($leadstatus)) {
            $a = array_diff($leadstatus, array(''));
            $b = implode(',', $a);
            $_query .= "AND com.status_id IN($b) ";
        }
        // if (!empty($leadform)) {
        //     $this->db->where_in('com.form_id', $leadform);
        // }
        $_query_serach = "";
        if (!empty($search)) {
            $x = 0;
            foreach ($this->column_search as $s_col) {
                if ($x == 0) {
                    $_query_serach .= "WHERE ($s_col LIKE '%".$search."%') ";
                } else {
                    $_query_serach .= "OR ($s_col LIKE '%".$search."%') ";
                }
                $x++;
            }
        }
        $query = $this->db->query("SELECT * FROM (SELECT com.*, com.account as name, con.fullname as contact, frm.name as leadform, ind.name as industry, sta.name as cstatus
                FROM crm_companies as com
                LEFT JOIN crm_contacts as con ON con.company_id = com.id   
                LEFT JOIN crm_forms as frm ON frm.id = com.form_id   
                LEFT JOIN crm_industries as ind ON ind.id = com.industry_id
                LEFT JOIN crm_statuses as sta ON sta.id = com.status_id  
                WHERE (com.account = com.entity)
                $_query) 
                AS data
                $_query_serach
        ");

        return $query->num_rows();
    }

    public function funneldatatablesearch($limit, $start, $search, $col, $dir, $leadstatus, $leadform) {
        $_query = "";
        if (!empty($leadstatus)) {
            $a = array_diff($leadstatus, array(''));
            $b = implode(',', $a);
            $_query .= "AND com.status_id IN($b) ";
        }
        // if (!empty($leadform)) {
        //     $this->db->where_in('com.form_id', $leadform);
        // }
        $_query_serach = "";
        if (!empty($search)) {
            $x = 0;
            foreach ($this->column_search as $s_col) {
                if ($x == 0) {
                    $_query_serach .= "WHERE ($s_col LIKE '%".$search."%') ";
                } else {
                    $_query_serach .= "OR ($s_col LIKE '%".$search."%') ";
                }
                $x++;
            }
        }
        $query = $this->db->query("SELECT * FROM (SELECT com.*, com.account as name, con.fullname as contact, frm.name as leadform, ind.name as industry, sta.name as cstatus
                FROM crm_companies as com
                LEFT JOIN crm_contacts as con ON con.company_id = com.id   
                LEFT JOIN crm_forms as frm ON frm.id = com.form_id   
                LEFT JOIN crm_industries as ind ON ind.id = com.industry_id
                LEFT JOIN crm_statuses as sta ON sta.id = com.status_id  
                WHERE (com.account = com.entity)
                $_query) 
                AS data
                $_query_serach
                GROUP BY name
                ORDER BY $col $dir
                LIMIT $start, $limit
        ");

        if ($query->num_rows() > 0) {
            $data = [];
            foreach($query->result() as $company) {
                $company->assignedto = '';
                $query2 = $this->db->query("SELECT intra.employee_info.* 
                    FROM crm_company_user
                    LEFT JOIN intra.employee_info ON crm_company_user.user_id = intra.employee_info.ID 
                    WHERE crm_company_user.company_id = $company->ID
                ");
                $company->assignedto = $query2->result();

                $company->work_groups = '';
                $query3 = $this->db->query("SELECT ugs.* 
                    FROM intra.workspace_client_tbl as wsc 
                    LEFT JOIN intra.user_group as ugs ON ugs.GID = wsc.groupid
                    WHERE wsc.clientid = $company->ID
                ");
                $company->work_groups = $query3->result();
                $data[] = $company;
            }
            return $data;
        } else {
            return null;
        }
    }

    public function clientdatatable($limit, $start, $col, $dir, $leadstatus, $leadform) {
        $_query = "";
        if (!empty($leadstatus)) {
            $a = array_diff($leadstatus, array(''));
            $b = implode(',', $a);
            $_query .= "AND (com.clientstatus_id IN($b)) ";
        }
        // if (!empty($leadform)) {
        //     $this->db->where_in('com.form_id', $leadform);
        // }
        $query = $this->db->query("SELECT * FROM (SELECT com.*, com.account as name, con.fullname as contact, frm.name as leadform, ind.name as industry, csta.name as clientstatus, sta.name as cstatus
                FROM crm_companies as com
                LEFT JOIN crm_contacts as con ON con.company_id = com.id   
                LEFT JOIN crm_forms as frm ON frm.id = com.form_id   
                LEFT JOIN crm_industries as ind ON ind.id = com.industry_id
                LEFT JOIN crm_statuses as sta ON sta.id = com.status_id  
                LEFT JOIN crm_client_statuses as csta ON csta.id = com.clientstatus_id  
                WHERE (com.status_id = 6)
                AND (com.account = com.entity)
                $_query) 
                AS data
                GROUP BY name
                ORDER BY $col $dir
                LIMIT $start, $limit
        ");

        if ($query->result() > 0) {
            $data = [];
            foreach($query->result() as $company) {
                $company->assignedto = '';
                $query2 = $this->db->query("SELECT intra.employee_info.* 
                    FROM crm_company_user
                    LEFT JOIN intra.employee_info ON crm_company_user.user_id = intra.employee_info.ID 
                    WHERE (crm_company_user.company_id = $company->ID)
                ");
                $company->assignedto = $query2->result();

                $company->work_groups = '';
                $query3 = $this->db->query("SELECT ugs.* 
                    FROM intra.workspace_client_tbl as wsc 
                    LEFT JOIN intra.user_group as ugs ON ugs.GID = wsc.groupid
                    WHERE wsc.clientid = $company->ID
                ");
                $company->work_groups = $query3->result();
                
                $company->services = '';
                $query4 = $this->db->query("SELECT crm_type_of_services.*, crm_services.name 
                    FROM crm_type_of_services
                    LEFT JOIN crm_companies ON crm_companies.id = crm_type_of_services.company_id
                    LEFT JOIN crm_services ON crm_services.id = crm_type_of_services.service_id
                    WHERE (crm_type_of_services.company_id = $company->ID)
                ");
                $company->services = $query4->result();
                $data[] = $company;
            }
            return $data;
        } else {
            return null;
        }
    }

    public function clientcount($leadstatus, $leadform) {
        $_query = "";
        if (!empty($leadstatus)) {
            $a = array_diff($leadstatus, array(''));
            $b = implode(',', $a);
            $_query .= "AND (com.clientstatus_id IN(".$b.")) ";
        }
        // if (!empty($leadform)) {
        //     $this->db->where_in('com.form_id', $leadform);
        // }
        $query = $this->db->query("SELECT *
            FROM crm_companies as com
            WHERE (com.status_id = 6)
            AND (com.account = com.entity)
            $_query
        ");

        return $query->num_rows();
    }

    public function client_contacts_search_count($search, $leadstatus, $leadform) {
        $_query = "";
        if (!empty($leadstatus)) {
            $a = array_diff($leadstatus, array(''));
            $b = implode(',', $a);
            $_query .= "AND (com.clientstatus_id IN($b)) ";
        }
        // if (!empty($leadform)) {
        //     $this->db->where_in('com.form_id', $leadform);
        // }
        $_query_serach = "";
        if (!empty($search)) {
            $x = 0;
            foreach ($this->column_search as $s_col) {
                if ($x == 0) {
                    $_query_serach .= "WHERE ($s_col LIKE '%".$search."%') ";
                } else {
                    $_query_serach .= "OR ($s_col LIKE '%".$search."%') ";
                }
                $x++;
            }
        }
        $query = $this->db->query("SELECT * FROM (SELECT com.*, com.account as name, con.fullname as contact, frm.name as leadform, ind.name as industry, sta.name as cstatus, csta.name as clientstatus
                FROM crm_companies as com
                LEFT JOIN crm_contacts as con ON con.company_id = com.id   
                LEFT JOIN crm_forms as frm ON frm.id = com.form_id   
                LEFT JOIN crm_industries as ind ON ind.id = com.industry_id
                LEFT JOIN crm_statuses as sta ON sta.id = com.status_id  
                LEFT JOIN crm_client_statuses as csta ON csta.id = com.clientstatus_id
                WHERE (com.status_id = 6)
                AND (com.account = com.entity)
                $_query) 
                AS data
                $_query_serach
        ");

        return $query->num_rows();
    }

    public function clientdatatablesearch($limit, $start, $search, $col, $dir, $leadstatus, $leadform) {
        $_query = "";
        if (!empty($leadstatus)) {
            $a = array_diff($leadstatus, array(''));
            $b = implode(',', $a);
            $_query .= "AND com.clientstatus_id IN($b) ";
        }
        // if (!empty($leadform)) {
        //     $this->db->where_in('com.form_id', $leadform);
        // }
        $_query_serach = "";
        if (!empty($search)) {
            $x = 0;
            foreach ($this->column_search as $s_col) {
                if ($x == 0) {
                    $_query_serach .= "WHERE ($s_col LIKE '%".$search."%') ";
                } else {
                    $_query_serach .= "OR ($s_col LIKE '%".$search."%') ";
                }
                $x++;
            }
        }
        $query = $this->db->query("SELECT * FROM (SELECT com.*, com.account as name, con.fullname as contact, frm.name as leadform, ind.name as industry, sta.name as cstatus, csta.name as clientstatus
                FROM crm_companies as com
                LEFT JOIN crm_contacts as con ON con.company_id = com.id   
                LEFT JOIN crm_forms as frm ON frm.id = com.form_id   
                LEFT JOIN crm_industries as ind ON ind.id = com.industry_id
                LEFT JOIN crm_statuses as sta ON sta.id = com.status_id  
                LEFT JOIN crm_client_statuses as csta ON csta.id = com.clientstatus_id  
                WHERE (com.status_id = 6)
                AND (com.account = com.entity)
                $_query) 
                AS data
                $_query_serach
                GROUP BY name
                ORDER BY $col $dir
                LIMIT $start, $limit
        ");

        if ($query->num_rows() > 0) {
            $data = [];
            foreach($query->result() as $company) {
                $company->assignedto = '';
                $query2 = $this->db->query("SELECT intra.employee_info.* 
                    FROM crm_company_user
                    LEFT JOIN intra.employee_info ON crm_company_user.user_id = intra.employee_info.ID 
                    WHERE crm_company_user.company_id = $company->ID
                ");
                $company->assignedto = $query2->result();

                $company->work_groups = '';
                $query3 = $this->db->query("SELECT ugs.* 
                    FROM intra.workspace_client_tbl as wsc 
                    LEFT JOIN intra.user_group as ugs ON ugs.GID = wsc.groupid
                    WHERE wsc.clientid = $company->ID
                ");
                $company->work_groups = $query3->result();

                $company->services = '';
                $query4 = $this->db->query("SELECT crm_type_of_services.*, crm_services.name 
                    FROM crm_type_of_services
                    LEFT JOIN crm_companies ON crm_companies.id = crm_type_of_services.company_id
                    LEFT JOIN crm_services ON crm_services.id = crm_type_of_services.service_id
                    WHERE (crm_type_of_services.company_id = $company->ID)
                ");
                $company->services = $query4->result();
                $data[] = $company;
            }
            return $data;
        } else {
            return null;
        }
    }

    public function former_clientdatatable($limit, $start, $col, $dir, $leadstatus, $leadform) {
        $_query = "";
        if (!empty($leadstatus)) {
            $a = array_diff($leadstatus, array(''));
            $b = implode(',', $a);
            $_query .= "AND (com.clientstatus_id IN($b)) ";
        }
        // if (!empty($leadform)) {
        //     $this->db->where_in('com.form_id', $leadform);
        // }
        $query = $this->db->query("SELECT * FROM (SELECT com.*, com.account as name, con.fullname as contact, frm.name as leadform, ind.name as industry, csta.name as clientstatus, sta.name as cstatus
                FROM crm_companies as com
                LEFT JOIN crm_contacts as con ON con.company_id = com.id   
                LEFT JOIN crm_forms as frm ON frm.id = com.form_id   
                LEFT JOIN crm_industries as ind ON ind.id = com.industry_id
                LEFT JOIN crm_statuses as sta ON sta.id = com.status_id  
                LEFT JOIN crm_client_statuses as csta ON csta.id = com.clientstatus_id  
                WHERE (com.clientstatus_id = 4)
                AND (com.account = com.entity)
                $_query) 
                AS data
                GROUP BY name
                ORDER BY $col $dir
                LIMIT $start, $limit
        ");

        if ($query->result() > 0) {
            $data = [];
            foreach($query->result() as $company) {
                $company->assignedto = '';
                $query2 = $this->db->query("SELECT intra.employee_info.* 
                    FROM crm_company_user
                    LEFT JOIN intra.employee_info ON crm_company_user.user_id = intra.employee_info.ID 
                    WHERE (crm_company_user.company_id = $company->ID)
                ");
                $company->assignedto = $query2->result();

                $company->work_groups = '';
                $query3 = $this->db->query("SELECT ugs.* 
                    FROM intra.workspace_client_tbl as wsc 
                    LEFT JOIN intra.user_group as ugs ON ugs.GID = wsc.groupid
                    WHERE wsc.clientid = $company->ID
                ");
                $company->work_groups = $query3->result();

                $company->services = '';
                $query4 = $this->db->query("SELECT crm_type_of_services.*, crm_services.name 
                    FROM crm_type_of_services
                    LEFT JOIN crm_companies ON crm_companies.id = crm_type_of_services.company_id
                    LEFT JOIN crm_services ON crm_services.id = crm_type_of_services.service_id
                    WHERE (crm_type_of_services.company_id = $company->ID)
                ");
                $company->services = $query4->result();
                $data[] = $company;
            }
            return $data;
        } else {
            return null;
        }
    }

    public function former_clientcount($leadstatus, $leadform) {
        $_query = "";
        if (!empty($leadstatus)) {
            $a = array_diff($leadstatus, array(''));
            $b = implode(',', $a);
            $_query .= "AND (com.clientstatus_id IN(".$b.")) ";
        }
        // if (!empty($leadform)) {
        //     $this->db->where_in('com.form_id', $leadform);
        // }
        $query = $this->db->query("SELECT *
            FROM crm_companies as com
            WHERE (com.clientstatus_id = 4)
            AND (com.account = com.entity)
            $_query
        ");

        return $query->num_rows();
    }

    public function former_client_contacts_search_count($search, $leadstatus, $leadform) {
        $_query = "";
        if (!empty($leadstatus)) {
            $a = array_diff($leadstatus, array(''));
            $b = implode(',', $a);
            $_query .= "AND (com.clientstatus_id IN($b)) ";
        }
        // if (!empty($leadform)) {
        //     $this->db->where_in('com.form_id', $leadform);
        // }
        $_query_serach = "";
        if (!empty($search)) {
            $x = 0;
            foreach ($this->column_search as $s_col) {
                if ($x == 0) {
                    $_query_serach .= "WHERE ($s_col LIKE '%".$search."%') ";
                } else {
                    $_query_serach .= "OR ($s_col LIKE '%".$search."%') ";
                }
                $x++;
            }
        }
        $query = $this->db->query("SELECT * FROM (SELECT com.*, com.account as name, con.fullname as contact, frm.name as leadform, ind.name as industry, sta.name as cstatus, csta.name as clientstatus
                FROM crm_companies as com
                LEFT JOIN crm_contacts as con ON con.company_id = com.id   
                LEFT JOIN crm_forms as frm ON frm.id = com.form_id   
                LEFT JOIN crm_industries as ind ON ind.id = com.industry_id
                LEFT JOIN crm_statuses as sta ON sta.id = com.status_id  
                LEFT JOIN crm_client_statuses as csta ON csta.id = com.clientstatus_id
                WHERE (com.clientstatus_id = 4)
                AND (com.account = com.entity)
                $_query) 
                AS data
                $_query_serach
        ");

        return $query->num_rows();
    }

    public function former_clientdatatablesearch($limit, $start, $search, $col, $dir, $leadstatus, $leadform) {
        $_query = "";
        if (!empty($leadstatus)) {
            $a = array_diff($leadstatus, array(''));
            $b = implode(',', $a);
            $_query .= "AND com.clientstatus_id IN($b) ";
        }
        // if (!empty($leadform)) {
        //     $this->db->where_in('com.form_id', $leadform);
        // }
        $_query_serach = "";
        if (!empty($search)) {
            $x = 0;
            foreach ($this->column_search as $s_col) {
                if ($x == 0) {
                    $_query_serach .= "WHERE ($s_col LIKE '%".$search."%') ";
                } else {
                    $_query_serach .= "OR ($s_col LIKE '%".$search."%') ";
                }
                $x++;
            }
        }
        $query = $this->db->query("SELECT * FROM (SELECT com.*, com.account as name, con.fullname as contact, frm.name as leadform, ind.name as industry, sta.name as cstatus, csta.name as clientstatus
                FROM crm_companies as com
                LEFT JOIN crm_contacts as con ON con.company_id = com.id   
                LEFT JOIN crm_forms as frm ON frm.id = com.form_id   
                LEFT JOIN crm_industries as ind ON ind.id = com.industry_id
                LEFT JOIN crm_statuses as sta ON sta.id = com.status_id  
                LEFT JOIN crm_client_statuses as csta ON csta.id = com.clientstatus_id  
                WHERE (com.clientstatus_id = 4)
                AND (com.account = com.entity)
                $_query) 
                AS data
                $_query_serach
                GROUP BY name
                ORDER BY $col $dir
                LIMIT $start, $limit
        ");

        if ($query->num_rows() > 0) {
            $data = [];
            foreach($query->result() as $company) {
                $company->assignedto = '';
                $query2 = $this->db->query("SELECT intra.employee_info.* 
                    FROM crm_company_user
                    LEFT JOIN intra.employee_info ON crm_company_user.user_id = intra.employee_info.ID 
                    WHERE crm_company_user.company_id = $company->ID
                ");
                $company->assignedto = $query2->result();

                $company->work_groups = '';
                $query3 = $this->db->query("SELECT ugs.* 
                    FROM intra.workspace_client_tbl as wsc 
                    LEFT JOIN intra.user_group as ugs ON ugs.GID = wsc.groupid
                    WHERE wsc.clientid = $company->ID
                ");
                $company->work_groups = $query3->result();

                $company->services = '';
                $query4 = $this->db->query("SELECT crm_type_of_services.*, crm_services.name 
                    FROM crm_type_of_services
                    LEFT JOIN crm_companies ON crm_companies.id = crm_type_of_services.company_id
                    LEFT JOIN crm_services ON crm_services.id = crm_type_of_services.service_id
                    WHERE (crm_type_of_services.company_id = $company->ID)
                ");
                $company->services = $query4->result();
                $data[] = $company;
            }
            return $data;
        } else {
            return null;
        }
    }

    public function contacts($id)
    {
        $client_query = $this->db->query("SELECT * FROM crm_companies WHERE ID = $id");
        $client = $client_query->row();
        $query1 = $this->db->query("SELECT ID FROM crm_companies WHERE account = '{$client->account}'");
        $companies = $query1->result();
        $entity_ids = [];
        foreach($companies as $company) {
            $entity_ids[] = $company->ID;
        }
        $ids = implode(', ',$entity_ids);
        return $this->db->query("SELECT crm_contacts.*, crm_companies.entity FROM crm_contacts 
                        LEFT JOIN crm_companies ON crm_companies.ID = crm_contacts.company_id
                        WHERE company_id IN ($ids) ORDER BY crm_companies.entity");
    }

    public function delete($id) 
    {
        $data = ['status_id' => 8];
        $this->db->where('id', $id);
        return $this->db->update('crm_companies', $data);
    }

    public function work_groups()
    {
        $query = $this->db->query("SELECT * FROM intra.user_group");
        $this->db->order_by('group_name');
        return $query->result();
    }

    public function last_update($id)
    {
        $this->db->where('ID', $id);
        return $this->db->update('crm_companies', ['updated_at' => date('Y-m-d h:i:s A')]);
    }

    public function get_ar($client_id)
    {
        return $this->db->query("SELECT * FROM crm_kpi_ar_details_tbl AS a LEFT JOIN (SELECT * FROM intra.user_group) as b ON b.GID = a.groupid  WHERE a.clientid = '" . $client_id . "' GROUP BY invoice_no ORDER BY a.ID DESC ");
    }

    public function get_ar_invoice($invoice, $clientid)
    {
        return $this->db->query("SELECT * FROM crm_kpi_ar_details_tbl AS a LEFT JOIN (SELECT * FROM intra.user_group) as b ON b.GID = a.groupid  WHERE a.invoice_no= '" . $invoice . "' and a.clientid= '" . $clientid . "' ORDER BY a.ID DESC ");
    }

    public function get_transactions($client_id)
    {
        $client_query = $this->db->query("SELECT * FROM crm_companies WHERE ID = $client_id");
        $client = $client_query->row();

        $query1 = $this->db->query("SELECT ID FROM crm_companies WHERE account = '{$client->account}'");
        $companies = $query1->result();
        $entity_ids = [];
        foreach($companies as $company) {
            $entity_ids[] = $company->ID;
        }


        $query = $this->db->query("SELECT crm_transactions.*, SUM(crm_transactions.line_item_amount) as total, crm_companies.entity, intra.user_group.group_name 
                            FROM crm_transactions
                            LEFT JOIN crm_companies ON crm_companies.ID = crm_transactions.client_id 
                            LEFT JOIN intra.user_group ON intra.user_group.GID = crm_transactions.group_id  
                            WHERE crm_transactions.client_id IN (".implode(', ',$entity_ids).")
                            AND crm_transactions.transaction_type = 'invoices' 
                            GROUP BY crm_transactions.invoice_number 
                            ORDER BY crm_transactions.transaction_date DESC ");
        $transactions = $query->result();

        $data = [];
        foreach($transactions as $transaction) {
            $transaction->payments = '';
            $query2 = $this->db->query("SELECT SUM(crm_transactions.line_item_amount) as payments
                            FROM crm_transactions
                            WHERE crm_transactions.client_id = '" . $client_id . "' 
                            AND crm_transactions.invoice_number = '". $transaction->invoice_number ."'
                            AND crm_transactions.transaction_type = 'payments' 
                            GROUP BY crm_transactions.invoice_number 
                            ORDER BY crm_transactions.ID DESC");
            $payment = $query2->row();
            $transaction->payments = $payment->payments ?? 0;
            $transaction->balance = $transaction->total - ($payment->payments ?? 0);
            $data[] = $transaction;
        }
        return $data;

    
        // return $this->db->query("SELECT crm_transactions.*, SUM(crm_transactions.line_item_amount) as total, crm_companies.entity, intra.user_group.group_name 
        //                     FROM crm_transactions
        //                     LEFT JOIN crm_companies ON crm_companies.ID = crm_transactions.client_id 
        //                     LEFT JOIN intra.user_group ON intra.user_group.GID = crm_transactions.group_id  
        //                     WHERE crm_transactions.client_id = '" . $client_id . "' 
        //                     AND crm_transactions.transaction_type = 'invoices' 
        //                     GROUP BY invoice_number 
        //                     ORDER BY crm_transactions.ID DESC ");
    }

    public function get_transaction_invoices($invoice, $client_id)
    {
        return $this->db->query("SELECT crm_transactions.*, crm_companies.entity, intra.user_group.group_name 
                            FROM crm_transactions
                            LEFT JOIN crm_companies ON crm_companies.ID = crm_transactions.client_id 
                            LEFT JOIN intra.user_group ON intra.user_group.GID = crm_transactions.group_id  
                            WHERE crm_transactions.invoice_number = '" . $invoice . "' 
                            AND crm_transactions.client_id = '" . $client_id . "' 
                            ORDER BY crm_transactions.transaction_type ASC ");
    }

    public function get_all_clients()
    {
        $clients = $this->db->query("SELECT * FROM crm_companies WHERE status_id = 6 ORDER BY entity");
        return $clients->result();
    }

    public function export($clients)
    {
        $data = array();
        $ids = implode(", ", $clients);
        $clients = $this->db->query("SELECT '' as 'Active?',
                                            a.entity as 'Customer Name*', 
                                            a.ID as 'Customer ID', 
                                            '' as 'Parent Customer Name', 
                                            '' as 'Parent Customer ID', 
                                            a.entity as 'Company Name', 
                                            '' as 'First Name', 
                                            '' as 'Last Name', 
                                            '' as 'Account Number', 
                                            a.full_address as 'Billing Address', 
                                            '' as 'billAddress2', 
                                            '' as 'billAddress3', 
                                            '' as 'billAddress4', 
                                            '' as 'City', 
                                            '' as 'State', 
                                            '' as 'Country', 
                                            '' as 'Zip', 
                                            a.full_address as 'Shipping Address', 
                                            '' as 'shipAddress2', 
                                            '' as 'shipAddress3', 
                                            '' as 'shipAddress4', 
                                            '' as 'Customer City', 
                                            '' as 'Customer State', 
                                            '' as 'Customer Country', 
                                            '' as 'Customer Zip', 
                                            a.email as 'Email', 
                                            a.phone as 'Phone', 
                                            '' as 'Alternate Phone', 
                                            '' as 'Fax', 
                                            '' as 'Description', 
                                            '' as 'Print As', 
                                            '' as 'Type', 
                                            '' as 'Payment Terms Name', 
                                            '' as 'Default Invoice Delivery Method', 
                                            '' as 'Is Auto Charge Dismissed', 
                                            '' as 'Is Tax Liable'
                                            FROM crm_companies as a 
                                            WHERE a.ID IN ($ids)  
                                            ORDER BY a.entity");
        // $query = $this->db->get();
        $results = $clients->result_array();
        foreach($results as $res){
            $data[] = $res;
        }
        return $data;
    }

    public function find($name)
    {
        $query = $this->db->query("SELECT * FROM crm_companies WHERE entity = '$name'");
        return $query->row();
    }
}

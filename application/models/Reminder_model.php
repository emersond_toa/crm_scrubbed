<?php
class Reminder_model extends CI_Model
{
	public function __construct()
	{
        $this->load->database();
        $this->load->model('Models');
    }

    public function store($data, $tags) {
        $this->db->insert('crm_reminders', $data);
        $insert_id = $this->db->insert_id();

        $row_data = array(
            'reminder_id' => $insert_id
        );

        if ($tags) {
            foreach ($tags as $tag) {
                $row_data['user_id'] = $tag;
                $this->db->insert('crm_reminder_user', $row_data);
            }
        }
        return true;
    }

    public function client_reminders($id) {
        $this->db->select('*');
        $this->db->from('crm_reminders');
        $this->db->where('company_id', $id);
        $query = $this->db->get();

        $data = [];
        foreach ($query->result() as $reminder) {
            $reminder->users = '';
            $query2 = $this->db->query("SELECT * FROM crm_reminder_user WHERE reminder_id = '{$reminder->id}' ");
            $users = [];
            foreach($query2->result() as $q2) {
                $users[] = $this->Models->get_user_info($q2->user_id);
            }
            $reminder->users = $users;
            $data[] = $reminder;
        }
        return $data;
    }

    public function show($id) {
        $this->db->select('*');
        $this->db->from('crm_reminders');
        $this->db->where('id', $id);
        $query = $this->db->get();

        $data = [];
        foreach ($query->result() as $reminder) {
            $reminder->users = '';
            $query2 = $this->db->query("SELECT * FROM crm_reminder_user WHERE reminder_id = '{$reminder->id}' ");
            $users = [];
            foreach($query2->result() as $q2) {
                $users[] = $this->Models->get_user_info($q2->user_id);
            }
            $reminder->users = $users;
            $data[] = $reminder;
        }
        return $data;
    }

    public function update($id, $data, $tags) {
        $this->db->where('id', $id);
        $this->db->update('crm_reminders', $data);

        // delete pivot 
        $this->db->where('reminder_id', $id);
        $this->db->delete('crm_reminder_user');

        $row_data = array(
            'reminder_id' => $id
        );

        if ($tags) {
            foreach ($tags as $tag) {
                $row_data['user_id'] = $tag;
                $this->db->insert('crm_reminder_user', $row_data);
            }
        }
        return true;
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('crm_reminders');
        return true;
    }
    public function update_crm_reminders($id,$data) {
        $this->db->where('id', $id);
        $this->db->update('crm_reminders',$data);
        return true;
    }
    public function crm_reminder_users_crontab($id) {
        return $this->db->query("SELECT * FROM crm_reminder_user as main WHERE main.reminder_id = '{$id}' ");
    }

    public function reminder_crontab() {
        return $this->db->query('SELECT * FROM crm_reminders as main WHERE main.done = "0" ');
    }

}

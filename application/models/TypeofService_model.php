<?php
class TypeofService_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
    }
    
    public function store($data)
    {
        $this->db->insert('crm_type_of_services', $data);
        return true;
    }

    public function client_services($id)
    {
        $client_query = $this->db->query("SELECT * FROM crm_companies WHERE ID = $id");
        $client = $client_query->row();
        $query1 = $this->db->query("SELECT ID FROM crm_companies WHERE account = '{$client->account}'");
        $companies = $query1->result();
        $entity_ids = [];
        foreach($companies as $company) {
            $entity_ids[] = $company->ID;
        }

        $this->db->select('crm_type_of_services.*, crm_services.name as service, crm_type_of_agreements.name as agreement, crm_companies.entity');
        $this->db->from('crm_type_of_services');
        $this->db->join('crm_companies', 'crm_companies.ID = crm_type_of_services.company_id');
        $this->db->join('crm_services', 'crm_services.id = crm_type_of_services.service_id');
        $this->db->join('crm_type_of_agreements', 'crm_type_of_services.agreement_id = crm_type_of_agreements.id');
        // $this->db->where('company_id', $id);
        $this->db->where_in('crm_type_of_services.company_id', $entity_ids);
        $this->db->order_by('crm_companies.entity', 'asc');
        $services = $this->db->get();
        return $services->result();
    }

    public function delete($id) 
    {
        $this->db->where('id', $id);
        $this->db->delete('crm_type_of_services');
        return true;
    }

    public function show($id) {
        $this->db->where('id', $id);
        $service = $this->db->get('crm_type_of_services');
        return $service->result();
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('crm_type_of_services', $data);
        return true;
    }
}

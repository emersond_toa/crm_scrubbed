<?php

class Entity_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        $this->load->model('Models');
    }

    public function store($data) {
        $this->db->insert('crm_companies', $data);
        return true;
    }

    public function entity_cx_reviewed($id) {
        return $this->db->query("SELECT ID,CONCAT(first_name,' ',last_name) AS fullname FROM `intra`.`employee_info` WHERE ID = '{$id}' || email = '{$id}' ");
    }

    function model_check_rewier($uid) {
        return $this->db->query("
                 
            SELECT ID,reviewed_by,userinfo.fullname,reviewed_timestamp FROM `crm_database`.`crm_client_experience` AS client
            
            LEFT JOIN (SELECT ID,CONCAT(first_name,' ',last_name) AS fullname FROM `intra`.`employee_info`) AS userinfo ON  userinfo.ID = client.reviewed_by
            
            WHERE client.UID = '{$uid}'
                 
                 
        ");
    }

    function check_if_exist_cx($month, $year, $groupid, $clientid) {
        return $this->db->query("SELECT * FROM `crm_database`.`crm_client_experience` WHERE month = '{$month}' and year = '{$year}' and groupid = '{$groupid}' and clientid = '{$clientid}'");
    }

    function model_entity_update($uid, $data) {
        $this->db->where(["UID" => $uid]);
        return $this->db->update('crm_database.crm_client_experience', $data);
    }

    function model_entity_insert($data) {
        return $this->db->insert('crm_database.crm_client_experience', $data);
    }

    public function entity_cx($month, $year, $clientid) {
        return $this->db->query("
            
                SELECT * FROM `crm_database`.`crm_client_experience` AS experience
                    
                LEFT JOIN (SELECT * FROM `intra`.`workspace_client_tbl`) AS workspace ON workspace.clientid = experience.clientid
                
                WHERE experience.month = '{$month}' and experience.year ='{$year}' and experience.clientid = '{$clientid}' LIMIT 1

        ");
    }

    public function client_entities($id) {
        $query = $this->db->query("SELECT * FROM crm_companies WHERE ID = $id");
        $company = $query->row();

        $query1 = $this->db->query("
            
                SELECT *,company.ID AS CID FROM crm_companies AS company
                
                LEFT JOIN (SELECT * FROM `intra`.`workspace_client_tbl`) AS workspace ON workspace.clientid = company.ID
                
                WHERE company.account = '{$company->account}' AND company.entity != '{$company->account}' GROUP BY company.entity ORDER BY entity
                    
        ");
        $companies = $query1->result();
        return $companies;
    }

    public function client_all_entities($id) {
        $query = $this->db->query("SELECT * FROM crm_companies WHERE ID = $id");
        $company = $query->row();

        $query1 = $this->db->query("SELECT * FROM crm_companies WHERE account = '{$company->account}' ORDER BY entity");
        $companies = $query1->result();
        return $companies;
    }

    public function show($id) {
        $this->db->select('*');
        $this->db->from('crm_companies');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('crm_companies', $data);
        return true;
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('crm_companies');
        return true;
    }

}

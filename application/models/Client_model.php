<?php
class Client_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_client($id)
	{
		$this->db->select("con.fullname as name, com.name as company, con.email as email, con.jobtitle as jobtitle", FALSE);
		$this->db->from('crm_contacts as con');
		$this->db->join('crm_companies as com', 'com.id = con.company_id', 'left');
		$this->db->where('con.id', $id);
		$query = $this->db->get();
        return $query->result();
	}

	public function getActiveClients()
	{
		$this->db->select("*");
		$this->db->from('intra.crm_hubspot_tbl');
		return $this->db->get()->result();
	}	

	public function getClient($id)
	{
		$this->db->select("*");
		$this->db->from('intra.xero_client_list_tbl');
		$this->db->where('ID', $id);
		return $this->db->get()->row();
	}	
}

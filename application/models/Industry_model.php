<?php
class Industry_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

    public function all_industries()
    {
        $this->db->order_by('name');
        $query = $this->db->get('crm_industries');
        return $query->result();
    }
}

<?php

header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');

class API_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }

    function model_revenue($month, $year, $group, $type) {
        return $this->db->query("
            
            SELECT *,get_year,get_month,groupid,SUM(line_item_amount) as total_amount FROM
            
            (SELECT *,FROM_UNIXTIME(UNIX_TIMESTAMP(transaction_date),'%Y') AS get_year,FROM_UNIXTIME(UNIX_TIMESTAMP(transaction_date),'%c') AS get_month FROM `crm_transactions` AS transaction 
            
            LEFT JOIN (SELECT groupid,clientid FROM `intra`.`workspace_client_tbl`) AS work ON work.clientid = transaction.client_id) AS main 
            
            WHERE get_year = '{$year}' and transaction_type = '{$type}' and get_month = '{$month}' and groupid IN ({$group}) GROUP BY get_month 

        ");
    }

    function model_experience($month, $year, $group, $field_name, $field_value) {
        // LEFT JOIN (SELECT groupid AS gid,clientid AS cid FROM `intra`.`workspace_client_tbl`) AS work ON work.cid = experience.clientid) AS main 
        return $this->db->query("
            
           SELECT COUNT(*) AS total_count,get_year,get_month FROM
            
           (SELECT *,year AS get_year,month AS get_month FROM `crm_database`.`crm_client_experience` as experience
           
           LEFT JOIN (SELECT groupid AS gid,clientid AS cid FROM `intra`.`workspace_client_tbl`) AS work ON work.cid = experience.clientid) AS main 

           WHERE get_year = '{$year}' and gid IN ({$group}) and get_month = '{$month}' and $field_name = '{$field_value}' GROUP BY get_month 


        ");
    }

    function model_exprnc($month, $year,$group, $key, $value) {
        return $this->db->query("
            
            SELECT COUNT(*) AS value FROM `crm_database`.`crm_client_experience` WHERE month='{$month}' and year='{$year}' and $key='$value' and groupid IN ({$group}) GROUP BY month
                        
        ");
    }
    function model_exprnc_sum($month, $year,$group, $key) {
        return $this->db->query("
            
            SELECT *,SUM($key) AS value FROM `crm_database`.`crm_client_experience` WHERE month='{$month}' and year='{$year}' and groupid IN ({$group}) GROUP BY month
                        
        ");
    }
}

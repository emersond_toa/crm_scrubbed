<?php

class Contact_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        // $this->column_search = array('con.fullname', 'com.name', 'ser.name', 'ind.name', 'usr.firstname', 'usr.lastname', 'sta.name');
    }

    // public function status_badge($status) {
    //     $result = '';
    //     if ($status == 'New Lead') {
    //         $result = '<span class="badge badge-warning">' . $status . '</span>';
    //     } elseif ($status == 'Called') {
    //         $result = '<span class="badge badge-dark">' . $status . '</span>';
    //     } elseif ($status == 'Proposal') {
    //         $result = '<span class="badge badge-secondary">' . $status . '</span>';
    //     } elseif ($status == 'Agreement') {
    //         $result = '<span class="badge badge-info">' . $status . '</span>';
    //     } elseif ($status == 'Signed') {
    //         $result = '<span class="badge badge-success">' . $status . '</span>';
    //     } elseif ($status == 'Declined') {
    //         $result = '<span class="badge badge-default">' . $status . '</span>';
    //     } elseif ($status == 'Qualified Lead') {
    //         $result = '<span class="badge badge-primary">' . $status . '</span>';
    //     } elseif ($status == 'Unqualified Lead') {
    //         $result = '<span class="badge badge-danger">' . $status . '</span>';
    //     } else {
    //         $result = '<span class="badge badge-light">' . $status . '</span>';
    //     }

    //     return $result;
    // }

    // public function allcontacts_count($leadstatus, $leadservice) {
    //     $this->db->select("*");
    //     $this->db->from('crm_contacts');
    //     if (!empty($leadstatus)) {
    //         $this->db->where_in('status_id', $leadstatus);
    //     }
    //     if (!empty($leadservice)) {
    //         $this->db->where_in('service_id', $leadservice);
    //     }
    //     $query = $this->db->get();

    //     return $query->num_rows();
    // }

    // public function allcontacts($limit, $start, $col, $dir, $leadstatus, $leadservice) {
    //     $this->db->select("con.id as id, con.fullname, com.name as company, ser.name as service, ind.name as industry, CONCAT(usr.firstname ,' ', usr.lastname) as assignedto, sta.name as status", FALSE);
    //     $this->db->from('crm_contacts as con');
    //     $this->db->join('crm_companies as com', 'com.id = con.company_id', 'left');
    //     $this->db->join('crm_services as ser', 'ser.id = con.service_id', 'left');
    //     $this->db->join('crm_industries as ind', 'ind.id = com.industry_id', 'left');
    //     $this->db->join('crm_users as usr', 'usr.id = con.assigned_id', 'left');
    //     $this->db->join('crm_statuses as sta', 'sta.id = con.status_id', 'left');
    //     if (!empty($leadstatus)) {
    //         $this->db->where_in('con.status_id', $leadstatus);
    //     }
    //     if (!empty($leadservice)) {
    //         $this->db->where_in('con.service_id', $leadservice);
    //     }
    //     $this->db->limit($limit, $start);
    //     $this->db->order_by($col, $dir);
    //     $query = $this->db->get();

    //     if ($query->num_rows() > 0) {
    //         return $query->result();
    //     } else {
    //         return null;
    //     }
    // }

    // public function contacts_search($limit, $start, $search, $col, $dir, $leadstatus, $leadservice) {
    //     $this->db->select("con.id as id, con.fullname, com.name as company, ser.name as service, ind.name as industry, CONCAT(usr.firstname ,' ', usr.lastname) as assignedto, sta.name as status", FALSE);
    //     $this->db->from('crm_contacts as con');
    //     $this->db->join('crm_companies as com', 'com.id = con.company_id', 'left');
    //     $this->db->join('crm_services as ser', 'ser.id = con.service_id', 'left');
    //     $this->db->join('crm_industries as ind', 'ind.id = com.industry_id', 'left');
    //     $this->db->join('crm_users as usr', 'usr.id = con.assigned_id', 'left');
    //     $this->db->join('crm_statuses as sta', 'sta.id = con.status_id', 'left');
    //     if (!empty($leadstatus)) {
    //         $this->db->where_in('con.status_id', $leadstatus);
    //     }
    //     if (!empty($leadservice)) {
    //         $this->db->where_in('con.service_id', $leadservice);
    //     }
    //     if (!empty($search)) {
    //         $x = 0;
    //         $this->db->group_start();
    //         foreach ($this->column_search as $s_col) {
    //             if ($x == 0) {
    //                 $this->db->like($s_col, $search);
    //             } else {
    //                 $this->db->or_like($s_col, $search);
    //             }
    //             $x++;
    //         }
    //         $this->db->group_end();
    //     }
    //     $this->db->limit($limit, $start);
    //     $this->db->order_by($col, $dir);
    //     $query = $this->db->get();


    //     if ($query->num_rows() > 0) {
    //         return $query->result();
    //     } else {
    //         return null;
    //     }
    // }

    // removed
    // function contacts_search_count($search)
    // {
    //     $query = $this
    //             ->db
    //             ->like('id',$search)
    //             ->or_like('firstname',$search)
    //             ->get('contacts');
    //     return $query->num_rows();
    // } 

    // public function get_contact($id) {
    //     $this->db->select("con.*, com.name as company, nte.content as content", FALSE);
    //     $this->db->from('crm_contacts as con');
    //     $this->db->join('crm_companies as com', 'com.id = con.company_id', 'left');
    //     $this->db->join('crm_notes as nte', 'con.id = nte.contact_id', 'left');
    //     $this->db->where('con.id', $id);
    //     $query = $this->db->get();
    //     return $query->result();
    // }

    // function add_existing_company($id, $data) {
    //     $this->db->where('id', $id);
    //     $this->db->update('crm_contacts', $data);
    // }

    // function remove_company($id, $data) {
    //     $this->db->where('id', $id);
    //     $this->db->update('crm_contacts', $data);
    // }

    public function add_client($data) {
        $this->db->insert('crm_contacts', $data);
    }

    public function add_company($data) {
        $this->db->insert('crm_companies', $data);
    }

    public function get_added_client($ref = '') {
        return $this->db->query("SELECT * FROM crm_companies WHERE ref_id ='{$ref}' ");
    }

    public function add_notes($data) {
        $this->db->insert('crm_notes', $data);
    }

    public function select_conversation($id) {
        return $this->db->query("SELECT * FROM crm_conversation WHERE company_id = '{$id}' ORDER BY `id` DESC");
    }

    public function output_conversation($id) {
        return $this->db->query("SELECT * FROM crm_conversation WHERE company_id = '{$id}' ORDER BY `id` DESC LIMIT 1");
    }

    public function insert_conversation($data) {
        return $this->db->insert("crm_conversation",$data);
    }

    public function store($data)
    {
        return $this->db->insert('crm_contacts', $data);
    }

    public function show($id) {
        return $this->db->query("SELECT * FROM crm_contacts WHERE id = '{$id}' ");
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('crm_contacts', $data);
    }

    public function update_conversation($data,$id) {
        $this->db->where('id', $id);
        return $this->db->update("crm_conversation",$data);
    }
    public function delete_conversation($id) {
        $this->db->where('id', $id);
        return $this->db->delete("crm_conversation");
    }
    public function edit_conversation($data,$id) {
        $cna = $this->db->query("SELECT * FROM crm_notes WHERE company_id = $id")->row();
        if($cna){
            $this->db->where('company_id', $id);
            return $this->db->update("crm_notes",$data);
        } else {
            $data['company_id'] = $id;
            $data['title'] = 'CNA';
            return $this->db->insert('crm_notes', $data);
        }
    }
    public function delete_contact($id) {
        $this->db->where('id', $id);
        return $this->db->delete("crm_contacts");
    }
}

<?php
class Form_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

    public function all_forms()
    {
        $query = $this->db->get('crm_forms');
        return $query->result();
    }
}

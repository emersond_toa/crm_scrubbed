<?php

header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Manila');
defined('BASEPATH') OR exit('No direct script access allowed');

class Models extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }

    function login_with_crm($token) {
        $CI = &get_instance();
        $this->db2 = $CI->load->database('default2', TRUE);
        return $this->db2->query("SELECT * FROM employee_info WHERE crm_token = '{$token}' ");
    }

    function get_user_info($id) {
        $CI = &get_instance();
        $this->db2 = $CI->load->database('default2', TRUE);
        $request = $this->db2->query("SELECT * FROM employee_info WHERE ID = '{$id}' ");
        return $request->row();
    }

    function contact_information($id) {
        // return $this->db->query(" SELECT company.*, contact.fullname as contactperson, contact.jobtitle as contactjobtitle, contact.email as contactemail, industry.name as industry, notes.content as content, referrals.name as referral, sources.name as source, reason.name as terminationreason
        $query = $this->db->query(" SELECT company.*, contact.fullname as contactperson, contact.jobtitle as contactjobtitle, contact.email as contactemail, industry.name as industry, notes.content as content, referrals.name as referral, sources.name as source, reason.name as terminationreason
            FROM crm_companies as company
            LEFT JOIN crm_contacts as contact ON contact.company_id = company.id  
            LEFT JOIN crm_notes as notes ON notes.company_id = company.id   
            LEFT JOIN crm_industries as industry ON industry.id = company.industry_id
            LEFT JOIN crm_referral_partners as referrals ON referrals.id = company.referral_partner_id  
            LEFT JOIN crm_lead_sources as sources ON sources.id = company.lead_source_id   
            LEFT JOIN crm_termination_reasons as reason ON reason.id = company.termination_reason_id   
            WHERE company.id = '{$id}'
        ");
        $companies = $query->result();

        $data = [];
        foreach ($companies as $company) {
            $company->work_groups = '';
            $query2 = $this->db->query("SELECT ugs.* 
                FROM intra.workspace_client_tbl as wsc 
                LEFT JOIN intra.user_group as ugs ON ugs.GID = wsc.groupid
                WHERE wsc.clientid = $company->ID
            ");

            $work_groups = [];
            foreach ($query2->result() as $work) {
                $work_groups[] = $work->group_name;
            }
            $company->work_groups = implode(", ", $work_groups);
            $data[] = $company;
        }

        return $data;
    }

    function crm_industries() {
        return $this->db->query("SELECT * FROM crm_industries");
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function add_client($client) {
        $this->db->insert('crm_client_website_form', $client);
    }

    function update_client_assesment($client, $cid) {
        $this->db->where(array('CAID' => $cid));
        $this->db->update('crm_client_assesment', $client);
    }

    function add_client_assesment($client) {
        $this->db->insert('crm_client_assesment', $client);
    }

    function add_crm_client_tax($client) {
        $this->db->insert('crm_client_tax', $client);
    }

    function update_client_tax($cid, $client) {
        $this->db->where(array('TID' => $cid));
        $this->db->update('crm_client_tax', $client);
    }

    function update_client($client, $cid) {
        $this->db->where(array('CID' => $cid));
        $this->db->update('crm_client_website_form', $client);
    }

    function delete_client_acc($cid) {
        $this->db->where(array('CAID' => $cid));
        $this->db->delete('crm_client_assesment');
    }

    function delete_client_tax($cid) {
        $this->db->where(array('TID' => $cid));
        $this->db->delete('crm_client_tax');
    }

    function page() {
        if (!$this->input->get('page')) {
            return '1';
        } else if (is_numeric($this->input->get('page'))) {
            return ltrim(rtrim($this->input->get('page')));
        } else {
            return '1';
        }
    }

    function max_row() {
        if (!($this->uri->segment(4))) {
            return '15';
        } else if (is_numeric($this->uri->segment(4))) {
            return ltrim(rtrim($this->uri->segment(4)));
        } else {
            return '15';
        }
    }

    function quotes($string) {
        $chars = array("'", '"', '\\');
        return str_replace($chars, '', $string);
    }

    function keyword() {
        if (!($this->input->get('keyword'))) {
            return '';
        } else {
            return $this->quotes($this->input->get('keyword'));
        }
    }

    function web_form_leads() {
        $setLimit = $this->max_row();
        $srh = explode(" ", $this->keyword());
        $pageLimit = ($this->page() * $setLimit) - ($setLimit);

        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "`Contact_Person` LIKE '%" . $field . "%'";
                $conditions[] = "`Email_Address` LIKE '%" . $field . "%'";
                $conditions[] = "`Company_Legal` LIKE '%" . $field . "%'";
            }
        }
        $_query = "SELECT * FROM `crm_client_website_form` ";
        if (count($conditions) > 0) {
            $_query .= "WHERE " . implode(' || ', $conditions);
        } else {
            $_query .= "WHERE status != '1'";
        }
        return $this->db->query($_query . "  LIMIT $pageLimit , $setLimit");
    }

    function user_pagination_leads() {
        $page_url = base_url($this->uri->segment(1) . '');
        $per_page = $this->max_row();
        $page = $this->page();
        $srh = explode(" ", $this->keyword());
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "`Contact_Person` LIKE '%" . $field . "%'";
                $conditions[] = "`Email_Address` LIKE '%" . $field . "%'";
                $conditions[] = "`Company_Legal` LIKE '%" . $field . "%'";
            }
        }
        $_query = "SELECT * FROM `crm_client_website_form` ";
        if (count($conditions) > 0) {
            $_query .= "WHERE " . implode(' || ', $conditions);
        } else {
            $_query .= "WHERE status != '1'";
        }
        $query = $this->db->query($_query);
        $total = $query->num_rows();
        $adjacents = "2";
        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;
        $prev = $page - 1;
        $next = $page + 1;
        $setLastpage = ceil($total / $per_page);
        $lpm1 = $setLastpage - 1;
        $setPaginate = "";
        if ($setLastpage > 1) {
            $setPaginate .= "<ul class='pagination pagination-sm'>";
            $setPaginate .= "<li class='disabled'><a>Page $page of $setLastpage</a></li>";
            if ($setLastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $setLastpage; $counter++) {
                    if ($counter == $page)
                        $setPaginate .= "<li class='active'><a>$counter</a></li>";
                    else
                        $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                }
            }
            elseif ($setLastpage > 5 + ($adjacents * 2)) {
                if ($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active btn-default'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                    $setPaginate .= "<li class='dot disabled'><a class='disabled'>...</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "'>$lpm1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>$setLastpage</a></li>";
                }
                elseif ($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                    $setPaginate .= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "'>1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "'>2</a></li>";
                    $setPaginate .= "<li class='dot disabled'><a>...</a></li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                    $setPaginate .= "<li class='dot disabled'><a>..</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "'>$lpm1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>$setLastpage</a></li>";
                }
                else {
                    $setPaginate .= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "'>1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "'>2</a></li>";
                    $setPaginate .= "<li class='dot disabled'><a>..</a></li>";
                    for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                }
            }
            if ($page < $counter - 1) {
                $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "'>Next</a></li>";
                $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>Last</a></li>";
            } else {
                $setPaginate .= "<li class='disabled'><a class='current_page'>Next</a></li>";
                $setPaginate .= "<li class='disabled'><a class='current_page'>Last</a></li>";
            }
            $setPaginate .= "</ul>\n";
        }
        return $setPaginate;
    }

    function web_form_client() {
        $setLimit = $this->max_row();
        $srh = explode(" ", $this->keyword());
        $pageLimit = ($this->page() * $setLimit) - ($setLimit);

        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "`Contact_Person` LIKE '%" . $field . "%'";
                $conditions[] = "`Email_Address` LIKE '%" . $field . "%'";
                $conditions[] = "`Company_Legal` LIKE '%" . $field . "%'";
            }
        }
        $_query = "SELECT * FROM `crm_client_website_form` ";
        if (count($conditions) > 0) {
            $_query .= "WHERE " . implode(' || ', $conditions) . "and status = '1'";
        } else {
            $_query .= "WHERE status = '1'";
        }
        return $this->db->query($_query . "  LIMIT $pageLimit , $setLimit");
    }

    function user_pagination_clients() {
        $page_url = base_url($this->uri->segment(1) . '');
        $per_page = $this->max_row();
        $page = $this->page();
        $srh = explode(" ", $this->keyword());
        $conditions = array();
        foreach ($srh as $field) {
            if ($field) {
                $conditions[] = "`Contact_Person` LIKE '%" . $field . "%'";
                $conditions[] = "`Email_Address` LIKE '%" . $field . "%'";
                $conditions[] = "`Company_Legal` LIKE '%" . $field . "%'";
            }
        }
        $_query = "SELECT * FROM `crm_client_website_form` ";
        if (count($conditions) > 0) {
            $_query .= "WHERE " . implode(' || ', $conditions);
        } else {
            $_query .= "WHERE status = '1'";
        }
        $query = $this->db->query($_query);
        $total = $query->num_rows();
        $adjacents = "2";
        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;
        $prev = $page - 1;
        $next = $page + 1;
        $setLastpage = ceil($total / $per_page);
        $lpm1 = $setLastpage - 1;
        $setPaginate = "";
        if ($setLastpage > 1) {
            $setPaginate .= "<ul class='pagination pagination-sm'>";
            $setPaginate .= "<li class='disabled'><a>Page $page of $setLastpage</a></li>";
            if ($setLastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $setLastpage; $counter++) {
                    if ($counter == $page)
                        $setPaginate .= "<li class='active'><a>$counter</a></li>";
                    else
                        $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                }
            }
            elseif ($setLastpage > 5 + ($adjacents * 2)) {
                if ($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active btn-default'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                    $setPaginate .= "<li class='dot disabled'><a class='disabled'>...</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "'>$lpm1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>$setLastpage</a></li>";
                }
                elseif ($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                    $setPaginate .= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "'>1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "'>2</a></li>";
                    $setPaginate .= "<li class='dot disabled'><a>...</a></li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                    $setPaginate .= "<li class='dot disabled'><a>..</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "'>$lpm1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>$setLastpage</a></li>";
                }
                else {
                    $setPaginate .= "<li><a href='{$page_url}?page=1&keyword=" . $this->keyword() . "'>1</a></li>";
                    $setPaginate .= "<li><a href='{$page_url}?page=2&keyword=" . $this->keyword() . "'>2</a></li>";
                    $setPaginate .= "<li class='dot disabled'><a>..</a></li>";
                    for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++) {
                        if ($counter == $page)
                            $setPaginate .= "<li class='active'><a>$counter</a></li>";
                        else
                            $setPaginate .= "<li><a href='{$page_url}?page=" . $counter . "&keyword=" . $this->keyword() . "'>$counter</a></li>";
                    }
                }
            }
            if ($page < $counter - 1) {
                $setPaginate .= "<li><a href='{$page_url}?page=" . $lpm1 . "&keyword=" . $this->keyword() . "'>Next</a></li>";
                $setPaginate .= "<li><a href='{$page_url}?page=" . $setLastpage . "&keyword=" . $this->keyword() . "'>Last</a></li>";
            } else {
                $setPaginate .= "<li class='disabled'><a class='current_page'>Next</a></li>";
                $setPaginate .= "<li class='disabled'><a class='current_page'>Last</a></li>";
            }
            $setPaginate .= "</ul>\n";
        }
        return $setPaginate;
    }

    function show_client_infomation($cid) {
        return $this->db->query('SELECT * FROM crm_client_website_form WHERE CID = ' . $cid);
    }

    function information_upload_doc($client) {
        $this->db->insert('crm_client_information_upload_doc', $client);
    }

    function former_information_upload_doc($client) {
        $this->db->insert('crm_former_name_upload_doc', $client);
    }

    function type_of_services_upload_doc($client) {
        $this->db->insert('crm_type_of_service_upload_doc', $client);
    }

    function former_uploaded_files_infomation($cid) {
        return $this->db->query('SELECT * FROM `crm_former_name_upload_doc` WHERE CID = ' . $cid);
    }

    function type_of_service_uploaded_files_infomation($cid) {
        return $this->db->query('SELECT * FROM `crm_type_of_service_upload_doc` WHERE CID = ' . $cid);
    }

    function uploaded_files_infomation($cid) {
        return $this->db->query('SELECT * FROM `crm_client_information_upload_doc` WHERE CID = ' . $cid);
    }

    function add_former_infomation($client) {
        $this->db->insert('crm_client_information_former_name', $client);
    }

    function update_former_infomation($client, $cid) {
        $this->db->where(array('ID' => $cid));
        $this->db->update('crm_client_information_former_name', $client);
    }

    function show_add_former_infomation() {
        return $this->db->query('SELECT * FROM `crm_client_information_former_name` ORDER BY ID DESC LIMIT 1');
    }

    function show_added_former_infomation($cid) {
        return $this->db->query('SELECT * FROM `crm_client_information_former_name` WHERE CID = "' . $cid . '" ORDER BY ID DESC');
    }

    function add_company_address($client) {
        $this->db->insert('crm_client_information_company_address', $client);
    }

    function show_added_company_address($cid) {
        return $this->db->query('SELECT * FROM `crm_client_information_company_address` WHERE CID = "' . $cid . '"  ORDER BY ID DESC');
    }

    function update_company_address($client, $cid) {
        $this->db->where(array('ID' => $cid));
        $this->db->update('crm_client_information_company_address', $client);
    }

    function delete_client_id($cid) {
        $this->db->where(array('CID' => $cid));
        $this->db->delete('crm_client_website_form');
        $this->db->where(array('CID' => $cid));
        $this->db->delete('crm_former_name_upload_doc');
        $this->db->where(array('CID' => $cid));
        $this->db->delete('crm_client_information_upload_doc');

        $this->db->where(array('CID' => $cid));
        $this->db->delete('crm_client_information_former_name');
    }

    function delete_service_id($id) {
        $this->db->where(array('ID' => $id));
        $this->db->delete('crm_type_of_services');
    }

    function delete_service_doc_id($id) {
        $this->db->where(array('ID' => $id));
        $this->db->delete('crm_type_of_service_upload_doc');
    }

    function delete_company_address_id($id) {
        $this->db->where(array('ID' => $id));
        $this->db->delete('crm_client_information_company_address');
    }

    function delete_former_name_id($id) {
        $this->db->where(array('ID' => $id));
        $this->db->delete('crm_client_information_former_name');
    }

    function delete_former_name_doc_id($id) {
        $this->db->where(array('ID' => $id));
        $this->db->delete('crm_former_name_upload_doc');
    }

    function delete_client_doc_id($id) {
        $this->db->where(array('ID' => $id));
        $this->db->delete('crm_client_information_upload_doc');
    }

    function client_check($cid, $client) {
        $this->db->where(array('CID' => $cid));
        $this->db->update('crm_client_website_form', $client);
    }

    function add_type_of_services($client) {
        $this->db->insert('crm_type_of_services', $client);
    }

    function show_added_type_of_services($cid) {
        return $this->db->query('SELECT * FROM `crm_type_of_services` WHERE CID = "' . $cid . '"  ORDER BY ID DESC');
    }

    function update_type_of_services($client, $cid) {
        $this->db->where(array('ID' => $cid));
        $this->db->update('crm_type_of_services', $client);
    }

    function add_reminder($client) {
        $this->db->insert('crm_reminders', $client);
    }

    function show_added_reminder($cid) {
        return $this->db->query('SELECT * FROM `crm_reminders` WHERE CID = "' . $cid . '"  ORDER BY ID DESC');
    }

    function update_reminder($client, $cid) {
        $this->db->where(array('ID' => $cid));
        $this->db->update('crm_reminders', $client);
    }

    function delete_reminder_id($id) {
        $this->db->where(array('ID' => $id));
        $this->db->delete('crm_reminders');
    }

    function leads_status_update($cid, $client) {
        $this->db->where(array('CID' => $cid));
        $this->db->update('crm_client_website_form', $client);
    }

    function leads_number($number, $year = '') {
        return $this->db->query("SELECT *,get_year FROM(SELECT *,FROM_UNIXTIME(UNIX_TIMESTAMP(start_date),'%Y') as get_year FROM crm_companies WHERE status_id = '{$number}' GROUP BY  account) AS main " . ($year ? "WHERE get_year = '{$year}' " : ""));
    }

    function leads_number_crm($year = '') {
        return $this->db->query("SELECT *,get_year FROM(SELECT *,FROM_UNIXTIME(UNIX_TIMESTAMP(start_date),'%Y') as get_year FROM crm_companies GROUP BY  account) AS main " . ($year ? "WHERE get_year = '{$year}' " : ""));
    }

    function industry_number($number, $year = '') {
        return $this->db->query("SELECT *,get_year FROM(SELECT *,FROM_UNIXTIME(UNIX_TIMESTAMP(start_date),'%Y') as get_year FROM crm_companies WHERE industry_id = '{$number}' GROUP BY  account) AS main " . ($year ? "WHERE get_year = '{$year}' " : ""));
    }

    function service_number($number, $year = '') {
        return $this->db->query("SELECT *,get_year FROM(SELECT *,FROM_UNIXTIME(UNIX_TIMESTAMP(start_date),'%Y') as get_year FROM crm_companies AS cmpny LEFT JOIN (SELECT company_id,service_id FROM `crm_type_of_services`) AS srvc ON cmpny.ID = srvc.company_id WHERE service_id = '{$number}' GROUP BY  account) AS main " . ($year ? "WHERE get_year = '{$year}' " : ""));
    }
    
    function service_select() {
        return $this->db->query("SELECT * FROM `crm_services` ORDER BY `crm_services`.`id` ASC");
    }
    
    function select_client_assesment($id, $ref) {
        //return $this->db->query("SELECT * FROM `crm_client_assesment` WHERE REF_ID = '{$ref}' ");
        return $this->db->query("SELECT * FROM (SELECT * FROM crm_client_assesment as main WHERE main.CID = '{$id}' OR main.REF_ID ='{$ref}' UNION SELECT * FROM crm_client_tax as tax WHERE tax.CID = '{$id}'  OR tax.REF_ID ='{$ref}') AS all_client ORDER BY all_client.date_to_string DESC");
    }

    function select_client_tax_cont() {
        return $this->db->query("SELECT * FROM (SELECT * FROM crm_client_assesment as main WHERE main.CID = '60' UNION SELECT * FROM crm_client_tax as tax WHERE tax.CID = '60') AS all_client ORDER BY CAID");
    }

//    function clients_loop(){
//        return $this->db->query("SELECT * FROM crm_companies WHERE ID IN (SELECT clientid FROM intra.workspace_client_tbl GROUP BY clientid) ");
//        //return $this->db->query("SELECT clientid FROM intra.workspace_client_tbl GROUP BY clientid");
//    }
//    function clients_update($id){
//        $this->db->where(array('ID' => $id));
//        $this->db->update('intra.xero_client_list_tbl2', array("clientstatus_id" => "1"));
//    }


    function select_group() {
        return $this->db->query('SELECT * FROM intra.user_group');
    }

    function temporary_aging_details($data) {
        return $this->db->insert('crm_temporary_aging_details', $data);
    }

    function temporary_aging_select_details() {
        return $this->db->query('SELECT * FROM crm_temporary_aging_details AS temp LEFT JOIN (SELECT *,ID AS CID FROM crm_companies WHERE account = entity) AS company ON temp.Customer = company.account ');
    }

    function kpi_revenue_details_per_invoice_tbl($data) {
        return $this->db->insert('crm_kpi_revenue_details_per_invoice_tbl', $data);
    }

    function crm_client_experience($data) {
        $data['clientid'];
        $date_now = strtotime('-1 month');
        $date = ($data['date'] ? " and (main.year = '" . date('Y', $data['date']) . "') " : " and (main.year = '" . date('Y', $date_now) . "') ");
        return $this->db->query('SELECT * FROM `crm_client_experience` AS main LEFT JOIN (SELECT ID,first_name,last_name FROM intra.employee_info) AS emp ON main.reviewed_by = emp.ID WHERE main.clientid = "' . $data['clientid'] . '" ' . $date . ' ORDER BY UID DESC');
    }

    function crm_add_client_experience($data) {
        return $this->db->insert('crm_client_experience', $data);
    }

    function crm_select_client_experience($data) {
        return $this->db->query('SELECT * FROM crm_client_experience AS main LEFT JOIN (SELECT ID,first_name,last_name FROM intra.employee_info) AS emp ON main.reviewed_by = emp.ID WHERE main.clientid = "' . $data . '" ORDER BY UID DESC  LIMIT 1');
    }

    function crm_select_update_client_experience($data) {
        return $this->db->query('SELECT * FROM crm_client_experience WHERE referenceid = "' . $data['referenceid'] . '" OR UID = "' . $data['uid'] . '" ORDER BY UID DESC  LIMIT 1');
    }

    function crm_delete_client_experience($data) {
        $this->db->where(array('UID' => $data));
        return $this->db->delete('crm_client_experience');
    }

    function crm_update_client_experience($referenceid, $uid, $data) {
        $this->db->where("referenceid='{$referenceid}' OR UID='{$uid}'");
        return $this->db->update('crm_client_experience', $data);
    }

    function crm_temporary_transactions_details($data) {
        return $this->db->insert('crm_transactions', $data);
    }

    function crm_temporary_transactions_select_details() {
        return $this->db->query('SELECT * FROM crm_transactions');
    }

    function crm_check_reviewer() {
        
    }

    public function getWorkgroups()
    {
        $this->db->select("GID, group_name");
		$this->db->from('intra.user_group');
		$this->db->order_by('group_name');
		return $this->db->get()->result();
    }

    public function getLineItems()
    {
        $this->db->select("*");
		$this->db->from('crm_line_items');
		$this->db->order_by('name');
		return $this->db->get()->result();
    }

}

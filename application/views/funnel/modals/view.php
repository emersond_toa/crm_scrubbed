<!-- Modal -->
<div class="full-modal modal" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Client Details</h5>
                <input type="hidden" name="companyId" id="companyId">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="container-1400">
                <div class="card company-header">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="company-logo company-logo-edit"
                                  action="<?php echo site_url('request/upload_logo') ?>" enctype="multipart/form-data"
                                  method="post">
                                <input type="hidden" name="client_id" class="hide client_id">
                                <label class="edit-logo-upload">
                                    <img src="<?php echo site_url('uploads/logos/default.jpg') ?>"
                                         class="c-logo d-inline-block">
                                    <input type="file" class="file-upload" name="userfile" accept="image/*">
                                    <span class="fa-edit-upload"><i class="fas fa-edit"></i></span>
                                </label>

                            </form>


                            <div class="right-infomation">
                                <div class="col-lg-6">
                                    <div class="d-flex">
                                        
                                        <div>
                                            <p><strong class="company"></strong></p>
                                            <p><span class="contactfullname"></span> - <span class="jobtitle"></span></p>
                                            <p class="contactemail"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <form action="">
                                        <div class="form-group mb-0">
                                            <label for="contactstatus" class="col-form-label pt-0">Status: <span
                                                    class="status-content mt-1"></span></label>
                                        </div>
                                    </form>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs2" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="Profile-tab" data-toggle="tab" href="#Profile" role="tab"
                                aria-controls="Profile" aria-selected="true">Profile</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="Entities-tab" data-toggle="tab" href="#Entities" role="tab"
                                aria-controls="Entities" aria-selected="false">Entities</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="Notes-tab" data-toggle="tab" href="#Notes" role="tab"
                                aria-controls="Notes" aria-selected="false">Notes</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="Reminder-tab" data-toggle="tab" href="#Reminder" role="tab"
                                aria-controls="Reminder" aria-selected="false">Reminder</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="tab-content mb-5" id="myTabContent">
                    <div class="tab-pane fade show active" id="Profile" role="tabpanel" aria-labelledby="Profile-tab">
                        <div class="card company-content">
                            <div class="row profile-tab-content">
                                <div class="col-lg-12">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="mt-2">Profile</h6>
                                        <a href="#" class="btn btn-default open-add-contact open-funnel-editmodal" data-toggle="modal" data-target="#editModal">Edit</a>
                                    </div>
                                    <hr>
                                </div>

                                <div class="col-lg-6">
                                    <form class="company-info">
                                        <div class="form-group row">
                                            <label for="companyname" class="col-sm-2 col-form-label">Company:</label>
                                            <div class="col-sm-10">
                                            <input type="text" readonly class="form-control-plaintext" id="companyname">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companywebsite" class="col-sm-2 col-form-label">Website:</label>
                                            <div class="col-sm-10">
                                            <input type="text" readonly class="form-control-plaintext" id="companywebsite">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyindustry" class="col-sm-2 col-form-label">Industry:</label>
                                            <div class="col-sm-10">
                                            <input type="text" readonly class="form-control-plaintext" id="companyindustry">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyaddress" class="col-sm-2 col-form-label">Address:</label>
                                            <div class="col-sm-10">
                                            <input type="text" readonly class="form-control-plaintext" id="companyaddress">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyphone" class="col-sm-2 col-form-label">Phone:</label>
                                            <div class="col-sm-10">
                                            <input type="text" readonly class="form-control-plaintext" id="companyphone">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyemail" class="col-sm-2 col-form-label">Email:</label>
                                            <div class="col-sm-10">
                                            <input type="text" readonly class="form-control-plaintext" id="companyemail">
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="col-lg-6">
                                    <form class="company-info">
                                        <div class="form-group row">
                                            <label for="clientworkgroups" class="col-sm-4 col-form-label">Work Groups:</label>
                                            <div class="col-sm-8">
                                            <input type="text" readonly class="form-control-plaintext" id="clientworkgroups">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyreferral" class="col-sm-4 col-form-label">Referral Partner:</label>
                                            <div class="col-sm-8">
                                            <input type="text" readonly class="form-control-plaintext" id="companyreferral">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companysource" class="col-sm-4 col-form-label">Lead Source:</label>
                                            <div class="col-sm-8">
                                            <input type="text" readonly class="form-control-plaintext" id="companysource">
                                            </div>
                                        </div>
                                        <!-- <div class="form-group row">
                                            <label for="companystartdate" class="col-sm-4 col-form-label">Start Date:</label>
                                            <div class="col-sm-8">
                                            <input type="text" readonly class="form-control-plaintext" id="companystartdate">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyterminationdate" class="col-sm-4 col-form-label">Termination Date:</label>
                                            <div class="col-sm-8">
                                            <input type="text" readonly class="form-control-plaintext" id="companyterminationdate">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyterminationreason" class="col-sm-4 col-form-label">Termination Reason:</label>
                                            <div class="col-sm-8">
                                            <input type="text" readonly class="form-control-plaintext" id="companyterminationreason">
                                            </div>
                                        </div> -->
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="card company-content mt-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="mb-0 pt-2">Contact Perons</h6>
                                        <div class="action-buttons">
                                            <button class="btn btn-default open-add-contact">
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="collapse my-2" id="collapseAddContact">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="<?php echo site_url('contacts/store') ?>"
                                                    class="create-contact">
                                                    <label class="col-form-label">Add Contact</label>
                                                    <div class="row">
                                                        <div class="col-lg-6">

                                                            <div class="form-group">
                                                                <label for="contactaddentity"
                                                                       class="col-form-label">Entity</label>
                                                                <select class="form-control" name="contactaddentity" id="contactaddentity">
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contactaddfullname"
                                                                    class="col-form-label">Name</label>
                                                                <input type="text" class="form-control"
                                                                    id="contactaddfullname"
                                                                    name="contactaddfullname">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contactaddjobtitle"
                                                                    class="col-form-label">Job
                                                                    Title</label>
                                                                <input type="text" class="form-control"
                                                                    id="contactaddjobtitle"
                                                                    name="contactaddjobtitle">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contactaddlinkedin"
                                                                    class="col-form-label">Linkedin</label>
                                                                <input type="text" class="form-control"
                                                                    id="contactaddlinkedin"
                                                                    name="contactaddlinkedin">
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="contactaddemail"
                                                                    class="col-form-label">Email</label>
                                                                <input type="text" class="form-control"
                                                                    id="contactaddemail" name="contactaddemail">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="contactaddphone"
                                                                    class="col-form-label">Phone</label>
                                                                <input type="text" class="form-control"
                                                                    id="contactaddphone" name="contactaddphone">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contactaddskype"
                                                                    class="col-form-label">Skype</label>
                                                                <input type="text" class="form-control"
                                                                    id="contactaddskype" name="contactaddskype">
                                                            </div>

                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                    class="btn btn-default mr-2 close-add-contact">Close</a>
                                                                <button type="submit"
                                                                    class="btn btn-primary save-contact">Save</button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="collapse my-2" id="collapseEditContact">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="<?php echo site_url('contacts/update') ?>"
                                                    class="update-contact">
                                                    <input type="hidden" name="contacteditid" id="contacteditid">
                                                    <label class="col-form-label">Edit Contact</label>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="contacteditentity"
                                                                       class="col-form-label">Entity</label>
                                                                <select class="form-control" name="contacteditentity" id="contacteditentity">
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contacteditfullname"
                                                                    class="col-form-label">Name</label>
                                                                <input type="text" class="form-control"
                                                                    id="contacteditfullname"
                                                                    name="contacteditfullname">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contacteditjobtitle"
                                                                    class="col-form-label">Job
                                                                    Title</label>
                                                                <input type="text" class="form-control"
                                                                    id="contacteditjobtitle"
                                                                    name="contacteditjobtitle">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contacteditlinkedin"
                                                                    class="col-form-label">Linkedin</label>
                                                                <input type="text" class="form-control"
                                                                    id="contacteditlinkedin"
                                                                    name="contacteditlinkedin">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contacteditstatus"
                                                                    class="col-form-label">Status</label>
                                                                <select class="form-control"
                                                                    name="contacteditstatus" id="contacteditstatus">
                                                                    <option value="1">Active</option>
                                                                    <option value="0">Inactive</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="contacteditemail"
                                                                    class="col-form-label">Email</label>
                                                                <input type="text" class="form-control"
                                                                    id="contacteditemail" name="contacteditemail">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="contacteditphone"
                                                                    class="col-form-label">Phone</label>
                                                                <input type="text" class="form-control"
                                                                    id="contacteditphone" name="contacteditphone">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="contacteditskype"
                                                                    class="col-form-label">Skype</label>
                                                                <input type="text" class="form-control"
                                                                    id="contacteditskype" name="contacteditskype">
                                                            </div>

                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                    class="btn btn-default mr-2 close-edit-contact">Close</a>
                                                                <button type="submit"
                                                                    class="btn btn-primary update-contact">Update</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="table-responsive mt-3">
                                        <table class="table table-bordered table-hover table-striped nopaddingmargin" width="100%" cellspacing="0"
                                            role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Entity</th>
                                                    <th>Contact Person</th>
                                                    <th>Job Title</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Linkedin</th>
                                                    <th>Skype</th>
                                                    <th class="width_100">Status</th>
                                                    <th class="text-center width_200">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="contact-table-body">
                                                <tr>
                                                    <td colspan="9" class="text-center">Loading...</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="Entities" role="tabpanel" aria-labelledby="Entities-tab">
                        <div class="card company-content">
                            <div class="row entity-tab-content">
                                <div class="col-lg-12">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="mb-0 pt-2">Entities</h6>
                                        <div class="action-buttons">
                                            <button class="btn btn-default open-add-entity">
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">    
                                    <div class="collapse my-2" id="collapseAddEntity">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="<?php echo site_url('entity/store') ?>"
                                                    class="create-entity">
                                                    <label class="col-form-label">Add Entity</label>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label for="entityname"
                                                                    class="col-form-label">Entity Name</label>
                                                                <input type="text" class="form-control"
                                                                    id="entityname" name="entityname">
                                                            </div>
                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                    class="btn btn-default mr-2 close-add-entity">Close</a>
                                                                <button type="submit"
                                                                    class="btn btn-primary save-entity">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
    
                                    <div class="collapse my-2" id="collapseEditEntity">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="<?php echo site_url('entity/update') ?>"
                                                    class="update-entity">
                                                    <input type="hidden" name="entityeditid" id="entityeditid">
                                                    <label class="col-form-label">Edit Enity</label>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label for="entityeditname"
                                                                    class="col-form-label">Entity Name</label>
                                                                <input type="text" class="form-control"
                                                                    id="entityeditname" name="entityeditname">
                                                            </div>
                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                    class="btn btn-default mr-2 close-edit-entity">Close</a>
                                                                <button type="submit"
                                                                    class="btn btn-primary update-entity">Update</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
    
                                    <div class="table-responsive mt-3">
                                        <table class="table table-bordered table-hover table-striped nopaddingmargin" width="100%" cellspacing="0"
                                            role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Entity</th>
                                                    <th class="text-center width_200">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody class="entity-table-body">
                                                <tr>
                                                    <td colspan="2">Loading...</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="Notes" role="tabpanel" aria-labelledby="Notes-tab">
                        <div class="card company-content">
                            <div class="d-flex justify-content-between">
                                <h6 class="mb-0 pt-2">Notes</h6>
                            </div>

                            <div class="accordion mt-3" id="accordionExample">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link btn-block btn-sm text-left cna-btn"
                                                    type="button" data-toggle="collapse" data-target="#collapseOne"
                                                    aria-expanded="true" aria-controls="collapseOne">
                                                CNA <span class="date-created"></span>
                                            </button>
                                        </h2>
                                    </div>
                                    <form action="<?php echo site_url('contacts/edit_cna') ?>" method="post" id="collapseOne" class="edit-cna collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <input type="hidden" value="" name="id" class="company_id"/>
                                        <div class="editor-body"> 
                                            <textarea class="form-control textarea-content" name="notepad" id="cna-notes"></textarea>
                                        </div>
                                        <hr>
                                        <div class="submit-cna text-right">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse" data-target="#collapseTwo"
                                                    aria-expanded="false" aria-controls="collapseTwo">
                                                Conversation
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                        <form class="note-convo" action="<?php echo site_url('contacts/add_conversation') ?>">
                                            <input type="hidden" value="<?php echo $this->session->userdata('user_session') ?>" name="uid" class="hide"/>
                                            <input type="hidden" value="" name="contact_id" class="company_id"/>
                                            <div class="form-group">
                                                <input type="text" value="" name="title" placeholder="Title" class="form-control"/>
                                            </div>
                                            <div class="note-section">
                                                <textarea class="form-control" name="note" id="cna-conversation-client" placeholder="Write a notes..."></textarea>
                                            </div>
                                            <div class="submit-note"><button type="submit" class="btn btn-primary">Add New</button></div>
                                            <div class="output"></div>
                                        </form>
                                        <div class="list-group-note"> 
                                            <div class="list-group">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="Reminder" role="tabpanel" aria-labelledby="Reminder-tab">
                        <div class="card company-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="mb-0 pt-2">Reminders</h6>
                                        <div class="action-buttons">
                                            <button class="btn btn-default open-add-reminder">
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="collapse my-2" id="collapseAddReminder">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="<?php echo site_url('reminder/store') ?>"
                                                    class="create-reminder">
                                                    <label class="col-form-label">Add Reminder</label>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="start_date"
                                                                    class="col-form-label">Date</label>
                                                                <input type="datetime-local" class="form-control"
                                                                    id="start_date" name="start_date">
                                                            </div>
    
                                                            <div class="form-group">
                                                                <label for="notify"
                                                                    class="col-form-label">Notify</label>
                                                                <select class="form-control chosen-select" multiple
                                                                    id="notify" name="notify[]">
                                                                    <option></option>
                                                                    <?php foreach ($users as $user) : ?>
                                                                    <option value="<?php echo $user->ID; ?>">
                                                                        <?php echo $user->first_name . ' ' . $user->last_name; ?>
                                                                    </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
    
                                                            <div class="form-group">
                                                                <label for="what"
                                                                    class="col-form-label">What</label>
                                                                <input type="text" class="form-control" id="what"
                                                                    name="what">
                                                            </div>
                                                        </div>
    
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="where"
                                                                    class="col-form-label">Where</label>
                                                                <input type="text" class="form-control" id="where"
                                                                    name="where">
                                                            </div>
    
                                                            <div class="form-group">
                                                                <label for="description"
                                                                    class="col-form-label">Description</label>
                                                                <textarea class="form-control" name="description"
                                                                    id="description" cols="30" rows="5"></textarea>
                                                            </div>
    
                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                    class="btn btn-default mr-2 close-add-reminder">Close</a>
                                                                <button type="submit"
                                                                    class="btn btn-primary save-reminder">Save</button>
                                                            </div>
    
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
    
                                    <div class="collapse my-2" id="collapseEditReminder">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="<?php echo site_url('reminder/update') ?>"
                                                    class="update-reminder">
                                                    <input type="hidden" name="editreminderid" id="editreminderid">
                                                    <label class="col-form-label">Edit Reminder</label>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="editstart_date"
                                                                    class="col-form-label">Date</label>
                                                                <input type="datetime-local" class="form-control"
                                                                    id="editstart_date" name="editstart_date">
                                                            </div>
    
                                                            <div class="form-group">
                                                                <label for="editnotify"
                                                                    class="col-form-label">Notify</label>
                                                                <select class="form-control chosen-select" multiple
                                                                    id="editnotify" name="editnotify[]">
                                                                    <option></option>
                                                                    <?php foreach ($users as $user) : ?>
                                                                    <option value="<?php echo $user->ID; ?>">
                                                                        <?php echo $user->first_name . ' ' . $user->last_name; ?>
                                                                    </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
    
                                                            <div class="form-group">
                                                                <label for="editwhat"
                                                                    class="col-form-label">What</label>
                                                                <input type="text" class="form-control"
                                                                    id="editwhat" name="editwhat">
                                                            </div>
                                                        </div>
    
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="editwhere"
                                                                    class="col-form-label">Where</label>
                                                                <input type="text" class="form-control"
                                                                    id="editwhere" name="editwhere">
                                                            </div>
    
                                                            <div class="form-group">
                                                                <label for="editdescription"
                                                                    class="col-form-label">Description</label>
                                                                <textarea class="form-control"
                                                                    name="editdescription" id="editdescription"
                                                                    cols="30" rows="5"></textarea>
                                                            </div>
    
                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                    class="btn btn-default mr-2 close-edit-reminder">Close</a>
                                                                <button type="submit"
                                                                    class="btn btn-primary update-reminder">Update</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
    
                                    <div class="table-responsive mt-3">
                                        <table class="table table-bordered table-hover table-striped nopaddingmargin" width="100%" cellspacing="0"
                                            role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Notify</th>
                                                    <th>What</th>
                                                    <th>Where</th>
                                                    <th>Description</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="reminder-table-body">
                                                <tr>
                                                    <td>Loading...</td>
                                                    <td>Loading...</td>
                                                    <td>Loading...</td>
                                                    <td>Loading...</td>
                                                    <td>Loading...</td>
                                                    <td>Loading...</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
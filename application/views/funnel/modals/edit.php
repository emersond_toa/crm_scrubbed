<!-- Modal -->
<div class="full-modal modal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit Client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="container-1400">
                <div class="card company-header mb-5">
                    <form class="edit-funnel-company" action="<?php echo site_url('company/update_funnel') ?>">
                        <!-- <label class="col-form-label">Edit Company</label> -->
                        <div class="row">
                            <input type="hidden" name="editcompanyid" id="editcompanyid">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="editname" class="col-form-label">Company</label>
                                    <input type="text" class="form-control" id="editname" name="editname">
                                </div>

                                <div class="form-group">
                                    <label for="editwebsite" class="col-form-label">Website</label>
                                    <input type="text" class="form-control" id="editwebsite" name="editwebsite">
                                </div>

                                <div class="form-group">
                                    <label for="editphone" class="col-form-label">Phone</label>
                                    <input type="text" class="form-control" id="editphone" name="editphone">
                                </div>

                                <div class="form-group">
                                    <label for="editaddress" class="col-form-label">Address</label>
                                    <textarea class="form-control" id="editaddress" name="editaddress" rows="5"></textarea>
                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label for="editindustry" class="col-form-label">Industry</label>
                                    <select class="form-control" id="editindustry" name="editindustry">
                                        <?php foreach($industries as $industry) : ?>
                                        <option value="<?php echo $industry->id; ?>"><?php echo $industry->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="editstatus" class="col-form-label">Status</label>
                                    <select class="form-control" id="editstatus" name="editstatus">
                                        <?php foreach($statuses as $status) : ?>
                                        <option value="<?php echo $status->id; ?>"><?php echo $status->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group" style="height: 0; opacity: 0; margin: 0;">
                                    <label for="editclientstatus" class="col-form-label">Client Status</label>
                                    <select class="form-control" id="editclientstatus" name="editclientstatus">
                                        <?php foreach($clientstatuses as $clientstatus) : ?>
                                        <option value="<?php echo $clientstatus->id; ?>"><?php echo $clientstatus->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="editassignedto" class="col-form-label">Assigned To</label>
                                    <select class="form-control chosen-select" id="editassignedto" multiple name="editassignedto[]">
                                        <option></option>
                                        <?php foreach($users as $user) : ?>
                                        <option value="<?php echo $user->ID; ?>">
                                            <?php echo $user->first_name.' '.$user->last_name;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="editreferralpartner" class="col-form-label">Referral</label>
                                    <select class="form-control" id="editreferralpartner" name="editreferralpartner">
                                        <option selected value="">Select Option</option>
                                        <?php foreach($referralpartners as $referral) : ?>
                                        <option value="<?php echo $referral->id; ?>"><?php echo $referral->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="editleadsource" class="col-form-label">Source</label>
                                    <select class="form-control" id="editleadsource" name="editleadsource">
                                        <option selected value="">Select Option</option>
                                        <?php foreach($leadsources as $source) : ?>
                                        <option value="<?php echo $source->id; ?>"><?php echo $source->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="editworkgroups" class="col-form-label">Work Groups</label>
                                    <select class="form-control chosen-select" id="editworkgroups" multiple name="editworkgroups[]">
                                        <option></option>
                                        <?php foreach($work_groups as $work_group) : ?>
                                        <option value="<?php echo $work_group->GID; ?>">
                                            <?php echo $work_group->group_name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group mt-4 text-right">
                                    <a href="" class="btn btn-default mr-2 close-add-contact" data-dismiss="modal">Close</a>
                                    <button type="submit"
                                        class="btn btn-primary update-funnel-company">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- <div class="row company-container mb-5">
                        <div class="col-lg-12">
                            <hr>
                            <p><strong>Company</strong></p>
                        </div>

                        <div class="col-lg-6 hide has-company">
                            <div class="card company-card">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between" style="align-items: baseline;">
                                        <p class="mb-0"><strong class="company_name"></strong></p>
                                        <div>
                                            <a class="mr-3 edit-contact-company" data-toggle="collapse" href="#collapseEditContactCompany" role="button" aria-expanded="false" aria-controls="collapseEditContactCompany" data-companyid="" data-action="<?php echo site_url('company/show') ?>">Edit</a>
                                            <a href="#" class="btn btn-circle btn-sm remove-company">
                                                <i class="fas fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 hide no-company">
                            <a class="btn btn-default" data-toggle="collapse" href="#collapseAddNew" role="button"
                                aria-expanded="false" aria-controls="collapseAddNew">
                                <i class="fa fa-plus"></i>
                            </a>
                            <div class="collapse" id="collapseAddNew">
                                <div class="mt-4">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link active" id="existing-tab" data-toggle="tab" href="#existing"
                                                role="tab" aria-controls="existing" aria-selected="true">Add existing
                                                Company</a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="newcompany-tab" data-toggle="tab" href="#new" role="tab"
                                                aria-controls="new" aria-selected="false">Add new Company</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="existing" role="tabpanel"
                                            aria-labelledby="existing-tab">
                                            <form class="add_existing_company"
                                                action="<?php echo site_url('contacts/add_existing_company') ?>"
                                                method="post">
                                                <div class="row mt-3">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <select data-placeholder="Select Company"
                                                                class="form-control chosen-select" id="company_id"
                                                                name="company_id">
                                                                <option></option>
                                                                <?php foreach($companies as $company) : ?>
                                                                <option value="<?php echo $company->id; ?>">
                                                                    <?php echo $company->name; ?>
                                                                </option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                    </div>

                                                    <div class="col-sm-6 text-right">
                                                        <button class="btn btn-primary save_existing_company"
                                                            type="submit">Save</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="tab-pane fade" id="new" role="tabpanel"
                                            aria-labelledby="newcompany-tab">
                                            <form class="contact_add_new_company"
                                                action="<?php echo site_url('contacts/add_new_company') ?>"
                                                method="post">
                                                <div class="row mt-3">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="companyname" class="col-form-label">Company
                                                                Name</label>
                                                            <input type="text" class="form-control" id="companyname"
                                                                name="companyname">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="website" class="col-form-label">Website</label>
                                                            <input type="text" class="form-control" id="website"
                                                                name="website">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="companyphone" class="col-form-label">Phone</label>
                                                            <input type="text" class="form-control" id="companyphone"
                                                                name="companyphone">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="industry" class="col-form-label">Industry</label>
                                                            <select class="form-control chosen-select" id="industry"
                                                                name="industry">
                                                                <option></option>
                                                                <?php foreach($industries as $industry) : ?>
                                                                <option value="<?php echo $industry->id; ?>">
                                                                    <?php echo $industry->name; ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="address" class="col-form-label">Address</label>
                                                            <textarea name="address" class="form-control" id="address" rows="5"></textarea>
                                                        </div>

                                                        <div class="form-group mt-4">
                                                            <button type="submit"
                                                                class="btn btn-primary float-right save_contact_new_company">Update</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="collapse" id="collapseEditContactCompany">
                                <div class="mt-3">
                                    <form class="contact_edit_company"
                                        action="<?php echo site_url('company/update') ?>"
                                        method="post">
                                        <div class="row mt-3">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="editcompanyname" class="col-form-label">Company
                                                        Name</label>
                                                    <input type="text" class="form-control" id="editcompanyname"
                                                        name="editcompanyname">
                                                </div>

                                                <div class="form-group">
                                                    <label for="editwebsite" class="col-form-label">Website</label>
                                                    <input type="text" class="form-control" id="editwebsite"
                                                        name="editwebsite">
                                                </div>

                                                <div class="form-group">
                                                    <label for="editcompanyphone" class="col-form-label">Phone</label>
                                                    <input type="text" class="form-control" id="editcompanyphone"
                                                        name="editcompanyphone">
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="editindustry" class="col-form-label">Industry</label>
                                                    <select class="form-control chosen-select" id="editindustry"
                                                        name="editindustry">
                                                        <option></option>
                                                        <?php foreach($industries as $industry) : ?>
                                                        <option value="<?php echo $industry->id; ?>">
                                                            <?php echo $industry->name; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="editaddress" class="col-form-label">Address</label>
                                                    <textarea name="editaddress" class="form-control" id="editaddress" rows="5"></textarea>
                                                </div>

                                                <div class="form-group mt-4">
                                                    <button type="submit"
                                                        class="btn btn-primary float-right update_contact_company">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>

    </div>
</div>
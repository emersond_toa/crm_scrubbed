<div class="container-fluid main-container">
    <h1 class="mt-4">Dashboard</h1>
    <div class="row">
        <div class="col-md-6 form-group">
            <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <br>
                    <select class="form-control get-year-leads">
                        <option value="">SELECT YEAR</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                    </select>
                    <br>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <canvas id="status-reports" height="250"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6 form-group">
            <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <br>
                    <select class="form-control get-year-service">
                        <option value="">SELECT YEAR</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                    </select>
                    <br>
                </div>
            </div>
            <div class="card">

                <div class="card-body">
                    <canvas id="service-lines" height="250"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6 form-group">
            <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <br>
                    <select class="form-control get-year-industry">
                        <option value="">SELECT YEAR</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                    </select> 
                    <br>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <canvas id="industry-lines" height="250"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6 form-group">
            <div class="card">
                <div class="card-body">

                    <table class="table table-bordered table-sm">
                        <tbody>
                            <tr>
                                <td class="text-center text-bold" colspan="6">Summary</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>New Monthly Leads</td>
                                <td>New Monthly Sign Ups</td>
                                <td>Total Leads</td>
                                <td>Total Sign Ups</td>
                                <td>Conversion Rate</td>
                            </tr>
                            <tr>
                                <td width="150">January 2020</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>February 2020</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>March 2020</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>April 2020</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>May 2020</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>June 2020</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>July 2020</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>August 2020</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>September 2020</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>October 2020</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>November 2020</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>December 2020</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- DivTable.com -->






                </div>
            </div>
        </div>
    </div>

</div>
<div class="container-fluid main-container">
    <div class="row">

        <div class="col-lg-12">

            <h1 class="h3 mb-4 text-gray-800">Former Clients</h1>
<!-- 
            <div id="filtercontent">
                <form class="filter-form" method="POST">
                    <div class="d-flex justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-filter"></i> Filter</h6>

                        <a href="#" class="close-filter">
                            <i class="fas fa-times"></i>
                        </a>
                    </div>

                    <div class="row mt-3">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="form">Forms</label>
                                <?php foreach ($forms as $form) : ?>
                                <div class="form-check">
                                    <input class="form-check-input leadform" name="leadform"
                                        id="<?php echo $form->name; ?>" type="checkbox"
                                        value="<?php echo $form->id; ?>" id="<?php echo $form->name; ?>">
                                    <label class="form-check-label" for="<?php echo $form->name; ?>">
                                        <?php echo $form->name; ?>
                                    </label>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="leadstatus">Status</label>
                                <?php foreach ($clientstatuses as $status) : ?>
                                <div class="form-check">
                                    <input class="form-check-input leadstatus" name="leadstatus"
                                        id="<?php echo $status->name; ?>" type="checkbox"
                                        value="<?php echo $status->id; ?>" id="<?php echo $status->name; ?>">
                                    <label class="form-check-label" for="<?php echo $status->name; ?>">
                                        <?php echo $status->name; ?>
                                    </label>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div> -->

            <div class="table-responsive">
                <table class="table table-striped table-hover" id="former-clients-table" width="100%" cellspacing="0"
                    role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                    <thead>
                        <tr>
                            <th scope="col">Company Name</th>
                            <th scope="col">Contact Name</th>
                            <th scope="col">Form</th>
                            <th scope="col">Industry</th>
                            <th scope="col">Services</th>
                            <th scope="col">Work Groups</th>
                            <th scope="col">Last Update</th>
                            <th scope="col">Termination Date</th>
                            <th scope="col" class="width_50">Status</th>
                            <th scope="col" class="width_150 text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
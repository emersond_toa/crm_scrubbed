<!-- Modal -->
<div class="modal full-modal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit Former Client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="edit-former-client-company" action="<?php echo site_url('company/update_client') ?>">
                    <div class="row">
                        <input type="hidden" name="editcompanyid" id="editcompanyid">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="editname" class="col-form-label">Company</label>
                                <input type="text" class="form-control" id="editname" name="editname">
                            </div>

                            <div class="form-group">
                                <label for="editwebsite" class="col-form-label">Website</label>
                                <input type="text" class="form-control" id="editwebsite" name="editwebsite">
                            </div>

                            <div class="form-group">
                                <label for="editphone" class="col-form-label">Phone</label>
                                <input type="text" class="form-control" id="editphone" name="editphone">
                            </div>

                            <div class="form-group">
                                <label for="editaddress" class="col-form-label">Address</label>
                                <textarea class="form-control" id="editaddress" name="editaddress" rows="5"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="editreferralpartner" class="col-form-label">Referral Partner</label>
                                <select class="form-control" id="editreferralpartner" name="editreferralpartner">
                                    <option selected value="">Select Option</option>
                                    <?php foreach($referralpartners as $referral) : ?>
                                    <option value="<?php echo $referral->id; ?>"><?php echo $referral->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="editleadsource" class="col-form-label">Lead Source</label>
                                <select class="form-control" id="editleadsource" name="editleadsource">
                                    <option selected value="">Select Option</option>
                                    <?php foreach($leadsources as $source) : ?>
                                    <option value="<?php echo $source->id; ?>"><?php echo $source->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-6">

                            <div class="form-group">
                                <label for="editindustry" class="col-form-label">Industry</label>
                                <select class="form-control" id="editindustry" name="editindustry">
                                    <?php foreach($industries as $industry) : ?>
                                    <option value="<?php echo $industry->id; ?>"><?php echo $industry->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="editstatus" class="col-form-label">Status</label>
                                <select class="form-control" id="editstatus" name="editstatus">
                                    <?php foreach($statuses as $status) : ?>
                                    <option value="<?php echo $status->id; ?>"><?php echo $status->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="editclientstatus" class="col-form-label">Client Status</label>
                                <select class="form-control" id="editclientstatus" name="editclientstatus">
                                    <?php foreach($clientstatuses as $clientstatus) : ?>
                                    <option value="<?php echo $clientstatus->id; ?>"><?php echo $clientstatus->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="editassignedto" class="col-form-label">Assigned To</label>
                                <select class="form-control chosen-select" id="editassignedto" multiple name="editassignedto[]">
                                    <option></option>
                                    <?php foreach($users as $user) : ?>
                                    <option value="<?php echo $user->ID; ?>">
                                        <?php echo $user->first_name.' '.$user->last_name;?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="editstartdate" class="col-form-label">Start Date</label>
                                <input type="date" class="form-control"
                                    id="editstartdate" name="editstartdate">
                            </div>

                            <div class="form-group">
                                <label for="editterminationdate" class="col-form-label">Termination Date</label>
                                <input type="date" class="form-control"
                                    id="editterminationdate" name="editterminationdate">
                            </div>

                            <div class="form-group">
                                <label for="editterminationreason" class="col-form-label">Termination Reason</label>
                                <select class="form-control" id="editterminationreason" name="editterminationreason">
                                    <option selected value="">Select Option</option>
                                    <?php foreach($reasons as $reason) : ?>
                                    <option value="<?php echo $reason->id; ?>"><?php echo $reason->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="editworkgroups" class="col-form-label">Work Groups</label>
                                <select class="form-control chosen-select" id="editworkgroups" multiple name="editworkgroups[]">
                                    <option></option>
                                    <?php foreach($work_groups as $work_group) : ?>
                                    <option value="<?php echo $work_group->GID; ?>">
                                        <?php echo $work_group->group_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group mt-4">
                                <button type="submit"
                                    class="btn btn-primary update-former-client-company float-right">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
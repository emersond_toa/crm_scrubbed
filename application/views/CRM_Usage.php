<div class="container-fluid main-container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="h3 mb-4 text-gray-800">System Usage</h1>
        </div>
        <div class="col-lg-12 form-group">
            <div class="row device-info">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="font-weight-bold mmc-total-text">Total Disk: Loading...</div>
                            <small>Used <span class="mmc-used-text"></span></small>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-purple progress-bar-striped mmc-used-html" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%">

                                </div>
                            </div>
                            <small>Remaining <span class="mmc-remaining-text"></span></small>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-purple progress-bar-striped mmc-remaining-html" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="font-weight-bold proc-total-text">Total CPU: 0</div>
                            <small>Used <span class="proc-used-text"></span></small>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-warning progress-bar-striped proc-used-html" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%">

                                </div>
                            </div>
                            <small>Remaining <span class="proc-remaining-text"></span></small>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-warning progress-bar-striped proc-remaining-html" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="font-weight-bold ">Total Ram: <span class="ram-total-text">0</span>Mb</div>
                            <small>Used <span class="ram-used-text">0</span>Mb</small>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-info progress-bar-striped ram-used-html" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%">

                                </div>
                            </div>
                            <small>Remaining  <span class="ram-remaining-text">0</span>Mb</small>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-info progress-bar-striped ram-remaining-html" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 form-group">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-sm">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Command</th>
                            <th>State</th>
                            <th>Info</th>
                            <th>Process</th>
                        </tr>
                    </thead>
                    <tbody class="process-list">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
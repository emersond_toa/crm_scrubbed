<style>
    .client-form{
        display: block;
        max-width: 860px;
    }
    .client-form .form-group{ 
        display: block;
        margin-bottom: 1rem;
        width: 100%;
    }
    .client-form .form-group label{
        display: inline-block;
        margin-bottom: .5rem;
    }
    .client-form .form-control{
        width: 100%;
        padding: 12px;
        margin: 6px 0 4px;
        border: 1px solid #ccc;
        background: #fafafa;
        color: #000;
        font-family: sans-serif;
        font-size: 12px;
        line-height: normal;
        box-sizing: border-box;
        border-radius: 6px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        outline: none !important;
    }
    .client-form .is-invalid{
        -webkit-transition-duration: 1s;
        -moz-transition-duration: 1s;
        -o-transition-duration: 1s;
        transition-duration: 1s;
        border: 1px solid #d20505;
        box-shadow: 0px 0px 5px transparent;
    }
    .client-form .is-invalid:focus{
        box-shadow: 0px 0px 5px #d20505;
    }
    .client-form .muted{
        padding: .5em 0 .5em;
        font-size: 12px;
        opacity: .7;
        display: block;
        white-space: pre-wrap;
    }
    .client-form .required {
        color: red;
    }
    .client-form .submit-form-btn{
        color: #fff;
        background-color: #272727;
        border-color: #272727;
        border-radius: 300px;
        display: inline-block;
        width: auto;
        height: auto;
        padding: 1em 2.5em;
        border: 1px solid #272727;
    }
</style>

<form class="client-form" action="<?php echo site_url('contacts/client_free_consultation') ?>" method="post">
    <input type="hidden" name="form_type" value="CorFin">
    <input type="hidden" name="service_id" value="1">
    <div class="form-group">
        <label>Company Legal/Trading Name <span class="required">*</span></label>
        <input type="text" class="form-control" name="Company"/>
    </div>
    <div class="form-group">
        <label>Name <span class="required">*</span></label>
        <input type="text" class="form-control" name="Contact_Person"/>
    </div>
    <div class="form-group">
        <label>Email Address <span class="required">*</span></label>
        <input type="text" class="form-control" name="Email_Address"/>
    </div>
    <div class="form-group">
        <label>Phone Number <span class="required">*</span></label>
        <input type="text" class="form-control" name="Phone_Number"/>
    </div>
    <div class="form-group">
        <label>How can we help reduce your accounting costs? <span class="required">*</span></label>
        <textarea class="form-control" name="note"></textarea>
    </div>
    <div class="">
        <button type="submit" class="submit-form-btn">SUBMIT</button>
    </div>
</form>
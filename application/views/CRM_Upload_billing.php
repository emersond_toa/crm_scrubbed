<div class="container-fluid main-container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="h3 mb-4 text-gray-800">Upload Billing</h1>
        </div>
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Invoice</label>
                            <form action="<?php echo site_url('Upload_billing/upload_billing') ?>" class="type-of-service-upload-doc" enctype="multipart/form-data" method="post">
                                <div class="upload-files-drop form-group">
                                    <input type="file" name="file[]" class="form-control file-uploader" multiple="" accept=".csv">
                                </div>
                                <div class="progress form-group hide">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                             <label>Payments</label>
                            <form action="<?php echo site_url('Upload_billing/upload_billing') ?>" class="type-of-service-upload-doc" enctype="multipart/form-data" method="post">
                                <div class="upload-files-drop form-group">
                                    <input type="file" name="file[]" class="form-control file-uploader" multiple="" accept=".csv">
                                </div>
                                <div class="progress form-group hide">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <a href="" class="btn btn-success">Approve <i class="fas fa-thumbs-up"></i></a>
                    </div>
                    <div class="responsive-table">
                        <?php $aging = $this->Models->temporary_aging_select_details(); ?>
                        <table class="table table-bordered table-hover table-striped nopaddingmargin">
                            <thead>
                                <tr>
                                    <th>Invoice</th>
                                    <th>Date</th>
                                    <th>Customer</th>
                                    <th>Due Date</th>
                                    <th>Aging</th>
                                    <th>Open Balance</th>
                                </tr>
                            </thead>
                            <?php foreach ($aging->result() as $value) { ?>
                                <tr>
                                    <td><?php echo $value->Invoice ?></td>
                                    <td><?php echo $value->Date ?></td>
                                    <td><?php echo $value->Customer ?></td>
                                    <td><?php echo $value->Due_Date ?></td>
                                    <td><?php echo $value->Aging ?></td>
                                    <td><?php echo $value->Open_Balance ?></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
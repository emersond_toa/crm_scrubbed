<div class="container-fluid main-container">
    <div class="table-responsive">
        <div class="divTable table-width1200">
            <div class="divTableBody">
                <div class="divTableRow divTbHead">
                    <div class="divTableCell">Company</div>
                    <div class="divTableCell">Type</div>
                    <div class="divTableCell">Marketing_Status</div>
                    <div class="divTableCell">Date_Signed</div>
                    <div class="divTableCell">Team</div>
                    <div class="divTableCell">First_Billing</div>
                    <div class="divTableCell">Hourly</div>
                    <div class="divTableCell">Monthly</div>
                    <div class="divTableCell">Non_recurring</div>
                    <div class="divTableCell">Industry</div>
                    <div class="divTableCell">Date_From</div>
                    <div class="divTableCell">Date_To</div>
                    <div class="divTableCell">Status</div>
                    <div class="divTableCell">Action</div>
                </div>
                <form class="divTableRow add-update-form" method="post" action="<?php echo site_url('request/add_new_client') ?>">
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Company"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Type"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Marketing_Status"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Date_Signed"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Team"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="First_Billing"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Hourly"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Monthly"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Non_recurring"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Industry"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Date_From"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Date_To"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Status"></div>
                    <div class="divTableCell text-center action-button">
                        <button class="btn btn-light btn-rounded btn-sm" type="submit"><i class="fas fa-plus"></i></button>
                    </div>
                </form>

                <form class="divTableRow add-update-form" method="post" action="<?php echo site_url('request/update_client') ?>">
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Company"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Type"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Marketing_Status"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Date_Signed"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Team"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="First_Billing"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Hourly"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Monthly"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Non_recurring"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Industry"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Date_From"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Date_To"></div>
                    <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="Status"></div>
                    <div class="divTableCell">
                        <div class="action-button text-center">
                            <button class="btn btn-light btn-rounded btn-sm" type="submit"><i class="fas fa-edit"></i></button>
                            <button class="btn btn-light btn-rounded btn-sm" type="button"><i class="fas fa-users"></i></button>
                            <button class="btn btn-light btn-rounded btn-sm" type="button"><i class="far fa-building"></i></button>
                            <button class="btn btn-light btn-rounded btn-sm" type="button"><i class="fas fa-upload"></i></button>
                            <button class="btn btn-light btn-rounded btn-sm" type="button"><i class="fas fa-trash"></i></button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<div class="container-fluid main-container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="h3 mb-4 text-gray-800">Invoices</h1>
        </div>

        <div class="col-lg-6">
            <div class="card mb-4">
                <div class="card-body">
                    <!-- Display status message -->
                    <?php if(!empty($success_msg)){ ?>
                    <div class="col-xs-12">
                        <div class="alert alert-success"><?php echo $success_msg; ?></div>
                    </div>
                    <?php } ?>
                    <?php if(!empty($error_msg)){ ?>
                    <div class="col-xs-12">
                        <div class="alert alert-danger"><?php echo $error_msg; ?></div>
                    </div>
                    <?php } ?>

                    <form action="<?php echo base_url('invoices/import'); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="file">Upload Invoice</label>
                            <input type="file" class="form-control" id="file" name="file">
                            <!-- <small class="form-text text-muted">Upload invoice csv file from bill.com</small> -->
                        </div>
                        <input type="submit" class="btn btn-primary float-right" name="importSubmit" value="IMPORT">
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-12 mt-4">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Client Name</th>
                        <th scope="col">Group</th>
                        <th scope="col">Invoice #</th>
                        <th scope="col">Credit Amount</th>
                        <th scope="col">Line Item Name</th>
                        <th scope="col">Transaction Date</th>
                        <th scope="col">Line Item Amount</th>
                        <th scope="col">Total Line Items</th>
                        <th scope="col">Created</th>
                        <th scope="col">Updated</th>
                    </tr>
                </thead>
                <tbody>
                <?php if(!empty($invoices)){ foreach($invoices as $row){ ?>
                <tr>
                    <td><?php echo $row['entity']; ?></td>
                    <td><?php echo $row['group_name']; ?></td>
                    <td><a href="<?php echo site_url('invoices'); ?>/show/<?php echo $row['invoice_number']; ?>"><?php echo $row['invoice_number']; ?></a></td>
                    <td><?php echo $row['credit_amount']; ?></td>
                    <td><?php echo $row['line_item_name']; ?></td>
                    <td><?php echo date('m/d/Y', strtotime($row['transaction_date'])); ?></td>
                    <td><?php echo $row['line_item_amount']; ?></td>
                    <td><?php echo $row['total_line_items']; ?></td>
                    <td><?php echo date('m/d/Y', strtotime($row['created_at'])); ?></td>
                    <td><?php echo date('m/d/Y', strtotime($row['updated_at'])); ?></td>
                </tr>
                <?php } }else{ ?>
                <tr><td colspan="10">No invoice(s) found...</td></tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
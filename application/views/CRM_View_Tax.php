<style>
    .client-tax-form{
        display: block;
        max-width: 860px;
    }
    .client-tax-form .form-group{ 
        display: block;
        margin-bottom: 1rem;
        width: 100%;
    }
    .client-tax-form .form-group label{
        display: inline-block;
        margin-bottom: .5rem;
    }
    .client-tax-form .form-control{
        width: 100%;
        padding: 12px;
        margin: 6px 0 4px;
        border: 1px solid #ccc;
        background: #fafafa;
        color: #000;
        font-family: sans-serif;
        font-size: 12px;
        line-height: normal;
        box-sizing: border-box;
        border-radius: 6px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        outline: none !important;
    }
    .client-tax-form .is-invalid{
        -webkit-transition-duration: 1s;
        -moz-transition-duration: 1s;
        -o-transition-duration: 1s;
        transition-duration: 1s;
        border: 1px solid #d20505;
        box-shadow: 0px 0px 5px transparent;
    }
    .client-tax-form .is-invalid:focus{
        box-shadow: 0px 0px 5px #d20505;
    }
    .client-tax-form .muted{
        padding: .5em 0 .5em;
        font-size: 12px;
        opacity: .7;
        display: block;
        white-space: pre-wrap;
    }
    .client-tax-form .required {
        color: red;
    }

    .client-tax-form .submit-form-btn{
        color: #fff;
        background-color: #272727;
        border-color: #272727;
        border-radius: 300px;
        display: inline-block;
        width: auto;
        height: auto;
        padding: 1em 2.5em;
        border: 1px solid #272727;
    }
</style>

<form class="client-tax-form" action="<?php echo site_url('Contacts/client_tax_form') ?>" method="post">
    <input type="hidden" name="form_type" value="TAX">
    <div class="form-group">
        <label>Contact Person <span class="required">*</span></label>
        <input type="text" class="form-control" name="Contact_Person"/>
    </div>
    <div class="form-group">
        <label>Position/Title <span class="required">*</span></label>
        <input type="text" class="form-control" name="Position_Title"/>
    </div>
    <div class="form-group">
        <label>Email Address <span class="required">*</span></label>
        <input type="text" class="form-control" name="Email_Address"/>
    </div>
    <div class="form-group">
        <label>Phone Number <span class="required">*</span></label>
        <input type="text" class="form-control" name="Phone_Number"/>
    </div>
    <div class="form-group">
        <label>Company Legal/Trading Name <span class="required">*</span></label>
        <input type="text" class="form-control" name="Company_Legal"/>
    </div>
    <div class="form-group">
        <label>Company Full Address <span class="required">*</span></label>
        <input type="text" class="form-control" name="Company_Full"/>
    </div>
    <div class="form-group">
        <label>Company Website <span class="required">*</span></label>
        <input type="text" class="form-control" name="Company_Website"/>
    </div>

    <div class="form-group">
        <label>What does your company do? <span class="required">*</span></label>
        <textarea class="form-control" name="What_does"></textarea>
    </div>

    <div class="form-group">
        <label>How long have you been in operation? <span class="required">*</span></label>
        <select class="form-control" name="How_long_have">
            <option value="Under 6 Months">Under 6 Months</option>
            <option value="6 - 12 Months">6 - 12 Months</option>
            <option value="1 - 2 Years">1 - 2 Years</option>
            <option value="2 - 5 Years">2 - 5 Years</option>
            <option value="Over 5 Years">Over 5 Years</option>
        </select>
    </div>

    <div class="form-group">
        <label>Who is currently handling your company's tax compliance & filing?<span class="required">*</span></label>
        <select class="form-control" name="Who_is_currently">
            <option value="In-House">In-House</option>
            <option value="Outside Service Provider">Outside Service Provider</option>
            <option value="None">None</option>
        </select>
    </div>


    <div class="form-group">
        <label>What type of annual income tax return do you file?<span class="required">*</span></label>
        <select class="form-control" name="What_type_of_annual">
            <option value="Business Federal and State Tax Return (1120 C Corp, 1120S,  1065)">Business Federal and State Tax Return (1120 C Corp, 1120S,  1065)</option>
            <option value="Individual Federal and State Tax Return (1040)">Individual Federal and State Tax Return (1040)</option>
            <option value="Exempt Organization Federal and State Tax  Return (990)">Exempt Organization Federal and State Tax  Return (990)</option>
            <option value="Not Sure">Not Sure</option>           
        </select>
    </div>


    <div class="form-group">
        <label>What year was your last completed income tax return?<span class="required">*</span></label>
        <select class="form-control" name="Tax_What_year_was">
            <option value="2019">2019</option>                  
            <option value="2018">2018</option>                  
            <option value="2017">2017</option>                 
            <option value="2016">2016</option>                 
            <option value="2015">2015</option>                 
            <option value="2014">2014</option>                 
            <option value="2013">2013</option>                 
            <option value="2012">2012</option>                 
            <option value="2011">2011</option>                 
            <option value="2010">2010</option>                 
            <option value="2009">2009</option>                 
            <option value="2008">2008</option>                 
            <option value="2007">2007</option>                 
            <option value="2006">2006</option>                 
            <option value="2005">2005</option>                 
            <option value="2004">2004</option>                 
            <option value="2003">2003</option>                 
            <option value="2002">2002</option>                 
            <option value="2001">2001</option>                  
            <option value="2000">2000</option>  
            <option value="Prior to 2000">Prior to 2000</option>
        </select>
    </div>



    <div class="form-group">
        <label>Do you have any delinquent tax years covering these taxes?</label>
        <div class="option"><label><input type="checkbox" name="Do_you_have_any1" value="None">None</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_have_any2" value="Sales Tax">Sales Tax</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_have_any3" value="Annual Income Tax">Annual Income Tax</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_have_any4" value="Local Business & Property Tax">Local Business & Property Tax</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_have_any5" value="Other"> Other</label></div>
    </div>



    <!---------->
    <div class="form-group">
        <label>What states are you registered in?<span class="required">*</span></label>
        <select class="form-control" name="What_states_are">
                <option value="">Select State</option>
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DE">Delaware</option>
                <option value="DC">District Of Columbia</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="IA">Iowa</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="ME">Maine</option>
                <option value="MD">Maryland</option>
                <option value="MA">Massachusetts</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MS">Mississippi</option>
                <option value="MO">Missouri</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NV">Nevada</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NY">New York</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VT">Vermont</option>
                <option value="VA">Virginia</option>
                <option value="WA">Washington</option>
                <option value="WV">West Virginia</option>
                <option value="WI">Wisconsin</option>
                <option value="WY">Wyoming</option>

        </select>
    </div>
    <!---------->



    <!----1------>
    <div class="form-group">
        <label>Are you currently under audit? <span class="required">*</span></label>
        <select class="form-control" name="Are_you_currently">
            <option value="Yes">Yes</option>
            <option value="No">No</option>
            <option value="Not Sure">Not Sure</option>on>
        </select>
    </div>




    <!----1------>
    <div class="form-group">
        <label>Are you required to file sales and use tax?<span class="required">*</span></label>
        <select class="form-control" name="Are_you_required_sales">
            <option value="No">No</option>
            <option value="Not Sure">Not Sure</option>
            <option value="Yes - I have less than 100 sales transactions per month">Yes - I have less than 100 sales transactions per month</option>
            <option value="Yes - I have between 100 and 300 sales transactions per month">Yes - I have between 100 and 300 sales transactions per month</option>
            <option value="Yes - I have more than 300 sales transactions per month">Yes - I have more than 300 sales transactions per month</option>
        </select>
    </div>




    <!----1------>
    <div class="form-group">
        <label>Are you required to file local business and property tax?<span class="required">*</span></label>
        <select class="form-control" name="Are_you_required">
            <option value="No">No</option>
            <option value="Yes - I have 1 - 50 Business Properties">Yes - I have 1 - 50 Business Properties</option>
            <option value="Yes - I have over 50 Business Properties">Yes - I have over 50 Business Properties</option>
        </select>
    </div>




    <!----1------>
    <div class="form-group">
        <label>Do you need tax planning & advisory services?</label>
        <div class="option"><label><input type="checkbox" name="Do_you_need1" value="None"> None</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need2" value="Other Federal Taxes (Excise, stamp, capital gains, etc)"> Other Federal Taxes (Excise, stamp, capital gains, etc)</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need3" value="Branch Income"> Branch Income</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need4" value="Group Taxation">  Group Taxation</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need5" value="Transfer Pricing"> Transfer Pricing</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need6" value="Thin Capitalization"> Thin Capitalization</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need7" value="Controlled Foreign Corporations"> Controlled Foreign Corporations</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need8" value="FATCA and FBAR"> FATCA and FBAR</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need9" value="Other"> Other</label></div>
    </div>



    <!----1------>
    <div class="form-group">
        <label>Do you need tax analysis services?</label>
        <div class="option"><label><input type="checkbox" name="Do_you_need_tax1" value="No"> No</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need_tax2" value="Net operating loss studies (tax asset quantification)"> Net operating loss studies (tax asset quantification)</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need_tax3" value="Ownership change analysis (Section 382 studies)"> Ownership change analysis (Section 382 studies)</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need_tax4" value="Cost Segregation studies (determine depreciability)">  Cost Segregation studies (determine depreciability)</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need_tax5" value="Earnings and Profits studies (distinguish non-taxable distributions from dividends)"> Earnings and Profits studies (distinguish non-taxable distributions from dividends)</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need_tax6" value="Nexus studies (determine state tax exposure)"> Nexus studies (determine state tax exposure)</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need_tax7" value="Basis studies (determine taxable gain or loss)"> Basis studies (determine taxable gain or loss)</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need_tax8" value="Built-in gain and loss studies (evaluate tax assets)"> Built-in gain and loss studies (evaluate tax assets)</label></div>
        <div class="option"><label><input type="checkbox" name="Do_you_need_tax9" value="Other"> Other</label></div>
    </div>





    <div class="form-group">
        <label>Please indicate any specific concern that you have about your taxes?</label>
        <textarea class="form-control" name="Please_indicate"></textarea>
    </div>



    <div class="">
        <button type="submit" class="submit-form-btn">SUBMIT</button>
    </div>
</form>
<script src='https://employeeportal.scrubbed.net/js/jquery.validate.min.js'></script>
<script src='https://employeeportal.scrubbed.net/js/form_validation.js'></script>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="site" content="<?php echo site_url() ?>">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>CRM</title>
        <link href="<?php echo site_url('vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?php echo site_url('css/simple-sidebar.css') ?>" rel="stylesheet">
        <link href="<?php echo site_url('css/all.min.css') ?>" rel="stylesheet">
        <link href="<?php echo site_url('css/select2.min.css') ?>" rel="stylesheet">
        <link href="<?php echo site_url('jquery-ui/jquery-ui.structure.min.css') ?>" rel="stylesheet">
        <link href="<?php echo site_url('jquery-ui/jquery-ui.theme.min.css') ?>" rel="stylesheet">
        <link href="<?php echo site_url('jquery-ui/jquery-ui.min.css') ?>" rel="stylesheet">
        
        <link href="<?php echo site_url('css/style.css') ?>" rel="stylesheet">
        <link href='//fonts.googleapis.com/css?family=Montserrat:thin,extra-light,light,100,200,300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="d-flex" id="wrapper">
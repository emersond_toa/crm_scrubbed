<!-- Sidebar -->
<div class="bg-light border-right" id="sidebar-wrapper">
    <div class="sidebar-heading"></div>
    <div class="list-group list-group-flush">
        <a href="<?php echo site_url() ?>" class="list-group-item list-group-item-action bg-light">Dashboard</a>
        <a href="<?php echo site_url('leads') ?>" class="list-group-item list-group-item-action bg-light">Leads</a>
        <a href="<?php echo site_url('clients') ?>" class="list-group-item list-group-item-action bg-light">Clients</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Profile</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Settings</a>
    </div>
</div>
<!-- /#sidebar-wrapper -->
<div id="page-content-wrapper">
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top background-scrubbed">
        <button class="btn mylogo" id="menu-toggle">
            <img src="<?php echo site_url('images/logo.png') ?>" class="logo"/> 
        </button>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo site_url() ?>">Dashboard</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo site_url('leads') ?>">Leads</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo site_url('clients') ?>">Clients</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Profile</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle nav-link-profile" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img data-bind="" src="https://employeeportal.scrubbed.net/hris/img/user-alt-512.webp" class="post-user-image">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mynav" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Settings</a>
                        <a class="dropdown-item" href="#">Logout</a>
                    </div>
                </li>

            </ul>
        </div>
    </nav>
<div class="container-fluid main-container">
    <div class="row">
        <?php $this->load->view('invoices/menu'); ?>
        <div class="col-lg-12 mt-4">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="" class="">
                                <input type="hidden" class="form-control" name="page" value="1">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <input type="month" name="filterDate" id="filterDate" class="form-control" value="<?php echo $filterDate; ?>"> 
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="input-group input-group-sm" style="width: 300px;">
                                            <input type="text" name="keyword"
                                                value="<?php echo $this->Invoice_model->keyword(); ?>"
                                                class="form-control float-right" placeholder="Search">
        
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <a class="btn btn-default float-right mr-2" data-toggle="collapse" href="#collapseAddAr" role="button" aria-expanded="false" aria-controls="collapseExample">Add</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-12">
                            <div class="collapse mt-3" id="collapseAddAr">
                                <div class="card card-body">
                                    <strong>Add AR</strong>
                                    <form class="add_ar" action="<?php echo base_url('invoices/add_ar'); ?>" method="POST"> 
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="invoice_no">Invoice No. <span class="text-danger">*</span></label>
                                                    <input type="text" name="invoice_no" id="invoice_no" class="form-control invoice_no">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="invoice_date">Invoice Date <span class="text-danger">*</span></label>
                                                    <input type="date" name="invoice_date" id="invoice_date" class="form-control invoice_date">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="customer_name">Client Name <span class="text-danger">*</span></label>
                                                    <select class="form-control customer_name chosen-select" id="customer_name" name="customer_name">
                                                        <option value=""></option>
                                                        <?php foreach($clients as $client) : ?>
                                                        <option value="<?php echo $client->entity; ?>"><?php echo $client->entity; ?></option>
                                                        <?php endforeach; ?>
                                                    </select> 
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="amount_due">Amount Due <span class="text-danger">*</span></label>
                                                    <input type="text" name="amount_due" id="amount_due" class="form-control amount_due">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="groupid">Work Group</label>
                                                    <select class="form-control groupid chosen-select" id="groupid" name="groupid">
                                                        <option value=""></option>
                                                        <?php foreach($work_groups as $group) : ?>
                                                        <option value="<?php echo $group->GID; ?>"><?php echo $group->group_name; ?></option>
                                                        <?php endforeach; ?>
                                                    </select> 
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="item_name">Line Item <span class="text-danger">*</span></label>
                                                    <select class="form-control item_name chosen-select" id="item_name" name="item_name">
                                                        <option value=""></option>
                                                        <?php foreach($line_items as $item) : ?>
                                                        <option value="<?php echo $item->name; ?>"><?php echo $item->name; ?></option>
                                                        <?php endforeach; ?>
                                                    </select> 
                                                </div>

                                                <div class="form-group text-right">
                                                    <button type="submit" class="btn btn-primary btn-submit">Add</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="table-responsive mt-3">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Month</th>
                                            <th scope="col">Year</th>
                                            <th scope="col">Invoice Number</th>
                                            <th scope="col">Invoice Date</th>
                                            <th scope="col">Client Name</th>
                                            <th scope="col">Group</th>
                                            <th scope="col">Line Item</th>
                                            <th scope="col">Amount Due</th>
                                            <th scope="col" class="width_100 text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="ar_row">
                                    <?php if(!empty($ars)){ foreach($ars as $row){ ?>
                                    <tr class="ar_item_<?php echo $row->ID; ?>">
                                        <td><?php echo $row->ID; ?></td>
                                        <td><?php echo $row->month; ?></td>
                                        <td><?php echo $row->year; ?></td>
                                        <td><?php echo $row->invoice_no; ?></td>
                                        <td><?php echo $row->invoice_date; ?></td>
                                        <td><?php echo $row->customer_name; ?></td>
                                        <td><?php echo $row->group_name; ?></td>
                                        <td><?php echo $row->item_name; ?></td>
                                        <td><?php echo $row->amount_due; ?></td>
                                        <td class="text-center">
                                            <button data-id="<?php echo $row->ID; ?>" data-toggle="modal" data-target="#editarModal" class="btn btn-light btn-sm mr-1 edit-ar"><i class="fa fa-edit"></i></button>
                                            <button data-id="<?php echo $row->ID; ?>" class="btn btn-light btn-sm delete-ar" ><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    <?php } }else{ ?>
                                    <tr><td colspan="13">No invoice(s) found...</td></tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <?php echo $arsPagination; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editarModal" tabindex="-1" role="dialog" aria-labelledby="editarModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editarModalLabel">Edit AR</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="edit_ar" action="<?php echo base_url('invoices/updateAr'); ?>"> 
                    <input type="hidden" name="edit_ar_id" id="edit_ar_id" class="form-control edit_ar_id">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="edit_invoice_no">Invoice No. <span class="text-danger">*</span></label>
                                <input type="text" name="edit_invoice_no" id="edit_invoice_no" class="form-control edit_invoice_no">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="edit_invoice_date">Invoice Date <span class="text-danger">*</span></label>
                                <input type="date" name="edit_invoice_date" id="edit_invoice_date" class="form-control edit_invoice_date">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="edit_customer_name">Client Name <span class="text-danger">*</span></label>
                                <select class="form-control edit_customer_name chosen-select" id="edit_customer_name" name="edit_customer_name">
                                    <option value=""></option>
                                    <?php foreach($clients as $client) : ?>
                                    <option value="<?php echo $client->entity; ?>"><?php echo $client->entity; ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="edit_amount_due">Amount Due <span class="text-danger">*</span></label>
                                <input type="text" name="edit_amount_due" id="edit_amount_due" class="form-control edit_amount_due">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="edit_groupid">Work Group</label>
                                <select class="form-control edit_groupid chosen-select" id="edit_groupid" name="edit_groupid">
                                    <option value=""></option>
                                    <?php foreach($work_groups as $group) : ?>
                                    <option value="<?php echo $group->GID; ?>"><?php echo $group->group_name; ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="edit_item_name">Line Item <span class="text-danger">*</span></label>
                                <select class="form-control edit_item_name chosen-select" id="edit_item_name" name="edit_item_name">
                                    <option value=""></option>
                                    <?php foreach($line_items as $item) : ?>
                                    <option value="<?php echo $item->name; ?>"><?php echo $item->name; ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary btn-submit">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
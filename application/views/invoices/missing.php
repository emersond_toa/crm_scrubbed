<div class="container-fluid main-container">
    <div class="row">

        <?php $this->load->view('invoices/menu'); ?>

        <div class="col-lg-12 mt-4">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5 class="card-title"><?php echo number_format($total); ?> Total Floating Invoices</h5>
                        </div>
                        <div class="col-lg-6">
                            <form class="float-right mt-1" method="get">
                                <input type="hidden" class="form-control" name="page" value="1">
                
                                <div class="input-group input-group-sm" style="width: 300px;">
                                    <input type="text" name="keyword"
                                        value="<?php echo $this->Invoice_model->keyword(); ?>"
                                        class="form-control float-right" placeholder="Search">
                
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default"><i
                                                class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-lg-12">
                            <div class="table-responsive mt-3">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">Client ID</th>
                                            <th scope="col">Client Name</th>
                                            <th scope="col">Work Group</th>
                                            <th scope="col">Invoice #</th>
                                            <th scope="col">Line Item Name</th>
                                            <th scope="col">Transaction Date</th>
                                            <th scope="col">Line Item Amount</th>
                                            <th scope="col">Source</th>
                                            <th scope="col" class="text-center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody class="revenue_row">
                                    <?php if(!empty($invoices)){ foreach($invoices as $row){ ?>
                                    <tr class="transaction_item_<?php echo $row->ID; ?>">
                                        <td><?php echo $row->client_id; ?></td>
                                        <td><?php echo $row->client_name; ?></td>
                                        <td><?php echo $row->group_name; ?></td>
                                        <td><a href="<?php echo site_url('invoices'); ?>/show/<?php echo $row->invoice_number; ?>"><?php echo $row->invoice_number; ?></a></td>
                                        <td><?php echo $row->line_item_name; ?></td>
                                        <td><?php echo date('m/d/Y', strtotime($row->transaction_date)); ?></td>
                                        <td><?php echo $row->line_item_amount; ?></td>
                                        <td><?php echo $row->source; ?></td>
                                        <td class="text-center">
                                            <button data-id="<?php echo $row->ID; ?>" data-toggle="modal" data-target="#editrevenueModal" class="btn btn-light btn-sm mr-1 edit-transaction"><i class="fa fa-edit"></i></button>
                                            <button data-id="<?php echo $row->ID; ?>" class="btn btn-light btn-sm delete-transaction" ><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    <?php } }else{ ?>
                                    <tr><td colspan="13">No invoice(s) found...</td></tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <?php echo $invoices_pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editrevenueModal" tabindex="-1" role="dialog" aria-labelledby="editrevenueModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editrevenueModalLabel">Edit line item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="edit_revenue" action="<?php echo base_url('invoices/updateRevenue'); ?>"> 
                    <input type="hidden" name="edit_transaction_id" id="edit_transaction_id" class="form-control edit_transaction_id">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="edit_client">Client <span class="text-danger">*</span></label>
                                <select class="form-control edit_client chosen-select" id="edit_client" name="edit_client">
                                    <option value=""></option>
                                    <?php foreach($clients as $client) : ?>
                                    <option value="<?php echo $client->ID; ?>"><?php echo $client->entity; ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>

                            <div class="form-group">
                                <label for="edit_invoice_no">Invoice No. <span class="text-danger">*</span></label>
                                <input type="text" name="edit_invoice_no" id="edit_invoice_no" class="form-control edit_invoice_no">
                            </div>

                            <div class="form-group">
                                <label for="edit_transaction_date">Transaction Date <span class="text-danger">*</span></label>
                                <input type="date" name="edit_transaction_date" id="edit_transaction_date" class="form-control edit_transaction_date">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="edit_work_group">Work Group</label>
                                <select class="form-control edit_work_group chosen-select" id="edit_work_group" name="edit_work_group">
                                    <option value=""></option>
                                    <?php foreach($work_groups as $group) : ?>
                                    <option value="<?php echo $group->GID; ?>"><?php echo $group->group_name; ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>

                            <div class="form-group">
                                <label for="edit_line_item_amount">Line Item Amount <span class="text-danger">*</span></label>
                                <input type="text" name="edit_line_item_amount" id="edit_line_item_amount" class="form-control edit_line_item_amount">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="edit_line_item_name">Line Item Name <span class="text-danger">*</span></label>
                                <select class="form-control edit_line_item_name chosen-select" id="edit_line_item_name" name="edit_line_item_name">
                                    <option value=""></option>
                                    <?php foreach($line_items as $item) : ?>
                                    <option value="<?php echo $item->name; ?>"><?php echo $item->name; ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>

                            <div class="form-group">
                                <label for="edit_source">Source <span class="text-danger">*</span></label>
                                <select class="form-control edit_source chosen-select" id="edit_source" name="edit_source">
                                    <option value=""></option>
                                    <?php foreach($sources as $source) : ?>
                                    <option value="<?php echo $source[0]; ?>"><?php echo $source[1]; ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-primary btn-submit">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
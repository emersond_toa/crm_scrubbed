<div class="container-fluid main-container">
    <div class="row">
        <?php $this->load->view('invoices/menu'); ?>

        <div class="col-lg-6">
            <div class="card mb-4">
                <div class="card-body">
                    <?php if(!empty($success_msg)){ ?>
                    <div class="col-xs-12">
                        <div class="alert alert-success"><?php echo $success_msg; ?></div>
                    </div>
                    <?php } ?>
                    <?php if(!empty($error_msg)){ ?>
                    <div class="col-xs-12">
                        <div class="alert alert-danger"><?php echo $error_msg; ?></div>
                    </div>
                    <?php } ?>

                    <form action="<?php echo base_url('invoices/import'); ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="source" value="bill">
                        <div class="form-group">
                            <label for="file">Upload Invoice from Bill.com</label>
                            <input type="file" class="form-control" id="file" name="file">
                        </div>
                        <input type="submit" class="btn btn-primary float-right" name="importSubmit" value="IMPORT">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

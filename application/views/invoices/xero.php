<div class="container-fluid main-container">
    <div class="row">
        <div class="col-lg-6">
            <h1 class="h3 mb-4 text-gray-800">Invoices</h1>
        </div>

        <div class="col-lg-6 text-right">
            <a href="<?php echo site_url('payments') ?>" class="btn btn-default">Payments</a>
        </div>

        <div class="col-lg-12">
            <ul class="nav nav-tabs2 mb-4" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link" href="<?php echo base_url('invoices'); ?>">Bill.com</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" href="<?php echo base_url('invoices/xero'); ?>">Xero</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" href="<?php echo base_url('payments'); ?>">Payments</a>
                </li>
            </ul>
        </div>

        <div class="col-lg-6">
            <div class="card mb-4">
                <div class="card-body">
                    <!-- Display status message -->
                    <?php if(!empty($success_msg)){ ?>
                    <div class="col-xs-12">
                        <div class="alert alert-success"><?php echo $success_msg; ?></div>
                    </div>
                    <?php } ?>
                    <?php if(!empty($error_msg)){ ?>
                    <div class="col-xs-12">
                        <div class="alert alert-danger"><?php echo $error_msg; ?></div>
                    </div>
                    <?php } ?>

                    <a href="<?php echo base_url('invoices/exportXeroTemplate'); ?>" onclick="event.preventDefault();
                                    document.getElementById('export-form').submit();">
                                    Download csv template
                    </a>
                    <form id="export-form" action="<?php echo base_url('invoices/exportXeroTemplate'); ?>" method="post" class="d-none">
                    </form>
                    <form class="mt-3" action="<?php echo base_url('invoices/import'); ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="source" value="xero">
                        <div class="form-group">
                            <label for="file">Upload from Xero</label>
                            <input type="file" class="form-control" id="file" name="file">
                            <!-- <small class="form-text text-muted">Upload invoice csv file from bill.com</small> -->
                        </div>
                        <input type="submit" class="btn btn-primary float-right" name="importSubmit" value="IMPORT">
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-12 mt-4">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 mb-3">
                            <ul class="nav nav-tabs2" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" href="<?php echo base_url('invoices'); ?>">Invoices</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="<?php echo base_url('invoices/not_included'); ?>">Floating Invoices</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <h5 class="card-title"><?php echo number_format($total); ?> Total Transactions</h5>
                        </div>
                        <div class="col-lg-6">
                            <form class="float-right" method="get">
                                <input type="hidden" class="form-control" name="page" value="1">
                
                                <div class="input-group input-group-sm" style="width: 300px;">
                                    <input type="text" name="keyword"
                                        value="<?php echo $this->Invoice_model->keyword(); ?>"
                                        class="form-control float-right" placeholder="Search">
                
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default"><i
                                                class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-lg-12">
                            <div class="table-responsive mt-3">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Client ID</th>
                                            <th scope="col">Client Name</th>
                                            <th scope="col">Group</th>
                                            <th scope="col">Invoice #</th>
                                            <th scope="col">Credit Amount</th>
                                            <th scope="col">Line Item Name</th>
                                            <th scope="col">Transaction Date</th>
                                            <th scope="col">Line Item Amount</th>
                                            <th scope="col">Total Line Items</th>
                                            <th scope="col">Created</th>
                                            <th scope="col">Updated</th>
                                            <th scope="col">Source</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(!empty($invoices)){ foreach($invoices as $row){ ?>
                                    <tr>
                                        <td><?php echo $row->ID; ?></td>
                                        <td><?php echo $row->client_id; ?></td>
                                        <td><?php echo $row->entity; ?></td>
                                        <td><?php echo $row->group_name; ?></td>
                                        <td><a href="<?php echo site_url('invoices'); ?>/show/<?php echo $row->invoice_number; ?>"><?php echo $row->invoice_number; ?></a></td>
                                        <td><?php echo $row->credit_amount; ?></td>
                                        <td><?php echo $row->line_item_name; ?></td>
                                        <td><?php echo date('m/d/Y', strtotime($row->transaction_date)); ?></td>
                                        <td><?php echo $row->line_item_amount; ?></td>
                                        <td><?php echo $row->total_line_items; ?></td>
                                        <td><?php echo date('m/d/Y', strtotime($row->created_at)); ?></td>
                                        <td><?php echo date('m/d/Y', strtotime($row->updated_at)); ?></td>
                                        <td><?php echo $row->source; ?></td>
                                    </tr>
                                    <?php } }else{ ?>
                                    <tr><td colspan="13">No invoice(s) found...</td></tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <?php echo $invoices_pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
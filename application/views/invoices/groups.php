<div class="container-fluid main-container">
    <div class="row">

        <?php $this->load->view('invoices/menu'); ?>

        <div class="col-lg-12 mt-4">
            <div class="card mb-5">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="row revenue_ar">
                                <div class="col-sm-4">
                                    <div class="panel panel-custom">
                                        <div class="panel-icon" style="background: #3a94d0;">
                                            <i class="fa fa-file-invoice-dollar"></i>
                                        </div>
                                        <div class="panel-wrap mt-4">
                                            <h4 class="text-gray-800">Total Revenue</h4>
                                            <h2 class="text-gray-800 total_revenue">00.00</h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="panel panel-custom">
                                        <div class="panel-icon" style="background: #52b2e4;">
                                            <i class="fa fa-money-check-alt"></i>
                                        </div>
                                        <div class="panel-wrap mt-4">
                                            <h4 class="text-gray-800">Total AR</h4>
                                            <h2 class="text-gray-800 total_ar">00.00</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row group_revenue hide">
                                <div class="col-sm-4">
                                    <div class="panel panel-custom">
                                        <div class="panel-icon" style="background: #3a94d0;">
                                            <i class="fa fa-file-invoice-dollar"></i>
                                        </div>
                                        <div class="panel-wrap mt-4">
                                            <h4 class="text-gray-800">Total Revenue</h4>
                                            <h2 class="text-gray-800 total_group_revenue">00.00</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row group_ar hide">
                                <div class="col-sm-4">
                                    <div class="panel panel-custom">
                                        <div class="panel-icon" style="background: #52b2e4;">
                                            <i class="fa fa-money-check-alt"></i>
                                        </div>
                                        <div class="panel-wrap mt-4">
                                            <h4 class="text-gray-800">Total AR</h4>
                                            <h2 class="text-gray-800 total_group_ar">00.00</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <form action="">
                                <input type="hidden" name="selected_group_id" id="selected_group_id" value="0">
                                <div class="form-group">
                                    <label for="filter_date">Date Filter</label>
                                    <input type="month" name="filter_date" id="filter_date" class="form-control" value="<?php echo date('Y-m', strtotime("-1 month")); ?>"> 
                                </div>
                            </form>
                        </div>


                        <div class="col-lg-12">
                            <div class="table-responsive mt-3 group_revenu_ar_table">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">Work Group</th>
                                            <th scope="col" class="text-center">Revenue</th>
                                            <th scope="col" class="text-center">Account Recievable</th>
                                        </tr>
                                    </thead>
                                    <tbody class="group_revenu_ar_tbody">
                                        <tr>
                                            <td colspan="3">Loading...</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive mt-3 revenue_table hide">
                                <div class="d-flex jsutify-content-left">
                                    <a href="javascript:void(0);" class="text-gray-800 mr-2 back_group_revenu_ar_table"> <i class="fa fa-arrow-left"></i> </a>
                                    <h6 class="group_name_text font-weight-bold">Group Name</h6>
                                </div>
                                <table class="table table-striped table-bordered mt-2">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Client Name</th>
                                            <th scope="col">Invoice #</th>
                                            <th scope="col">Transaction Date</th>
                                            <!-- <th scope="col">Line Item Name</th> -->
                                            <th scope="col" class="text-center">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody class="revenue_body">
                                        <tr>
                                            <td colspan="5">Loading...</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive mt-3 ar_table hide">
                                <div class="d-flex jsutify-content-left">
                                    <a href="javascript:void(0);" class="text-gray-800 mr-2 back_group_revenu_ar_table"> <i class="fa fa-arrow-left"></i> </a>
                                    <h6 class="group_name_text font-weight-bold">Group Name</h6>
                                </div>
                                <table class="table table-striped table-bordered mt-2">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Client Name</th>
                                            <th scope="col">Invoice #</th>
                                            <th scope="col">Invoice Date</th>
                                            <!-- <th scope="col">Line Item Name</th> -->
                                            <th scope="col" class="text-center">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody class="ar_body">
                                        <tr>
                                            <td colspan="5">Loading...</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
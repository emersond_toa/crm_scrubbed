<div class="container-fluid main-container">
    <div class="row">
        <div class="col-lg-6">
            <h1 class="h3 mb-4 text-gray-800">Invoice# <?php echo $this->uri->segment(3); ?></h1>
        </div>

        <div class="col-lg-6 text-right">
            <a href="<?php echo site_url('invoices') ?>" class="btn btn-default">Invoices</a>
            <a href="<?php echo site_url('payments') ?>" class="btn btn-default">Payments</a>
        </div>

        <div class="col-lg-12 mt-4">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Created</th>
                        <th scope="col">Updated</th>
                        <th scope="col">Client Name</th>
                        <th scope="col">Group</th>
                        <!-- <th scope="col">Invoice #</th> -->
                        <th scope="col">Credit Amount</th>
                        <th scope="col">Line Item Name</th>
                        <th scope="col">Transaction Date</th>
                        <th scope="col">Line Item Amount</th>
                        <th scope="col">Total Line Items</th>
                        <th scope="col">Type</th>
                    </tr>
                </thead>
                <tbody>
                <?php if(!empty($invoices)){ foreach($invoices as $row){ ?>
                <tr>
                    <td><?php echo date('m/d/Y', strtotime($row['created_at'])); ?></td>
                    <td><?php echo date('m/d/Y', strtotime($row['updated_at'])); ?></td>
                    <td><?php echo $row['entity']; ?></td>
                    <td><?php echo $row['group_name']; ?></td>
                    <!-- <td><?php echo $row['invoice_number']; ?></td> -->
                    <td><?php echo $row['credit_amount']; ?></td>
                    <td><?php echo $row['line_item_name']; ?></td>
                    <td><?php echo date('m/d/Y', strtotime($row['transaction_date'])); ?></td>
                    <td><?php echo number_format($row['line_item_amount'], 2); ?></td>
                    <td><?php echo $row['total_line_items']; ?></td>
                    <td><?php echo ucfirst($row['transaction_type']); ?></td>
                </tr>
                <?php } }else{ ?>
                <tr><td colspan="9">No invoice(s) found...</td></tr>
                <?php } ?>

                <?php if(!empty($payments)){ foreach($payments as $row){ ?>
                <tr>
                    <td><?php echo date('m/d/Y', strtotime($row['created_at'])); ?></td>
                    <td><?php echo date('m/d/Y', strtotime($row['updated_at'])); ?></td>
                    <td><?php echo $row['entity']; ?></td>
                    <td><?php echo $row['group_name']; ?></td>
                    <!-- <td><?php echo $row['invoice_number']; ?></td> -->
                    <td><?php echo $row['credit_amount']; ?></td>
                    <td><?php echo $row['line_item_name']; ?></td>
                    <td><?php echo date('m/d/Y', strtotime($row['transaction_date'])); ?></td>
                    <td>-<?php echo number_format($row['line_item_amount'], 2); ?></td>
                    <td><?php echo $row['total_line_items']; ?></td>
                    <td><?php echo ucfirst($row['transaction_type']); ?></td>
                </tr>
                <?php } }else{ ?>
                <tr><td colspan="10">No payments(s) found...</td></tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

        <div class="col-lg-4 ml-auto mt-4">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Invoice</th>
                        <th scope="col">Payment</th>
                        <th scope="col">Balance</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo number_format($total_invoice, 2); ?></td>
                        <td>-<?php echo number_format($total_payments, 2); ?></td>
                        <td><?php echo number_format($total_balance, 2); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <ul class="nav nav-tabs2 mb-4" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link <?php echo ($this->uri->segment(1) == "Invoices" && $this->uri->segment(2) == "") ? 'active' : ''; ?>" href="<?php echo base_url('invoices'); ?>">Invoices</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link <?php echo ($this->uri->segment(2) == "not_included") ? 'active' : ''; ?>" href="<?php echo base_url('invoices/not_included'); ?>">Floating Invoices</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link <?php echo ($this->uri->segment(2) == "upload_invoices") ? 'active' : ''; ?>" href="<?php echo base_url('invoices/upload_invoices'); ?>">Upload Invoices</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link <?php echo ($this->uri->segment(1) == "payments" && $this->uri->segment(2) == "") ? 'active' : ''; ?>" href="<?php echo base_url('payments'); ?>">Payments</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link <?php echo ($this->uri->segment(1) == "payments" && $this->uri->segment(2) == "upload") ? 'active' : ''; ?>" href="<?php echo base_url('payments/upload'); ?>">Upload Payments</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link <?php echo ($this->uri->segment(2) == "work_groups") ? 'active' : ''; ?>" href="<?php echo base_url('invoices/work_groups'); ?>">Work Groups</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link <?php echo ($this->uri->segment(2) == "AccountsReceivable") ? 'active' : ''; ?>" href="<?php echo base_url('invoices/AccountsReceivable'); ?>">Accounts Receivable</a>
        </li>
    </ul>
</div>
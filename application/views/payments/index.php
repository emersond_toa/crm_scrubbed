<div class="container-fluid main-container">
    <div class="row">
        <?php $this->load->view('invoices/menu'); ?>
        <div class="col-lg-12 mt-4">
            <div class="card mb-5">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5 class="card-title"><?php echo number_format($total); ?> Total Payments</h5>
                        </div>
                        <div class="col-lg-6">
                            <form class="float-right" method="get">
                                <input type="hidden" class="form-control" name="page" value="1">
                
                                <div class="input-group input-group-sm" style="width: 300px;">
                                    <input type="text" name="keyword"
                                        value="<?php echo $this->Payment_model->keyword(); ?>"
                                        class="form-control float-right" placeholder="Search">
                
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default"><i
                                                class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-lg-12">
                            <div class="table-responsive mt-3">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">Client ID</th>
                                            <th scope="col">Client Name</th>
                                            <!-- <th scope="col">Work Group</th> -->
                                            <th scope="col">Invoice #</th>
                                            <th scope="col">Transaction Date</th>
                                            <th scope="col">Line Item Amount</th>
                                            <!-- <th scope="col">Total Line Items</th> -->
                                            <!-- <th scope="col">Created</th> -->
                                            <!-- <th scope="col">Updated</th> -->
                                            <th scope="col">Source</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(!empty($payments)){ foreach($payments as $row){ ?>
                                    <tr>
                                        <td><?php echo $row->client_id; ?></td>
                                        <td><?php echo $row->entity; ?></td>
                                        <!-- <td><?php echo $row->group_name; ?></td> -->
                                        <td><a href="<?php echo site_url('invoices'); ?>/show/<?php echo $row->invoice_number; ?>"><?php echo $row->invoice_number; ?></a></td>
                                        <td><?php echo date('m/d/Y', strtotime($row->transaction_date)); ?></td>
                                        <td><?php echo $row->line_item_amount; ?></td>
                                        <!-- <td><?php echo $row->total_line_items; ?></td> -->
                                        <!-- <td><?php echo date('m/d/Y', strtotime($row->created_at)); ?></td> -->
                                        <!-- <td><?php echo date('m/d/Y', strtotime($row->updated_at)); ?></td> -->
                                        <td><?php echo $row->source; ?></td>
                                    </tr>
                                    <?php } }else{ ?>
                                    <tr><td colspan="8">No payment(s) found...</td></tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-12">
                        <?php echo $payments_pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
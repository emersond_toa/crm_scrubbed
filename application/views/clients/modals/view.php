<!-- Modal -->
<div class="full-modal modal" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModalLabel"
     aria-hidden="true">
    <div class="modal-dialog  modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Client Details</h5>
                <input type="hidden" name="companyId" id="companyId">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="container-1400">
                <div class="card company-header">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="company-logo company-logo-edit" action="<?php echo site_url('request/upload_logo') ?>" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="client_id" class="hide client_id">
                                <label class="edit-logo-upload">
                                    <img src="<?php echo site_url('uploads/logos/default.jpg') ?>"
                                         class="c-logo d-inline-block">
                                    <input type="file" class="file-upload" name="userfile" accept="image/*">
                                    <span class="fa-edit-upload"><i class="fas fa-edit"></i></span>
                                </label>

                            </form>
                            <div class="right-infomation">
                                <div class="col-lg-6">
                                    <div class="d-flex">

                                        <div>
                                            <p><strong class="company"></strong></p>
                                            <p><span class="contactfullname"></span> - <span class="jobtitle"></span></p>
                                            <p class="contactemail"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <form action="">
                                        <div class="form-group mb-0">
                                            <label for="contactstatus" class="col-form-label pt-0">Status: <span
                                                    class="status-content mt-1"></span></label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs2" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="Profile-tab" data-toggle="tab" href="#Profile" role="tab"
                                   aria-controls="Profile" aria-selected="true">Profile</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="Entities-tab" data-toggle="tab" href="#Entities" role="tab"
                                   aria-controls="Entities" aria-selected="false">Entities</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="Services-tab" data-toggle="tab" href="#Services" role="tab"
                                   aria-controls="Services" aria-selected="false">Services</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="Notes-tab" data-toggle="tab" href="#Notes" role="tab"
                                   aria-controls="Notes" aria-selected="false">Notes</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="Files-tab" data-toggle="tab" href="#Files" role="tab"
                                   aria-controls="Files" aria-selected="false">Files</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="Reminder-tab" data-toggle="tab" href="#Reminder" role="tab"
                                   aria-controls="Reminder" aria-selected="false">Reminders</a>
                            </li>
                            <li class="nav-item hide" role="presentation">
                                <a class="nav-link" id="Billing-tab" data-toggle="tab" href="#Billing" role="tab"
                                   aria-controls="Billing" aria-selected="false">Billing</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="Transaction-tab" data-toggle="tab" href="#Transaction" role="tab"
                                   aria-controls="Transaction" aria-selected="false">Transactions</a> 
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="Experience-tab" data-toggle="tab" href="#Experience" role="tab"
                                   aria-controls="Experience" aria-selected="false">Client Experience</a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="tab-content mb-5" id="myTabContent">
                    <div class="tab-pane fade show active" id="Profile" role="tabpanel" aria-labelledby="Profile-tab">
                        <div class="card company-content">
                            <div class="row profile-tab-content">
                                <div class="col-lg-12">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="mt-2">Profile</h6>
                                        <a href="#" class="btn btn-default open-add-contact open-client-editmodal" data-toggle="modal" data-target="#editModal">Edit</a>
                                    </div>
                                    <hr>
                                </div>
                                <div class="col-lg-6">
                                    <form class="company-info">
                                        <div class="form-group row">
                                            <label for="companyname" class="col-sm-2 col-form-label">Company:</label>
                                            <div class="col-sm-10">
                                                <input type="text" readonly class="form-control-plaintext" id="companyname">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companywebsite" class="col-sm-2 col-form-label">Website:</label>
                                            <div class="col-sm-10">
                                                <input type="text" readonly class="form-control-plaintext" id="companywebsite">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyindustry" class="col-sm-2 col-form-label">Industry:</label>
                                            <div class="col-sm-10">
                                                <input type="text" readonly class="form-control-plaintext" id="companyindustry">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyaddress" class="col-sm-2 col-form-label">Address:</label>
                                            <div class="col-sm-10">
                                                <input type="text" readonly class="form-control-plaintext" id="companyaddress">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyphone" class="col-sm-2 col-form-label">Phone:</label>
                                            <div class="col-sm-10">
                                                <input type="text" readonly class="form-control-plaintext" id="companyphone">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyemail" class="col-sm-2 col-form-label">Email:</label>
                                            <div class="col-sm-10">
                                                <input type="text" readonly class="form-control-plaintext" id="companyemail">
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="col-lg-6">
                                    <form class="company-info">
                                        <div class="form-group row">
                                            <label for="clientworkgroups" class="col-sm-4 col-form-label">Work Groups:</label>
                                            <div class="col-sm-8">
                                                <input type="text" readonly class="form-control-plaintext" id="clientworkgroups">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyreferral" class="col-sm-4 col-form-label">Referral Partner:</label>
                                            <div class="col-sm-8">
                                                <input type="text" readonly class="form-control-plaintext" id="companyreferral">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companysource" class="col-sm-4 col-form-label">Lead Source:</label>
                                            <div class="col-sm-8">
                                                <input type="text" readonly class="form-control-plaintext" id="companysource">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companystartdate" class="col-sm-4 col-form-label">Start Date:</label>
                                            <div class="col-sm-8">
                                                <input type="text" readonly class="form-control-plaintext" id="companystartdate">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyterminationdate" class="col-sm-4 col-form-label">Termination Date:</label>
                                            <div class="col-sm-8">
                                                <input type="text" readonly class="form-control-plaintext" id="companyterminationdate">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyterminationreason" class="col-sm-4 col-form-label">Termination Reason:</label>
                                            <div class="col-sm-8">
                                                <input type="text" readonly class="form-control-plaintext" id="companyterminationreason">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="card company-content mt-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="mb-0 pt-2">Contact Persons</h6>
                                        <div class="action-buttons">
                                            <button class="btn btn-default open-add-contact">
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="collapse my-2" id="collapseAddContact">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="<?php echo site_url('contacts/store') ?>"
                                                      class="create-contact">
                                                    <label class="col-form-label">Add Contact</label>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="contactaddentity"
                                                                       class="col-form-label">Entity</label>
                                                                <select class="form-control" name="contactaddentity" id="contactaddentity">
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contactaddfullname"
                                                                       class="col-form-label">Name</label>
                                                                <input type="text" class="form-control"
                                                                       id="contactaddfullname"
                                                                       name="contactaddfullname">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contactaddjobtitle"
                                                                       class="col-form-label">Job
                                                                    Title</label>
                                                                <input type="text" class="form-control"
                                                                       id="contactaddjobtitle"
                                                                       name="contactaddjobtitle">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contactaddlinkedin"
                                                                       class="col-form-label">Linkedin</label>
                                                                <input type="text" class="form-control"
                                                                       id="contactaddlinkedin"
                                                                       name="contactaddlinkedin">
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="contactaddemail"
                                                                       class="col-form-label">Email</label>
                                                                <input type="text" class="form-control"
                                                                       id="contactaddemail" name="contactaddemail">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="contactaddphone"
                                                                       class="col-form-label">Phone</label>
                                                                <input type="text" class="form-control"
                                                                       id="contactaddphone" name="contactaddphone">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contactaddskype"
                                                                       class="col-form-label">Skype</label>
                                                                <input type="text" class="form-control"
                                                                       id="contactaddskype" name="contactaddskype">
                                                            </div>

                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                   class="btn btn-default mr-2 close-add-contact">Close</a>
                                                                <button type="submit"
                                                                        class="btn btn-primary save-contact">Save</button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="collapse my-2" id="collapseEditContact">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="<?php echo site_url('contacts/update') ?>"
                                                      class="update-contact">
                                                    <input type="hidden" name="contacteditid" id="contacteditid">
                                                    <label class="col-form-label">Edit Contact</label>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="contacteditentity"
                                                                       class="col-form-label">Entity</label>
                                                                <select class="form-control" name="contacteditentity" id="contacteditentity">
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contacteditfullname"
                                                                       class="col-form-label">Name</label>
                                                                <input type="text" class="form-control"
                                                                       id="contacteditfullname"
                                                                       name="contacteditfullname">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contacteditjobtitle"
                                                                       class="col-form-label">Job
                                                                    Title</label>
                                                                <input type="text" class="form-control"
                                                                       id="contacteditjobtitle"
                                                                       name="contacteditjobtitle">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contacteditlinkedin"
                                                                       class="col-form-label">Linkedin</label>
                                                                <input type="text" class="form-control"
                                                                       id="contacteditlinkedin"
                                                                       name="contacteditlinkedin">
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="contacteditemail"
                                                                       class="col-form-label">Email</label>
                                                                <input type="text" class="form-control"
                                                                       id="contacteditemail" name="contacteditemail">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="contacteditphone"
                                                                       class="col-form-label">Phone</label>
                                                                <input type="text" class="form-control"
                                                                       id="contacteditphone" name="contacteditphone">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="contacteditskype"
                                                                       class="col-form-label">Skype</label>
                                                                <input type="text" class="form-control"
                                                                       id="contacteditskype" name="contacteditskype">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="contacteditstatus"
                                                                       class="col-form-label">Status</label>
                                                                <select class="form-control"
                                                                        name="contacteditstatus" id="contacteditstatus">
                                                                    <option value="1">Active</option>
                                                                    <option value="0">Inactive</option>
                                                                </select>
                                                            </div>

                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                   class="btn btn-default mr-2 close-edit-contact">Close</a>
                                                                <button type="submit"
                                                                        class="btn btn-primary update-contact">Update</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="table-responsive mt-3">
                                        <table class="table table-bordered table-hover table-striped nopaddingmargin table-middle" width="100%" cellspacing="0"
                                               role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Entity</th>
                                                    <th>Contact Person</th>
                                                    <th>Job Title</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Linkedin</th>
                                                    <th>Skype</th>
                                                    <th class="width_100">Status</th>
                                                    <th class="text-center width_200">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="contact-table-body">
                                                <tr>
                                                    <td colspan="9" class="text-center">Loading...</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="Entities" role="tabpanel" aria-labelledby="Entities-tab">
                        <div class="card company-content">
                            <div class="row entity-tab-content">
                                <div class="col-lg-12">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="mb-0 pt-2">Entities</h6>
                                        <div class="action-buttons">
                                            <button class="btn btn-default open-add-entity">
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">    
                                    <div class="collapse my-2" id="collapseAddEntity">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="<?php echo site_url('entity/store') ?>"
                                                      class="create-entity">
                                                    <label class="col-form-label">Add Entity</label>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label for="entityname"
                                                                       class="col-form-label">Entity Name</label>
                                                                <input type="text" class="form-control"
                                                                       id="entityname" name="entityname">
                                                            </div>
                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                   class="btn btn-default mr-2 close-add-entity">Close</a>
                                                                <button type="submit"
                                                                        class="btn btn-primary save-entity">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="collapse my-2" id="collapseEditEntity">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="<?php echo site_url('entity/update') ?>"
                                                      class="update-entity">
                                                    <input type="hidden" name="entityeditid" id="entityeditid">
                                                    <label class="col-form-label">Edit Enity</label>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label for="entityeditname"
                                                                       class="col-form-label">Entity Name</label>
                                                                <input type="text" class="form-control"
                                                                       id="entityeditname" name="entityeditname">
                                                            </div>
                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                   class="btn btn-default mr-2 close-edit-entity">Close</a>
                                                                <button type="submit"
                                                                        class="btn btn-primary update-entity">Update</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="table-responsive mt-3">
                                        <table class="table table-bordered table-hover table-striped nopaddingmargin table-middle" width="100%" cellspacing="0"
                                               role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Entity</th>
                                                    <th class="text-center width_200">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody class="entity-table-body">
                                                <tr>
                                                    <td colspan="2">Loading...</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="Services" role="tabpanel" aria-labelledby="Services-tab">
                        <div class="card company-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="mb-0 pt-2">Services</h6>
                                        <div class="action-buttons">
                                            <button class="btn btn-default open-add-type-of-service">
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="collapse my-2" id="collapseAddServiceType">
                                        <div class="card">
                                            <div class="card-body">
                                                <form
                                                    action="<?php echo site_url('Type_of_Service/store') ?>"
                                                    class="create-type-of-services">
                                                    <label class="col-form-label">Add Type of
                                                        Service</label>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <!-- <div class="form-group">
                                                                <label for="group"
                                                                    class="col-form-label">Group</label>
                                                                <input type="text" class="form-control"
                                                                    id="group" name="group">
                                                            </div> -->

                                                            <div class="form-group">
                                                                <label for="tosentity"
                                                                       class="col-form-label">Entity</label>
                                                                <select class="form-control" name="tosentity" id="tosentity">
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="typeofservice"
                                                                       class="col-form-label">Type of
                                                                    Service</label>
                                                                <select class="form-control"
                                                                        name="typeofservice" id="typeofservice">
                                                                    <option></option>
                                                                    <?php foreach ($services as $service) : ?>
                                                                        <option
                                                                            value="<?php echo $service->id; ?>">
                                                                                <?php echo $service->name; ?>
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="agreement"
                                                                       class="col-form-label">Agreement</label>
                                                                <select class="form-control"
                                                                        name="agreement" id="agreement">
                                                                    <option></option>
                                                                    <?php foreach ($agreements as $agreement) : ?>
                                                                        <option
                                                                            value="<?php echo $agreement->id; ?>">
                                                                                <?php echo $agreement->name; ?>
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="onetimefee"
                                                                       class="col-form-label">One-time
                                                                    Fee</label>
                                                                <input type="text" class="form-control"
                                                                       id="onetimefee" name="onetimefee">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="recurringfee"
                                                                       class="col-form-label">Recurring
                                                                    Fee</label>
                                                                <input type="text" class="form-control"
                                                                       id="recurringfee" name="recurringfee">
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="hourlymonthly"
                                                                       class="col-form-label">Hourly/Monthly</label>
                                                                <select class="form-control"
                                                                        name="hourlymonthly" id="hourlymonthly">
                                                                    <option value="">Select</option>
                                                                    <option value="hourly">Hourly</option>
                                                                    <option value="monthly">Monthly</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="startdate"
                                                                       class="col-form-label">Start
                                                                    Date</label>
                                                                <input type="date" class="form-control"
                                                                       id="startdate" name="startdate">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="enddate"
                                                                       class="col-form-label">End Date</label>
                                                                <input type="date" class="form-control"
                                                                       id="enddate" name="enddate">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="notes"
                                                                       class="col-form-label">Notes</label>
                                                                <textarea class="form-control" name="notes"
                                                                          id="notes" rows="4"></textarea>
                                                            </div>

                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                   class="btn btn-default mr-2 close-add-type-of-service">Close</a>
                                                                <button type="submit"
                                                                        class="btn btn-primary save-type-of-service">Save</button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="collapse my-2" id="collapseEditServiceType">
                                        <div class="card">
                                            <div class="card-body">
                                                <form
                                                    action="<?php echo site_url('Type_of_Service/update') ?>"
                                                    class="update-type-of-services">
                                                    <input type="hidden" name="editid" id="editid">
                                                    <label class="col-form-label">Edit Type of
                                                        Service</label>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <!-- <div class="form-group">
                                                                <label for="editgroup"
                                                                    class="col-form-label">Group</label>
                                                                <input type="text" class="form-control"
                                                                    id="editgroup" name="editgroup">
                                                            </div> -->
                                                            <div class="form-group">
                                                                <label for="edittosentity"
                                                                       class="col-form-label">Entity</label>
                                                                <select class="form-control" name="edittosentity" id="edittosentity">
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="edittypeofservice"
                                                                       class="col-form-label">Type of
                                                                    Service</label>
                                                                <select class="form-control"
                                                                        name="edittypeofservice"
                                                                        id="edittypeofservice">
                                                                    <option></option>
                                                                    <?php foreach ($services as $service) : ?>
                                                                        <option
                                                                            value="<?php echo $service->id; ?>">
                                                                                <?php echo $service->name; ?>
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="editagreement"
                                                                       class="col-form-label">Agreement</label>
                                                                <select class="form-control"
                                                                        name="editagreement" id="editagreement">
                                                                    <option></option>
                                                                    <?php foreach ($agreements as $agreement) : ?>
                                                                        <option
                                                                            value="<?php echo $agreement->id; ?>">
                                                                                <?php echo $agreement->name; ?>
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="editonetimefee"
                                                                       class="col-form-label">One-time
                                                                    Fee</label>
                                                                <input type="text" class="form-control"
                                                                       id="editonetimefee"
                                                                       name="editonetimefee">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="editrecurringfee"
                                                                       class="col-form-label">Recurring
                                                                    Fee</label>
                                                                <input type="text" class="form-control"
                                                                       id="editrecurringfee"
                                                                       name="editrecurringfee">
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="edithourlymonthly"
                                                                       class="col-form-label">Hourly/Monthly</label>
                                                                <select class="form-control"
                                                                        name="edithourlymonthly"
                                                                        id="edithourlymonthly">
                                                                    <option value="">Select</option>
                                                                    <option value="hourly">Hourly</option>
                                                                    <option value="monthly">Monthly</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="editservicestartdate"
                                                                       class="col-form-label">Start
                                                                    Date</label>
                                                                <input type="date" class="form-control"
                                                                       id="editservicestartdate"
                                                                       name="editservicestartdate">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="editenddate"
                                                                       class="col-form-label">End Date</label>
                                                                <input type="date" class="form-control"
                                                                       id="editenddate" name="editenddate">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="editnotes"
                                                                       class="col-form-label">Notes</label>
                                                                <textarea class="form-control"
                                                                          name="editnotes" id="editnotes"
                                                                          rows="4"></textarea>
                                                            </div>

                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                   class="btn btn-default mr-2 close-edit-type-of-service">Close</a>
                                                                <button type="submit"
                                                                        class="btn btn-primary update-type-of-service">Update</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="table-responsive mt-3">
                                        <table class="table table-bordered table-hover table-striped nopaddingmargin table-middle" width="100%"
                                               cellspacing="0" role="grid" aria-describedby="dataTable_info"
                                               style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Entity</th>
                                                    <!-- <th>Group</th> -->
                                                    <th>Type of Service</th>
                                                    <th>Agreement</th>
                                                    <th>One-time Fee</th>
                                                    <th>Recurring Fee</th>
                                                    <th>Hourly/Monthly</th>
                                                    <th>Start Date</th>
                                                    <th>End Date</th>
                                                    <th class="text-center width_200">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="type-of-services-table-body">
                                                <tr>
                                                    <td colspan="9">Loading...</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <!-- <div class="col-lg-12">
                                    <nav class="nav nav-pills">
                                        <a class="nav-item nav-link active" data-toggle="tab" href="#services-info"
                                            role="tab" aria-controls="nav-home" aria-selected="true">Services</a>
                                        <a class="nav-item nav-link" data-toggle="tab" href="#services-upload-files"
                                            role="tab" aria-controls="nav-home" aria-selected="true">Upload
                                            Files</a>
                                    </nav>
                                    <div class="tab-content mt-3" id="nav-tabContent">
                                        <div class="tab-pane fade active show" id="services-info" role="tabpanel"
                                            aria-labelledby="nav-contact-tab">
    
                                            <button class="btn btn-default open-add-type-of-service">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                        <div class="tab-pane fade" id="services-upload-files" role="tabpanel"
                                            aria-labelledby="nav-contact-tab">
                                            <form
                                                action="<?php echo site_url('request/crm_type_of_service_upload_doc') ?>"
                                                class="type-of-service-upload-doc" enctype="multipart/form-data"
                                                method="post">
                                                <input type="hidden" name="CID" class="client_id" />
                                                <div class="upload-files-drop form-group">
                                                    <input type="file" name="file[]"
                                                        class="form-control file-uploader" multiple=""
                                                        accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword, application/vnd.ms-excel,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*">
                                                </div>
                                                <div class="progress form-group hide">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated"
                                                        role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                                        aria-valuemax="100" style="width: 0%"></div>
                                                </div>
                                            </form>
    
                                            <ul class="list-group service-files-uploaded uploaded-list">
    
                                            </ul>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="Notes" role="tabpanel" aria-labelledby="Notes-tab">
                        <div class="card company-content">
                            <div class="d-flex justify-content-between">
                                <h6 class="mb-0 pt-2">Notes</h6>
                            </div>

                            <div class="accordion mt-3" id="accordionExample">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link btn-block btn-sm text-left cna-btn"
                                                    type="button" data-toggle="collapse" data-target="#collapseOne"
                                                    aria-expanded="true" aria-controls="collapseOne">
                                                CNA <span class="date-created"></span>
                                            </button>
                                        </h2>
                                    </div>

                                    <form action="<?php echo site_url('contacts/edit_cna') ?>" method="post"
                                          id="collapseOne" class="edit-cna collapse show" aria-labelledby="headingOne"
                                          data-parent="#accordionExample">
                                        <input type="hidden" value="" name="id" class="client_id" />
                                        <div class="editor-body">
                                            <textarea class="form-control textarea-content" name="notepad"
                                                      id="cna-notes-client"></textarea>
                                        </div>
                                        <hr>
                                        <div class="submit-cna text-right">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse" data-target="#collapseTwo"
                                                    aria-expanded="false" aria-controls="collapseTwo">
                                                Conversation
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                         data-parent="#accordionExample">
                                        <form class="note-convo"
                                              action="<?php echo site_url('contacts/add_conversation') ?>">
                                            <input type="hidden"
                                                   value="<?php echo $this->session->userdata('user_session') ?>"
                                                   name="uid" class="hide" />
                                            <input type="hidden" value="" name="contact_id" class="client_id" />
                                            <div class="form-group">
                                                <input type="text" value="" name="title" placeholder="Title"
                                                       class="form-control" />
                                            </div>
                                            <div class="note-section">
                                                <textarea class="form-control" name="note"
                                                          id="cna-conversation-client"
                                                          placeholder="Write a notes..."></textarea>
                                            </div>
                                            <div class="submit-note"><button type="submit"
                                                                             class="btn btn-primary">Add New</button></div>
                                            <div class="output"></div>
                                        </form>
                                        <div class="list-group-note">
                                            <div class="list-group">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="Files" role="tabpanel" aria-labelledby="Files-tab">
                        <div class="card company-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="mb-0 pt-2">Files</h6>
                                    </div>
                                </div>
                                <div class="col-lg-12 mt-3">
                                    <form action="<?php echo site_url('request/client_information_upload_doc') ?>"
                                          class="information_upload_doc" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="CID" class="client_id" />
                                        <div class="upload-files-drop form-group">
                                            <input type="file" name="file[]" class="form-control file-uploader"
                                                   multiple=""
                                                   accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword, application/vnd.ms-excel,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*">
                                        </div>
                                        <div class="progress form-group hide">
                                            <div class="progress-bar progress-bar-striped progress-bar-animated"
                                                 role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                                 aria-valuemax="100" style="width: 0%"></div>
                                        </div>
                                    </form>
                                    <ul class="list-group client-files-uploaded uploaded-list">

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="Reminder" role="tabpanel" aria-labelledby="Reminder-tab">
                        <div class="card company-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="mb-0 pt-2">Reminders</h6>
                                        <div class="action-buttons">
                                            <button class="btn btn-default open-add-reminder">
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="collapse my-2" id="collapseAddReminder">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="<?php echo site_url('reminder/store') ?>"
                                                      class="create-reminder">
                                                    <label class="col-form-label">Add Reminder</label>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="start_date"
                                                                       class="col-form-label">Date</label>
                                                                <input type="datetime-local" class="form-control"
                                                                       id="start_date" name="start_date">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="notify"
                                                                       class="col-form-label">Notify</label>
                                                                <select class="form-control chosen-select" multiple
                                                                        id="notify" name="notify[]">
                                                                    <option></option>
                                                                    <?php foreach ($users as $user) : ?>
                                                                        <option value="<?php echo $user->ID; ?>">
                                                                            <?php echo $user->first_name . ' ' . $user->last_name; ?>
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="what"
                                                                       class="col-form-label">What</label>
                                                                <input type="text" class="form-control" id="what"
                                                                       name="what">
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="where"
                                                                       class="col-form-label">Where</label>
                                                                <input type="text" class="form-control" id="where"
                                                                       name="where">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="description"
                                                                       class="col-form-label">Description</label>
                                                                <textarea class="form-control" name="description"
                                                                          id="description" cols="30" rows="5"></textarea>
                                                            </div>

                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                   class="btn btn-default mr-2 close-add-reminder">Close</a>
                                                                <button type="submit"
                                                                        class="btn btn-primary save-reminder">Save</button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="collapse my-2" id="collapseEditReminder">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="<?php echo site_url('reminder/update') ?>"
                                                      class="update-reminder">
                                                    <input type="hidden" name="editreminderid" id="editreminderid">
                                                    <label class="col-form-label">Edit Reminder</label>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="editstart_date"
                                                                       class="col-form-label">Date</label>
                                                                <input type="datetime-local" class="form-control"
                                                                       id="editstart_date" name="editstart_date">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="editnotify"
                                                                       class="col-form-label">Notify</label>
                                                                <select class="form-control chosen-select" multiple
                                                                        id="editnotify" name="editnotify[]">
                                                                    <option></option>
                                                                    <?php foreach ($users as $user) : ?>
                                                                        <option value="<?php echo $user->ID; ?>">
                                                                            <?php echo $user->first_name . ' ' . $user->last_name; ?>
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="editwhat"
                                                                       class="col-form-label">What</label>
                                                                <input type="text" class="form-control"
                                                                       id="editwhat" name="editwhat">
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="editwhere"
                                                                       class="col-form-label">Where</label>
                                                                <input type="text" class="form-control"
                                                                       id="editwhere" name="editwhere">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="editdescription"
                                                                       class="col-form-label">Description</label>
                                                                <textarea class="form-control"
                                                                          name="editdescription" id="editdescription"
                                                                          cols="30" rows="5"></textarea>
                                                            </div>

                                                            <div class="float-right mt-4">
                                                                <a href="#"
                                                                   class="btn btn-default mr-2 close-edit-reminder">Close</a>
                                                                <button type="submit"
                                                                        class="btn btn-primary update-reminder">Update</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="table-responsive mt-3">
                                        <table class="table table-bordered table-hover table-striped nopaddingmargin table-middle" width="100%" cellspacing="0"
                                               role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Notify</th>
                                                    <th>What</th>
                                                    <th>Where</th>
                                                    <th>Description</th>
                                                    <th class="text-center width_200">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="reminder-table-body">
                                                <tr>
                                                    <td colspan="6">Loading...</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="Billing" role="tabpanel" aria-labelledby="Billing-tab">
                        <div class="card company-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="d-flex justify-content-between">
                                        <nav class="nav nav-pills">
                                            <a class="nav-item nav-link active" data-toggle="tab"
                                               href="#revenue-container" role="tab" aria-controls="nav-home"
                                               aria-selected="true">Revenue</a>
                                            <a class="nav-item nav-link" data-toggle="tab" href="#ar-container"
                                               role="tab" aria-controls="nav-home" aria-selected="true">Account
                                                Receivable</a>
                                        </nav>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="tab-content mt-3" id="nav-tabContent">
                                        <div class="tab-pane fade active show" id="revenue-container"
                                             role="tabpanel" aria-labelledby="nav-contact-tab">
                                            <form class="add-revenue addmanualrevenue">
                                                <input type="hidden" name="client_id" class="hide client_id" value="" />
                                                <div class="text-right">
                                                    <button class="btn btn-default btn-add-revenue" type="button">Add</button>
                                                </div>
                                                <div class="my-2 toogle-revenue hide">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <strong class="kpi_title">Add Revenue</strong> <br><br>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label for="cc-name"
                                                                               class="control-label mb-1 defaultColorbold">Client
                                                                            Name</label><br>
                                                                        <input type="hidden" name="clientnameselect"
                                                                               class="clientnameselect billing_company form-control"
                                                                               value="" />
                                                                        <div class="clientnameselect billing_company form-control"
                                                                             value=""></div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group has-success"> <label
                                                                            for="cc-name"
                                                                            class="control-label mb-1 defaultColorbold">Invoice
                                                                            No</label> <input id="invoiceno2"
                                                                                          name="invoiceno" type="text"
                                                                                          class="form-control invoiceno"> <span
                                                                                          class="help-block field-validation-valid"
                                                                                          data-valmsg-for="cc-name"
                                                                                          data-valmsg-replace="true"></span> </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group has-success">
                                                                        <label for="cc-name"
                                                                               class="control-label mb-1 defaultColorbold">Invoice
                                                                            Date (mm/dd/yyyy)</label>

                                                                        <input id="invoicemonth" name="invoicedate"
                                                                               type="date"
                                                                               class="readonlybackground form-control invoicemonth"
                                                                               value="<?php echo date('Y-m-d', strtotime('-1 month')) ?>">

                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label for="cc-name"
                                                                               class="control-label mb-1 defaultColorbold">Item
                                                                            Name</label><br>
                                                                        <select name="itemnameselect"
                                                                                placeholder="Select Line Item Name"
                                                                                class="itemnameselect form-control chosen-selected"
                                                                                single="" tabindex="-1">
                                                                            <option value=""></option>
                                                                            <option value="Acctg &amp; Bkkpg Serv">Acctg
                                                                                &amp; Bkkpg Serv</option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Services - Rec">
                                                                                Acctg &amp; Bkkpg Services - Rec
                                                                            </option>
                                                                            <option value="Consulting Services">
                                                                                Consulting Services</option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Services - Reimb">
                                                                                Acctg &amp; Bkkpg Services - Reimb
                                                                            </option>
                                                                            <option value="Coop-Marketing Support Fund">
                                                                                Coop-Marketing Support Fund</option>
                                                                            <option value="Onboarding">Onboarding
                                                                            </option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Serv - Non-Rec">
                                                                                Acctg &amp; Bkkpg Serv - Non-Rec
                                                                            </option>
                                                                            <option
                                                                                value="Tax comp supp serv - Non-Recur">
                                                                                Tax comp supp serv - Non-Recur</option>
                                                                            <option value="Financial projection">
                                                                                Financial projection</option>
                                                                            <option value="Professional Courtesy">
                                                                                Professional Courtesy</option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Services - 1099">
                                                                                Acctg &amp; Bkkpg Services - 1099
                                                                            </option>
                                                                            <option
                                                                                value="Corp Fin Services - Recurring">
                                                                                Corp Fin Services - Recurring</option>
                                                                            <option value="Bank Charges">Bank Charges
                                                                            </option>
                                                                            <option value="Prof. Services - Recurring">
                                                                                Prof. Services - Recurring</option>
                                                                            <option value="Staff Support">Staff Support
                                                                            </option>
                                                                            <option value="Other Income">Other Income
                                                                            </option>
                                                                            <option value="Setup Services">Setup
                                                                                Services</option>
                                                                            <option
                                                                                value="Prof Services -Aud- Recurring">
                                                                                Prof Services -Aud- Recurring</option>
                                                                            <option
                                                                                value="Prof Services -Sox- Recurring">
                                                                                Prof Services -Sox- Recurring</option>
                                                                            <option value="Prof Services - TAS - Reimb">
                                                                                Prof Services - TAS - Reimb</option>
                                                                            <option
                                                                                value="Prof Services - Netsuite - Rec">
                                                                                Prof Services - Netsuite - Rec</option>
                                                                            <option
                                                                                value="Prof Serv -Comm &amp; Media- Rec">
                                                                                Prof Serv -Comm &amp; Media- Rec
                                                                            </option>
                                                                            <option value="Prof Services - Sox - Reimb">
                                                                                Prof Services - Sox - Reimb</option>
                                                                            <option
                                                                                value="Prof Services -DPC- Recurring">
                                                                                Prof Services -DPC- Recurring</option>
                                                                            <option
                                                                                value="Prof. Services -Val- Recurring">
                                                                                Prof. Services -Val- Recurring</option>
                                                                            <option value="Prof Services - Aud - Reimb">
                                                                                Prof Services - Aud - Reimb</option>
                                                                            <option
                                                                                value="Prof Services -TAS- Recurring">
                                                                                Prof Services -TAS- Recurring</option>
                                                                            <option
                                                                                value="Tax comp supp services - Recur">
                                                                                Tax comp supp services - Recur</option>
                                                                            <option
                                                                                value="Prof. Services - Non-Recurring">
                                                                                Prof. Services - Non-Recurring</option>
                                                                            <option
                                                                                value="Prof. Services - Reimbursable">
                                                                                Prof. Services - Reimbursable</option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Services - Disc">
                                                                                Acctg &amp; Bkkpg Services - Disc
                                                                            </option>
                                                                            <option value="Corp Fin Services - Reimb">
                                                                                Corp Fin Services - Reimb</option>
                                                                            <option
                                                                                value="Tax comp supp services - Disc">
                                                                                Tax comp supp services - Disc</option>
                                                                            <option value="Customer Advances">Customer
                                                                                Advances</option>
                                                                            <option value="Reimbursable Expense">
                                                                                Reimbursable Expense</option>
                                                                            <option value="Financial projection work">
                                                                                Financial projection work</option>
                                                                            <option value="Invoicing services">Invoicing
                                                                                services</option>
                                                                            <option value="Bad Debt Expense">Bad Debt
                                                                                Expense</option>
                                                                            <option value="Corp Fin Services - Non-Rec">
                                                                                Corp Fin Services - Non-Rec</option>
                                                                            <option value="Equity Warrants">Equity
                                                                                Warrants</option>
                                                                            <option
                                                                                value="Tax comp supp services - Reimb">
                                                                                Tax comp supp services - Reimb</option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Services - Adv">
                                                                                Acctg &amp; Bkkpg Services - Adv
                                                                            </option>
                                                                            <option value="Marketing Exp">Marketing Exp
                                                                            </option>
                                                                            <option
                                                                                value="Tax comp supp services - Adv">Tax
                                                                                comp supp services - Adv</option>
                                                                            <option value="Accrued Comm and Fees">
                                                                                Accrued Comm and Fees</option>
                                                                            <option value="Prof Services-TAS-Disc">Prof
                                                                                Services-TAS-Disc</option>
                                                                            <option value="Prof Services-SOX-Disc">Prof
                                                                                Services-SOX-Disc</option>
                                                                            <option value="Tech Support Serv - Rec">Tech
                                                                                Support Serv - Rec</option>
                                                                            <option
                                                                                value="Prof Services-SOX-IA-Non Rec">
                                                                                Prof Services-SOX-IA-Non Rec</option>
                                                                            <option value="Tech Support Serv - Non-Rec">
                                                                                Tech Support Serv - Non-Rec</option>
                                                                            <option value="Prof Services-TA-Non Rec">
                                                                                Prof Services-TA-Non Rec</option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Services - Non Rec">
                                                                                Acctg &amp; Bkkpg Services - Non Rec
                                                                            </option>
                                                                            <option value="Line Item Item Name">Line
                                                                                Item Item Name</option>
                                                                            <option value="Prof Services -TAS- Non-Rec">
                                                                                Prof Services -TAS- Non-Rec</option>
                                                                            <option value="Prof Services - Val - Reimb">
                                                                                Prof Services - Val - Reimb</option>
                                                                            <option value="Prof Serv - Cybersec - Rec">
                                                                                Prof Serv - Cybersec - Rec</option>
                                                                            <option
                                                                                value="Prof Services-OFA- Recurring">
                                                                                Prof Services-OFA- Recurring</option>
                                                                            <option value="Tech Support Serv - Reimb">
                                                                                Tech Support Serv - Reimb</option>
                                                                        </select>

                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group has-success">
                                                                        <label for="cc-name"
                                                                               class="control-label mb-1 defaultColorbold">Total
                                                                            Amount</label>
                                                                        <input id="total2" name="total" type="text"
                                                                               class="form-control total">
                                                                        <span class="help-block field-validation-valid"
                                                                              data-valmsg-for="cc-name"
                                                                              data-valmsg-replace="true"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label for="cc-name"
                                                                               class="control-label mb-1 defaultColorbold">Work
                                                                            Group</label><br>
                                                                        <select name="groupid" id="groupid"
                                                                                placeholder="Select Line Item Name"
                                                                                class="itemnameselect form-control chosen-selected work-group"
                                                                                single="" tabindex="-1">

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 text-right">
                                                                    <label for="cc-name"
                                                                           class="control-label mb-1 defaultColorbold">&nbsp;</label><br>

                                                                </div>
                                                                <div class="col-lg-6 text-right">
                                                                    <label for="cc-name"
                                                                           class="control-label mb-1 defaultColorbold">&nbsp;</label><br>
                                                                    <button class="btn btn-default"
                                                                            type="submit">Save</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="revenue-table table-responsive">
                                                    <table
                                                        class="table table-bordered table-hover table-striped nopaddingmargin table-middle">
                                                        <thead>
                                                            <tr>
                                                                <th class="aligncenter">#</th>
                                                                <th class="aligncenter">Record ID</th>
                                                                <th class="aligncenter">Client</th>
                                                                <th class="aligncenter">Invoice No</th>
                                                                <th class="aligncenter">Invoice Date</th>
                                                                <th class="aligncenter">Item Name</th>
                                                                <th class="text-right">Total</th>
                                                                <th class="text-center">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="added-revenue">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="ar-container" role="tabpanel"
                                             aria-labelledby="nav-contact-tab">
                                            <form class="add-ar addmanualar">
                                                <input type="hidden" name="client_id" class="hide client_id" value="" />
                                                <div class="text-right">
                                                    <button class="btn btn-default btn-add-ar" type="button">Add</button>
                                                </div>
                                                <div class="my-2 toogle-ar hide">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <strong class="kpi_title">Add Account Receivable</strong> <br><br>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label for="cc-name"
                                                                               class="control-label mb-1 defaultColorbold">Client
                                                                            Name</label><br>
                                                                        <input type="hidden" name="clientnameselect"
                                                                               class="clientnameselect billing_company form-control"
                                                                               value="" />
                                                                        <div class="clientnameselect billing_company form-control"
                                                                             value=""></div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group has-success"> <label
                                                                            for="cc-name"
                                                                            class="control-label mb-1 defaultColorbold">Invoice
                                                                            No</label> <input id="invoiceno"
                                                                                          name="invoiceno" type="text"
                                                                                          class="form-control invoiceno"> <span
                                                                                          class="help-block field-validation-valid"
                                                                                          data-valmsg-for="cc-name"
                                                                                          data-valmsg-replace="true"></span> </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group has-success">
                                                                        <label for="cc-name"
                                                                               class="control-label mb-1 defaultColorbold">Invoice
                                                                            Date (mm/dd/yyyy)</label>

                                                                        <input id="invoicemonth2" name="invoicedate"
                                                                               type="date"
                                                                               class="readonlybackground form-control invoicemonth"
                                                                               value="<?php echo date('Y-m-d', strtotime('-1 month')) ?>">

                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label for="cc-name"
                                                                               class="control-label mb-1 defaultColorbold">Item
                                                                            Name</label><br>
                                                                        <select name="itemnameselect"
                                                                                placeholder="Select Line Item Name"
                                                                                class="itemnameselect form-control chosen-selected"
                                                                                single="" tabindex="-1">
                                                                            <option value=""></option>
                                                                            <option value="Acctg &amp; Bkkpg Serv">Acctg
                                                                                &amp; Bkkpg Serv</option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Services - Rec">
                                                                                Acctg &amp; Bkkpg Services - Rec
                                                                            </option>
                                                                            <option value="Consulting Services">
                                                                                Consulting Services</option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Services - Reimb">
                                                                                Acctg &amp; Bkkpg Services - Reimb
                                                                            </option>
                                                                            <option value="Coop-Marketing Support Fund">
                                                                                Coop-Marketing Support Fund</option>
                                                                            <option value="Onboarding">Onboarding
                                                                            </option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Serv - Non-Rec">
                                                                                Acctg &amp; Bkkpg Serv - Non-Rec
                                                                            </option>
                                                                            <option
                                                                                value="Tax comp supp serv - Non-Recur">
                                                                                Tax comp supp serv - Non-Recur</option>
                                                                            <option value="Financial projection">
                                                                                Financial projection</option>
                                                                            <option value="Professional Courtesy">
                                                                                Professional Courtesy</option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Services - 1099">
                                                                                Acctg &amp; Bkkpg Services - 1099
                                                                            </option>
                                                                            <option
                                                                                value="Corp Fin Services - Recurring">
                                                                                Corp Fin Services - Recurring</option>
                                                                            <option value="Bank Charges">Bank Charges
                                                                            </option>
                                                                            <option value="Prof. Services - Recurring">
                                                                                Prof. Services - Recurring</option>
                                                                            <option value="Staff Support">Staff Support
                                                                            </option>
                                                                            <option value="Other Income">Other Income
                                                                            </option>
                                                                            <option value="Setup Services">Setup
                                                                                Services</option>
                                                                            <option
                                                                                value="Prof Services -Aud- Recurring">
                                                                                Prof Services -Aud- Recurring</option>
                                                                            <option
                                                                                value="Prof Services -Sox- Recurring">
                                                                                Prof Services -Sox- Recurring</option>
                                                                            <option value="Prof Services - TAS - Reimb">
                                                                                Prof Services - TAS - Reimb</option>
                                                                            <option
                                                                                value="Prof Services - Netsuite - Rec">
                                                                                Prof Services - Netsuite - Rec</option>
                                                                            <option
                                                                                value="Prof Serv -Comm &amp; Media- Rec">
                                                                                Prof Serv -Comm &amp; Media- Rec
                                                                            </option>
                                                                            <option value="Prof Services - Sox - Reimb">
                                                                                Prof Services - Sox - Reimb</option>
                                                                            <option
                                                                                value="Prof Services -DPC- Recurring">
                                                                                Prof Services -DPC- Recurring</option>
                                                                            <option
                                                                                value="Prof. Services -Val- Recurring">
                                                                                Prof. Services -Val- Recurring</option>
                                                                            <option value="Prof Services - Aud - Reimb">
                                                                                Prof Services - Aud - Reimb</option>
                                                                            <option
                                                                                value="Prof Services -TAS- Recurring">
                                                                                Prof Services -TAS- Recurring</option>
                                                                            <option
                                                                                value="Tax comp supp services - Recur">
                                                                                Tax comp supp services - Recur</option>
                                                                            <option
                                                                                value="Prof. Services - Non-Recurring">
                                                                                Prof. Services - Non-Recurring</option>
                                                                            <option
                                                                                value="Prof. Services - Reimbursable">
                                                                                Prof. Services - Reimbursable</option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Services - Disc">
                                                                                Acctg &amp; Bkkpg Services - Disc
                                                                            </option>
                                                                            <option value="Corp Fin Services - Reimb">
                                                                                Corp Fin Services - Reimb</option>
                                                                            <option
                                                                                value="Tax comp supp services - Disc">
                                                                                Tax comp supp services - Disc</option>
                                                                            <option value="Customer Advances">Customer
                                                                                Advances</option>
                                                                            <option value="Reimbursable Expense">
                                                                                Reimbursable Expense</option>
                                                                            <option value="Financial projection work">
                                                                                Financial projection work</option>
                                                                            <option value="Invoicing services">Invoicing
                                                                                services</option>
                                                                            <option value="Bad Debt Expense">Bad Debt
                                                                                Expense</option>
                                                                            <option value="Corp Fin Services - Non-Rec">
                                                                                Corp Fin Services - Non-Rec</option>
                                                                            <option value="Equity Warrants">Equity
                                                                                Warrants</option>
                                                                            <option
                                                                                value="Tax comp supp services - Reimb">
                                                                                Tax comp supp services - Reimb</option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Services - Adv">
                                                                                Acctg &amp; Bkkpg Services - Adv
                                                                            </option>
                                                                            <option value="Marketing Exp">Marketing Exp
                                                                            </option>
                                                                            <option
                                                                                value="Tax comp supp services - Adv">Tax
                                                                                comp supp services - Adv</option>
                                                                            <option value="Accrued Comm and Fees">
                                                                                Accrued Comm and Fees</option>
                                                                            <option value="Prof Services-TAS-Disc">Prof
                                                                                Services-TAS-Disc</option>
                                                                            <option value="Prof Services-SOX-Disc">Prof
                                                                                Services-SOX-Disc</option>
                                                                            <option value="Tech Support Serv - Rec">Tech
                                                                                Support Serv - Rec</option>
                                                                            <option
                                                                                value="Prof Services-SOX-IA-Non Rec">
                                                                                Prof Services-SOX-IA-Non Rec</option>
                                                                            <option value="Tech Support Serv - Non-Rec">
                                                                                Tech Support Serv - Non-Rec</option>
                                                                            <option value="Prof Services-TA-Non Rec">
                                                                                Prof Services-TA-Non Rec</option>
                                                                            <option
                                                                                value="Acctg &amp; Bkkpg Services - Non Rec">
                                                                                Acctg &amp; Bkkpg Services - Non Rec
                                                                            </option>
                                                                            <option value="Line Item Item Name">Line
                                                                                Item Item Name</option>
                                                                            <option value="Prof Services -TAS- Non-Rec">
                                                                                Prof Services -TAS- Non-Rec</option>
                                                                            <option value="Prof Services - Val - Reimb">
                                                                                Prof Services - Val - Reimb</option>
                                                                            <option value="Prof Serv - Cybersec - Rec">
                                                                                Prof Serv - Cybersec - Rec</option>
                                                                            <option
                                                                                value="Prof Services-OFA- Recurring">
                                                                                Prof Services-OFA- Recurring</option>
                                                                            <option value="Tech Support Serv - Reimb">
                                                                                Tech Support Serv - Reimb</option>
                                                                        </select>

                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group has-success">
                                                                        <label for="cc-name"
                                                                               class="control-label mb-1 defaultColorbold">Total
                                                                            Amount</label>
                                                                        <input id="total" name="total" type="text"
                                                                               class="form-control total">
                                                                        <span class="help-block field-validation-valid"
                                                                              data-valmsg-for="cc-name"
                                                                              data-valmsg-replace="true"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label for="cc-name"
                                                                               class="control-label mb-1 defaultColorbold">Work
                                                                            Group</label><br>
                                                                        <select name="groupid" id="groupid2"
                                                                                placeholder="Select Line Item Name"
                                                                                class="itemnameselect form-control chosen-selected work-group"
                                                                                single="" tabindex="-1">

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 text-right">
                                                                    <label for="cc-name"
                                                                           class="control-label mb-1 defaultColorbold">&nbsp;</label><br>

                                                                </div>
                                                                <div class="col-lg-6 text-right">
                                                                    <label for="cc-name"
                                                                           class="control-label mb-1 defaultColorbold">&nbsp;</label><br>
                                                                    <button class="btn btn-default"
                                                                            type="submit">Save</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="ar-table">
                                                    <table
                                                        class="table table-bordered table-hover table-striped nopaddingmargin">
                                                        <thead>
                                                            <tr>
                                                                <th class="aligncenter">#</th>
                                                                <th class="aligncenter">Invoice Number </th>
                                                                <th class="aligncenter">Invoice Date </th>
                                                                <th class="aligncenter">Client Name</th>
                                                                <th class="aligncenter">Group</th>
                                                                <th class="text-right">Amount Due</th>
                                                                <th class="text-center">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="added-ar">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="Experience" role="tabpanel" aria-labelledby="Experience-tab">
                        <div class="card company-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="hidden" name="client_id" class="hide client_id" value="" /> 
                                    <div class="row">
                                        <div class="col-lg-8 form-inline">
                                            <div class="input-group hide">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Date</span>
                                                </div>
                                                <input name="dateexp" type="hidden" class="form-control dateexp experience-picker" value="<?php echo date('m/21/Y', strtotime('-1 month')) ?>" >


                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Date</span>
                                                </div>
                                                <select class="form-control dateexp width_150 select_month hide">
                                                    <?php for ($x = 1; $x <= 12; $x++) { ?>
                                                        <?php
                                                        $dateObj = DateTime::createFromFormat('!m', $x);
                                                        $monthName = $dateObj->format('F');
                                                        ?>
                                                        <option value="<?php echo $x ?>" <?php echo ($x == date('n', strtotime('-1 month')) ? "selected='selected'" : "") ?>><?php echo $monthName ?></option>
                                                    <?php } ?>
                                                </select>
                                                <select class="form-control dateexp width_100 select_year">
                                                    <?php for ($x = 2019; $x < 2025; $x++) { ?>
                                                        <option value="<?php echo $x ?>" <?php echo ($x == date('Y', strtotime('-1 month')) ? "selected='selected'" : "") ?>><?php echo $x ?></option>
                                                    <?php } ?>
                                                </select>
                                                <button class="btn btn-primary filter-button" type="button" id="button-addon2"><i class="fas fa-filter"></i> Filter</button>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 text-right">
                                            <button class="btn btn-default btn-add-revenue" type="button">Add</button>
                                        </div>
                                    </div>
                                    <form class="toogle-revenue hide add-experience my-2" method="post" action="<?php echo site_url('request/add_experience') ?>">
                                        <div class="card">
                                            <div class="card-body">
                                                <input type="hidden" name="related_id" class="hide related_id related_id_new" value="refid-<?php echo strtotime('now') . rand(1000, 9999) ?>" /> 
                                                <input type="hidden" name="client_id" class="hide client_id" value="" /> 
                                                <strong class="kpi_title">Add Client Experience</strong> <br><br>
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <label for="cc-name" class="control-label mb-1 defaultColorbold">Service Continuum</label><br> 
                                                            <input type="number" name="Service_Continuum" class="form-control" value="0" /> 
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group "> 
                                                            <label for="cc-name" class="control-label mb-1 defaultColorbold">Calls made</label> 
                                                            <input type="number" name="Calls_made" class="form-control" value="0" /> 
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group "> 
                                                            <label for="cc-name" class="control-label mb-1 defaultColorbold">QRC</label> 
                                                            <select class="form-control" name="QRC">
                                                                <option value="">Select</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group "> 
                                                            <label for="cc-name" class="control-label mb-1 defaultColorbold">Note</label> 
                                                            <textarea class="form-control" name="Note"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group "> 
                                                            <label for="cc-name" class="control-label mb-1 defaultColorbold">CI log</label> 
                                                            <input type="text" name="CI_log" class="form-control form-group" value="0" /> 

                                                            <label for="cc-name" class="control-label mb-1 defaultColorbold">CI Log Notes</label> 
                                                            <div class="ci-log-note ckeditor-experience">
                                                                <textarea class="form-control logs-note" id="logs-note" name="ci_log_note" height="300"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group "> 
                                                            <label for="cc-name" class="control-label mb-1 defaultColorbold">No# of errors</label> 
                                                            <input type="text" name="Errors" class="form-control form-group" value="0" /> 

                                                            <label for="cc-name" class="control-label mb-1 defaultColorbold">No# of errors Notes</label>
                                                            <div class="ci-log-note ckeditor-experience">
                                                                <textarea class="form-control error-notes" id="error-notes" name="error_note"></textarea>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    <div class="col-lg-12">
                                                        <div class="form-group "> 
                                                            <label for="cc-name" class="control-label mb-1 defaultColorbold">Client satifaction</label> 
                                                            <textarea class="form-control" name="Client_satifaction"></textarea>
                                                        </div>
                                                    </div>


                                                    <div class="col-lg-4"> 
                                                        <label for="cc-name" class="control-label mb-1 defaultColorbold">Deadline missed</label><br> 
                                                        <select class="form-control" name="Deadline_missed">
                                                            <option value="">Select</option>
                                                            <option>Yes</option>
                                                            <option>No</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <label for="cc-name" class="control-label mb-1 defaultColorbold">Date</label><br> 
                                                        <input name="dateexp" type="date" class="form-control" value="<?php echo date('Y-m-21', strtotime('-1 month')) ?>" >
                                                    </div>

                                                    <div class="col-lg-4 text-right"> 
                                                        <label for="cc-name" class="control-label mb-1 defaultColorbold">&nbsp;</label><br> 
                                                        <button class="btn btn-default" type="reset">Reset</button> 
                                                        <button class="btn btn-primary submit-button" type="submit">Save</button> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="ar-table table-responsive mt-3">
                                        <table class="table table-bordered table-hover table-striped nopaddingmargin table-middle">
                                            <thead>
                                                <tr>
                                                    <th class="width_100 text-center" title="Date">Date</th>
                                                    <th class="width_50 text-center" title="Service Continuum">SC</th>
                                                    <th class="width_50 text-center" title="Calls made">CM</th>
                                                    <th class="width_50 text-center" title="QRC">QRC</th>
                                                    <th class="">Notes</th>
                                                    <th class="width_50 text-center" title="CI log">CIL</th>
                                                    <th class="width_50 text-center" title="No# of errors">NOE</th>
                                                    <th class="width_50 text-center" title="CS">CS</th>
                                                    <th class="width_50 text-center" title="Deadline">Deadline</th>
                                                    <th class="width_100 text-center" title="Reviewed by">RB</th>
                                                    <th class="width_150 text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="added-experience">

                                            </tbody>
                                        </table>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="Transaction" role="tabpanel" aria-labelledby="Transaction-tab">
                        <div class="card company-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="mb-0 pt-2">Transactions</h6>
                                        <div class="action-buttons">
                                            <!-- <button class="btn btn-default open-add-reminder">
                                                Add
                                            </button> -->
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-lg-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Date</span>
                                        </div>
                                        <select class="form-control dateexp width_150 transaction_month">
                                <?php for ($x = 1; $x <= 12; $x++) { ?>
                                    <?php
                                    $dateObj = DateTime::createFromFormat('!m', $x);
                                    $monthName = $dateObj->format('F');
                                    ?>
                                                                        <option value="<?php echo $x ?>" <?php echo ($x == date('n', strtotime('-1 month')) ? "selected='selected'" : "") ?>><?php echo $monthName ?></option>
                                <?php } ?>
                                        </select>
                                        <select class="form-control dateexp width_100 transaction_year">
                                <?php for ($x = 2019; $x < 2025; $x++) { ?>
                                                                        <option value="<?php echo $x ?>" <?php echo ($x == date('Y', strtotime('-1 month')) ? "selected='selected'" : "") ?>><?php echo $x ?></option>
                                <?php } ?>
                                        </select>
                                        <button class="btn btn-primary filter-transaction ml-2" type="button"><i class="fas fa-filter"></i> Filter</button>
                                    </div>
                                </div> -->
                                <div class="col-lg-12 mt-3">
                                    <table class="table table-bordered table-hover table-striped nopaddingmargin">
                                        <thead>
                                            <tr>
                                                <th class="aligncenter">#</th>
                                                <th class="aligncenter">Invoice Number</th>
                                                <th class="aligncenter">Invoice Date</th>
                                                <th class="aligncenter">Client Name</th>
                                                <th class="text-right">Invoice</th>
                                                <th class="text-right">Payments</th>
                                                <th class="text-right">Balance</th>
                                                <!-- <th class="text-center">Action</th> -->
                                            </tr>
                                        </thead>
                                        <tbody class="added-transactions">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal modal-drill-down" tabindex="99" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Invoice</h5>
                <input type="hidden" name="companyId" id="companyId">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover table-striped nopaddingmargin">
                    <thead>
                        <tr>
                            <th class="aligncenter">#</th>
                            <th class="aligncenter">Invoice Number </th>
                            <th class="aligncenter">Invoice Date </th>
                            <th class="aligncenter">Client Name</th>
                            <th class="aligncenter">Group</th>
                            <th class="aligncenter">Entity</th>
                            <th class="text-right">Amount Due</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody class="added-drill-revenue">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-drill-down-ar" tabindex="99" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Invoice</h5>
                <input type="hidden" name="companyId" id="companyId">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover table-striped nopaddingmargin">
                    <thead>
                        <tr>
                            <th class="aligncenter">#</th>
                            <th class="aligncenter">Invoice Number </th>
                            <th class="aligncenter">Invoice Date </th>
                            <th class="aligncenter">Client Name</th>
                            <th class="aligncenter">Group</th>
                            <th class="aligncenter">Entity</th>
                            <th class="text-right">Amount Due</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody class="added-drill-ar">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-client-experience" tabindex="99" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Update Client Experience</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="update-experience" method="post" action="<?php echo site_url('request/update_experience') ?>">
                    <input type="hidden" name="data_id" class="hide data_id" value="" />  
                    <input type="hidden" name="related_id" class="hide related_id related_id_new" value="" /> 
                    <input type="hidden" name="client_id" class="hide client_id" value="" /> 


                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="cc-name" class="control-label mb-1 defaultColorbold">Service Continuum</label><br> 
                                <input type="number" name="Service_Continuum" class="form-control" value="0" /> 
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group "> 
                                <label for="cc-name" class="control-label mb-1 defaultColorbold">Calls made</label> 
                                <input type="number" name="Calls_made" class="form-control" value="0" /> 
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group "> 
                                <label for="cc-name" class="control-label mb-1 defaultColorbold">QRC</label> 
                                <select class="form-control" name="QRC">
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group "> 
                                <label for="cc-name" class="control-label mb-1 defaultColorbold">Note</label> 
                                <textarea class="form-control" name="Note"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group "> 
                                <label for="cc-name" class="control-label mb-1 defaultColorbold">CI log</label> 
                                <input type="text" name="CI_log" class="form-control form-group" value="0" /> 

                                <label for="cc-name" class="control-label mb-1 defaultColorbold">CI Log Notes</label> 
                                <div class="ci-log-note ckeditor-experience">
                                    <textarea class="form-control update-logs-note" id="update-logs-note" name="ci_log_note" height="300"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group "> 
                                <label for="cc-name" class="control-label mb-1 defaultColorbold">No# of errors</label> 
                                <input type="text" name="Errors" class="form-control form-group" value="0" /> 

                                <label for="cc-name" class="control-label mb-1 defaultColorbold">No# of errors Notes</label>
                                <div class="ci-log-note ckeditor-experience">
                                    <textarea class="form-control update-error-notes" id="update-error-notes" name="error_note"></textarea>
                                </div>
                            </div>
                        </div> 
                        <div class="col-lg-12">
                            <div class="form-group "> 
                                <label for="cc-name" class="control-label mb-1 defaultColorbold">Client satifaction</label> 
                                <textarea class="form-control" name="Client_satifaction"></textarea>
                            </div>
                        </div>


                        <div class="col-lg-4"> 
                            <label for="cc-name" class="control-label mb-1 defaultColorbold">Deadline missed</label><br> 
                            <select class="form-control" name="Deadline_missed">
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>

                        <div class="col-lg-4">
                            <label for="cc-name" class="control-label mb-1 defaultColorbold">Date</label><br> 
                            <input name="dateexp" type="date" class="form-control" value="<?php echo date('Y-m-21', strtotime('-1 month')) ?>" >
                        </div>

                        <div class="col-lg-4 text-right"> 
                            <label for="cc-name" class="control-label mb-1 defaultColorbold">&nbsp;</label><br> 

                            <button class="btn btn-primary submit-button" type="submit">Save</button> 
                        </div>
                    </div>

                </form>


            </div>
        </div>
    </div>
</div>

<div class="modal modal-drill-down-transaction" tabindex="99" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-max" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Invoice</h5>
                <input type="hidden" name="companyId" id="companyId">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-5">
                <table class="table table-bordered table-hover table-striped nopaddingmargin mt-4">
                    <thead>
                        <tr>
                            <th class="aligncenter">Created</th>
                            <th class="aligncenter">Updated</th>
                            <th class="aligncenter">Client Name</th>
                            <th class="aligncenter">Group</th>
                            <th class="aligncenter">Invoice Number</th>
                            <th class="aligncenter">Transaction Date</th>
                            <th class="aligncenter">Credit Amount</th>
                            <th class="aligncenter">Line Item Name</th>
                            <th class="aligncenter">Line Item Amount</th>
                            <th class="aligncenter">Total Line Items</th>
                            <th class="aligncenter">Type</th>
                        </tr>
                    </thead>
                    <tbody class="added-drill-transactions">

                    </tbody>
                </table>

                <div class="row">
                    <div class="col-lg-4 ml-auto mt-4">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Invoice</th>
                                    <th scope="col">Payment</th>
                                    <th scope="col">Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><span id="total_invoice"></span></td>
                                    <td>-<span id="total_payments"></span></td>
                                    <td><span id="total_balance"></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal modal-cx" tabindex="99" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-max" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Client Experience</h5>
                <input type="hidden" name="companyId" id="companyId">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-5">


                <form class="update-experience" action="<?php echo site_url('Entity/InsertUpdateSubjective'); ?>" method="post" novalidate="novalidate">

                    <input type="hidden" name="data_id" class="hide data-id" value=""> 
                    <input type="hidden" name="client_id" class="hide" value=""> 
                    <input type="hidden" name="entry_update" class="hide entry_update" value=""> 
                    <input type="hidden" name="group_id" class="hide group_id" value=""> 

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group"> <label for="cc-name" class="control-label mb-1 defaultColorbold">Service Continuum</label>
                                <br> 
                                <input type="number" name="Service_Continuum" class="form-control" value="0"> </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group "> <label for="cc-name" class="control-label mb-1 defaultColorbold">Calls made</label> 
                                <input type="number" name="Calls_made" class="form-control" value="0"> </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group ">
                                <label for="cc-name" class="control-label mb-1 defaultColorbold">QRC</label> 
                                <select class="form-control" name="QRC">
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No" selected="selected">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group ">
                                <label for="cc-name" class="control-label mb-1 defaultColorbold">Ongoing Concern</label> 
                                <div class="ckeditor-note ckeditor-experience">
                                    <textarea class="form-control ongoing-concern-crm" id="ongoing-concern-crm" name="Note" height="200"></textarea>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group editor-con">
                                <label for="cc-name" class="control-label mb-1 defaultColorbold">CI log</label> <input type="number" name="CI_log" class="form-control form-group ci-logs" value="0"> <label for="cc-name" class="control-label mb-1 defaultColorbold">CI Log Notes</label> 
                                <div class="ci-log-note ckeditor-experience">
                                    <textarea class="form-control update-logs-note-crm" id="update-logs-note-crm" name="ci_log_note" height="200"></textarea>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group editor-con">
                                <label for="cc-name" class="control-label mb-1 defaultColorbold">No# of errors</label> <input type="number" name="Errors" class="form-control form-group error-logs" value="0"> <label for="cc-name" class="control-label mb-1 defaultColorbold">No# of errors Notes</label> 
                                <div class="ci-log-note ckeditor-experience">
                                    <textarea class="form-control update-error-notes-crm" id="update-error-notes-crm" name="error_note" height="200"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group ">
                                <label for="cc-name" class="control-label mb-1 defaultColorbold">Client satifaction</label> 
                                <div class="ckeditor-note ckeditor-experience">
                                    <textarea class="form-control client-satifaction-crm" id="client-satifaction-crm" name="Client_satifaction" height="200"></textarea>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <label for="cc-name" class="control-label mb-1 defaultColorbold">Deadline missed</label><br> 
                            <select class="form-control" name="Deadline_missed">
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No" selected="selected">No</option>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label for="cc-name" class="control-label mb-1 defaultColorbold">Audit Ready</label><br> 
                            <select class="form-control" name="Audit_Ready">
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No" selected="selected">No</option>
                            </select>
                        </div>
                        <div class="col-lg-3"> <label for="cc-name" class="control-label mb-1 defaultColorbold">Reviewed By</label><br> <span class="client-experience-reviewer"></span> </div>
                        <div class="col-lg-3 text-right"> <label for="cc-name" class="control-label mb-1 defaultColorbold">&nbsp;</label><br> <button class="btn btn-default review-button hide " data-bind="0" data-reviewer="" disabled="disabled" type="button"><span class="btn-load">Review</span></button> <button class="btn btn-primary submit-button " type="submit">Save</button> </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid main-container">
    <h1 class="mt-4">Leads</h1>
    <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6 form-group">
            <form action="" method="get">
                <input type="hidden" class="form-control" name="page" value="1">

                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" value="<?php echo $this->Models->keyword(); ?>" placeholder="Search">
                    <div class="input-group-append">
                        <button class="btn btn-default" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="table-responsive">
        <div class="divTable table-width1200">
            <div class="divTableBody">
                <div class="divTableRow divTbHead">
                    <div class="divTableCell">Contact Person</div>
                    <div class="divTableCell">Position Title</div>
                    <div class="divTableCell">Email Address</div>
                    <div class="divTableCell">Phone Number</div>
                    <div class="divTableCell">Company Address</div>
                    <div class="divTableCell">Status</div>
                    <div class="divTableCell text-center">Action</div>
                </div>
                <?php $query = $this->Models->web_form_leads() ?>
                <?php foreach ($query->result() as $value) { ?>
                    <div class="divTableRow " >
                        <div class="divTableCell"><?php echo $value->Contact_Person ?></div>
                        <div class="divTableCell"><?php echo $value->Position_Title ?></div>
                        <div class="divTableCell"><?php echo $value->Email_Address ?></div>
                        <div class="divTableCell"><?php echo $value->Phone_Number ?></div>
                        <div class="divTableCell"><?php echo $value->Company_Full ?></div>
                        <div class="divTableCell">
                            <select name="status" class="form-control form-control-sm select-status" data-bind="<?php echo $value->CID ?>">
                                <option value="0">Select</option>
                                <option value="1" <?php echo ($value->status == 1 ? 'selected="selected"' : '') ?>>Won</option>
                                <option value="2" <?php echo ($value->status == 2 ? 'selected="selected"' : '') ?>>Pending</option>
                                <option value="3" <?php echo ($value->status == 3 ? 'selected="selected"' : '') ?>>Loss</option>
                            </select>
                        </div>
                        <div class="divTableCell width_150">
                            <div class="text-center">
                                <button class="btn btn-default btn-rounded btn-sm open-modal-info" type="button" data-ref="<?php echo $value->REF_ID ?>" data-id="<?php echo $value->CID ?>"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-default btn-rounded btn-sm delete-client" type="button" data-id="<?php echo $value->CID ?>"><i class="fa fa-trash"></i></button>
                                <button class="btn btn-rounded btn-sm <?php echo ($value->client_check ? 'btn-success' : 'btn-default') ?> potential-client" type="button" data-id="<?php echo $value->CID ?>"><i class="fas fa-check"></i></button>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="pagination-cont text-right">
            <?php echo $this->Models->user_pagination_leads(); ?>
        </div>
    </div>
</div>

<div class="modal fade open-modal-information" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-full">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Client Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <nav class="form-group">
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active"  data-toggle="tab" href="#nav-person" role="tab" aria-controls="nav-home" aria-selected="true">Contact Person</a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#upload-files" role="tab" aria-controls="nav-home" aria-selected="true">Upload Files</a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-former" role="tab" aria-controls="nav-profile" aria-selected="false">Former Name</a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-company" role="tab" aria-controls="nav-contact" aria-selected="false">Company Address</a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#services" role="tab" aria-controls="nav-contact" aria-selected="false">Type of services</a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#reminder-info" role="tab" aria-controls="nav-contact" aria-selected="false">Reminder</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-person" role="tabpanel" aria-labelledby="nav-home-tab">
                        <form class="row update-contact-person" action="<?php echo site_url('request/update_admin_client_form') ?>">
                            <input type='hidden' class='form-control client_id' name='CID' value=''>
                            <div class='col-md-3 form-group'><label class='btn-block'>Contact Person</label><input type='text' class='form-control' name='Contact_Person' value=''></div>
                            <div class='col-md-3 form-group'><label class='btn-block'>Position Title</label><input type='text' class='form-control' name='Position_Title' value=''></div>
                            <div class='col-md-3 form-group'><label class='btn-block'>Email Address</label><input type='text' class='form-control' name='Email_Address' value=''></div>
                            <div class='col-md-3 form-group'><label class='btn-block'>Phone Number</label><input type='text' class='form-control' name='Phone_Number' value=''></div>
                            <div class='col-md-3 form-group'><label class='btn-block'>Company Legal</label><input type='text' class='form-control' name='Company_Legal' value=''></div>
                            <div class='col-md-3 form-group'><label class='btn-block'>Company Full</label><input type='text' class='form-control' name='Company_Full' value=''></div>
                            <div class='col-md-3 form-group'><label class='btn-block'>Company Website</label><input type='text' class='form-control' name='Company_Website' value=''></div>
                            <div class='col-md-3 form-group'><label class='btn-block'>&nbsp;</label><button class="btn btn-default btn-block">Update</button></div>
                        </form>
                        <div class="row">
                            <div class="col-6"><h5 class="form-group">Accounting Assestment</h5></div>
                            <div class="col-6 text-right">
                                <button class="btn btn-default btn-rounded btn-sm add-cna-btn"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="add-cna-tables hide">
                            <form class="row add-contact-person" action="<?php echo site_url('request/add_cna_form') ?>">
                                <input type='hidden' class='form-control client_id' name='CID' value=''>
                                <input type='hidden' class='form-control reference_id' name='REF' value=''> 
                                <div class='col-md-3 form-group'><label class='btn-block'>What does</label><input type='text' class='form-control' name='Add_What_does' value=''></div>
                                <div class='col-md-3 form-group'><label class='btn-block'>What industry</label><input type='text' class='form-control' name='Add_What_industry' value=''></div>
                                <div class='col-md-3 form-group'><label class='btn-block'>How long_have</label><input type='text' class='form-control' name='Add_How_long_have' value=''></div>
                                <div class='col-md-3 form-group'><label class='btn-block'>How is the company</label><input type='text' class='form-control' name='Add_How_is_the_company' value=''></div>
                                <div class='col-md-3 form-group'><label class='btn-block'>Do you currently</label><input type='text' class='form-control' name='Add_Do_you_currently' value=''></div>
                                <div class='col-md-3 form-group'><label class='btn-block'>What is your current</label><input type='text' class='form-control' name='Add_What_is_your_current' value=''></div>
                                <div class='col-md-3 form-group'><label class='btn-block'>Do we need to bring</label><input type='text' class='form-control' name='Add_Do_we_need_to_bring' value=''></div>
                                <div class='col-md-3 form-group'><label class='btn-block'>Do you need to track</label><input type='text' class='form-control' name='Add_Do_you_need_to_track' value=''></div>
                                <div class='col-md-3 form-group'><label class='btn-block'>How did you</label><input type='text' class='form-control' name='Add_How_did_you' value=''></div>
                                <div class='col-md-3 form-group'><label class='btn-block'>Submitted date</label><input type='text' class='form-control' name='Add_submitted_date' value=''></div>

                                <div class='col-md-3 form-group service-checkbox'>
                                    <label>What other Scrubbed services..</label>
                                    <input type='hidden' class='form-control' name='Add_What_other_Scrubbed' value=''>
                                    <div class="option"><label><input type="checkbox" name="Add_What_other_Scrubbed1" value="Income Tax Prep and Filing"> Income Tax Prep and Filing</label></div>
                                    <div class="option"><label><input type="checkbox" name="Add_What_other_Scrubbed2" value="State and Local Taxes"> State and Local Taxes</label></div>
                                    <div class="option"><label><input type="checkbox" name="Add_What_other_Scrubbed3" value="Tax Planning"> Tax Planning</label></div>
                                    <div class="option"><label><input type="checkbox" name="Add_What_other_Scrubbed4" value="Financial Modeling"> Financial Modeling</label></div>
                                    <div class="option"><label><input type="checkbox" name="Add_What_other_Scrubbed5" value="Investor Pitch Deck"> Investor Pitch Deck</label></div>
                                    <div class="option"><label><input type="checkbox" name="Add_What_other_Scrubbed6" value="Financial Planning and Analysis"> Financial Planning and Analysis</label></div>
                                    <div class="option"><label><input type="checkbox" name="Add_What_other_Scrubbed7" value="Transaction Advisory Services"> Transaction Advisory Services</label></div>
                                </div>
                                <div class='col-md-3 form-group'><label class='btn-block'>&nbsp;</label><button class="btn btn-default btn-block">Update</button></div>
                            </form>
                        </div>


                        <div class="row">
                            <div class="col-6"><h5 class="form-group">Tax Assestment</h5></div>
                            <div class="col-6 text-right">
                                <button class="btn btn-default btn-rounded btn-sm add-tax-btn"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="add-tax-tables hide">
                            <form class="row add-tax-form" method="post" action="<?php echo site_url('request/add_tax_form') ?>">
                                <input type='hidden' class='form-control client_id' name='CID' value=''>
                                <input type='hidden' class='form-control reference_id' name='REF' value=''> 
                                <div class="col-md-3 form-group"><label class="btn-block">What does</label><input type="text" class="form-control" name="What_does" value=""></div>
                                <div class="col-md-3 form-group">
                                    <label class="btn-block">How long have you been in operation?</label>
                                    <select class="form-control" name="How_long_have">
                                        <option value="Under 6 Months">Under 6 Months</option>
                                        <option value="6 - 12 Months">6 - 12 Months</option>
                                        <option value="1 - 2 Years">1 - 2 Years</option>
                                        <option value="2 - 5 Years">2 - 5 Years</option>
                                        <option value="Over 5 Years">Over 5 Years</option>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label class="btn-block">Who is currently handling your</label>
                                    <select class="form-control" name="Who_is_currently">
                                        <option value="In-House">In-House</option>
                                        <option value="Outside Service Provider">Outside Service Provider</option>
                                        <option value="None">None</option>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label class="btn-block">What type of annual income</label>
                                    <select class="form-control" name="What_type_of_annual">
                                        <option value="Business Federal and State Tax Return (1120 C Corp, 1120S,  1065)">Business Federal and State Tax Return (1120 C Corp, 1120S,  1065)</option>
                                        <option value="Individual Federal and State Tax Return (1040)">Individual Federal and State Tax Return (1040)</option>
                                        <option value="Exempt Organization Federal and State Tax  Return (990)">Exempt Organization Federal and State Tax  Return (990)</option>
                                        <option value="Not Sure">Not Sure</option>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label class="btn-block">What year was your last</label>
                                    <select class="form-control" name="Tax_What_year_was">
                                        <option value="2019">2019</option>
                                        <option value="2018">2018</option>
                                        <option value="2017">2017</option>
                                        <option value="2016">2016</option>
                                        <option value="2015">2015</option>
                                        <option value="2014">2014</option>
                                        <option value="2013">2013</option>
                                        <option value="2012">2012</option>
                                        <option value="2011">2011</option>
                                        <option value="2010">2010</option>
                                        <option value="2009">2009</option>
                                        <option value="2008">2008</option>
                                        <option value="2007">2007</option>
                                        <option value="2006">2006</option>
                                        <option value="2005">2005</option>
                                        <option value="2004">2004</option>
                                        <option value="2003">2003</option>
                                        <option value="2002">2002</option>
                                        <option value="2001">2001</option>
                                        <option value="2000">2000</option>
                                        <option value="Prior to 2000">Prior to 2000</option>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label class="btn-block">Do you have any delinquent</label><input type="hidden" class="form-control" name="Update_What_is_your_currentelect-current" value="Sales Tax,Annual Income Tax,Local Business &amp; Property Tax,Other,">
                                    <div class="option"><label><input type="checkbox" name="Do_you_have_any1" value="None"> None</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_have_any2" value="Sales Tax"> Sales Tax</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_have_any3" value="Annual Income Tax"> Annual Income Tax</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_have_any4" value="Local Business &amp; Property Tax"> Local Business &amp; Property Tax</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_have_any5" value="Other"> Other</label></div>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Are you currently under audit? <span class="required">*</span></label>
                                    <select class="form-control" name="Are_you_currently">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="Not Sure">Not Sure</option>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Are you required to file</label>
                                    <select class="form-control" name="Are_you_required_sales">
                                        <option value="No">No</option>
                                        <option value="Not Sure">Not Sure</option>
                                        <option value="Yes - I have less than 100 sales transactions per month">Yes - I have less than 100 sales transactions per month</option>
                                        <option value="Yes - I have between 100 and 300 sales transactions per month">Yes - I have between 100 and 300 sales transactions per month</option>
                                        <option value="Yes - I have more than 300 sales transactions per month">Yes - I have more than 300 sales transactions per month</option>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Are you required to file local</label>
                                    <select class="form-control" name="Are_you_required">
                                        <option value="No">No</option>
                                        <option value="Yes - I have 1 - 50 Business Properties">Yes - I have 1 - 50 Business Properties</option>
                                        <option value="Yes - I have over 50 Business Properties">Yes - I have over 50 Business Properties</option>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <input type="hidden" class="form-control" value="Group Taxation,Transfer Pricing,Thin Capitalization,Controlled Foreign Corporations,FATCA and FBAR,"><label>Do you need tax planning &amp; advisory services?</label>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need1" value="None"> None</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need2" value="Other Federal Taxes (Excise, stamp, capital gains, etc)"> Other Federal Taxes (Excise, stamp, capital gains, etc)</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need3" value="Branch Income"> Branch Income</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need4" value="Group Taxation">  Group Taxation</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need5" value="Transfer Pricing"> Transfer Pricing</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need6" value="Thin Capitalization"> Thin Capitalization</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need7" value="Controlled Foreign Corporations"> Controlled Foreign Corporations</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need8" value="FATCA and FBAR"> FATCA and FBAR</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need9" value="Other"> Other</label></div>
                                </div>
                                <div class="col-md-3 form-group">
                                    <input type="hidden" class="form-control" value="Ownership change analysis (Section 382 studies),Cost Segregation studies (determine depreciability),"><label>Do you need tax analysis services?</label>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need_tax1" value="No"> No</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need_tax2" value="Net operating loss studies (tax asset quantification)"> Net operating loss studies (tax asset quantification)</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need_tax3" value="Ownership change analysis (Section 382 studies)"> Ownership change analysis (Section 382 studies)</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need_tax4" value="Cost Segregation studies (determine depreciability)">  Cost Segregation studies (determine depreciability)</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need_tax5" value="Earnings and Profits studies (distinguish non-taxable distributions from dividends)"> Earnings and Profits studies (distinguish non-taxable distributions from dividends)</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need_tax6" value="Nexus studies (determine state tax exposure)"> Nexus studies (determine state tax exposure)</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need_tax7" value="Basis studies (determine taxable gain or loss)"> Basis studies (determine taxable gain or loss)</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need_tax8" value="Built-in gain and loss studies (evaluate tax assets)"> Built-in gain and loss studies (evaluate tax assets)</label></div>
                                    <div class="option"><label><input type="checkbox" name="Do_you_need_tax9" value="Other"> Other</label></div>
                                </div>
                                <div class="col-md-3 form-group"><label class="btn-block">&nbsp;</label><button class="btn btn-default btn-block">Submit</button></div>
                            </form>
                        </div>



                        <div class="list update-cna-form">
                            <ul class="list-group update-cna-form-html">




                            </ul>
                        </div>


                    </div>
                    <div class="tab-pane fade" id="upload-files" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <form action="<?php echo site_url('request/client_information_upload_doc') ?>" class="information_upload_doc" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="CID" class="client_id"/>
                            <div class="upload-files-drop form-group">
                                <input type="file" name="file[]" class="form-control file-uploader" multiple="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword, application/vnd.ms-excel,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*">
                            </div>
                            <div class="progress form-group hide">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                            </div>
                        </form>
                        <ul class="list-group client-files-uploaded uploaded-list">

                        </ul>
                    </div>
                    <div class="tab-pane fade" id="nav-former" role="tabpanel" aria-labelledby="nav-profile-tab">

                        <nav class="form-group">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active"  data-toggle="tab" href="#former-info" role="tab" aria-controls="nav-home" aria-selected="true">Former Name</a>
                                <a class="nav-item nav-link" data-toggle="tab" href="#former-upload-files" role="tab" aria-controls="nav-home" aria-selected="true">Upload Files</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade active show" id="former-info" role="tabpanel" aria-labelledby="nav-contact-tab">
                                <div class="table-responsive">
                                    <div class="divTable table-width1200">
                                        <div class="divTableBody former-info">
                                            <div class="divTableRow divTbHead">
                                                <div class="divTableCell width_350">Date Change</div>
                                                <div class="divTableCell width_350">Former Name</div>
                                                <div class="divTableCell width_350">Reason</div>
                                                <div class="divTableCell text-center">Action</div>
                                            </div>
                                            <form class="divTableRow add-former-name-info former-js-validate" method="post" action="<?php echo site_url('request/add_former_name_info') ?>">
                                                <input type="hidden" name="CID" class="client_id"/>
                                                <div class="divTableCell"><input type="date" name="Date_Change" class="form-control"></div>
                                                <div class="divTableCell"><input type="text" name="Former_Name" class="form-control"></div>
                                                <div class="divTableCell"><input type="text" name="Reason" class="form-control"></div>
                                                <div class="divTableCell text-center min_width_200"><button class="btn btn-default btn-rounded"><i class="fas fa-plus"></i></button></div>
                                            </form>
                                        </div> 
                                        <div class="divTableBody added-former-info">

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="former-upload-files" role="tabpanel" aria-labelledby="nav-contact-tab">
                                <form action="<?php echo site_url('request/crm_former_name_upload_doc') ?>" class="information_upload_doc" enctype="multipart/form-data" method="post">
                                    <input type="hidden" name="CID" class="client_id"/>
                                    <div class="upload-files-drop form-group">
                                        <input type="file" name="file[]" class="form-control file-uploader" multiple="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword, application/vnd.ms-excel,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*">
                                    </div>
                                    <div class="progress form-group hide">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                    </div>
                                </form>
                                <ul class="list-group former-files-uploaded uploaded-list">

                                </ul>
                            </div>
                        </div>


                    </div>
                    <div class="tab-pane fade" id="nav-company" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <div class="table-responsive">
                            <div class="divTable table-width1200">
                                <div class="divTableBody">
                                    <div class="divTableRow divTbHead">
                                        <div class="divTableCell width_350">State</div>
                                        <div class="divTableCell width_350">City</div>
                                        <div class="divTableCell width_350">Zip Code</div>
                                        <div class="divTableCell text-center">Action</div>
                                    </div>
                                    <form class="divTableRow company-address" action="<?php echo site_url('request/add_compny_address') ?>" method="post">
                                        <input type="hidden" name="CID" class="client_id"/>
                                        <div class="divTableCell"><input type="text" name="State" class="form-control"></div>
                                        <div class="divTableCell"><input type="text" name="City" class="form-control"></div>
                                        <div class="divTableCell"><input type="text" name="Zip_code" class="form-control"></div>
                                        <div class="divTableCell text-center width_200">
                                            <button class="btn btn-default btn-rounded"><i class="fas fa-plus"></i></button>
                                        </div>
                                    </form>

                                </div> 
                                <div class="divTableBody show-added-compny-address">

                                </div>
                            </div> 
                        </div> 


                    </div>
                    <div class="tab-pane fade" id="services" role="tabpanel" aria-labelledby="nav-contact-tab">

                        <nav class="form-group">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active"  data-toggle="tab" href="#services-info" role="tab" aria-controls="nav-home" aria-selected="true">Services</a>
                                <a class="nav-item nav-link" data-toggle="tab" href="#services-upload-files" role="tab" aria-controls="nav-home" aria-selected="true">Upload Files</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade active show" id="services-info" role="tabpanel" aria-labelledby="nav-contact-tab">
                                <div class="table-responsive">
                                    <div class="divTable table-width1200">
                                        <div class="divTableBody">
                                            <div class="divTableRow divTbHead">
                                                <div class="divTableCell width_200">Group</div>
                                                <div class="divTableCell width_200">Type of service</div>
                                                <div class="divTableCell width_200">Agreement (Yes No)</div>
                                                <div class="divTableCell width_200">Date from</div>
                                                <div class="divTableCell width_200">Date to</div>
                                                <div class="divTableCell width_200">Hourly/Monthly</div>
                                                <div class="divTableCell width_200">Amount</div>
                                                <div class="divTableCell text-center width_200">Action</div>
                                            </div>
                                            <form class="divTableRow type-of-services" action="<?php echo site_url('request/add_type_of_services') ?>" method="post">
                                                <input type="hidden" name="CID" class="client_id"/>
                                                <div class="divTableCell width_200"><input type="text" class="form-control" name="Group"></div>
                                                <div class="divTableCell width_200"><input type="text" class="form-control" name="Type_of_service"></div>
                                                <div class="divTableCell width_200">  
                                                    <select class="form-control" name="Agreement">
                                                        <option value="">Select</option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </div>
                                                <div class="divTableCell width_200"><input type="date" class="form-control" name="Date_from"></div>
                                                <div class="divTableCell width_200"><input type="date" class="form-control" name="Date_to"></div>
                                                <div class="divTableCell">
                                                    <select class="form-control" name="Hourly_Monthly">
                                                        <option value="">Select</option>
                                                        <option value="hourly">Hourly</option>
                                                        <option value="monthly">Monthly</option>
                                                    </select>
                                                </div>
                                                <div class="divTableCell width_200"><input type="text" class="form-control" name="Amount"></div>
                                                <div class="divTableCell text-center">
                                                    <button class="btn btn-default btn-rounded"><i class="fas fa-plus"></i></button>
                                                </div>
                                            </form>
                                        </div> 

                                        <div class="divTableBody show-added-type-of-services">

                                        </div>
                                    </div> 
                                </div>


                            </div>
                            <div class="tab-pane fade" id="services-upload-files" role="tabpanel" aria-labelledby="nav-contact-tab">
                                <form action="<?php echo site_url('request/crm_type_of_service_upload_doc') ?>" class="type-of-service-upload-doc" enctype="multipart/form-data" method="post">
                                    <input type="hidden" name="CID" class="client_id"/>
                                    <div class="upload-files-drop form-group">
                                        <input type="file" name="file[]" class="form-control file-uploader" multiple="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword, application/vnd.ms-excel,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*">
                                    </div>
                                    <div class="progress form-group hide">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                    </div>
                                </form>

                                <ul class="list-group service-files-uploaded uploaded-list">

                                </ul>
                            </div>
                        </div>

                    </div>


                    <div class="tab-pane fade" id="reminder-info" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="table-responsive">
                            <div class="divTable table-width1200">
                                <div class="divTableBody">
                                    <div class="divTableRow divTbHead">
                                        <div class="divTableCell">Start Date</div>
                                        <div class="divTableCell">End Date</div>
                                        <div class="divTableCell">Notify</div>
                                        <div class="divTableCell">What</div>
                                        <div class="divTableCell">Where</div>
                                        <div class="divTableCell">Description</div>
                                        <div class="divTableCell text-center">Action</div>
                                    </div>
                                    <form class="divTableRow reminders" action="<?php echo site_url('request/add_reminder') ?>" method="post">
                                        <input type="hidden" name="CID" class="client_id"/>
                                        <div class="divTableCell"><input type="datetime-local" class="form-control" name="start_date"/></div>
                                        <div class="divTableCell"><input type="datetime-local" class="form-control" name="end_date"/></div>
                                        <div class="divTableCell"><input type="text" class="form-control" name="notify"/></div>
                                        <div class="divTableCell"><input type="text" class="form-control" name="what"/></div>
                                        <div class="divTableCell"><input type="text" class="form-control" name="where"/></div>
                                        <div class="divTableCell"><input type="text" class="form-control" name="description"/></div>
                                        <div class="divTableCell width_150">
                                            <div class="text-center">
                                                <button class="btn btn-default btn-rounded"><i class="fas fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                <div class="divTableBody show-added-reminders">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
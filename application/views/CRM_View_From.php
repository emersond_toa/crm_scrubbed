<style>
    .client-form{
        display: block;
        max-width: 860px;
    }
    .client-form .form-group{ 
        display: block;
        margin-bottom: 1rem;
        width: 100%;
    }
    .client-form .form-group label{
        display: inline-block;
        margin-bottom: .5rem;
    }
    .client-form .form-control{
        width: 100%;
        padding: 12px;
        margin: 6px 0 4px;
        border: 1px solid #ccc;
        background: #fafafa;
        color: #000;
        font-family: sans-serif;
        font-size: 12px;
        line-height: normal;
        box-sizing: border-box;
        border-radius: 6px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        outline: none !important;
    }
    .client-form .is-invalid{
        -webkit-transition-duration: 1s;
        -moz-transition-duration: 1s;
        -o-transition-duration: 1s;
        transition-duration: 1s;
        border: 1px solid #d20505;
        box-shadow: 0px 0px 5px transparent;
    }
    .client-form .is-invalid:focus{
        box-shadow: 0px 0px 5px #d20505;
    }
    .client-form .muted{
        padding: .5em 0 .5em;
        font-size: 12px;
        opacity: .7;
        display: block;
        white-space: pre-wrap;
    }
    .client-form .required {
        color: red;
    }

    .client-form .submit-form-btn{
        color: #fff;
        background-color: #272727;
        border-color: #272727;
        border-radius: 300px;
        display: inline-block;
        width: auto;
        height: auto;
        padding: 1em 2.5em;
        border: 1px solid #272727;
    }
</style>

<form class="client-form" action="<?php echo site_url('contacts/client_accounting_form') ?>" method="post">
    <input type="hidden" name="form_type" value="ACCOUNTING">
    <div class="form-group">
        <label>Contact Person <span class="required">*</span></label>
        <input type="text" class="form-control" name="Contact_Person"/>
    </div>
    <div class="form-group">
        <label>Position/Title <span class="required">*</span></label>
        <input type="text" class="form-control" name="Position_Title"/>
    </div>
    <div class="form-group">
        <label>Email Address <span class="required">*</span></label>
        <input type="text" class="form-control" name="Email_Address"/>
    </div>
    <div class="form-group">
        <label>Phone Number <span class="required">*</span></label>
        <input type="text" class="form-control" name="Phone_Number"/>
    </div>
    <div class="form-group">
        <label>Company Legal/Trading Name <span class="required">*</span></label>
        <input type="text" class="form-control" name="Company_Legal"/>
    </div>
    <div class="form-group">
        <label>Company Full Address <span class="required">*</span></label>
        <input type="text" class="form-control" name="Company_Full"/>
    </div>
    <div class="form-group">
        <label>Company Website <span class="required">*</span></label>
        <input type="text" class="form-control" name="Company_Website"/>
    </div>
    <div class="form-group">
        <label>What does your company do? <span class="required">*</span></label>
        <select class="form-control" name="What_does" aria-required="true">
            <option value="My company provides professional services.">My company provides professional services.</option>
            <option value="My company provides real estate services.">My company provides real estate services.</option>
            <option value="My company provides technology services.">My company provides technology services.</option>
            <option value="My company is a non-profit organization.">My company is a non-profit organization.</option>
            <option value="My company is still in research and development.">My company is still in research and development.</option>
            <option value="My company distributes products.">My company distributes products.</option>
            <option value="My company manufactures products.">My company manufactures products.</option>
        </select>
    </div>
    <div class="form-group">
        <label>What industry? <span class="required">*</span></label>
        <select class="form-control" name="What_industry">
            <?php
            $industry = $this->Models->crm_industries();
            foreach ($industry->result() as $value) {
                ?>
                <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
            <?php }?>
        </select>

    </div>
    <div class="form-group">
        <label>How long have you been in operation? <span class="required">*</span></label>
        <select class="form-control" name="How_long_have">
            <option value="Under 6 Months">Under 6 Months</option>
            <option value="6 - 12 Months">6 - 12 Months</option>
            <option value="1 - 2 Years">1 - 2 Years</option>
            <option value="2 - 5 Years">2 - 5 Years</option>
            <option value="Over 5 Years">Over 5 Years</option>
        </select>
    </div>
    <div class="form-group">
        <label>How is the company currently funded? <span class="required">*</span></label>
        <select class="form-control" name="How_is_the_company">
            <option value="Self Funded">Self Funded</option>
            <option value="Angel Investors">Angel Investors</option>
            <option value="Family &amp; Friends">Family &amp; Friends</option>
            <option value="Venture Capitalist">Venture Capitalist</option>
            <option value="Bank Loans">Bank Loans</option>
            <option value="Private Equity">Private Equity</option>
        </select>
    </div>
    <div class="form-group">
        <label>Do you currently have accounting system in place? <span class="required">*</span></label>
        <select class="form-control" name="Do_you_currently">
            <option value="No Accounting system in place">No Accounting system in place</option>                 
            <option value="Xero">Xero</option>                 
            <option value="Quickbooks Online">Quickbooks Online</option>                 
            <option value="Quickbooks Desktop">Quickbooks Desktop</option>                 
            <option value="Quickbooks Enterprise">Quickbooks Enterprise</option>                  
            <option value="Netsuite">Netsuite</option>                 
            <option value="Intacct">Intacct</option>                 
            <option value="Quicken">Quicken</option>                
            <option value="Other">Other</option>                
        </select>
    </div>
    <div class="form-group">
        <label>What is your current average monthly company financial transactions? <span class="required">*</span></label>
        <div class="muted">(bank + credit cards + ecommerce transactions)</div>
        <select class="form-control" name="What_is_your_current">
            <option value="Less than 50 Transactions per Month">Less than 50 Transactions per Month</option>
            <option value="51 - 100 Transactions per Month">51 - 100 Transactions per Month</option>
            <option value="101 - 300 Transactions per Month">101 - 300 Transactions per Month</option>
            <option value="301 - 500 Transactions per Month">301 - 500 Transactions per Month</option>
            <option value="501 - 1000 Transactions per Month">501 - 1000 Transactions per Month</option>
            <option value="1001 - 2000 Transactions per Month">1001 - 2000 Transactions per Month</option>
            <option value="2001 - 5000 Transactions per Month">2001 - 5000 Transactions per Month</option>
            <option value="Over 5000 Transactions per Month">Over 5000 Transactions per Month</option>
        </select>
    </div>
    <div class="form-group">
        <label>Do we need to bring your books up to date? <span class="required">*</span></label>
        <select class="form-control" name="Do_we_need_to_bring">
            <option value="No - I'm completely up to date">No - I'm completely up to date</option>
            <option value="Yes - I'm 1 - 3 Months Behind">Yes - I'm 1 - 3 Months Behind</option>
            <option value="Yes - I'm 4 - 6 Months Behind">Yes - I'm 4 - 6 Months Behind</option>
            <option value="Yes - I'm 7 - 9 Months Behind">Yes - I'm 7 - 9 Months Behind</option>
            <option value="Yes - I'm 10 - 12 Months Behind">Yes - I'm 10 - 12 Months Behind</option>
            <option value="Yes - I'm over 12 Months Behind">Yes - I'm over 12 Months Behind</option>
        </select>
    </div>
    <div class="form-group">
        <label>Do you need to track expenses by department, by project, or by any other segment? <span class="required">*</span></label>
        <select class="form-control" name="Do_you_need_to_track">
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
    <div class="form-group">
        <label>What other Scrubbed services are you interested in?</label>
        <div class="option"><label><input type="checkbox" name="What_other_Scrubbed1" value="Income Tax Prep and Filing"> Income Tax Prep and Filing</label></div>
        <div class="option"><label><input type="checkbox" name="What_other_Scrubbed2" value="State and Local Taxes"> State and Local Taxes</label></div>
        <div class="option"><label><input type="checkbox" name="What_other_Scrubbed3" value="Tax Planning"> Tax Planning</label></div>
        <div class="option"><label><input type="checkbox" name="What_other_Scrubbed4" value="Financial Modeling"> Financial Modeling</label></div>
        <div class="option"><label><input type="checkbox" name="What_other_Scrubbed5" value="Investor Pitch Deck"> Investor Pitch Deck</label></div>
        <div class="option"><label><input type="checkbox" name="What_other_Scrubbed6" value="Financial Planning and Analysis"> Financial Planning and Analysis</label></div>
        <div class="option"><label><input type="checkbox" name="What_other_Scrubbed7" value="Transaction Advisory Services"> Transaction Advisory Services</label></div>
    </div>
    <div class="form-group">
        <label>How did you find out about us?</label>
        <input class="form-control" type="text" name="How_did_you">
    </div>
    <div class="">
        <button type="submit" class="submit-form-btn">SUBMIT</button>
    </div>
</form>
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
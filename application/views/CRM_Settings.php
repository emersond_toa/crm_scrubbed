<div class="container-fluid main-container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="h3 mb-4 text-gray-800">Settings</h1>
        </div>

        <div class="col-lg-6">
            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="card-title">Referral Partners</h5>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0"
                            role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col">Referral</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <td><input type="text" name="referral" id="referral" class="form-control"></td>
                                    <td><button class="btn btn-light btn-sm" id="add-referral"><i class="fa fa-plus"></i></button></td>
                                </tr>
                            </thead>
                            <tbody id="referral-body">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="card-title">Lead Sources</h5>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0"
                            role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col">Source</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <td><input type="text" name="source" id="source" class="form-control"></td>
                                    <td><button class="btn btn-light btn-sm" id="add-source"><i class="fa fa-plus"></i></button></td>
                                </tr>
                            </thead>
                            <tbody id="source-body">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="card-title">Termination Reasons</h5>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0"
                            role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col">Reason</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <td><input type="text" name="terminationreason" id="terminationreason" class="form-control"></td>
                                    <td><button class="btn btn-light btn-sm" id="add-terminationreason"><i class="fa fa-plus"></i></button></td>
                                </tr>
                            </thead>
                            <tbody id="terminationreason-body">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="card-title">Uploads</h5>

                    
                    <a class="btn btn-default" href="<?php echo site_url('Invoices') ?>">Invoices</a>
                    <a class="btn btn-default" href="<?php echo site_url('Payments') ?>">Payments</a>
                    <a class="btn btn-default" href="<?php echo site_url('Export') ?>">Export Client</a>
                </div>
            </div>
        </div>

        <!-- <div class="col-lg-6">
            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="card-title">Termination Reasons</h5>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0"
                            role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col">Reason</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <td><input type="text" name="terminationreason" id="terminationreason" class="form-control"></td>
                                    <td><button class="btn btn-light btn-sm" id="add-terminationreason"><i class="fa fa-plus"></i></button></td>
                                </tr>
                            </thead>
                            <tbody id="terminationreason-body">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</div>
<div class="container-fluid main-container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="h3 mb-4 text-gray-800">Export Client</h1>
        </div>

        <div class="col-lg-8">
            <div class="card mb-4">
                <div class="card-body">
                    <?php if(!empty($error_msg)) : ?>
                        <div class="col-xs-12">
                            <div class="alert alert-danger"><?php echo $error_msg; ?></div>
                        </div>
                    <?php endif; ?>
                    <form method="post" action="<?php echo base_url('export/export_clients'); ?>">
                        <div class="form-group">
                            <label for="client">Client Names</label>
                            <select name="client[]" id="client" class="form-control chosen-select2" multiple>
                                <?php foreach($clients as $client) : ?>
                                    <option value="<?php echo $client->ID; ?>"><?php echo $client->entity; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div> 

                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-primary">Export</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
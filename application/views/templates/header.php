<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="site" content="<?php echo site_url() ?>">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo $title ?></title>
        <link href="<?php echo site_url('vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?php echo site_url('css/simple-sidebar.css') ?>" rel="stylesheet">
        <link href="<?php echo site_url('css/all.min.css') ?>" rel="stylesheet">
        <link href="<?php echo site_url('css/select2.min.css') ?>" rel="stylesheet">
        <link href="<?php echo site_url('jquery-ui/jquery-ui.structure.min.css') ?>" rel="stylesheet">
        <link href="<?php echo site_url('jquery-ui/jquery-ui.theme.min.css') ?>" rel="stylesheet">
        <link href="<?php echo site_url('jquery-ui/jquery-ui.min.css') ?>" rel="stylesheet">

        <!-- <link href="<?php echo site_url('css/sb-admin.min.css') ?>" rel="stylesheet"> -->
        
        
        <link href='<?php echo site_url('css/select2-bootstrap.min.css') ?>' rel='stylesheet'>
        <link href="<?php echo site_url('css/style.css') ?>" rel="stylesheet">
        <link href='//fonts.googleapis.com/css?family=Montserrat:thin,extra-light,light,100,200,300,400,500,600,700,800'rel='stylesheet' type='text/css'>
        <link href='<?php echo site_url('css/dataTables.bootstrap4.min.css') ?>' rel='stylesheet'>
        <link href='<?php echo site_url('css/chosen.css') ?>' rel='stylesheet'>
        <link href='<?php echo site_url('css/pnotify.all.min.css') ?>' rel='stylesheet'>
        <link href='<?php echo site_url('css/jquery-confirm.css') ?>' rel='stylesheet'>
        
    </head>

    <body>
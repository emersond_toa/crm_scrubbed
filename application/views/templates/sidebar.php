<?php $url = 'https://employeeportal.scrubbed.net/' ?>
<div class="d-flex" id="wrapper">
    <?php $info = $this->Models->get_user_info($this->session->userdata('user_session')) ?>
    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
        <br><br><br><br>
        <div class="sidebar-heading text-center">
            <div class="row">
                <div class="col-sm-12">
                    <?php if (!$info->profile_photo) { ?>
                        <img data-bind="" src="<?php echo $url . 'images/profile.png'; ?>" class="post-user-image-sidebar">    
                    <?php } else { ?>
                        <img data-bind="" src="<?php echo $url . "uploads/$info->profile_photo"; ?>" class="post-user-image-sidebar">
                    <?php } ?>
                    <p><?php //echo $info->first_name . ' ' . $info->last_name ?></p>
                    <h4 class="job-title-sidbar"><?php echo $info->job_title ?></h4>
                </div>
            </div>
        </div>

        <ul class="navbar-nav sidebar sidebar-light mb-5">
            <hr class="sidebar-divider my-0">
            <li class="nav-item <?php echo ($this->uri->segment(1) == "") ? 'active' : ''; ?>">
                <a class="nav-link" href="<?php echo site_url() ?>">
                    <i class="fas fa-fw fa-tachometer-alt" aria-hidden="true"></i>
                    <span>Dashboard</span></a>
            </li>

            <li class="nav-item <?php echo ($this->uri->segment(1) == "funnel") ? 'active' : ''; ?>">
                <a class="nav-link" href="<?php echo site_url('funnel') ?>">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span>Funnel</span></a>
            </li>

            <li class="nav-item <?php echo ($this->uri->segment(1) == "clients") ? 'active' : ''; ?>">
                <a class="nav-link" href="<?php echo site_url('clients') ?>">
                    <i class="fa fa-address-card" aria-hidden="true"></i>
                    <span>Clients</span></a>
            </li>

            <li class="nav-item <?php echo ($this->uri->segment(1) == "Former_Clients") ? 'active' : ''; ?>">
                <a class="nav-link" href="<?php echo site_url('Former_Clients') ?>">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span>Former Clients</span></a>
            </li>

            <?php if($info->crm_access == 2) : ?>
            <li class="nav-item <?php echo ($this->uri->segment(1) == "Invoices") ? 'active' : ''; ?>">
                <a class="nav-link" href="<?php echo site_url('Invoices') ?>">
                    <i class="fa fa-file-invoice" aria-hidden="true"></i>
                    <span>Invoices</span></a>
            </li>
            <?php endif; ?>

            <!-- <li class="nav-item <?php echo ($this->uri->segment(1) == "Payments") ? 'active' : ''; ?>">
                <a class="nav-link" href="<?php echo site_url('Payments') ?>">
                    <i class="fa fa-file-invoice" aria-hidden="true"></i>
                    <span>Payments</span></a>
            </li> -->

            <li class="nav-item <?php echo ($this->uri->segment(1) == "settings") ? 'active' : ''; ?>">
                <a class="nav-link" href="<?php echo site_url('settings') ?>">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span>Settings</span></a>
            </li>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <div id="page-content-wrapper">
        <nav class="navbar navbar-expand-lg navbar-light main-nav-new border-bottom fixed-top">
            <button class="btn view-btn mylogo ml-2" id="menu-toggle"><img src="<?php echo site_url('images/logo.png') ?>" /></button>




            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">


                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo $url; ?>"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to Intranet <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">


                            <?php if (!$info->profile_photo) { ?>>    
                                <img data-bind="image-1"  src="<?php echo $url . 'images/profile.png'; ?>" class="post-user-image">
                            <?php } else { ?>
                                <img data-bind="image-1"  src="<?php echo $url . "uploads/$info->profile_photo"; ?>" class="post-user-image">
                            <?php } ?>



                        </a> 
                        <div class="dropdown-menu dropdown-menu-right mynav" aria-labelledby="navbarDropdown">
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item logout-btn" href="#">Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>

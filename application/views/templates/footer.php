            </div>
        </div> 
        <script src="<?php echo site_url('vendor/jquery/jquery.min.js') ?>"></script>
        <script src="<?php echo site_url('vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
        <script src="<?php echo site_url('jquery-ui/jquery-ui.min.js') ?>"></script>
        <script src="<?php echo site_url('vendor/jquery.validate.min.js') ?>"></script>
        <script src="<?php echo site_url('vendor/all.js') ?>"></script>
        <script src="<?php echo site_url('vendor/script.js') ?>"></script>
        <script src="<?php echo site_url('js/Chart.min.js') ?>"></script>
        <script src="<?php echo site_url('js/jquery.form.min.js') ?>"></script>
        <script src="<?php echo site_url('js/confirm.js') ?>"></script>
        <script src="<?php echo site_url('js/jquery.datatables.min.js') ?>"></script>
        <script src="<?php echo site_url('js/jquery.datatables-bootstrap.min.js') ?>"></script>
        <script src="<?php echo site_url('js/dataTables.buttons.min.js') ?>"></script>
        <script src="<?php echo site_url('js/chosen.jquery.min.js') ?>"></script>
        <script src="<?php echo site_url('js/pnotify.all.min.js') ?>"></script>
        <script src="<?php echo site_url('js/jquery-confirm.js') ?>"></script>
        <script src="<?php echo site_url('ckeditor/ckeditor.js') ?>"></script> 
        <script src="<?php echo site_url('js/select2.full.min.js') ?>"></script>
        <script src="<?php echo site_url('js/moment.js') ?>"></script>
        <script src="<?php echo site_url('js/script.js') ?>"></script>
        <script src="<?php echo site_url('js/javascript.js') ?>"></script>
    </body>
</html>

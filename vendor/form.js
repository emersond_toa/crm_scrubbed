var EM = EM || {};
EM.script = (function (j) {
    var _url = j('[name="site"]').attr('content');
    var init = function () {
        _add_form();

    };
    var add_update_client = function () {
        j('.add-update-form').each(function () {
            var _this = j(this);
            _this.validate({
                rules: {
                    Contact_Person: {required: true},
                    Position_Title: {required: true},
                    Email_Address: {required: true},
                    Phone_Number: {required: true},
                    Team: {required: true},
                    First_Billing: {required: true},
                    Hourly: {required: true},
                    Monthly: {required: true},
                    Date_From: {required: true},
                    Date_To: {required: true},
                    Status: {required: true}
                },
                showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.addClass('is-invalid');
                        _elm.popover('dispose').popover({content: value.message, trigger: 'hover', placement: 'top'});
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.removeClass('is-invalid');
                        _elm.popover('dispose');
                    });
                }, submitHandler: function (form) {
                    var _this = j(form);
                    j.ajax({
                        url: _this.attr("action"),
                        dataType: 'json',
                        method: 'post',
                        data: _this.serialize(),
                        success: function (e) {
                            if (e.message === 'success' && e.message === 'add') {
                                _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                    _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                                        var _this = j(this);
                                        _this.removeAttr('style');
                                    });
                                });
                            }
                        }
                    });
                    return false;
                }
            });
        });
    };
    var _add_form = function () {
        var _header = j('head');
        var _style = '<style>';
        _style += '.btn-default{';
        _style += 'color: #fff;';
        _style += 'background-color: #272727;';
        _style += 'border-color: #272727;';
        _style += 'padding: 15px;';
        _style += 'border-radius: 30px;';
        _style += '}';

        _style += '.group-input{';
        _style += 'margin-bottom: 15px;';
        _style += '}';
        _style += 'input.form-control {';
        _style += 'display: block;';
        _style += 'width: 100%;';
        _style += 'border: 1px solid #d7d7d7;';
        _style += 'padding: 10px;';
        _style += '}';
        _style += '</style>';
        _header.append(_style);

        var _append = j('.append-form');
        var _html = "<form class='add-update-form' action=''>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='Contact_Person'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='Position_Title'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='Email_Address'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='Phone_Number'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='Company_Legal'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='Company_Full'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='Company_Website'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='What_does'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='What_industry'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='How_long_have'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='How_is_the_company'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='Do_you_currently'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='What_is_your_current'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='Do_we_need_to_bring'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='Do_you_need_to_track'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='What_other_Scrubbed'/></div>";
        _html += "<div class='group-input'><input type='text' class='form-control' name='How_did_you'/></div>";
        _html += "<div class='group-input'><button class='btn btn-default'>Submit</button></div>";
        _html += "<form>";
        _append.append(_html);
        add_update_client();
    };
    return {
        init: init
    };
})(jQuery);

(function () {
    EM.script.init();
})(jQuery);

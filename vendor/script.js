var EM = EM || {};
EM.script = (function (j) {
    var _url = j('[name="site"]').attr('content');
    var init = function () {
        add_update_client();
        side_bar();
    };
    var side_bar = function () {
        j("#menu-toggle").click(function (e) {
            e.preventDefault();
            console.log('asdasd');
            j("#wrapper").toggleClass("toggled");
        });
    };
    var add_update_client = function () {
        j('.add-update-form').each(function () {
            var _this = j(this);
            _this.validate({
                rules: {
                    Company: {required: true},
                    Type: {required: true},
                    Marketing_Status: {required: true},
                    Date_Signed: {required: true},
                    Team: {required: true},
                    First_Billing: {required: true},
                    Hourly: {required: true},
                    Monthly: {required: true},
                    Non_recurring: {required: true},
                    Industry: {required: true},
                    Date_From: {required: true},
                    Date_To: {required: true},
                    Status: {required: true}
                },
                showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.addClass('is-invalid');
                        _elm.popover('dispose').popover({content: value.message, trigger: 'hover', placement: 'top'});
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.removeClass('is-invalid');
                        _elm.popover('dispose');
                    });
                }, submitHandler: function (form) {
                    var _this = j(form);
                    j.ajax({
                        url: _this.attr("action"),
                        dataType: 'json',
                        method: 'post',
                        data: _this.serialize(),
                        success: function (e) {
                            if (e.message === 'success' && e.message === 'add') {
                                _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                    _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                                        var _this = j(this);
                                        _this.removeAttr('style');
                                    });
                                });
                            }
                        }
                    });
                    return false;
                }
            });
        });
    };
    return {
        init: init
    };
})(jQuery);

(function () {
    EM.script.init();
})(jQuery);
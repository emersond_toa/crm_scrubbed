var EM = EM || {};
EM.script = (function (j) {
    var _url = j('[name="site"]').attr('content');
    var init = function () {
        add_update_client();
        client_information();
        _client_update_information();
        // _upload_client_files();
        _show_former_name();
        _show_company_address_form();
        delete_client();
        client_check();
        // _show_added_type_of_services();
        // _show_type_of_services_form();
        // _upload_type_of_services_files();
        _select_status();
        dashboard();
        _add_cna_btn();
        _open_update_cna();
        _add_contact_person();
        // _show_reminders_form();
        // _show_added_reminders();
    };


    var get_leads_dashboard = function (year = '') {
        if (j("#status-reports").length) {
            j.ajax({
                url: _url + 'request/leads_number',
                dataType: 'json',
                method: 'post',
                data: {'year': year},
                success: function (e) {
                    if (typeof window.stats !== 'undefined') {
                        window.stats.destroy();
                    }
                    window.stats = new Chart(j("#status-reports"), {
                        type: 'horizontalBar',
                        data: e.data,
                        options: {
                            legend: {
                                display: false,
                                position: 'top'
                            },
                            title: {
                                display: true,
                                text: 'CLIENTS'
                            },
                            scales: {
                                yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                            }
                        }
                    });
                }
            });
    }
    };
    var get_industry_dashboard = function (year = '') {
        if (j("#industry-lines").length) {
            j.ajax({
                url: _url + 'request/industry_lines',
                dataType: 'json',
                method: 'post',
                data: {'year': year},
                success: function (e) {
                    if (typeof window.industry !== 'undefined') {
                        window.industry.destroy();
                    }
                    window.industry = new Chart.Bar(j("#industry-lines"), {
                        data: e,
                        options: {
                            elements: {
                                rectangle: {borderWidth: 2}
                            },
                            responsive: true,
                            legend: {
                                display: false,
                                position: 'top'
                            },
                            title: {
                                display: true,
                                text: 'INDUSTRY'
                            }
                        }
                    });
                }
            });
    }
    };

    var get_service_dashboard = function (year = '') {

        if (j("#service-lines").length) {
            j.ajax({
                url: _url + 'request/service_line',
                dataType: 'json',
                method: 'post',
                data: {'year': year},
                success: function (e) {
                    if (typeof window.service_line !== 'undefined') {
                        window.service_line.destroy();
                    }
                    window.service_line = new Chart.Bar(j("#service-lines"), {
                        data: e,
                        options: {
                            elements: {
                                rectangle: {borderWidth: 2}
                            },
                            responsive: true,
                            legend: {
                                display: false,
                                position: 'top'
                            },
                            title: {
                                display: true,
                                text: 'SERVICES'
                            }
                        }
                    });
                }
            });
    }

    };


    var dashboard = function () {
        get_leads_dashboard();
        get_industry_dashboard();
        get_service_dashboard();

        j('.get-year-leads').change(function () {
            var _this = j(this);
            get_leads_dashboard(_this.val());
        });
        j('.get-year-industry').change(function () {
            var _this = j(this);
            get_industry_dashboard(_this.val());
        });
        j('.get-year-service').change(function () {
            var _this = j(this);
            get_service_dashboard(_this.val());
        });


    };


    var delete_client = function () {
        j('.delete-client').each(function () {
            var _this = j(this);
            _this.click(function () {
                var _delete = j(this);
                j.cnfrm({
                    title: 'Confirmation',
                    text: 'Are you sure you want to delete this client?',
                    accept: function () {
                        j.ajax({
                            url: _url + 'request/delete_client',
                            dataType: 'json',
                            method: 'post',
                            data: {delete_client: _delete.attr('data-id')},
                            success: function (e) {
                                if (e.message === 'success') {
                                    _this.parents('.divTableRow').remove();
                                    _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                        _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                                            var _this = j(this);
                                            _this.removeAttr('style');
                                        });
                                    });
                                }
                            }
                        });
                    }
                });
            });
        });
    };
    var add_update_client = function () {
        j('.add-update-form').each(function () {
            var _this = j(this);
            _this.validate({
                rules: {
                    Company: {required: true},
                    Type: {required: true},
                    Marketing_Status: {required: true},
                    Date_Signed: {required: true},
                    Team: {required: true},
                    First_Billing: {required: true},
                    Hourly: {required: true},
                    Monthly: {required: true},
                    Date_From: {required: true},
                    Date_To: {required: true},
                    Status: {required: true}
                },
                showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.addClass('is-invalid');
                        _elm.popover('dispose').popover({content: value.message, trigger: 'hover', placement: 'top'});
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.removeClass('is-invalid');
                        _elm.popover('dispose');
                    });
                }, submitHandler: function (form) {
                    var _this = j(form);
                    j.ajax({
                        url: _this.attr("action"),
                        dataType: 'json',
                        method: 'post',
                        data: _this.serialize(),
                        success: function (e) {
                            if (e.message === 'success' && e.message === 'add') {
                                _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                    _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                                        var _this = j(this);
                                        _this.removeAttr('style');
                                    });
                                });
                            }
                        }
                    });
                    return false;
                }
            });
        });
    };
    var _former_name_upload_doc = function ($cid) {
        var _uploaded = j('.former-files-uploaded');
        j.ajax({
            url: _url + 'request/former_uploaded_files',
            dataType: 'json',
            method: 'post',
            data: {"upload": $cid},
            success: function (e) {
                var _html = '';
                for (var i = 0; i < e.length; i++) {
                    var _ext = '';
                    if (e[i].extension === 'pdf') {
                        _ext = '<i class="fas fa-file-pdf"></i>';
                    } else if (e[i].extension === 'doc' || e[i].extension === 'docx') {
                        _ext = '<i class="fas fa-file-word"></i>';
                    } else if (e[i].extension === 'xlsx' || e[i].extension === 'xls') {
                        _ext = '<i class="fas fa-file-excel"></i>';
                    } else if (e[i].extension === 'ptt' || e[i].extension === 'pttx') {
                        _ext = '<i class="fas fa-file-powerpoint"></i>';
                    } else {
                        _ext = '<i class="fas fa-file-image"></i>';
                    }

                    _html += '<li class="list-group-item">' + _ext + ' <a href="' + _url + 'uploads/' + e[i].file_name.replace($cid + '_', '') + '" download>' + e[i].file_name.replace($cid + '_', '') + '</a> <span class="date-uploaded">' + e[i].date_uploaded + '</span> <button class="btn btn-default delete-file-btn btn-rounded delete-former-name-doc" data-id="' + e[i].ID + '"><i class="fa fa-trash"></i></button></li>';
                }
                _uploaded.html(_html);
                delete_former_name_doc();
            }
        });
    };
    // var _client_files_uploaded = function ($cid) {
    //     var _uploaded = j('.client-files-uploaded');
    //     j.ajax({
    //         url: _url + 'request/uploaded_files',
    //         dataType: 'json',
    //         method: 'post',
    //         data: {"upload": $cid},
    //         success: function (e) {
    //             var _html = '';
    //             for (var i = 0; i < e.length; i++) {
    //                 var _ext = '';
    //                 if (e[i].extension === 'pdf') {
    //                     _ext = '<i class="fas fa-file-pdf"></i>';
    //                 } else if (e[i].extension === 'doc' || e[i].extension === 'docx') {
    //                     _ext = '<i class="fas fa-file-word"></i>';
    //                 } else if (e[i].extension === 'xlsx' || e[i].extension === 'xls') {
    //                     _ext = '<i class="fas fa-file-excel"></i>';
    //                 } else if (e[i].extension === 'ptt' || e[i].extension === 'pttx') {
    //                     _ext = '<i class="fas fa-file-powerpoint"></i>';
    //                 } else {
    //                     _ext = '<i class="fas fa-file-image"></i>';
    //                 }

    //                 _html += '<li class="list-group-item">' + _ext + ' <a href="' + _url + 'uploads/' + e[i].file_name.replace($cid + '_', '') + '" download>' + e[i].file_name.replace($cid + '_', '') + '</a> <span class="date-uploaded">' + e[i].date_uploaded + '</span> <button class="btn btn-default delete-file-btn btn-rounded delete-client-doc" data-id="' + e[i].ID + '"><i class="fa fa-trash"></i></button></li>';
    //             }
    //             _uploaded.html(_html);
    //             delete_client_doc();
    //         }
    //     });
    // };


    // var _crm_type_of_service_upload_doc = function ($cid) {
    //     var _uploaded = j('.service-files-uploaded');
    //     j.ajax({
    //         url: _url + 'request/type_of_services_uploaded_files',
    //         dataType: 'json',
    //         method: 'post',
    //         data: {"upload": $cid},
    //         success: function (e) {
    //             var _html = '';
    //             for (var i = 0; i < e.length; i++) {
    //                 var _ext = '';
    //                 if (e[i].extension === 'pdf') {
    //                     _ext = '<i class="fas fa-file-pdf"></i>';
    //                 } else if (e[i].extension === 'doc' || e[i].extension === 'docx') {
    //                     _ext = '<i class="fas fa-file-word"></i>';
    //                 } else if (e[i].extension === 'xlsx' || e[i].extension === 'xls') {
    //                     _ext = '<i class="fas fa-file-excel"></i>';
    //                 } else if (e[i].extension === 'ptt' || e[i].extension === 'pttx') {
    //                     _ext = '<i class="fas fa-file-powerpoint"></i>';
    //                 } else {
    //                     _ext = '<i class="fas fa-file-image"></i>';
    //                 }

    //                 _html += '<li class="list-group-item">' + _ext + ' <a href="' + _url + 'uploads/' + e[i].file_name.replace($cid + '_', '') + '" download>' + e[i].file_name.replace($cid + '_', '') + '</a> <span class="date-uploaded">' + e[i].date_uploaded + '</span> <button class="btn btn-default delete-file-btn btn-rounded delete-service-doc" data-id="' + e[i].ID + '"><i class="fa fa-trash"></i></button></li>';
    //             }
    //             _uploaded.html(_html);
    //             delete_service_doc();
    //         }
    //     });
    // };


    // var _upload_client_files = function () {
    //     var info = j('.information_upload_doc');
    //     info.submit(function () {
    //         var _this = j(this);
    //         _this.ajaxSubmit({
    //             url: _this.attr('action'),
    //             beforeSend: function () {
    //                 _this.find('.progress').removeClass('hide');
    //             },
    //             uploadProgress: function (event, position, total, percentComplete) {
    //                 _this.find('.progress').removeClass('hide');
    //                 _this.find('.progress-bar').css({width: percentComplete + '%'}).html(percentComplete + '%');
    //             },
    //             complete: function (e) {
    //                 var _e = (j.parseJSON(e.responseText));
    //                 if (_e.message === 'success') {
    //                     _former_name_upload_doc(_e.cid);
    //                     _client_files_uploaded(_e.cid);
    //                     delete_former_name_doc();
    //                 } else {

    //                 }

    //             }
    //         });
    //         return false;
    //     });
    //     info.find('.file-uploader').change(function () {
    //         var _this = j(this);
    //         _this.parents('.information_upload_doc').submit();
    //         _this.parents('.information_upload_doc').find('.progress-bar').css({width: 0});
    //     });
    //     j('.upload-files-drop').on('dragover dragenter', function (e) {
    //         var _this = j(this);
    //         _this.addClass('drop-files');
    //     }).on('dragleave dragend drop', function () {
    //         var _this = j(this);
    //         _this.removeClass('drop-files');
    //     });
    // };

    // var _upload_type_of_services_files = function () {
    //     var info = j('.type-of-service-upload-doc');
    //     info.submit(function () {
    //         var _this = j(this);
    //         _this.ajaxSubmit({
    //             url: _this.attr('action'),
    //             beforeSend: function () {
    //                 _this.find('.progress').removeClass('hide');
    //             },
    //             uploadProgress: function (event, position, total, percentComplete) {
    //                 _this.find('.progress').removeClass('hide');
    //                 _this.find('.progress-bar').css({width: percentComplete + '%'}).html(percentComplete + '%');
    //             },
    //             complete: function (e) {
    //                 var _e = (j.parseJSON(e.responseText));
    //                 if (_e.message === 'success') {
    //                     _crm_type_of_service_upload_doc(_e.cid);
    //                     delete_service_doc();
    //                 } else {

    //                 }

    //             }
    //         });
    //         return false;
    //     });
    //     info.find('.file-uploader').change(function () {
    //         var _this = j(this);
    //         _this.parents('.type-of-service-upload-doc').submit();
    //         _this.parents('.type-of-service-upload-doc').find('.progress-bar').css({width: 0});
    //     });
    //     j('.upload-files-drop').on('dragover dragenter', function (e) {
    //         var _this = j(this);
    //         _this.addClass('drop-files');
    //     }).on('dragleave dragend drop', function () {
    //         var _this = j(this);
    //         _this.removeClass('drop-files');
    //     });
    // };

    var _client_update_information = function () {
        j('.update-contact-person').validate({
            rules: {
                Contact_Person: {required: true},
                Position_Title: {required: true},
                Email_Address: {required: true},
                Phone_Number: {required: true},
                Company_Legal: {required: true},
                Company_Full: {required: true},
                Company_Website: {required: true},
                What_does: {required: true},
                What_industry: {required: true},
                How_long_have: {required: true},
                How_is_the_company: {required: true},
                Do_you_currently: {required: true},
                What_is_your_current: {required: true},
                Do_we_need_to_bring: {required: true}
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _this.attr("action"),
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        console.log(e);
                        if (e.message === "success" && e.type === "update") {
                            _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                                    var _this = j(this);
                                    _this.removeAttr('style');
                                });
                            });
                        }
                    }
                });
                return false;
            }
        });
    };

    var testjson = function () {
        Object.keys(e[0]).length;
    };

    var _show_former_name = function () {
        var _modal = j('.open-modal-information');
        j('.former-js-validate').each(function () {
            var _this = j(this);
            _this.validate({
                rules: {
                    CID: {required: true},
                    Date_Change: {required: true},
                    Former_Name: {required: true},
                    Reason: {required: true}
                },
                showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.addClass('is-invalid');
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.removeClass('is-invalid');
                    });
                }, submitHandler: function (form) {
                    var _this = j(form);

                    j.ajax({
                        url: _this.attr('action'),
                        dataType: 'json',
                        method: 'post',
                        data: _this.serialize(),
                        success: function (e) {
                            console.log(e.type);
                            if (e.type === 'add') {
                                var _html = '';
                                for (var i = 0; i < e.result.length; i++) {
                                    _html += '<form class="divTableRow former-js-validate add-new-form" method="post" action="' + _url + 'request/update_former_name_info">';
                                    _html += '<input type="hidden" name="ID" class="hide" value="' + e.result[i].ID + '"/>';
                                    _html += '<input type="hidden" name="CID" class="client_id"/>';
                                    _html += '<div class="divTableCell width_350"><input type="date" name="Date_Change" value="' + e.result[i].Date_Change + '" class="form-control"></div>';
                                    _html += '<div class="divTableCell width_350"><input type="text" name="Former_Name" value="' + e.result[i].Former_Name + '" class="form-control"></div>';
                                    _html += '<div class="divTableCell width_350"><input type="text" name="Reason" value="' + e.result[i].Reason + '" class="form-control"></div>';
                                    _html += '<div class="divTableCell text-center min_width_150">';
                                    _html += '<button class="btn btn-default btn-rounded" type="submit"><i class="fa fa-edit"></i></button>';
                                    _html += '<button class="btn btn-default btn-rounded delete-former-name" type="button" data-id="' + e.result[i].ID + '"><i class="fa fa-trash"></i></button>';
                                    _html += '</div>';
                                    _html += '</form>';
                                }
                                _modal.find('.added-former-info').html(_html);
                                _show_former_name();
                                _this[0].reset();
                            } else {
                                _this.find('.form-control').animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                    _this.find('.form-control').animate({"backgroundColor": "#ffffff"}, 500, function () {
                                        var _this = j(this);
                                        _this.removeAttr('style');
                                    });
                                });
                            }
                            delete_former_name();
                        }
                    });
                    return false;
                }
            });
        });

    };






    var _show_all_former_name = function ($cid) {
        var _modal = j('.open-modal-information');
        j.ajax({
            url: _url + "request/show_added_former_info",
            dataType: 'json',
            method: 'post',
            data: {'CID': $cid},
            success: function (e) {
                // console.log(e);
                var _html = '';
                for (var i = 0; i < e.length; i++) {
                    _html += '<form class="divTableRow former-js-validate" method="post" action="' + _url + 'request/update_former_name_info">';
                    _html += '<input type="hidden" name="ID" class="hide" value="' + e[i].ID + '"/>';
                    _html += '<input type="hidden" name="CID" class="client_id"/>';
                    _html += '<div class="divTableCell width_350"><input type="date" name="Date_Change" value="' + e[i].Date_Change + '" class="form-control"></div>';
                    _html += '<div class="divTableCell width_350"><input type="text" name="Former_Name" value="' + e[i].Former_Name + '" class="form-control"></div>';
                    _html += '<div class="divTableCell width_350"><input type="text" name="Reason" value="' + e[i].Reason + '" class="form-control"></div>';
                    _html += '<div class="divTableCell text-center min_width_150">';
                    _html += '<button class="btn btn-default btn-rounded" type="submit"><i class="fa fa-edit"></i></button>';
                    _html += '<button class="btn btn-default btn-rounded delete-former-name" type="button" data-id="' + e[i].ID + '"><i class="fa fa-trash"></i></button>';
                    _html += '</div>';
                    _html += '</form>';
                }
                _modal.find('.added-former-info').html(_html);

                _show_former_name();
                delete_former_name();
            }
        });
    };




    var _show_contact_person = function ($cid) {
        var _modal = j('.open-modal-information');
        _modal.find(".modal-body").addClass('loader-modal');
        _modal.find('.update-contact-person')[0].reset();
        j.ajax({
            url: _url + "request/show_client_info",
            dataType: 'json',
            method: 'post',
            data: {'cid': $cid},
            success: function (e) {
                var _jn = (Object.keys(e[0]));
                for (var i = 0; i < _jn.length; i++) {
                    console.log(e[0][Object.keys(e[0])[i]]);
                    /*console.log("<div class='col-sm-3 form-group'><label class="btn-block">" + Object.keys(e[0])[i] + "</label><input type="text" class="form-control" name='" + Object.keys(e[0])[i] + "' value=''></div>");*/
                    _modal.find('.update-contact-person [name="' + Object.keys(e[0])[i] + '"]').val(e[0][Object.keys(e[0])[i]]);
                    if (Object.keys(e[0])[i] === 'What_other_Scrubbed') {
                        /*
                         var _get = _modal.find('.update-contact-person [name="' + Object.keys(e[0])[i] + '"]').val().split(',');
                         for (var x = 0; x < _get.length; x++) {
                         
                         
                         _modal.find('.update-contact-person .service-checkbox [value="' + _get[x] + '"]').prop('checked', 'checked');
                         
                         
                         }
                         */
                    }
                    _modal.find(".modal-body").removeClass('loader-modal');
                }
            }
        });
    };




    var _show_added_company_address = function ($cid) {
        var _modal = j('.open-modal-information');
        j.ajax({
            url: _url + "request/show_added_compny_address",
            dataType: 'json',
            method: 'post',
            data: {'CID': $cid},
            success: function (e) {
                var _html = '';
                for (var i = 0; i < e.length; i++) {
                    _html += '<form class="divTableRow company-address" action="' + _url + 'request/update_compny_address" method="post">';
                    _html += '<input type="hidden" name="ID" class="hide" value="' + e[i].ID + '" />';
                    _html += '<input type="hidden" name="CID" class="client_id" value="' + e[i].CID + '" />';
                    _html += '<div class="divTableCell width_350"><input type="text" name="State" class="form-control" value="' + e[i].State + '"></div>';
                    _html += '<div class="divTableCell width_350"><input type="text" name="City" class="form-control" value="' + e[i].City + '"></div>';
                    _html += '<div class="divTableCell width_350"><input type="text" name="Zip_code" class="form-control" value="' + e[i].Zip_Code + '"></div>';
                    _html += '<div class="divTableCell text-center">';
                    _html += '<button class="btn btn-default btn-rounded" type="submit"><i class="fas fa-edit"></i></button>';
                    _html += '<button class="btn btn-default btn-rounded delete-company-address" type="button" data-id="' + e[i].ID + '"><i class="fas fa-trash"></i></button>';
                    _html += '</div>';
                    _html += '</form>';
                }
                _modal.find('.show-added-compny-address').html(_html);
                _show_company_address_form();
                delete_company_address();
            }
        });
    };




    var _show_company_address_form = function () {
        j('.company-address').each(function () {
            var _this = j(this);
            _this.validate({
                rules: {
                    CID: {required: true},
                    Date_Change: {required: true},
                    Former_Name: {required: true},
                    Reason: {required: true}
                },
                showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.addClass('is-invalid');
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.removeClass('is-invalid');
                    });
                }, submitHandler: function (form) {
                    var _this = j(form);
                    j.ajax({
                        url: _this.attr('action'),
                        dataType: 'json',
                        method: 'post',
                        data: _this.serialize(),
                        success: function (e) {
                            console.log(e.type);
                            if (e.type === 'add') {
                                _show_added_company_address(_this.find('.client_id').val());
                                _this[0].reset();
                            } else {
                                _this.find('.form-control').animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                    _this.find('.form-control').animate({"backgroundColor": "#ffffff"}, 500, function () {
                                        var _this = j(this);
                                        _this.removeAttr('style');
                                    });
                                });
                            }
                            delete_company_address();
                        }
                    });
                    return false;
                }
            });
        });
    };

    // var _show_added_type_of_services = function ($cid) {
    //     var _modal = j('.open-modal-information');
    //     j.ajax({
    //         url: _url + "request/show_added_type_of_services",
    //         dataType: 'json',
    //         method: 'post',
    //         data: {'CID': $cid},
    //         success: function (e) {
    //             var _html = '';
    //             for (var i = 0; i < e.length; i++) {
    //                 _html += '<form class="divTableRow type-of-services" action="' + _url + 'request/update_type_of_services" method="post">';
    //                 _html += '<input type="hidden" name="ID" class="hide" value="' + e[i].ID + '" />';
    //                 _html += '<input type="hidden" name="CID" class="client_id" value="' + e[i].CID + '" />';
    //                 _html += '<div class="divTableCell width_200"><input type="text" name="Group" class="form-control" value="' + e[i].group_text + '"></div>';
    //                 _html += '<div class="divTableCell width_200"><input type="text" name="Type_of_service" class="form-control" value="' + e[i].type_of_service + '"></div>';
    //                 _html += '<div class="divTableCell width_200">';
    //                 _html += '<select class="form-control" name="Agreement">';
    //                 _html += '<option value="">Select</option>';
    //                 if (e[i].agreement == 1) {
    //                     _html += '<option value="1" selected>Yes</option>';
    //                 } else {
    //                     _html += '<option value="1">Yes</option>';
    //                 }
    //                 if (e[i].agreement == 0) {
    //                     _html += '<option value="0" selected>No</option>';
    //                 } else {
    //                     _html += '<option value="0">No</option>';
    //                 }
    //                 _html += '</select>';
    //                 _html += '</div>';
    //                 _html += '<div class="divTableCell width_200"><input type="date" name="Date_from" class="form-control" value="' + e[i].date_from + '"></div>';
    //                 _html += '<div class="divTableCell width_200"><input type="date" name="Date_to" class="form-control" value="' + e[i].date_to + '"></div>';
    //                 _html += '<div class="divTableCell">';
    //                 _html += '<select class="form-control" name="Hourly_Monthly">';
    //                 _html += '<option value="">Select</option>';
    //                 if (e[i].hourly_monthly === 'hourly') {
    //                     _html += '<option value="hourly" selected>Hourly</option>';
    //                 } else {
    //                     _html += '<option value="hourly">Hourly</option>';
    //                 }
    //                 if (e[i].hourly_monthly === 'monthly') {
    //                     _html += '<option value="monthly" selected>Monthly</option>';
    //                 } else {
    //                     _html += '<option value="monthly">Monthly</option>';
    //                 }
    //                 _html += '</select>';
    //                 _html += '</div>';
    //                 _html += '<div class="divTableCell width_200"><input type="text" name="Amount" class="form-control" value="' + e[i].amount + '"></div>';
    //                 _html += '<div class="divTableCell text-center">';
    //                 _html += '<button class="btn btn-default btn-rounded" type="submit"><i class="fas fa-edit"></i></button>';
    //                 _html += '<button class="btn btn-default btn-rounded delete-service" type="button" data-id="' + e[i].ID + '"><i class="fas fa-trash"></i></button>';
    //                 _html += '</div>';
    //                 _html += '</form>';
    //             }
    //             _modal.find('.show-added-type-of-services').html(_html);
    //             _show_type_of_services_form();
    //             delete_service();
    //         }
    //     });
    // };

    // var _show_added_reminders = function ($cid) {
    //     var _content = j('#services-info');
    //     j.ajax({
    //         url: _url + "request/show_added_reminder",
    //         dataType: 'json',
    //         method: 'post',
    //         data: {'CID': $cid},
    //         success: function (e) {
    //             var _html = '';
    //             for (var i = 0; i < e.length; i++) {
    //                 _html += '<form class="divTableRow reminders" action="' + _url + 'request/update_reminder" method="post">';
    //                 _html += '<input type="hidden" name="ID" class="hide" value="' + e[i].ID + '" />';
    //                 _html += '<input type="hidden" name="CID" class="client_id" value="' + e[i].CID + '" />';
    //                 _html += '<div class="divTableCell width_200"><input type="datetime-local" name="start_date" class="form-control" value="' + e[i].start_date + '"></div>';
    //                 _html += '<div class="divTableCell width_200"><input type="datetime-local" name="end_date" class="form-control" value="' + e[i].end_date + '"></div>';
    //                 _html += '<div class="divTableCell width_200"><input type="text" name="notify" class="form-control" value="' + e[i].notify + '"></div>';
    //                 _html += '<div class="divTableCell width_200"><input type="text" name="what" class="form-control" value="' + e[i].reminder_what + '"></div>';
    //                 _html += '<div class="divTableCell width_200"><input type="text" name="where" class="form-control" value="' + e[i].reminder_where + '"></div>';
    //                 _html += '<div class="divTableCell width_200"><input type="text" name="description" class="form-control" value="' + e[i].description + '"></div>';
    //                 _html += '<div class="divTableCell text-center">';
    //                 _html += '<button class="btn btn-default btn-rounded" type="submit"><i class="fas fa-edit"></i></button>';
    //                 _html += '<button class="btn btn-default btn-rounded delete-reminder" type="button" data-id="' + e[i].ID + '"><i class="fas fa-trash"></i></button>';
    //                 _html += '</div>';
    //                 _html += '</form>';
    //             }
    //             _content.find('.show-added-reminders').html(_html);
    //             _show_reminders_form();
    //             delete_reminder();
    //         }
    //     });
    // };

    // var _show_type_of_services_form = function () {
    //     j('.type-of-services').each(function () {
    //         var _this = j(this);
    //         _this.validate({
    //             rules: {
    //                 CID: {required: true},
    //                 Group: {required: true},
    //                 Type_of_service: {required: true},
    //                 Agreement: {required: true},
    //                 Date_from: {required: true},
    //                 Date_to: {required: true},
    //                 Hourly_Monthly: {required: true},
    //                 Amount: {required: true}
    //             },
    //             showErrors: function (errorMap, errorList) {
    //                 j.each(this.errorList, function (index, value) {
    //                     var _elm = j(value.element);
    //                     _elm.addClass('is-invalid');
    //                 });
    //                 j.each(this.successList, function (index, value) {
    //                     var _elm = j(value);
    //                     _elm.removeClass('is-invalid');
    //                 });
    //             }, submitHandler: function (form) {
    //                 var _this = j(form);
    //                 j.ajax({
    //                     url: _this.attr('action'),
    //                     dataType: 'json',
    //                     method: 'post',
    //                     data: _this.serialize(),
    //                     success: function (e) {
    //                         console.log(e.type);
    //                         if (e.type === 'add') {
    //                             _show_added_type_of_services(_this.find('.client_id').val());
    //                             _this[0].reset();
    //                         } else {
    //                             _this.find('.form-control').animate({"backgroundColor": "#ffeb00"}, 500, function () {
    //                                 _this.find('.form-control').animate({"backgroundColor": "#ffffff"}, 500, function () {
    //                                     var _this = j(this);
    //                                     _this.removeAttr('style');
    //                                 });
    //                             });
    //                         }
    //                         delete_service();
    //                     }
    //                 });
    //                 return false;
    //             }
    //         });
    //     });
    // };

    // var _show_reminders_form = function () {
    //     j('.reminders').each(function () {
    //         var _this = j(this);
    //         _this.validate({
    //             rules: {
    //                 CID: {required: true},
    //                 start_date: {required: true},
    //                 end_date: {required: true},
    //                 notify: {required: true},
    //                 what: {required: true},
    //                 where: {required: true},
    //                 description: {required: true}
    //             },
    //             showErrors: function (errorMap, errorList) {
    //                 j.each(this.errorList, function (index, value) {
    //                     var _elm = j(value.element);
    //                     _elm.addClass('is-invalid');
    //                 });
    //                 j.each(this.successList, function (index, value) {
    //                     var _elm = j(value);
    //                     _elm.removeClass('is-invalid');
    //                 });
    //             }, submitHandler: function (form) {
    //                 var _this = j(form);
    //                 j.ajax({
    //                     url: _this.attr('action'),
    //                     dataType: 'json',
    //                     method: 'post',
    //                     data: _this.serialize(),
    //                     success: function (e) {
    //                         console.log(e.type);
    //                         if (e.type === 'add') {
    //                             _show_added_reminders(_this.find('.client_id').val());
    //                             _this[0].reset();
    //                         } else {
    //                             _this.find('.form-control').animate({"backgroundColor": "#ffeb00"}, 500, function () {
    //                                 _this.find('.form-control').animate({"backgroundColor": "#ffffff"}, 500, function () {
    //                                     var _this = j(this);
    //                                     _this.removeAttr('style');
    //                                 });
    //                             });
    //                         }
    //                         delete_reminder();
    //                     }
    //                 });
    //                 return false;
    //             }
    //         });
    //     });
    // };

    var _select_checkbox = function () {
        j('.select-current').each(function () {
            var _this = j(this);
            if (_this.val().length) {
                var strArray = _this.val().split(",");
                for (var i = 0; i < strArray.length; i++) {
                    console.log(strArray[i]);
                    _this.parent().find('[value="' + strArray[i] + '"]').attr('checked', 'checked');
                }
            }
        });
    };

    var _accounting_validator = function () {

        j('.update-tax').each(function () {
            var _this = j(this);
            _this.validate({
                rules: {
                    CAID: {required: true},
                    Update_What_does: {required: true},
                    How_long_have: {required: true},
                    Who_is_currently: {required: true},
                    What_type_of_annual: {required: true},
                    Tax_What_year_was: {required: true},
                    Are_you_currently: {required: true},
                    Are_you_required_sales: {required: true},
                    Are_you_required: {required: true}
                },
                showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.addClass('is-invalid');
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.removeClass('is-invalid');
                    });
                }, submitHandler: function (form) {
                    var _this = j(form);
                    j.ajax({
                        url: _this.attr('action'),
                        dataType: 'json',
                        method: 'post',
                        data: _this.serialize(),
                        success: function (e) {
                            console.log(e);
                            if (e.message === 'success') {
                                _this.parent().animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                    _this.parent().animate({"backgroundColor": "#ffffff"}, 500, function () {
                                        var _this = j(this);
                                        _this.parent().removeAttr('style');
                                    });
                                });
                            } else {

                            }
                        }
                    });
                    return false;
                }
            });
        });


        j('.update-accounting').each(function () {
            var _this = j(this);
            _this.validate({
                rules: {
                    CAID: {required: true},
                    Update_What_does: {required: true},
                    Update_What_industry: {required: true},
                    Update_How_long_have: {required: true},
                    Update_How_is_the_company: {required: true},
                    Update_Do_you_currently: {required: true},
                    Update_What_is_your_current: {required: true},
                    Update_Do_we_need_to_bring: {required: true},
                    Update_Do_you_need_to_track: {required: true},
                    Update_How_did_you: {required: true}
                },
                showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.addClass('is-invalid');
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.removeClass('is-invalid');
                    });
                }, submitHandler: function (form) {
                    var _this = j(form);
                    j.ajax({
                        url: _this.attr('action'),
                        dataType: 'json',
                        method: 'post',
                        data: _this.serialize(),
                        success: function (e) {
                            console.log(e);
                            if (e.message === 'success') {
                                _this.parent().animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                    _this.parent().animate({"backgroundColor": "#ffffff"}, 500, function () {
                                        var _this = j(this);
                                        _this.parent().removeAttr('style');
                                    });
                                });
                            } else {

                            }
                        }
                    });
                    return false;
                }
            });
        });
    };
    var _open_cna_form = function ($data, $ref) {
        var update_cna_form = j('.update-cna-form');
        var _html = '';
        update_cna_form.find('.update-cna-form-html').html(_html);
        j.ajax({
            url: _url + 'request/get_cna_data',
            dataType: 'json',
            method: 'post',
            data: {cid: $data, cna: $ref},
            success: function (e) {

                for (var i = 0; i < e.length; i++) {

                    _html += '<li class="list-group-item">';
                    _html += '<div class="row">';
                    _html += '<div class="col-6"><label>' + e[i].form_type + '</label></div>';
                    _html += '<div class="col-6 text-right">';
                    _html += '<button class="btn btn-default btn-rounded btn-sm open-update-cna"><i class="fa fa-edit"></i></button>';
                    _html += '<button class="btn btn-default btn-rounded btn-sm delete-added-cta" data-delete="' + (e[i].CAID) + '" data-type="' + (e[i].form_type) + '"><i class="fa fa-trash"></i></button>';
                    _html += '</div>';
                    _html += '</div>';

                    if (e[i].form_type === 'ACCOUNTING') {
                        _html += '<form class="row update-cna-person hide update-accounting" method="post" action="' + _url + 'request/update_accounting">';
                        _html += '<input type="hidden" class="form-control" name="REF" value="' + e[i].REF_ID + '">';
                        _html += '<input type="hidden" class="form-control accounting" name="CAID" value="' + e[i].CAID + '">';
                        _html += '<div class="col-md-3 form-group"><label class="btn-block">What does</label><input type="text" class="form-control" name="Update_What_does" value="' + e[i].What_does + '"></div>';
                        _html += '<div class="col-md-3 form-group"><label class="btn-block">What industry</label><input type="text" class="form-control" name="Update_What_industry" value="' + e[i].What_industry + '"></div>';
                        _html += '<div class="col-md-3 form-group"><label class="btn-block">How long_have</label><input type="text" class="form-control" name="Update_How_long_have" value="' + e[i].How_long_have + '"></div>';
                        _html += '<div class="col-md-3 form-group"><label class="btn-block">How is the company</label><input type="text" class="form-control" name="Update_How_is_the_company" value="' + e[i].How_is_the_company + '"></div>';
                        _html += '<div class="col-md-3 form-group"><label class="btn-block">Do you currently</label><input type="text" class="form-control" name="Update_Do_you_currently" value="' + e[i].Do_you_currently + '"></div>';
                        _html += '<div class="col-md-3 form-group"><label class="btn-block">What is your current</label><input type="text" class="form-control" name="Update_What_is_your_current" value="' + e[i].What_is_your_current + '"></div>';
                        _html += '<div class="col-md-3 form-group"><label class="btn-block">Do we need to bring</label><input type="text" class="form-control" name="Update_Do_we_need_to_bring" value="' + e[i].Do_we_need_to_bring + '"></div>';
                        _html += '<div class="col-md-3 form-group"><label class="btn-block">Do you need to track</label><input type="text" class="form-control" name="Update_Do_you_need_to_track" value="' + e[i].Do_you_need_to_track + '"></div>';
                        _html += '<div class="col-md-3 form-group"><label class="btn-block">How did you</label><input type="text" class="form-control" name="Update_How_did_you" value="' + e[i].How_did_you + '"></div>';
                        _html += '<div class="col-md-3 form-group"><label class="btn-block">Submitted date</label><input type="date" class="form-control" name="Update_submitted_date" value="' + e[i].submitted_date + '"></div>';
                        _html += '<div class="col-md-3 form-group service-checkbox">';
                        _html += '<label>What other Scrubbed services..</label>';

                        _html += '<div class="option-checkbox">';
                        _html += '<input type="hidden" class="form-control select-current" name="Update_What_other_Scrubbed" value="' + e[i].What_other_Scrubbed + '">';

                        _html += '<div class="option"><label><input type="checkbox" name="Update_What_other_Scrubbed1" id="Scrubbed1' + i + '" value="Income Tax Prep and Filing"> Income Tax Prep and Filing</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Update_What_other_Scrubbed2" id="Scrubbed2' + i + '" value="State and Local Taxes"> State and Local Taxes</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Update_What_other_Scrubbed3" id="Scrubbed3' + i + '" value="Tax Planning"> Tax Planning</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Update_What_other_Scrubbed4" id="Scrubbed4' + i + '" value="Financial Modeling"> Financial Modeling</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Update_What_other_Scrubbed5" id="Scrubbed5' + i + '" value="Investor Pitch Deck"> Investor Pitch Deck</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Update_What_other_Scrubbed6" id="Scrubbed6' + i + '" value="Financial Planning and Analysis"> Financial Planning and Analysis</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Update_What_other_Scrubbed7" id="Scrubbed7' + i + '" value="Transaction Advisory Services"> Transaction Advisory Services</label></div>';
                        _html += '</div>';

                        _html += '</div>';
                        _html += '<div class="col-md-3 form-group"><label class="btn-block">&nbsp;</label><button class="btn btn-default btn-block">Update</button></div>';
                        _html += '</form>';
                    }
                    if (e[i].form_type === 'TAX') {
                        _html += '<form class="row update-cna-person update-tax hide" method="post" action="' + _url + 'request/update_tax">';
                        _html += '<input type="hidden" class="form-control" name="REF" value="' + e[i].REF_ID + '">';
                        _html += '<input type="hidden" class="form-control tax" name="TID" value="' + e[i].TID + '"> ';
                        _html += '<div class="col-md-3 form-group"><label class="btn-block">What does</label><input type="text" class="form-control" name="Update_What_does" value="' + e[i].What_does + '"></div>';
                        _html += '<div class="col-md-3 form-group"><label class="btn-block">How long have you been in operation?</label>';
                        _html += '<select class="form-control" name="How_long_have">';
                        _html += '<option ' + (e[i].What_industry == 'Under 6 Months' ? 'selected="selected"' : '') + ' value="Under 6 Months">Under 6 Months</option>';
                        _html += '<option ' + (e[i].What_industry == '6 - 12 Months' ? 'selected="selected"' : '') + ' value="6 - 12 Months">6 - 12 Months</option>';
                        _html += '<option ' + (e[i].What_industry == '1 - 2 Years' ? 'selected="selected"' : '') + ' value="1 - 2 Years">1 - 2 Years</option>';
                        _html += '<option ' + (e[i].What_industry == '2 - 5 Years' ? 'selected="selected"' : '') + ' value="2 - 5 Years">2 - 5 Years</option>';
                        _html += '<option ' + (e[i].What_industry == 'Over 5 Years' ? 'selected="selected"' : '') + ' value="Over 5 Years">Over 5 Years</option>';
                        _html += '</select>';
                        _html += '</div>';

                        _html += '<div class="col-md-3 form-group"><label class="btn-block">Who is currently handling your</label>';
                        _html += '<select class="form-control" name="Who_is_currently">';
                        _html += '<option ' + (e[i].How_long_have == 'In-House' ? 'selected="selected"' : '') + ' value="In-House">In-House</option>';
                        _html += '<option ' + (e[i].How_long_have == 'Outside Service Provider' ? 'selected="selected"' : '') + ' value="Outside Service Provider">Outside Service Provider</option>';
                        _html += '<option ' + (e[i].How_long_have == 'None' ? 'selected="selected"' : '') + '  value="None">None</option>';
                        _html += '</select>';
                        _html += '</div>';

                        _html += '<div class="col-md-3 form-group"><label class="btn-block">What type of annual income</label>';
                        _html += '<select class="form-control" name="What_type_of_annual">';
                        _html += '<option  ' + (e[i].How_is_the_company == 'Business Federal and State Tax Return (1120 C Corp, 1120S,  1065)' ? 'selected="selected"' : '') + ' value="Business Federal and State Tax Return (1120 C Corp, 1120S,  1065)">Business Federal and State Tax Return (1120 C Corp, 1120S,  1065)</option>';
                        _html += '<option  ' + (e[i].How_is_the_company == 'Individual Federal and State Tax Return (1040' ? 'selected="selected"' : '') + ' value="Individual Federal and State Tax Return (1040)">Individual Federal and State Tax Return (1040)</option>';
                        _html += '<option  ' + (e[i].How_is_the_company == 'Exempt Organization Federal and State Tax  Return (990)' ? 'selected="selected"' : '') + ' value="Exempt Organization Federal and State Tax  Return (990)">Exempt Organization Federal and State Tax  Return (990)</option>';
                        _html += '<option  ' + (e[i].How_is_the_company == 'Not Sure' ? 'selected="selected"' : '') + ' value="Not Sure">Not Sure</option>';
                        _html += '</select>';
                        _html += '</div>';



                        _html += '<div class="col-md-3 form-group"><label class="btn-block">What year was your last</label>';
                        _html += '<select class="form-control" name="Tax_What_year_was">';
                        _html += '<option  ' + (e[i].Do_you_currently == '2019' ? 'selected="selected"' : '') + ' value="2019">2019</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2018' ? 'selected="selected"' : '') + ' value="2018">2018</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2017' ? 'selected="selected"' : '') + ' value="2017">2017</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2016' ? 'selected="selected"' : '') + ' value="2016">2016</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2015' ? 'selected="selected"' : '') + ' value="2015">2015</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2014' ? 'selected="selected"' : '') + ' value="2014">2014</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2013' ? 'selected="selected"' : '') + ' value="2013">2013</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2012' ? 'selected="selected"' : '') + ' value="2012">2012</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2011' ? 'selected="selected"' : '') + ' value="2011">2011</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2010' ? 'selected="selected"' : '') + ' value="2010">2010</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2009' ? 'selected="selected"' : '') + ' value="2009">2009</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2008' ? 'selected="selected"' : '') + ' value="2008">2008</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2007' ? 'selected="selected"' : '') + ' value="2007">2007</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2006' ? 'selected="selected"' : '') + ' value="2006">2006</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2005' ? 'selected="selected"' : '') + ' value="2005">2005</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2004' ? 'selected="selected"' : '') + ' value="2004">2004</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2003' ? 'selected="selected"' : '') + ' value="2003">2003</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2002' ? 'selected="selected"' : '') + ' value="2002">2002</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2001' ? 'selected="selected"' : '') + ' value="2001">2001</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == '2000' ? 'selected="selected"' : '') + ' value="2000">2000</option>';
                        _html += '<option  ' + (e[i].Do_you_currently == 'Prior to 2000' ? 'selected="selected"' : '') + ' value="Prior to 2000">Prior to 2000</option>';
                        _html += '</select>';
                        _html += '</div>';



                        _html += '<div class="col-md-3 form-group"><label class="btn-block">Do you have any delinquent</label>';
                        _html += '<input type="hidden" class="form-control select-current"  name="Update_What_is_your_current" value="' + e[i].What_is_your_current + '">';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_have_any1" value="None"> None</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_have_any2" value="Sales Tax"> Sales Tax</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_have_any3" value="Annual Income Tax"> Annual Income Tax</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_have_any4" value="Local Business & Property Tax"> Local Business & Property Tax</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_have_any5" value="Other"> Other</label></div>';
                        _html += '</div>';



                        _html += '<div class="col-md-3 form-group">';
                        _html += '<label>Are you currently under audit? <span class="required">*</span></label>';
                        _html += '<select class="form-control" name="Are_you_currently">';
                        _html += '<option ' + (e[i].Do_we_need_to_bring == 'Yes' ? 'selected="selected"' : '') + ' value="Yes">Yes</option>';
                        _html += '<option ' + (e[i].Do_we_need_to_bring == 'No' ? 'selected="selected"' : '') + ' value="No">No</option>';
                        _html += '<option ' + (e[i].Do_we_need_to_bring == 'Not Sure' ? 'selected="selected"' : '') + ' value="Not Sure">Not Sure</option>';
                        _html += '</select>';
                        _html += '</div>';


                        _html += '<div class="col-md-3 form-group">';
                        _html += '<label>Are you required to file</label>';
                        _html += '<select class="form-control" name="Are_you_required_sales">';
                        _html += '<option ' + (e[i].Do_you_need_to_track == 'No' ? 'selected="selected"' : '') + ' value="No">No</option>';
                        _html += '<option ' + (e[i].Do_you_need_to_track == 'Not Sure' ? 'selected="selected"' : '') + ' value="Not Sure">Not Sure</option>';
                        _html += '<option ' + (e[i].Do_you_need_to_track == 'Yes - I have less than 100 sales transactions per month' ? 'selected="selected"' : '') + ' value="Yes - I have less than 100 sales transactions per month">Yes - I have less than 100 sales transactions per month</option>';
                        _html += '<option ' + (e[i].Do_you_need_to_track == 'Yes - I have between 100 and 300 sales transactions per month' ? 'selected="selected"' : '') + ' value="Yes - I have between 100 and 300 sales transactions per month">Yes - I have between 100 and 300 sales transactions per month</option>';
                        _html += '<option ' + (e[i].Do_you_need_to_track == 'Yes - I have more than 300 sales transactions per month' ? 'selected="selected"' : '') + ' value="Yes - I have more than 300 sales transactions per month">Yes - I have more than 300 sales transactions per month</option>';
                        _html += '</select>';
                        _html += '</div>';


                        _html += '<div class="col-md-3 form-group">';
                        _html += '<label>Are you required to file local</label>';
                        _html += '<select class="form-control" name="Are_you_required">';
                        _html += '<option ' + (e[i].What_other_Scrubbed == 'No' ? 'selected="selected"' : '') + ' value="No">No</option>';
                        _html += '<option ' + (e[i].What_other_Scrubbed == 'Yes - I have 1 - 50 Business Properties' ? 'selected="selected"' : '') + ' value="Yes - I have 1 - 50 Business Properties">Yes - I have 1 - 50 Business Properties</option>';
                        _html += '<option ' + (e[i].What_other_Scrubbed == 'Yes - I have over 50 Business Properties' ? 'selected="selected"' : '') + ' value="Yes - I have over 50 Business Properties">Yes - I have over 50 Business Properties</option>';
                        _html += '</select>';
                        _html += '</div>';


                        _html += '<div class="col-md-3 form-group">';
                        _html += '<input type="hidden" class="form-control select-current"  value="' + e[i].How_did_you + '">';
                        _html += '<label>Do you need tax planning & advisory services?</label>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need1" value="None"> None</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need2" value="Other Federal Taxes (Excise, stamp, capital gains, etc)"> Other Federal Taxes (Excise, stamp, capital gains, etc)</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need3" value="Branch Income"> Branch Income</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need4" value="Group Taxation">  Group Taxation</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need5" value="Transfer Pricing"> Transfer Pricing</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need6" value="Thin Capitalization"> Thin Capitalization</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need7" value="Controlled Foreign Corporations"> Controlled Foreign Corporations</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need8" value="FATCA and FBAR"> FATCA and FBAR</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need9" value="Other"> Other</label></div>';
                        _html += '</div>';


                        _html += '<div class="col-md-3 form-group">';
                        _html += '<input type="hidden" class="form-control select-current"  value="' + e[i].tax_status + '">';
                        _html += '<label>Do you need tax analysis services?</label>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need_tax1" value="No"> No</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need_tax2" value="Net operating loss studies (tax asset quantification)"> Net operating loss studies (tax asset quantification)</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need_tax3" value="Ownership change analysis (Section 382 studies)"> Ownership change analysis (Section 382 studies)</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need_tax4" value="Cost Segregation studies (determine depreciability)">  Cost Segregation studies (determine depreciability)</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need_tax5" value="Earnings and Profits studies (distinguish non-taxable distributions from dividends)"> Earnings and Profits studies (distinguish non-taxable distributions from dividends)</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need_tax6" value="Nexus studies (determine state tax exposure)"> Nexus studies (determine state tax exposure)</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need_tax7" value="Basis studies (determine taxable gain or loss)"> Basis studies (determine taxable gain or loss)</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need_tax8" value="Built-in gain and loss studies (evaluate tax assets)"> Built-in gain and loss studies (evaluate tax assets)</label></div>';
                        _html += '<div class="option"><label><input type="checkbox" name="Do_you_need_tax9" value="Other"> Other</label></div>';
                        _html += '</div>';




                        _html += '<div class="col-md-3 form-group"><label class="btn-block">&nbsp;</label><button class="btn btn-default btn-block">Update</button></div>';
                        _html += '</form>';
                    }
                    _html += '</li>';

                }
                update_cna_form.find('.update-cna-form-html').html(_html);
                _accounting_validator();
                _open_update_cna();
                _select_checkbox();
                _delete_added_cta();
            }
        });
    };
    var client_information = function () {
        j('.open-modal-info').unbind().bind('click', function () {
            var _this = j(this);
            var _modal = j('.open-modal-information');
            _modal.modal({
                keyboard: false
            });
            _modal.find('.client_id').val(_this.attr('data-id'));
            _modal.find('.reference_id').val(_this.attr('data-ref'));
            _show_contact_person(_this.attr('data-id'));
            _client_files_uploaded(_this.attr('data-id'));
            _former_name_upload_doc(_this.attr('data-id'));
            _show_all_former_name(_this.attr('data-id'));
            _show_added_company_address(_this.attr('data-id'));
            // _show_added_type_of_services(_this.attr('data-id'));
            // _crm_type_of_service_upload_doc(_this.attr('data-id'));
            // _show_added_reminders(_this.attr('data-id'));


            _open_cna_form(_this.attr('data-id'), _this.attr('data-ref'));
        });
    };
    var client_check_ajax = function ($num, $id) {
        j.ajax({
            url: _url + 'request/check_client',
            dataType: 'json',
            method: 'post',
            data: {'ID': $id, 'check_data': $num},
            success: function (e) {

            }
        });
    };
    var client_check = function () {
        j('.potential-client').click(function () {
            var _this = j(this);
            if (_this.hasClass('btn-default')) {
                _this.addClass('btn-success');
                _this.removeClass('btn-default');
                client_check_ajax(1, _this.attr('data-id'));
            } else {
                _this.addClass('btn-default');
                _this.removeClass('btn-success');
                client_check_ajax(0, _this.attr('data-id'));
            }
        });
    };

    var delete_service = function () {
        j('.delete-service').each(function () {
            var _this = j(this);
            _this.click(function () {
                var _delete = j(this);
                j.cnfrm({
                    title: 'Confirmation',
                    text: 'Are you sure you want to delete this service?',
                    accept: function () {
                        j.ajax({
                            url: _url + 'request/delete_service',
                            dataType: 'json',
                            method: 'post',
                            data: {delete_service: _delete.attr('data-id')},
                            success: function (e) {
                                if (e.message === 'success') {
                                    _this.parents('.divTableRow').remove();
                                    _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                        _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                                            var _this = j(this);
                                            _this.removeAttr('style');
                                        });
                                    });
                                }
                            }
                        });
                    }
                });
            });
        });
    };

    // var delete_service_doc = function () {
    //     j('.delete-service-doc').each(function () {
    //         var _this = j(this);
    //         _this.click(function () {
    //             var _delete = j(this);
    //             j.cnfrm({
    //                 title: 'Confirmation',
    //                 text: 'Are you sure you want to delete this file?',
    //                 accept: function () {
    //                     j.ajax({
    //                         url: _url + 'request/delete_service_doc',
    //                         dataType: 'json',
    //                         method: 'post',
    //                         data: {delete_service_doc: _delete.attr('data-id')},
    //                         success: function (e) {
    //                             if (e.message === 'success') {
    //                                 _this.parents('.list-group-item').remove();
    //                                 _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
    //                                     _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
    //                                         var _this = j(this);
    //                                         _this.removeAttr('style');
    //                                     });
    //                                 });
    //                             }
    //                         }
    //                     });
    //                 }
    //             });
    //         });
    //     });
    // };

    var delete_company_address = function () {
        j('.delete-company-address').each(function () {
            var _this = j(this);
            _this.click(function () {
                var _delete = j(this);
                j.cnfrm({
                    title: 'Confirmation',
                    text: 'Are you sure you want to delete this address?',
                    accept: function () {
                        j.ajax({
                            url: _url + 'request/delete_company_address',
                            dataType: 'json',
                            method: 'post',
                            data: {delete_company_address: _delete.attr('data-id')},
                            success: function (e) {
                                if (e.message === 'success') {
                                    _this.parents('.divTableRow').remove();
                                    _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                        _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                                            var _this = j(this);
                                            _this.removeAttr('style');
                                        });
                                    });
                                }
                            }
                        });
                    }
                });
            });
        });
    };

    var delete_former_name = function () {
        j('.delete-former-name').each(function () {
            var _this = j(this);
            _this.click(function () {
                var _delete = j(this);
                j.cnfrm({
                    title: 'Confirmation',
                    text: 'Are you sure you want to delete this former name?',
                    accept: function () {
                        j.ajax({
                            url: _url + 'request/delete_former_name',
                            dataType: 'json',
                            method: 'post',
                            data: {delete_former_name: _delete.attr('data-id')},
                            success: function (e) {
                                if (e.message === 'success') {
                                    _this.parents('.divTableRow').remove();
                                    _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                        _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                                            var _this = j(this);
                                            _this.removeAttr('style');
                                        });
                                    });
                                }
                            }
                        });
                    }
                });
            });
        });
    };

    var delete_former_name_doc = function () {
        j('.delete-former-name-doc').each(function () {
            var _this = j(this);
            _this.click(function () {
                var _delete = j(this);
                j.cnfrm({
                    title: 'Confirmation',
                    text: 'Are you sure you want to delete this file?',
                    accept: function () {
                        j.ajax({
                            url: _url + 'request/delete_former_name_doc',
                            dataType: 'json',
                            method: 'post',
                            data: {
                                delete_former_name_doc: _delete.attr('data-id'),
                                file_name: _this.attr('data-bind')
                            },
                            success: function (e) {
                                if (e.message === 'success') {
                                    _this.parents('.list-group-item').remove();
                                    _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                        _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                                            var _this = j(this);
                                            _this.removeAttr('style');
                                        });
                                    });
                                }
                            }
                        });
                    }
                });
            });
        });
    };

    // var delete_client_doc = function () {
    //     j('.delete-client-doc').each(function () {
    //         var _this = j(this);
    //         _this.click(function () {
    //             var _delete = j(this);
    //             j.cnfrm({
    //                 title: 'Confirmation',
    //                 text: 'Are you sure you want to delete this file?',
    //                 accept: function () {
    //                     j.ajax({
    //                         url: _url + 'request/delete_client_doc',
    //                         dataType: 'json',
    //                         method: 'post',
    //                         data: {delete_client_doc: _delete.attr('data-id')},
    //                         success: function (e) {
    //                             if (e.message === 'success') {
    //                                 _this.parents('.list-group-item').remove();
    //                                 _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
    //                                     _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
    //                                         var _this = j(this);
    //                                         _this.removeAttr('style');
    //                                     });
    //                                 });
    //                             }
    //                         }
    //                     });
    //                 }
    //             });
    //         });
    //     });
    // };

    var delete_reminder = function () {
        j('.delete-reminder').each(function () {
            var _this = j(this);
            _this.click(function () {
                var _delete = j(this);
                j.cnfrm({
                    title: 'Confirmation',
                    text: 'Are you sure you want to delete this reminder?',
                    accept: function () {
                        j.ajax({
                            url: _url + 'request/delete_reminder',
                            dataType: 'json',
                            method: 'post',
                            data: {delete_reminder: _delete.attr('data-id')},
                            success: function (e) {
                                if (e.message === 'success') {
                                    _this.parents('.divTableRow').remove();
                                    _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                        _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                                            var _this = j(this);
                                            _this.removeAttr('style');
                                        });
                                    });
                                }
                            }
                        });
                    }
                });
            });
        });
    };

    var _select_status = function () {
        j('.select-status').change(function () {
            var _this = j(this);
            j.ajax({
                url: _url + 'request/leads_status',
                dataType: 'json',
                method: 'post',
                data: {
                    cid: _this.attr('data-bind'),
                    status: _this.val()
                },
                success: function (e) {
                    if (e.message === 'success') {
                        _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                            _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                                var _this = j(this);
                                _this.removeAttr('style');
                            });
                        });
                    }
                }
            });
        });
    };

    var _add_contact_person = function () {

        j('.add-tax-form').validate({
            rules: {
                What_does: {required: true},
                How_long_have: {required: true},
                Do_you_currently: {required: true},
                What_type_of_annual: {required: true},
                Tax_What_year_was: {required: true},
                Are_you_currently: {required: true},
                Are_you_required_sales: {required: true},
                Are_you_required: {required: true}
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _this.attr('action'),
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.type === 'add') {
                            _this[0].reset();
                            _open_cna_form(e.cid, e.ref);
                            _this.parents('.add-tax-tables').addClass('hide');
                        }
                    }
                });
                return false;
            }
        });




        j('.add-contact-person').validate({
            rules: {
                Add_What_does: {required: true},
                Add_What_industry: {required: true},
                Add_How_long_have: {required: true},
                Add_How_is_the_company: {required: true},
                Add_Do_you_currently: {required: true},
                Add_What_is_your_current: {required: true},
                Add_Do_we_need_to_bring: {required: true},
                Add_Do_you_need_to_track: {required: true},
                Add_How_did_you: {required: true}
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _this.attr('action'),
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.type === 'add') {
                            _this[0].reset();
                            _open_cna_form(e.cid, e.ref);
                            _this.parents('.add-tax-tables').addClass('hide');
                        }
                    }
                });
                return false;
            }
        });
    };

    var _open_update_cna = function () {
        var _open = j('.open-update-cna');
        var _cna_form = j('.update-cna-form');
        _open.unbind().bind('click', function () {
            var _this = j(this);
            if (_this.parents('.list-group-item').find('.update-cna-person').hasClass('hide')) {
                _cna_form.find('.update-cna-person').addClass('hide');
                _this.parents('.list-group-item').find('.update-cna-person').removeClass('hide');
                _this.parents('.list-group-item').animate({"backgroundColor": "#e7ebef"}, 500, function () {
                    var _this = j(this);
                    _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                        var _this = j(this);
                        _this.removeAttr('style');
                    });
                });
            } else {
                _this.parents('.list-group-item').find('.update-cna-person').addClass('hide');
            }
        });
    };

    var _add_cna_btn = function () {
        j('.add-cna-btn').click(function () {
            var _add = j('.add-cna-tables');
            if (!_add.is(':visible')) {
                _add.removeClass('hide');
            } else {
                _add.addClass('hide');
            }
        });

        j('.add-tax-btn').click(function () {
            var _add = j('.add-tax-tables');
            if (!_add.is(':visible')) {
                _add.removeClass('hide');
            } else {
                _add.addClass('hide');
            }
        });

    };

    var _delete_added_cta = function () {
        j('.delete-added-cta').unbind().bind('click', function (e) {
            e.preventDefault();
            var _this = j(this);
            j.cnfrm({
                title: 'Confirmation',
                text: 'Are you sure you want to delete this?',
                accept: function () {
                    j.ajax({
                        url: _url + 'request/delete_added_cta',
                        dataType: 'json',
                        method: 'post',
                        data: {
                            delete_client: _this.attr('data-delete'),
                            delete_type: _this.attr('data-type'),
                        },
                        success: function (e) {
                            if (e.message === 'success') {
                                _this.parents('.list-group-item').animate({"backgroundColor": "#f02849"}, 500, function () {
                                    _this.parents('.list-group-item').animate({"backgroundColor": "#ffffff"}, 500, function () {
                                        var _this = j(this);
                                        _this.remove();
                                    });
                                });
                            }
                        }
                    });
                }
            });
        });
    };
    return {
        init: init
    };
})(jQuery);

(function () {
    EM.script.init();
})(jQuery);
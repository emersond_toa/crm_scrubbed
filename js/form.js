var EM = EM || {};
EM.script = (function (j) {
    var _url = j('[name="site"]').attr('content');
    var init = function () {
        _client_website_form();
    };
    var _client_website_form_script = function () {
        j('.client-form').validate({
            rules: {
                Contact_Person: {required: true},
                Position_Title: {required: true},
                Email_Address: {required: true},
                Phone_Number: {required: true},
                Company_Legal: {required: true},
                Company_Full: {required: true},
                Company_Website: {required: true},
                What_does: {required: true},
                What_industry: {required: true},
                How_long_have: {required: true},
                How_is_the_company: {required: true},
                Do_you_currently: {required: true},
                What_is_your_current: {required: true},
                Do_we_need_to_bring: {required: true}
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _this.attr("action"),
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.message === 'success' && e.type === 'add') {
                            _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                                    var _this = j(this);
                                    _this.removeAttr('style');
                                });
                            });
                            _this[0].reset(); 
                            window.location = "https://scrubbed.net/thankyou";
                        }
                    }
                });
                return false;
            }
        });
    };
    var _client_tax_form_script = function () {
        j('.client-tax-form').validate({
            rules: {
                Contact_Person: {required: true},
                Position_Title: {required: true},
                Email_Address: {required: true},
                Phone_Number: {required: true},
                Company_Legal: {required: true},
                Company_Full: {required: true},
                Company_Website: {required: true},
                What_does: {required: true},
                
                How_long_have: {required: true},
                Who_is_currently: {required: true},
                What_type_of_annual: {required: true},
                Tax_What_year_was: {required: true},
                Are_you_required: {required: true},
                Are_you_required_sales: {required: true}
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _this.attr("action"),
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.message === 'success' && e.type === 'add') {
                            _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                                    var _this = j(this);
                                    _this.removeAttr('style');
                                });
                            });
                            _this[0].reset(); 
                            window.location = "https://scrubbed.net/thankyou";
                        }
                    }
                });
                return false;
            }
        });
    };
    var _client_website_form = function () {
        //jQuery('#block-a4e3eac0853f65aab296').load("https://employeeportal.scrubbed.net/crm/View_Form");
        j('.form-tax').load("hhttps://employeeportal.scrubbed.net/crm/View_Tax");
        j('.form-appended').load("https://employeeportal.scrubbed.net/crm/View_Form");
        setTimeout(function(){
            _client_tax_form_script();
            _client_website_form_script();
        },1000);
    };
    
    return {
        init: init
    };
})(jQuery);
(function () {
    EM.script.init();
})(jQuery);

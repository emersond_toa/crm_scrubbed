(function (j) {
    j('.client-form').validate({
        rules: {
            Company: {required: true},
            Contact_Person: {required: true},
            Email_Address: {required: true},
            Phone_Number: {required: true},
            note: {required: true}
        },
        showErrors: function (errorMap, errorList) {
            j.each(this.errorList, function (index, value) {
                var _elm = j(value.element);
                _elm.addClass('is-invalid');
            });
            j.each(this.successList, function (index, value) {
                var _elm = j(value);
                _elm.removeClass('is-invalid');
            });
        }, submitHandler: function (form) {
            var _this = j(form);
            j.ajax({
                url: _this.attr('action'),
                dataType: 'json',
                method: 'post',
                data: _this.serialize(),
                success: function (e) {
                    console.log(e);
                    if (e.message === 'success') {
                        _this.parent().animate({"backgroundColor": "#ffeb00"}, 500, function () {
                            _this.parent().animate({"backgroundColor": "#ffffff"}, 500, function () {
                                var _this = j(this);
                                _this.parent().removeAttr('style');
                            });
                        });
                    } else {

                    }
                }
            });
            return false;
        }
    });
})(jQuery);
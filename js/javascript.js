var JA = JA || {};
JA.javascript = (function (j) {
    var _url = j('[name="site"]').attr('content');

    var init = function () {
        funnel_datatables();
        clients_datatables();
        create_type_of_services();
        delete_type_of_services();
        edit_type_of_services();
        update_type_of_services();
        close_edit_type_of_services();
        open_add_type_of_services();
        close_add_type_of_services();
        _upload_type_of_services_files();
        delete_service_doc();
        _upload_client_files();

        open_add_reminder();
        create_reminder();
        close_add_reminder();
        edit_reminder();
        update_reminder();
        close_edit_reminder();
        delete_reminder();

        _modal_ckeditr();
        _client_experience_entry();

        open_add_contact();
        create_contact();
        close_add_contact();
        edit_contact();
        update_contact();
        close_edit_contact();
        delete_contact();

        referral_settings();
        create_referral();
        delete_referral();
        update_referral();

        source_settings();
        create_source();
        delete_source();
        update_source();

        terminationreason_settings();
        create_terminationreason();
        delete_terminationreason();
        update_terminationreason();

        former_clients_datatables();


        billing_select();
        manualRevenue();
        manualAR();

        open_add_entity();
        create_entity();
        close_add_entity();
        edit_entity();
        update_entity();
        close_edit_entity();
        delete_entity();
        toogle_add_revenue();
        toogle_add_ar();
        experience_calendar();
        _modal_attach_ckeditor();
        _add_attach_client_experience();

        _get_device_information();
        _upload_logo();


        logout();
        j('.chosen-select2').chosen({width: '100%'});

        add_transaction();
        edit_transaction();
        update_transaction();
        delete_transaction();

        load_group_revenue_ar();
        load_group_revenue_ar_filter_date();
        ar_date_filter();
        add_ar();
        edit_ar();
        update_ar();
        delete_ar();
    };
    var logout = function () {
        j('.logout-btn').click(function () {
            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this client?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'request/logout',
                                dataType: 'json',
                                method: 'post',
                                success: function (e) {
                                    window.location.reload();
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
            return false;
        });
    };
    var experience_calendar = function () {
        j('.experience-picker').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'MM/21/yy',
            onClose: function (dateText, inst) {
                j(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            }
        });
    };
    var leadstatus = [];
    var getleadstatus = function () {
        $('.leadstatus').each(function (i) {
            if ($(this).prop('checked')) {
                leadstatus[i] = $(this).val();
            }
        });
    };

    var leadform = [];
    var getleadform = function () {
        $('.leadform').each(function (i) {
            if ($(this).prop('checked')) {
                leadform[i] = $(this).val();
            }
        });
    };

    var funnel_datatables = function () {

        j(".chosen-select").chosen({width: '100%', allow_single_deselect: true});

        getleadstatus();
        getleadform();
        if (j('#cna-notes').length) {
            var cna_notes = CKEDITOR.inline('cna-notes', {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        }

        if (j('#cna-conversation').length) {
            var cna_conversation = CKEDITOR.inline('cna-conversation', {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        }


        j(".leadstatus").click(function () {
            leadstatus = [];
            getleadstatus();
            contactstable.ajax.reload();
        });

        j(".leadform").click(function () {
            leadform = [];
            getleadform();
            contactstable.ajax.reload();
        });

        var contactstable = j('#contacts-table').DataTable({
            "dom": '<"row "<"col-lg-6"><"col-lg-6 d-flex justify-content-end" f<"dt-dropdown" B<"dt-dropcontent" >>>>rt<"row mt-3"<"col-lg-6" l><"col-lg-6" p>><"clear">',
            buttons: [
                {
                    text: '<i class="fa fa-filter"></i> Filter',
                    className: 'ml-2 d-sm-inline-block btn btn-primary shadow-sm filter-btn',
                    action: function (e, dt, node, config) {
                        j(".dt-dropcontent").toggle("show");
                    }
                }
            ],
            "processing": true,
            "serverSide": true,
            "bInfo": false,
            "language": {
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "searchPlaceholder": "Search..."
            },
            "lengthMenu": [[10, 25, 50, 100], ["10 per page", "25 per page", "50 per page", "100 per page"]],
            "columnDefs": [
                {"orderable": false, "targets": 7},
                {"orderable": false, "targets": 4}
            ],
            "order": [[5, "desc"]],
            "fnDrawCallback": function (oSettings) {
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            },
            "ajax": {
                "url": _url + "funnel/get_allcompanies",
                "dataType": "json",
                "type": "POST",
                "data": function (d) {
                    d.leadstatus = leadstatus, d.leadform = leadform
                }
            },
            "columns": [
                {"data": "name"},
                {"data": "contact"},
                {"data": "leadform"},
                {"data": "industry"},
                {"data": "work_groups"},
                {"data": "lastupdate"},
                {"data": "cstatus"},
                {"data": "actions"}
            ]
        });

        $(document).on("click", ".mark-as-spam", function () {
            var _this = j(this);
            var id = _this.data('id');
            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this client?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'company/destroy',
                                method: 'post',
                                data: {
                                    id: id,
                                },
                                success: function (e) {
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'success',
                                        type: 'success',
                                    });

                                    contactstable.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });

        j('#filtercontent').appendTo(j('.dt-dropcontent'));

        j(".close-filter").click(function () {
            j(".dt-dropcontent").toggle("show");
        });

        j(document).on("click", ".open-funnel-viewmodal", function () {
            var _this = j(this);
            var cid = _this.data('id');
            var _modal = j('#viewModal');
            var companytext = _modal.find('.company');
            var contactfullnametext = _modal.find('.contactfullname');
            var jobtitletext = _modal.find('.jobtitle');
            var contactemailtext = _modal.find('.contactemail');
            var cardtext = _modal.find('.card-body');
            var profile_tab_content = j('.profile-tab-content');
            var c_logo = j('.c-logo');
            c_logo.addClass('blur-image');
            j('.c-logo').attr('src', _url + 'request/company_logo/' + cid);
            j.ajax({
                url: _url + 'request/contact_info',
                dataType: 'json',
                method: 'post',
                data: {id: cid},
                success: function (e) {
                    c_logo.removeClass('blur-image');
                    companytext.text(e[0].account);
                    contactfullnametext.text(e[0].contactperson);
                    jobtitletext.text(e[0].contactjobtitle);
                    contactemailtext.text(e[0].contactemail);
                    cna_notes.setData(e[0].content);

                    profile_tab_content.find('#companyname').val(e[0].account);
                    profile_tab_content.find('#companywebsite').val(e[0].website);
                    profile_tab_content.find('#companyindustry').val(e[0].industry);
                    profile_tab_content.find('#companyaddress').val(e[0].full_address);
                    profile_tab_content.find('#companyphone').val(e[0].phone);
                    profile_tab_content.find('#companyemail').val(e[0].email);

                    profile_tab_content.find('#contactname').text(e[0].fullname);
                    profile_tab_content.find('#contactjobtitle').text(e[0].jobtitle);
                    profile_tab_content.find('#contactemail').text(e[0].email);
                    profile_tab_content.find('#contactphone').text(e[0].phone);

                    profile_tab_content.find('#companyreferral').val(e[0].referral);
                    profile_tab_content.find('#companysource').val(e[0].source);

                    profile_tab_content.find('#companyterminationreason').val(e[0].terminationreason);
                    profile_tab_content.find('#clientworkgroups').val(e[0].work_groups);
                    if (e[0].start_date) {
                        profile_tab_content.find('#companystartdate').val(moment(e[0].start_date).format("MM/DD/YYYY"));
                    }
                    if (e[0].termination_date) {
                        profile_tab_content.find('#companyterminationdate').val(moment(e[0].termination_date).format("MM/DD/YYYY"));
                    }

                    if (e[0].status_id == 1) {
                        j('.status-content').html('<span class="badge badge-warning">New Lead</span>');
                    } else if (e[0].status_id == 2) {
                        j('.status-content').html('<span class="badge badge-light">Qualified Lead</span>');
                    } else if (e[0].status_id == 3) {
                        j('.status-content').html('<span class="badge badge-dark">Called</span>');
                    } else if (e[0].status_id == 4) {
                        j('.status-content').html('<span class="badge badge-secondary">Proposal</span>');
                    } else if (e[0].status_id == 5) {
                        j('.status-content').html('<span class="badge badge-info">Agreement</span>');
                    } else if (e[0].status_id == 6) {
                        j('.status-content').html('<span class="badge badge-success">Signed</span>');
                    } else if (e[0].status_id == 7) {
                        j('.status-content').html('<span class="badge badge-default">Declined</span>');
                    } else if (e[0].status_id == 8) {
                        j('.status-content').html('<span class="badge badge-danger">Unqualified Lead</span>');
                    }

                    j('.assignedto-content').text(e[0].assignedto);
                    j('#companyId').val(e[0].ID);
                    j('.company_id').val(e[0].ID);

                    _show_added_contact(e[0].ID);
                    _show_added_reminder(e[0].ID);
                    _show_added_conversation(e[0].ID);
                    _revenue_select();
                    _ar_select();
                    _show_added_entity(e[0].ID);
                    j('.open-funnel-editmodal').attr('data-id', e[0].ID);
                    j('.client_id').val(e[0].ID);
                    _get_client_entities(e[0].ID);
                }
            });
        });

        j(document).on("click", ".open-funnel-editmodal", function () {
            var _this = j(this);
            var cid = _this.data('id');
            var editmodal = j('#editModal');

            j.ajax({
                url: _url + 'company/show',
                dataType: 'json',
                method: 'post',
                data: {id: cid},
                success: function (e) {
                    editmodal.find('#editcompanyid').val(e.data[0].ID);
                    editmodal.find('#editname').val(e.data[0].account);
                    editmodal.find('#editwebsite').val(e.data[0].website);
                    editmodal.find('#editphone').val(e.data[0].phone);
                    editmodal.find('#editaddress').val(e.data[0].full_address);
                    editmodal.find('#editindustry').val(e.data[0].industry_id).trigger("chosen:updated");
                    editmodal.find('#editstatus').val(e.data[0].status_id).trigger("chosen:updated");
                    var test = [];
                    for (var x = 0; x < e.data[0].users.length; x++) {
                        test.push(e.data[0].users[x].ID);
                    }
                    editmodal.find('#editassignedto').val(test).trigger("chosen:updated");

                    var groups = [];
                    for (x = 0; x < e.data[0].work_groups.length; x++) {
                        groups.push(e.data[0].work_groups[x].GID)
                    }
                    editmodal.find('#editworkgroups').val(groups).trigger("chosen:updated");

                    editmodal.find('#editreferralpartner').val(e.data[0].referral_partner_id).trigger("chosen:updated");
                    editmodal.find('#editleadsource').val(e.data[0].lead_source_id).trigger("chosen:updated");
                }
            });
        });

        j('.edit-funnel-company').validate({
            rules: {
                editcompanyid: {required: true},
                editname: {required: true},
                editwebsite: {required: false},
                editphone: {required: false},
                editaddress: {required: false},
                editindustry: {required: false},
                editstatus: {required: false},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.update-funnel-company')
                submit_btn.attr('disabled', 'disabled');

                j.ajax({
                    url: _this.attr("action"),
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            contactstable.ajax.reload();
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });

        // reset modal to scratch
        $('#editModal').on('hidden.bs.modal', function (e) {
            var container = j('.company-container');
            container.find('.has-company').addClass('hide');
            container.find('.no-company').addClass('hide');
            j('#collapseAddNew').collapse('hide');
            j('#collapseEditContactCompany').collapse('hide');
        });

        j('.add_existing_company').validate({
            rules: {
                contact_id: {required: true},
                company_id: {required: true},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.save_existing_company')
                submit_btn.attr('disabled', 'disabled');

                j.ajax({
                    url: _this.attr("action"),
                    dataType: 'json',
                    method: 'post',
                    data: {
                        company_id: _this.find('#company_id').val(),
                        contact_id: j('#contact_id').val(),
                    },
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            j('.has-company .company_name').text(e.data[0].name);
                            j('.has-company .edit-contact-company').attr('data-companyid', e.data[0].ID);
                            j('.company-container').find('.no-company').addClass('hide');
                            j('.company-container').find('.has-company').removeClass('hide');

                            contactstable.ajax.reload();
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });

        j('.contact_add_new_company').validate({
            rules: {
                contact_id: {required: true},
                companyname: {required: true},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.save_contact_new_company')
                submit_btn.attr('disabled', 'disabled');

                j.ajax({
                    url: _this.attr("action"),
                    dataType: 'json',
                    method: 'post',
                    data: {
                        contact_id: j('#contact_id').val(),
                        companyname: _this.find('#companyname').val(),
                        website: _this.find('#website').val(),
                        companyphone: _this.find('#companyphone').val(),
                        industry: _this.find('#industry').val(),
                        address: _this.find('#address').val(),
                    },
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            j('.has-company .company_name').text(e.companyname);
                            j('.has-company .edit-contact-company').attr('data-companyid', e.companyid);
                            j('.company-container').find('.no-company').addClass('hide');
                            j('.company-container').find('.has-company').removeClass('hide');

                            _this.find('#companyname').val(''),
                                    _this.find('#website').val(''),
                                    _this.find('#companyphone').val(''),
                                    _this.find('#industry').val('').trigger('chosen:updated');
                            _this.find('#address').val(''),
                                    j('#company_id').val('').trigger('chosen:updated');

                            contactstable.ajax.reload();
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });

        j('.contact_edit_company').validate({
            rules: {
                companyname: {required: true},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.update_contact_company')
                submit_btn.attr('disabled', 'disabled');

                j.ajax({
                    url: _this.attr("action"),
                    dataType: 'json',
                    method: 'post',
                    data: {
                        company_id: j('.edit-contact-company').attr('data-companyid'),
                        companyname: _this.find('#editcompanyname').val(),
                        website: _this.find('#editwebsite').val(),
                        companyphone: _this.find('#editcompanyphone').val(),
                        industry: _this.find('#editindustry').val(),
                        address: _this.find('#editaddress').val(),
                    },
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            j('.has-company .company_name').text(e.companyname);

                            contactstable.ajax.reload();
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });

        j(document).on("click", ".edit-contact-company", function () {
            var _this = j(this);
            j.ajax({
                url: _this.data("action"),
                dataType: 'json',
                method: 'post',
                data: {
                    company_id: _this.attr('data-companyid'),
                },
                success: function (e) {
                    if (e.status === "ok") {
                        j('#editcompanyname').val(e.data[0].name);
                        j('#editwebsite').val(e.data[0].website);
                        j('#editcompanyphone').val(e.data[0].phone);
                        j('#editindustry').val(e.data[0].industry_id).trigger('chosen:updated');
                        j('#editaddress').val(e.data[0].full_address);
                    } else {
                        new PNotify({
                            title: 'Notification!',
                            text: 'error',
                            type: 'error'
                        });
                    }
                }
            });
            // todo 
            // if collapse was not shown 
            // if(!j('#collapseEditFunnelCompany').hasClass('collapse show')) {
            // }
        });

        j(document).on("click", ".remove-company", function () {
            j.confirm({
                title: "Confirmation",
                content: "Do you want to remove this company?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'contacts/remove_company',
                                method: 'post',
                                data: {
                                    contact_id: j('#contact_id').val(),
                                },
                                success: function (e) {
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'success',
                                        type: 'success'
                                    });

                                    j('.company-container').find('.no-company').removeClass('hide');
                                    j('.company-container').find('.has-company').addClass('hide');

                                    contactstable.ajax.reload();
                                    j('#collapseAddNew').collapse('hide');
                                    j('#collapseEditContactCompany').collapse('hide');
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });
    };



    var _show_group = function (cid) {
        var itemnameselect = j('.work-group');
        j.ajax({
            url: _url + 'company/show',
            dataType: 'json',
            method: 'post',
            data: {id: cid},
            success: function (e) {
                var x = (e.data[0].work_groups);
                var _option = '';
                for (var i = 0; i < x.length; i++) {
                    _option += '<option value="' + x[i].GID + '" >' + x[i].group_name + '</option>';
                }
                itemnameselect.html(_option);
                billing_select();
            }
        });
    };


    var randomInteger = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };



    var _delete_client_experience = function () {
        j('.delete-exp').unbind().bind('click', function () {
            var _this = j(this);
            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this item?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'request/delete_client_experience',
                                method: 'post',
                                data: {
                                    id: _this.attr('data-id')
                                },
                                success: function (e) {
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'success',
                                        type: 'success'
                                    });
                                    _this.parents('tr').animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                        _this.parents('tr').animate({"backgroundColor": "#ffffff"}, 500, function () {
                                            _this.parents('tr').removeAttr('style');
                                            _this.parents('tr').remove();
                                        });
                                    });
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });
    };
    var _click_review_button = function () {
        j('.reviewed-btn').unbind().bind('click', function () {
            var _this = j(this);
            _this.attr('disabled', 'disabled');
            j.ajax({
                url: _url + 'request/review_this_client_experience',
                dataType: 'json',
                method: 'post',
                data: {
                    id: _this.attr('data-id'),
                    related: _this.attr('data-related'),
                    review_undo: _this.attr('data-review'),
                    client_id: _this.attr('data-id')
                },
                success: function (e) {
                    var _rev = _this.parents('tr').find('.review-by');
                    _this.removeAttr('disabled');
                    if (Number(_this.attr('data-review')) === 1) {
                        _this.attr('data-review', 0);
                        _this.removeClass('btn-light').addClass('btn-success');
                        _rev.html(e.name);
                    } else {
                        _this.attr('data-review', 1);
                        _this.removeClass('btn-success').addClass('btn-light');
                        _rev.html('');
                    }
                }
            });
        });
    };


    var _update_client_experience = function () {
        var experience = j('.update-experience');
        experience.validate({
            rules: {
                Service_Continuum: {required: true},
                Calls_made: {required: true},
                QRC: {required: true},
                Note: {required: true},
                CI_log: {required: true},
                Errors: {required: true},
                Client_satifaction: {required: true},
                Deadline_missed: {required: true},
                dateexp: {required: true}
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.submit-button');
                submit_btn.attr('disabled', 'disabled');

                var logsnote = _this.find('.update-logs-note');
                var value = CKEDITOR.instances[logsnote.attr('id')].getData();
                logsnote.html(value);

                var errornotes = _this.find('.update-error-notes');
                var value = CKEDITOR.instances[errornotes.attr('id')].getData();
                errornotes.html(value);

                j.ajax({
                    url: _this.attr("action"),
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        submit_btn.removeAttr('disabled');
                        new PNotify({
                            title: 'Notification!',
                            text: 'Success',
                            type: 'success'
                        });
                        _this.find('.form-control,.cke_editable').animate({"backgroundColor": "#ffeb00"}, 500, function () {
                            _this.find('.form-control,.cke_editable').animate({"backgroundColor": "#ffffff"}, 500, function () {
                                _this.find('.form-control,.cke_editable').removeAttr('style');
                            });
                        });

                    }
                });
                return false;
            }
        });
    };

    var _add_attach_client_experience = function () {
        if (j('#logs-note').length) {
            var logs_note = CKEDITOR.inline('logs-note', {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        }
        if (j('#error-notes').length) {
            var error_notes = CKEDITOR.inline('error-notes', {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        }
    };
    var _add_client_experience = function () {
        var experience = j('.add-experience');


        var logsnote = experience.find('.logs-note');
        var errornotes = experience.find('.error-notes');
        errornotes.html('');
        logsnote.html('');

        experience[0].reset();
        experience.validate({
            rules: {
                Service_Continuum: {required: true},
                Calls_made: {required: true},
                QRC: {required: true},
                Note: {required: true},
                CI_log: {required: true},
                Errors: {required: true},
                Client_satifaction: {required: true},
                Deadline_missed: {required: true},
                dateexp: {required: true}
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.submit-button');
                submit_btn.attr('disabled', 'disabled');

                var logsnote = _this.find('.logs-note');
                var value = CKEDITOR.instances[logsnote.attr('id')].getData();
                logsnote.html(value);

                var errornotes = _this.find('.error-notes');
                var value = CKEDITOR.instances[errornotes.attr('id')].getData();
                errornotes.html(value);

                j.ajax({
                    url: _this.attr("action"),
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        var added_experience = j('.added-experience');

                        submit_btn.removeAttr('disabled');
                        new PNotify({
                            title: 'Notification!',
                            text: 'Success',
                            type: 'success'
                        });
                        _this.find('.form-control').animate({"backgroundColor": "#ffeb00"}, 500, function () {
                            _this.find('.form-control').animate({"backgroundColor": "#ffffff"}, 500, function () {
                                _this.find('.form-control').removeAttr('style');
                            });
                        });

                        experience[0].reset();
                        errornotes.html('');
                        logsnote.html('');
                        CKEDITOR.instances[logsnote.attr('id')].setData('');
                        CKEDITOR.instances[errornotes.attr('id')].setData('');

                        var _html = '';
                        for (var i = 0; i < e.length; i++) {

                            _html += '<tr>';
                            _html += '<td class="width_100 text-center">' + e[i].month + '-' + e[i].year + '</td>';
                            _html += '<td class="width_50 text-center">' + (e[i].service_continum ? e[i].service_continum : "") + '</td>';
                            _html += '<td class="width_50 text-center">' + (e[i].calls_made ? e[i].calls_made : "") + '</td>';
                            _html += '<td class="width_50 text-center">' + (e[i].qrc ? e[i].qrc : "") + '</td>';
                            _html += '<td class="">' + (e[i].ongoing_concern ? e[i].ongoing_concern : "") + '</td>';
                            _html += '<td class="width_50 text-center">' + (e[i].ci_log ? e[i].ci_log : "") + '</td>';
                            _html += '<td class="width_50 text-center">' + (e[i].no_of_errors ? e[i].no_of_errors : "") + '</td>';
                            _html += '<td class="width_50 text-center">' + (e[i].client_satifaction ? e[i].client_satifaction : "") + '</td>';
                            _html += '<td class="width_50 text-center">' + (e[i].deadline_missed ? e[i].deadline_missed : '') + '</td>';
                            _html += '<td class="width_100 text-center review-by">' + (e[i].first_name ? e[i].first_name : '') + ' ' + (e[i].last_name ? e[i].last_name : '') + '</td>';
                            _html += '<td class="text-center width_200">';
                            _html += '<button type="button" class="btn btn-light reviewed-btn action-button-sm" data-review="1" data-id="' + e[i].UID + '" data-related="' + (e[i].referenceid ? e[i].referenceid : e[i].UID) + '""><i class="fas fa-check"></i></button> ';
                            _html += '<button type="button" class="btn btn-light update-exp action-button-sm" data-id="' + e[i].UID + '" data-related="' + (e[i].referenceid ? e[i].referenceid : e[i].UID) + '"><i class="fas fa-edit"></i></button> ';
                            _html += '<button type="button" class="btn btn-light delete-exp action-button-sm" data-id="' + e[i].UID + '"><i class="fa fa-trash"></i></button>';
                            _html += '</td>';
                            _html += '</tr>';


                        }
                        added_experience.prepend(_html);
                        _modal_client_experience();
                        _click_review_button();
                        var add_new_data = j('.add-new-data');
                        var related_id_new = j('.related_id_new');
                        related_id_new.val('refid-' + randomInteger(11111111111111, 99999999999999));
                        add_new_data.find('td').animate({"backgroundColor": "#ffeb00"}, 500, function () {
                            add_new_data.find('td').animate({"backgroundColor": "#ffffff"}, 500, function () {
                                add_new_data.find('td').removeAttr('style').removeClass('add-new-data');
                            });
                        });
                    }
                });
                return false;
            }
        });
    };



    var _modal_attach_ckeditor = function () {
        if (j('#update-logs-note').length) {
            var update_logs_note = CKEDITOR.inline('update-logs-note', {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        }
        if (j('#update-error-notes').length) {
            var update_error_notes = CKEDITOR.inline('update-error-notes', {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        }
    };
    var _modal_client_experience = function () {
        j('.update-exp').unbind().bind('click', function (e) {
            e.preventDefault();
            var _modal = j('.modal-client-experience');
            var _full = j('.full-modal');
            var _this = j(this);
            _modal.find('.related_id_new').val(_this.attr('data-related'));
            _modal.find('.data_id').val(_this.attr('data-id'));
            _full.removeClass('show').addClass('hide');
            _modal.modal();




            j.ajax({
                url: _url + 'request/select_update_client_experience',
                dataType: 'json',
                method: 'post',
                data: {
                    'data_id': _this.attr('data-id'),
                    'related_id': _this.attr('data-related'),
                },
                success: function (e) {
                    console.log(e);
                    _update_client_experience();
                    _modal.find('[name="Service_Continuum"]').val(e[0].service_continum);
                    _modal.find('[name="Calls_made"]').val(e[0].calls_made);
                    _modal.find('[name="QRC"] option').removeAttr('selected');
                    _modal.find('[name="QRC"] option').each(function () {
                        var _option = j(this);
                        if (_option.val() === e[0].qrc) {
                            _option.attr('selected', 'selected');
                        }
                    });
                    var _logsnote = _modal.find('.update-logs-note');
                    var _errornotes = _modal.find('.update-error-notes');

                    CKEDITOR.instances[_logsnote.attr('id')].setData(e[0].ci_log_note);
                    CKEDITOR.instances[_errornotes.attr('id')].setData(e[0].no_errors_note);

                    _modal.find('[name="Note"]').val(e[0].ongoing_concern);
                    _modal.find('[name="CI_log"]').val(e[0].ci_log);
                    _modal.find('[name="Errors"]').val(e[0].no_of_errors);
                    _modal.find('[name="Client_satifaction"]').val(e[0].client_satifaction);
                    _modal.find('[name="Deadline_missed"] option').each(function () {
                        var _option = j(this);
                        if (_option.val() === e[0].deadline_missed) {
                            _option.attr('selected', 'selected');
                        }
                    });
                    _modal.find('[name="dateexp"]').val(e[0].year + '-' + e[0].month + '-21');
                }
            });
            _modal.on('hidden.bs.modal', function () {
                _full.removeClass('hide').addClass('show');
            });
        });
    };


    var _client_experience = function (cid, edate) {
        var experience = j('.added-experience');
        var _html = '';
        _html += '<tr class="opacity-loader">';
        _html += '<td  class="text-center" colspan="11">Loading....</td>';
        _html += '</tr>';
        experience.html(_html);
        j.ajax({
            url: _url + 'request/client_experience',
            dataType: 'json',
            method: 'post',
            data: {
                'clientid': cid,
                'date': edate
            },
            success: function (e) {
                _add_client_experience();
                var _html = '';
                for (var i = 0; i < e.length; i++) {
                    _html += '<tr>';
                    _html += '<td class="width_100 text-center">' + e[i].month + '-' + e[i].year + '</td>';
                    _html += '<td class="width_50 text-center">' + (e[i].service_continum ? e[i].service_continum : "") + '</td>';
                    _html += '<td class="width_50 text-center">' + (e[i].calls_made ? e[i].calls_made : "") + '</td>';
                    _html += '<td class="width_50 text-center">' + (e[i].qrc ? e[i].qrc : "") + '</td>';
                    _html += '<td class="">' + (e[i].ongoing_concern ? e[i].ongoing_concern : "") + '</td>';
                    _html += '<td class="width_50 text-center">' + (e[i].ci_log ? e[i].ci_log : "") + '</td>';
                    _html += '<td class="width_50 text-center">' + (e[i].no_of_errors ? e[i].no_of_errors : "") + '</td>';
                    _html += '<td class="width_50 text-center">' + (e[i].client_satifaction ? e[i].client_satifaction : "") + '</td>';
                    _html += '<td class="width_50 text-center">' + (e[i].deadline_missed ? e[i].deadline_missed : '') + '</td>';
                    _html += '<td class="width_100 text-center review-by">' + (e[i].first_name ? e[i].first_name : '') + ' ' + (e[i].last_name ? e[i].last_name : '') + '</td>';
                    _html += '<td class="text-center width_200">';
                    _html += '<button type="button" class="btn ' + (e[i].reviewed_by ? 'btn-success' : 'btn-light') + ' reviewed-btn action-button-sm" data-review="' + (e[i].reviewed_by ? 0 : 1) + '" data-id="' + e[i].UID + '" data-related="' + (e[i].referenceid ? e[i].referenceid : e[i].UID) + '""><i class="fas fa-check"></i></button> ';
                    _html += '<button type="button" class="btn btn-light update-exp action-button-sm" data-id="' + e[i].UID + '" data-related="' + (e[i].referenceid ? e[i].referenceid : e[i].UID) + '"><i class="fas fa-edit"></i></button> ';
                    _html += '<button type="button" class="btn btn-light delete-exp action-button-sm" data-id="' + e[i].UID + '"><i class="fa fa-trash"></i></button>';
                    _html += '</td>';
                    _html += '</tr>';

                }
                experience.html(_html);
                _modal_client_experience();
                _delete_client_experience();
                _click_review_button();
                if (!e.length) {
                    var _html = '';
                    _html += '<tr>';
                    _html += '<td  class="text-center" colspan="11">No Data</td>';
                    _html += '</tr>';
                    experience.html(_html);
                }
            }
        });

        j('.filter-button').unbind().bind('click', function () {
            var _this = j(this);
            var _id = j('.client_id');
            var _select_month = j('.select_month');
            var _select_year = j('.select_year');
            _client_experience(_id.val(), _select_month.val() + '/21/' + _select_year.val());
        });
    };


    var clients_datatables = function () {

        getleadstatus();
        getleadform();

        $(".leadstatus").click(function () {
            leadstatus = [];
            getleadstatus();
            clientstable.ajax.reload();
        });

        $(".leadform").click(function () {
            leadform = [];
            getleadform();
            clientstable.ajax.reload();
        });

        if (j('#cna-notes-client').length) {
            var cna_notes_client = CKEDITOR.inline('cna-notes-client', {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        }

        if (j('#cna-conversation-client').length) {
            var cna_conversation = CKEDITOR.inline('cna-conversation-client', {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        }


        var clientstable = j('#clients-table').DataTable({
            "dom": '<"row "<"col-lg-6"><"col-lg-6 d-flex justify-content-end" f<"dt-dropdown" B<"dt-dropcontent" >>>>rt<"row mt-3"<"col-lg-6" l><"col-lg-6" p>><"clear">',
            buttons: [
                {
                    text: '<i class="fa fa-filter"></i> Filter',
                    className: 'ml-2 d-sm-inline-block btn btn-primary shadow-sm filter-btn',
                    action: function (e, dt, node, config) {
                        j(".dt-dropcontent").toggle("show");
                    }
                }
            ],
            "processing": true,
            "serverSide": true,
            "bInfo": false,
            "language": {
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "searchPlaceholder": "Search..."
            },
            "lengthMenu": [[10, 25, 50, 100], ["10 per page", "25 per page", "50 per page", "100 per page"]],
            "columnDefs": [
                {"orderable": false, "targets": 8},
                {"orderable": false, "targets": 5},
                {"orderable": false, "targets": 4}
            ],
            "order": [[6, "desc"]],
            "fnDrawCallback": function (oSettings) {
                $(function () {

                    $('[data-toggle="tooltip"]').tooltip();
                });
            },
            "ajax": {
                "url": _url + "clients/get_allcompanies",
                "dataType": "json",
                "type": "POST",
                "dataType": "json",
                "type": "POST",
                "data": function (d) {
                    j('.clients-table_filter .form-control').removeClass('form-control-sm');
                    d.leadstatus = leadstatus, d.leadform = leadform;
                }
            },
            "columns": [
                {"data": "name"},
                {"data": "contact"},
                {"data": "leadform"},
                {"data": "industry"},
                {"data": "services"},
                {"data": "work_groups"},
                {"data": "lastupdate"},
                {"data": "clientstatus"},
                {"data": "actions"}
            ]
        });

        $(document).on("click", ".delete-client", function () {
            var _this = j(this);
            var id = _this.data('id');
            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this client?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'company/destroy',
                                method: 'post',
                                data: {
                                    id: id,
                                },
                                success: function (e) {
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'success',
                                        type: 'success'
                                    });

                                    clientstable.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });

        j('#filtercontent').appendTo(j('.dt-dropcontent'));

        $(".close-filter").click(function () {
            $('#collapseFilter').collapse('toggle');
        });

        // dito 
        $(document).on("click", ".open-client-viewmodal", function () {
            var _this = j(this);
            var cid = _this.data('id');
            var _modal = j('#viewModal');
            var companytext = _modal.find('.company');
            var contactfullnametext = _modal.find('.contactfullname');
            var jobtitletext = _modal.find('.jobtitle');
            var contactemailtext = _modal.find('.contactemail');
            var date_created = j('#viewModal').find('.date-created');
            var profile_tab_content = j('.profile-tab-content');
            var c_logo = j('.c-logo');
            c_logo.addClass('blur-image');
            j('.c-logo').attr('src', _url + 'request/company_logo/' + cid);
            j.ajax({
                url: _url + 'request/contact_info',
                dataType: 'json',
                method: 'post',
                data: {id: cid},
                success: function (e) {

                    c_logo.removeClass('blur-image');

                    companytext.text(e[0].account);
                    contactfullnametext.text(e[0].contactperson);
                    jobtitletext.text(e[0].contactjobtitle);
                    contactemailtext.text(e[0].contactemail);
                    cna_notes_client.setData(e[0].content);

                    profile_tab_content.find('#companyname').val(e[0].account);
                    profile_tab_content.find('#companywebsite').val(e[0].website);
                    profile_tab_content.find('#companyindustry').val(e[0].industry);
                    profile_tab_content.find('#companyaddress').val(e[0].full_address);
                    profile_tab_content.find('#companyphone').val(e[0].phone);
                    profile_tab_content.find('#companyemail').val(e[0].email);

                    profile_tab_content.find('#contactname').text(e[0].fullname);
                    profile_tab_content.find('#contactjobtitle').text(e[0].jobtitle);
                    profile_tab_content.find('#contactemail').text(e[0].email);
                    profile_tab_content.find('#contactphone').text(e[0].phone);

                    profile_tab_content.find('#companyreferral').val(e[0].referral);
                    profile_tab_content.find('#companysource').val(e[0].source);
                    profile_tab_content.find('#companyterminationreason').val(e[0].terminationreason);

                    profile_tab_content.find('#clientworkgroups').val(e[0].work_groups);
                    if (e[0].start_date) {
                        profile_tab_content.find('#companystartdate').val(moment(e[0].start_date).format("MM/DD/YYYY"));
                    }
                    if (e[0].termination_date) {
                        profile_tab_content.find('#companyterminationdate').val(moment(e[0].termination_date).format("MM/DD/YYYY"));
                    }

                    if (e[0].clientstatus_id == 1) {
                        j('.status-content').html('<span class="badge badge-success">Current</span>');
                    } else if (e[0].clientstatus_id == 2) {
                        j('.status-content').html('<span class="badge badge-primary">Past Due</span>');
                    } else if (e[0].clientstatus_id == 3) {
                        j('.status-content').html('<span class="badge badge-secondary">Offboarding</span>');
                    } else if (e[0].clientstatus_id == 4) {
                        j('.status-content').html('<span class="badge badge-info">Former Client</span>');
                    } else {
                        j('.status-content').html('<span class="badge badge-danger">-</span>');
                    }

                    j('.assignedto-content').text(e[0].assignedto);
                    j('#companyId').val(e[0].ID);
                    j('.client_id').val(e[0].ID);


                    _show_added_contact(e[0].ID);
                    _show_added_type_of_services(e[0].ID);
                    _crm_type_of_service_upload_doc(e[0].ID);
                    _client_files_uploaded(e[0].ID);
                    _show_added_reminder(e[0].ID);
                    _show_added_conversation(e[0].ID);
                    _show_added_entity(e[0].ID);
                    _show_group(e[0].ID);
                    // _revenue_select(e[0].ID);
                    // _ar_select(e[0].ID);
                    _client_experience(e[0].ID, '');
                    j('.billing_company').text(e[0].account);
                    j('.billing_company').val(e[0].account);
                    experience_calendar();
                    _show_transactions(e[0].ID);

                    j('.client_id').val(e[0].ID);
                    j('.open-client-editmodal').attr('data-id', e[0].ID);
                    _get_client_entities(e[0].ID);
                }
            });
        });

        j(document).on("click", ".open-client-editmodal", function () {
            var _this = j(this);
            var cid = _this.data('id');
            var editmodal = j('#editModal');

            j.ajax({
                url: _url + 'company/show',
                dataType: 'json',
                method: 'post',
                data: {id: cid},
                success: function (e) {
                    editmodal.find('#editcompanyid').val(e.data[0].ID);
                    editmodal.find('#editname').val(e.data[0].account);
                    editmodal.find('#editwebsite').val(e.data[0].website);
                    editmodal.find('#editphone').val(e.data[0].phone);
                    editmodal.find('#editaddress').val(e.data[0].full_address);
                    editmodal.find('#editindustry').val(e.data[0].industry_id);
                    editmodal.find('#editstatus').val(e.data[0].status_id);
                    editmodal.find('#editclientstatus').val(e.data[0].clientstatus_id);
                    var test = [];
                    for (x = 0; x < e.data[0].users.length; x++) {
                        test.push(e.data[0].users[x].ID)
                    }
                    editmodal.find('#editassignedto').val(test).trigger("chosen:updated");

                    var groups = [];
                    for (x = 0; x < e.data[0].work_groups.length; x++) {
                        groups.push(e.data[0].work_groups[x].GID)
                    }
                    editmodal.find('#editworkgroups').val(groups).trigger("chosen:updated");

                    editmodal.find('#editreferralpartner').val(e.data[0].referral_partner_id).trigger("chosen:updated");
                    editmodal.find('#editleadsource').val(e.data[0].lead_source_id).trigger("chosen:updated");
                    editmodal.find('#editstartdate').val(moment(e.data[0].start_date).format('YYYY-MM-DD'));
                    editmodal.find('#editterminationdate').val(moment(e.data[0].termination_date).format('YYYY-MM-DD'));
                    editmodal.find('#editterminationreason').val(e.data[0].termination_reason_id).trigger("chosen:updated");
                }
            });
        });

        j('.edit-client-company').validate({
            rules: {
                editcompanyid: {required: true},
                editname: {required: true},
                editwebsite: {required: false},
                editphone: {required: false},
                editaddress: {required: false},
                editindustry: {required: false},
                editstatus: {required: false},
                editclientstatus: {required: false},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.update-client-company')
                submit_btn.attr('disabled', 'disabled');

                j.ajax({
                    url: _this.attr("action"),
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            clientstable.ajax.reload();
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });

        // j('.edit-client-contact').validate({
        //     rules: {
        //         contact_id: {required: true},
        //         fullname: {required: true},
        //         jobtitle: {required: true},
        //         email: {required: true},
        //         phone: {required: true},
        //     },
        //     showErrors: function (errorMap, errorList) {
        //         j.each(this.errorList, function (index, value) {
        //             var _elm = j(value.element);
        //             _elm.addClass('is-invalid');
        //         });
        //         j.each(this.successList, function (index, value) {
        //             var _elm = j(value);
        //             _elm.removeClass('is-invalid');
        //         });
        //     }, submitHandler: function (form) {
        //         var _this = j(form);
        //         var submit_btn = _this.find('.update-client-contact')
        //         submit_btn.attr('disabled', 'disabled');

        //         j.ajax({
        //             url: _this.attr("action"),
        //             dataType: 'json',
        //             method: 'post',
        //             data: _this.serialize(),
        //             success: function (e) {
        //                 if (e.status === "ok") {
        //                     setTimeout(function () {
        //                         submit_btn.removeAttr('disabled');
        //                     }, 1000);

        //                     new PNotify({
        //                         title: 'Notification!',
        //                         text: 'success',
        //                         type: 'success'
        //                     });

        //                     clientstable.ajax.reload();
        //                 } else {
        //                     new PNotify({
        //                         title: 'Notification!',
        //                         text: 'error',
        //                         type: 'error'
        //                     });
        //                     setTimeout(function () {
        //                         submit_btn.removeAttr('disabled');
        //                     }, 1000);
        //                 }
        //             }
        //         });
        //         return false;
        //     }
        // });
    };

    var _client_files_uploaded = function ($cid) {
        var _uploaded = j('.client-files-uploaded');
        j.ajax({
            url: _url + 'request/uploaded_files',
            dataType: 'json',
            method: 'post',
            data: {"upload": $cid},
            success: function (e) {
                var _html = '';
                for (var i = 0; i < e.length; i++) {
                    var _ext = '';
                    if (e[i].extension === 'pdf') {
                        _ext = '<i class="fas fa-file-pdf"></i>';
                    } else if (e[i].extension === 'doc' || e[i].extension === 'docx') {
                        _ext = '<i class="fas fa-file-word"></i>';
                    } else if (e[i].extension === 'xlsx' || e[i].extension === 'xls') {
                        _ext = '<i class="fas fa-file-excel"></i>';
                    } else if (e[i].extension === 'ptt' || e[i].extension === 'pttx') {
                        _ext = '<i class="fas fa-file-powerpoint"></i>';
                    } else {
                        _ext = '<i class="fas fa-file-image"></i>';
                    }

                    _html += '<li class="list-group-item">' + _ext + ' <a href="' + _url + 'uploads/' + $cid + '_' + e[i].file_name.replace($cid + '_', '') + '" download data-bind="' + $cid + '_' + e[i].file_name.replace($cid + '_', '') + '">' + e[i].file_name.replace($cid + '_', '') + '</a> <span class="date-uploaded">' + e[i].date_uploaded + '</span> <button class="btn btn-light  action-button-sm  delete-file-btn btn-rounded delete-client-doc action-button-sm" data-bind="' + $cid + '_' + e[i].file_name.replace($cid + '_', '') + '" data-id="' + e[i].ID + '"><i class="fa fa-trash"></i></button></li>';
                }
                _uploaded.html(_html);
                delete_client_doc();
            }
        });
    };

    var delete_client_doc = function () {
        j(document).on("click", ".delete-client-doc", function () {
            var _this = j(this);
            var _delete = j(this);

            j.confirm({
                title: "Confirmation",
                content: "Are you sure you want to delete this file?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'request/delete_client_doc',
                                method: 'post',
                                data: {
                                    delete_client_doc: _delete.attr('data-id'),
                                    file_name: _this.attr('data-bind')
                                },
                                success: function (e) {
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'success',
                                        type: 'success'
                                    });

                                    _this.parents('.list-group-item').remove();
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });
    };

    var _list_group_note_hide = function () {
        var _open_note = j('.list-group-note');
        var _delete_note = j('.delete-conversation');
        var _update_convo = j('.update-convo');
        _open_note.find('.title').unbind().bind('click', function () {
            var _this = j(this);
            if (_this.parents('.list-group-item').find('.con-note').hasClass('hidden')) {
                _open_note.find('.con-note').addClass('hidden');
                _this.parents('.list-group-item').find('.con-note').removeClass('hidden');
            } else {
                _open_note.find('.con-note').addClass('hidden');
            }
        });
        _open_note.find('.textarea-content').each(function () {
            var _this = j(this);
            CKEDITOR.inline(_this.attr('id'), {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        });
        _update_convo.unbind().bind('submit', function () {
            var _this = j(this);
            var content = _this.find('.textarea-content');
            var value = CKEDITOR.instances[content.attr('id')].getData();
            content.html(value);
            j.ajax({
                url: _url + 'contacts/update_conversation',
                method: 'post',
                data: _this.serialize(),
                success: function (e) {
                    _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                        _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                            var _this = j(this);
                            _this.removeAttr('style');
                        });
                    });
                    new PNotify({
                        title: 'Notification!',
                        text: 'success',
                        type: 'success'
                    });
                }
            });
            return false;
        });
        _delete_note.unbind().bind('click', function () {
            var _this = j(this);
            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'contacts/delete_conversation',
                                method: 'post',
                                data: {id: _this.attr('data-bind')},
                                success: function (e) {
                                    _this.parents('.update-convo').animate({"backgroundColor": "#ffeb00"}, 500, function () {
                                        _this.parents('.update-convo').animate({"backgroundColor": "#ffffff"}, 500, function () {
                                            var _this = j(this);
                                            _this.remove();
                                        });
                                    });
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'Success deleted',
                                        type: 'success'
                                    });
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });



        j('.edit-cna').unbind().bind('submit', function () {
            var _this = j(this);
            var content = _this.find('.textarea-content');
            var value = CKEDITOR.instances[content.attr('id')].getData();
            content.html(value);
            j.ajax({
                url: _url + 'contacts/edit_cna',
                method: 'post',
                data: _this.serialize(),
                success: function (e) {
                    _this.animate({"backgroundColor": "#ffeb00"}, 500, function () {
                        _this.animate({"backgroundColor": "#ffffff"}, 500, function () {
                            var _this = j(this);
                            _this.removeAttr('style');
                        });
                    });
                    new PNotify({
                        title: 'Notification!',
                        text: 'Successfully Updated',
                        type: 'success'
                    });
                }
            });
            return false;
        });


    };

    var _show_add_conversation = function () {
        var _note_convo = j('.note-convo');
        var _conversation = j('#cna-conversation-client');
        _note_convo.unbind().bind('submit', function () {
            var _this = j(this);
            var value = CKEDITOR.instances['cna-conversation-client'].getData();
            _conversation.html(value);
            var _list_group_note = j('.list-group-note');
            var _html = '';
            j.ajax({
                url: _url + "contacts/add_conversation",
                dataType: 'json',
                method: 'post',
                data: _this.serialize(),
                success: function (e) {
                    _this[0].reset();
                    for (var i = 0; i < e.length; i++) {
                        _html += '<form action="' + _url + 'contacts/update_conversation" class="update-convo list-group-item list-group-item-action flex-column align-items-start new-conversation">';
                        _html += '<input type="hidden" name="id" value="' + e[i].id + '">';
                        _html += '<div class="d-flex w-100 justify-content-between">';
                        _html += '<h5 class="mb-1 title">' + (e[i].title ? e[i].title : 'No title') + '</h5>';

                        _html += '<small class="text-muted btn-group">';
                        _html += '<button data-bind="' + e[i].id + '" class="btn btn-sm btn-light  action-button-sm  delete-conversation conversation-css" type="button"><i class="fa fa-trash"></i></button>';
                        _html += '</small>';

                        _html += '</div>';

                        _html += '<div class="mb-1 con-note hidden"><textarea class="form-control textarea-content" name="notepad" id="textarea_' + e[i].id + '">' + e[i].content + '</textarea>';
                        _html += '<div class="text-right conver-submit">';
                        _html += '<button type="submit" class="btn btn-primary">Update</button>';
                        _html += '</div>';
                        _html += '</div>';

                        _html += '<small class="text-muted">' + e[i].update_at + ' by ' + e[i].userid + '</small>';
                        _html += '</div>';
                        _html += '</form>';
                    }
                    _list_group_note.find('.list-group').prepend(_html);
                    _conversation.html('');
                    CKEDITOR.instances['cna-conversation-client'].setData('');
                    _list_group_note.find('.new-conversation').animate({"backgroundColor": "#ffeb00"}, 500, function () {
                        _list_group_note.find('.new-conversation').animate({"backgroundColor": "#ffffff"}, 500, function () {
                            var _this = j(this);
                            _this.removeClass('new-conversation');
                        });
                    });
                    _list_group_note_hide();
                }
            });
            return false;
        });
    };

    var _show_added_conversation = function ($cid) {
        console.log($cid + " test id");
        _show_add_conversation();
        var _list_group_note = j('.list-group-note');
        var _html = '';
        _list_group_note.find('.list-group').html(_html);
        j.ajax({
            url: _url + "contacts/conversation",
            dataType: 'json',
            method: 'post',
            data: {'company_id': $cid},
            success: function (e) {
                for (var i = 0; i < e.length; i++) {
                    _html += '<form action="' + _url + 'contacts/update_conversation" class="update-convo list-group-item list-group-item-action flex-column align-items-start">';
                    _html += '<input type="hidden" name="id" value="' + e[i].id + '">';
                    _html += '<div class="d-flex w-100 justify-content-between">';
                    _html += '<h5 class="mb-1 title">' + (e[i].title ? e[i].title : 'No title') + '</h5>';

                    _html += '<small class="text-muted btn-group">';
                    _html += '<button data-bind="' + e[i].id + '" class="btn btn-sm btn-light action-button-sm  delete-conversation conversation-css" type="button"><i class="fa fa-trash"></i></button>';
                    _html += '</small>';

                    _html += '</div>';

                    _html += '<div class="mb-1 con-note hidden"><textarea class="form-control textarea-content" name="notepad" id="textarea_' + e[i].id + '">' + e[i].content + '</textarea>';
                    _html += '<div class="text-right conver-submit">';
                    _html += '<button type="submit" class="btn btn-primary">Update</button>';
                    _html += '</div>';
                    _html += '</div>';

                    _html += '<small class="text-muted">' + e[i].update_at + ' by ' + e[i].userid + '</small>';
                    _html += '</div>';
                    _html += '</form>';
                }
                _list_group_note.find('.list-group').html(_html);
                _list_group_note_hide();
            }
        });
    };

    var _show_added_type_of_services = function ($id) {
        var table_body = j('.type-of-services-table-body');
        j.ajax({
            url: _url + "Type_of_Service/client_services",
            dataType: 'json',
            method: 'post',
            data: {'id': $id},
            success: function (e) {
                var _html = '';
                if (e != '') {
                    for (var i = 0; i < e.length; i++) {
                        _html += '<tr>';
                        _html += '<td>' + e[i].entity + '</td>';
                        _html += '<td>' + e[i].service + '</td>';
                        _html += '<td>' + e[i].agreement + '</td>';
                        if (e[i].onetimefee == null) {
                            _html += '<td>-</td>';
                        } else {
                            _html += '<td>$' + e[i].onetimefee + '</td>';
                        }
                        if (e[i].recurringfee == null) {
                            _html += '<td>-</td>';
                        } else {
                            _html += '<td>$' + e[i].recurringfee + '</td>';
                        }
                        _html += '<td>' + e[i].hourly_monthly + '</td>';
                        _html += '<td>' + moment(e[i].start_date).format("MM/DD/YYYY") + '</td>';
                        _html += '<td>' + moment(e[i].end_date).format("MM/DD/YYYY") + '</td>';
                        _html += '<td class="text-center"><button class="btn btn-light action-button-sm mr-1 edit-type-of-services" data-id="' + e[i].id + '"><i class="fa fa-edit"></i></button><button class="btn btn-light action-button-sm delete-type-of-services" data-id="' + e[i].id + '"><i class="fa fa-trash"></i></button></td>';
                        _html += '</tr>';
                    }
                } else {
                    _html += '<tr>';
                    _html += '<td colspan="9" class="text-center">No result</td>';
                    _html += '<tr>';
                }
                table_body.html(_html);
                // _show_type_of_services_form();
                // delete_service();
            }
        });
    };

    var create_type_of_services = function () {
        j('.create-type-of-services').validate({
            rules: {
                group: {required: true},
                typeofservice: {required: true},
                agreement: {required: true},
                // hourlymonthly: {required: true},
                // startdate: {required: true},
                // enddate: {required: true},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.save-type-of-service');
                submit_btn.attr('disabled', 'disabled');

                var data = _this.serializeArray(); // convert form to array
                var companyId = j('#companyId').val();
                data.push({name: 'companyId', value: companyId});
                j.ajax({
                    url: _this.attr('action'),
                    dataType: 'json',
                    method: 'post',
                    data: data,
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            _this.find('#group').val('');
                            _this.find('#typeofservice').val('');
                            _this.find('#agreement').val('');
                            _this.find('#onetimefee').val('');
                            _this.find('#recurringfee').val('');
                            _this.find('#hourlymonthly').val('');
                            _this.find('#startdate').val('');
                            _this.find('#enddate').val('');
                            _this.find('#notes').val('');

                            _show_added_type_of_services(companyId);
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });
    };

    var delete_type_of_services = function () {
        j(document).on("click", ".delete-type-of-services", function () {
            var _this = j(this);
            var companyId = j('#companyId').val();
            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this service?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'Type_of_Service/destroy',
                                method: 'post',
                                data: {
                                    id: _this.attr('data-id'),
                                },
                                success: function (e) {
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'success',
                                        type: 'success'
                                    });

                                    _show_added_type_of_services(companyId);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });
    };

    var edit_type_of_services = function () {
        j(document).on("click", ".edit-type-of-services", function () {
            var _this = j(this);
            var id = _this.attr('data-id');
            var form = j('.update-type-of-services');

            $('#collapseEditServiceType').collapse('show');
            $('#collapseAddServiceType').collapse('hide');

            j.ajax({
                url: _url + 'Type_of_Service/show',
                dataType: 'json',
                method: 'post',
                data: {id: id},
                success: function (e) {
                    if (e.status === "ok") {
                        form.find('#editid').val(e.data[0].id);
                        form.find('#editgroup').val(e.data[0].group_text);
                        form.find('#edittypeofservice').val(e.data[0].service_id);
                        form.find('#editagreement').val(e.data[0].agreement_id);
                        form.find('#editonetimefee').val(e.data[0].onetimefee);
                        form.find('#editrecurringfee').val(e.data[0].recurringfee);
                        form.find('#edithourlymonthly').val(e.data[0].hourly_monthly);
                        form.find('#editservicestartdate').val(moment(e.data[0].start_date).format('YYYY-MM-DD'));
                        form.find('#editenddate').val(moment(e.data[0].end_date).format('YYYY-MM-DD'));
                        form.find('#editnotes').val(e.data[0].notes);
                        form.find('#edittosentity').val(e.data[0].company_id);
                    } else {
                        new PNotify({
                            title: 'Notification!',
                            text: 'error',
                            type: 'error'
                        });
                    }
                }
            });
        });
    };

    var update_type_of_services = function () {
        j('.update-type-of-services').validate({
            rules: {
                editgroup: {required: true},
                edittypeofservice: {required: true},
                editagreement: {required: true},
                edithourlymonthly: {required: true},
                editstartdate: {required: true},
                editenddate: {required: true},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.update-type-of-service')
                submit_btn.attr('disabled', 'disabled');

                var data = _this.serializeArray(); // convert form to array
                var companyId = j('#companyId').val();
                j.ajax({
                    url: _this.attr('action'),
                    dataType: 'json',
                    method: 'post',
                    data: data,
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            _show_added_type_of_services(companyId);
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });
    };

    var close_edit_type_of_services = function () {
        j(document).on("click", ".close-edit-type-of-service", function () {
            $('#collapseEditServiceType').collapse('hide');
        });
    };

    var open_add_type_of_services = function () {
        j(document).on("click", ".open-add-type-of-service", function () {
            $('#collapseAddServiceType').collapse('show');
            $('#collapseEditServiceType').collapse('hide');
        });
    };

    var close_add_type_of_services = function () {
        j(document).on("click", ".close-add-type-of-service", function () {
            $('#collapseAddServiceType').collapse('hide');
        });
    };

    var _upload_type_of_services_files = function () {
        var info = j('.type-of-service-upload-doc');
        info.submit(function () {
            var _this = j(this);
            _this.ajaxSubmit({
                url: _this.attr('action'),
                beforeSend: function () {
                    _this.find('.progress').removeClass('hide');
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    _this.find('.progress').removeClass('hide');
                    _this.find('.progress-bar').css({width: percentComplete + '%'}).html(percentComplete + '%');
                },
                complete: function (e) {
                    var _e = (j.parseJSON(e.responseText));
                    if (_e.message === 'success') {
                        _crm_type_of_service_upload_doc(_e.cid);
                    } else {

                    }

                }
            });
            return false;
        });
        info.find('.file-uploader').change(function () {
            var _this = j(this);
            _this.parents('.type-of-service-upload-doc').submit();
            _this.parents('.type-of-service-upload-doc').find('.progress-bar').css({width: 0});
        });
        j('.upload-files-drop').on('dragover dragenter', function (e) {
            var _this = j(this);
            _this.addClass('drop-files');
        }).on('dragleave dragend drop', function () {
            var _this = j(this);
            _this.removeClass('drop-files');
        });
    };

    var _crm_type_of_service_upload_doc = function ($cid) {
        var _uploaded = j('.service-files-uploaded');
        j.ajax({
            url: _url + 'request/type_of_services_uploaded_files',
            dataType: 'json',
            method: 'post',
            data: {"upload": $cid},
            success: function (e) {
                var _html = '';
                for (var i = 0; i < e.length; i++) {
                    var _ext = '';
                    if (e[i].extension === 'pdf') {
                        _ext = '<i class="fas fa-file-pdf"></i>';
                    } else if (e[i].extension === 'doc' || e[i].extension === 'docx') {
                        _ext = '<i class="fas fa-file-word"></i>';
                    } else if (e[i].extension === 'xlsx' || e[i].extension === 'xls') {
                        _ext = '<i class="fas fa-file-excel"></i>';
                    } else if (e[i].extension === 'ptt' || e[i].extension === 'pttx') {
                        _ext = '<i class="fas fa-file-powerpoint"></i>';
                    } else {
                        _ext = '<i class="fas fa-file-image"></i>';
                    }

                    _html += '<li class="list-group-item">' + _ext + ' <a href="' + _url + 'uploads/' + $cid + '_' + e[i].file_name.replace($cid + '_', '') + '" download>' + e[i].file_name.replace($cid + '_', '') + '</a> <span class="date-uploaded">' + e[i].date_uploaded + '</span> <button class="btn btn-light action-button-sm  delete-file-btn btn-rounded delete-service-doc" data-bind="' + $cid + '_' + e[i].file_name.replace($cid + '_', '') + '" data-id="' + e[i].ID + '"><i class="fa fa-trash"></i></button></li>';
                }
                _uploaded.html(_html);
            }
        });
    };

    var delete_service_doc = function () {
        j(document).on("click", ".delete-service-doc", function () {
            var _this = j(this);
            var _delete = j(this);

            j.confirm({
                title: "Confirmation",
                content: "Are you sure you want to delete this file?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'request/delete_service_doc',
                                method: 'post',
                                data: {
                                    delete_service_doc: _delete.attr('data-id'),
                                    file_name: _this.attr('data-bind')
                                },
                                success: function (e) {
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'success',
                                        type: 'success'
                                    });

                                    _this.parents('.list-group-item').remove();
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });
    };

    var _upload_client_files = function () {
        var info = j('.information_upload_doc');
        info.submit(function () {
            var _this = j(this);
            _this.ajaxSubmit({
                url: _this.attr('action'),
                beforeSend: function () {
                    _this.find('.progress').removeClass('hide');
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    _this.find('.progress').removeClass('hide');
                    _this.find('.progress-bar').css({width: percentComplete + '%'}).html(percentComplete + '%');
                },
                complete: function (e) {
                    var _e = (j.parseJSON(e.responseText));
                    if (_e.message === 'success') {
                        _client_files_uploaded(_e.cid);
                    } else {

                    }

                }
            });
            return false;
        });
        info.find('.file-uploader').change(function () {
            var _this = j(this);
            _this.parents('.information_upload_doc').submit();
            _this.parents('.information_upload_doc').find('.progress-bar').css({width: 0});
        });
        j('.upload-files-drop').on('dragover dragenter', function (e) {
            var _this = j(this);
            _this.addClass('drop-files');
        }).on('dragleave dragend drop', function () {
            var _this = j(this);
            _this.removeClass('drop-files');
        });
    };

    var open_add_reminder = function () {
        j(document).on("click", ".open-add-reminder", function () {
            $('#collapseAddReminder').collapse('show');
            $('#collapseEditReminder').collapse('hide');
        });
    };

    var create_reminder = function () {
        j('.create-reminder').validate({
            rules: {
                start_date: {required: true},
                notify: {required: true},
                what: {required: true},
                where: {required: true},
                description: {required: true},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.save-reminder')
                submit_btn.attr('disabled', 'disabled');

                var data = _this.serializeArray(); // convert form to array
                var companyId = j('#companyId').val();
                data.push({name: 'companyId', value: companyId});
                j.ajax({
                    url: _this.attr('action'),
                    dataType: 'json',
                    method: 'post',
                    data: data,
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            _this.find('#start_date').val('');
                            _this.find('#notify').val('').trigger('chosen:updated');
                            _this.find('#what').val('');
                            _this.find('#where').val('');
                            _this.find('#description').val('');

                            _show_added_reminder(companyId);
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });
    };

    var _show_added_reminder = function ($id) {
        var table_body = j('.reminder-table-body');
        j.ajax({
            url: _url + "reminder/client_reminders",
            dataType: 'json',
            method: 'post',
            data: {'id': $id},
            success: function (e) {
                var _html = '';
                if (e != '') {
                    for (var i = 0; i < e.length; i++) {
                        _html += '<tr>';
                        _html += '<td>' + moment(e[i].start_date).format("MM/DD/YYYY h:mm a") + '</td>';
                        _html += '<td>';
                        for (var x = 0; x < e[i].users.length; x++) {
                            _html += '<span>' + e[i].users[x].first_name + ' ' + e[i].users[x].last_name + '</span>';
                            if (x + 1 != e[i].users.length) {
                                _html += ', ';
                            }
                        }
                        _html += '</td>';
                        _html += '<td>' + e[i].reminder_what + '</td>';
                        _html += '<td>' + e[i].reminder_where + '</td>';
                        _html += '<td>' + e[i].description + '</td>';
                        // _html += '<td>' + moment(e[i].start_date).format("MMMM D, YYYY") + '</td>';
                        // _html += '<td>' + moment(e[i].end_date).format("MMMM D, YYYY") + '</td>';
                        _html += '<td class="text-center width_200"><button class="btn btn-light action-button-sm mr-1 edit-reminder" data-id="' + e[i].id + '"><i class="fa fa-edit"></i></button><button class="btn btn-light action-button-sm  delete-reminder" data-id="' + e[i].id + '"><i class="fa fa-trash"></i></button></td>';
                        _html += '</tr>';
                    }
                } else {
                    _html += '<tr>';
                    _html += '<td colspan="9" class="text-center">No result</td>';
                    _html += '<tr>';
                }
                table_body.html(_html);
                // _show_type_of_services_form();
                // delete_service();
            }
        });
    };

    var close_add_reminder = function () {
        j(document).on("click", ".close-add-reminder", function () {
            $('#collapseAddReminder').collapse('hide');
        });
    };

    var edit_reminder = function () {
        j(document).on("click", ".edit-reminder", function () {
            var _this = j(this);
            var id = _this.attr('data-id');
            var form = j('.update-reminder');

            $('#collapseEditReminder').collapse('show');
            $('#collapseAddReminder').collapse('hide');

            j.ajax({
                url: _url + 'reminder/show',
                dataType: 'json',
                method: 'post',
                data: {id: id},
                success: function (e) {
                    if (e.status === "ok") {
                        form.find('#editreminderid').val(e.data[0].id);
                        form.find('#editstart_date').val(moment(e.data[0].start_date).format('YYYY-MM-DDTHH:mm'));
                        var test = [];
                        for (x = 0; x < e.data[0].users.length; x++) {
                            test.push(e.data[0].users[x].ID)
                        }
                        form.find('#editnotify').val(test).trigger('chosen:updated');
                        form.find('#editwhat').val(e.data[0].reminder_what);
                        form.find('#editwhere').val(e.data[0].reminder_where);
                        form.find('#editdescription').val(e.data[0].description);
                    } else {
                        new PNotify({
                            title: 'Notification!',
                            text: 'error',
                            type: 'error'
                        });
                    }
                }
            });
        });
    };

    var update_reminder = function () {
        j('.update-reminder').validate({
            rules: {
                editstart_date: {required: true},
                editnotify: {required: true},
                editwhat: {required: true},
                editwhere: {required: true},
                editdescription: {required: true},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.update-reminder')
                submit_btn.attr('disabled', 'disabled');

                var data = _this.serializeArray(); // convert form to array
                var companyId = j('#companyId').val();

                j.ajax({
                    url: _this.attr('action'),
                    dataType: 'json',
                    method: 'post',
                    data: data,
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            _show_added_reminder(companyId);
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });
    };

    var close_edit_reminder = function () {
        j(document).on("click", ".close-edit-reminder", function () {
            $('#collapseEditReminder').collapse('hide');
        });
    };

    var delete_reminder = function () {
        j(document).on("click", ".delete-reminder", function () {
            var _this = j(this);
            var companyId = j('#companyId').val();

            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this reminder?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'reminder/destroy',
                                method: 'post',
                                data: {
                                    id: _this.attr('data-id'),
                                },
                                success: function (e) {
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'success',
                                        type: 'success'
                                    });

                                    _show_added_reminder(companyId);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });
    };

    var open_add_contact = function () {
        j(document).on("click", ".open-add-contact", function () {
            $('#collapseAddContact').collapse('show');
            $('#collapseEditContact').collapse('hide');
        });
    };

    var create_contact = function () {
        j('.create-contact').validate({
            rules: {
                contactaddfullname: {required: true},
                contactaddjobtitle: {required: true},
                contactaddemail: {required: true},
                contactaddphone: {required: true},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.save-contact')
                submit_btn.attr('disabled', 'disabled');

                var data = _this.serializeArray(); // convert form to array
                var companyId = j('#companyId').val();
                data.push({name: 'companyId', value: companyId});
                j.ajax({
                    url: _this.attr('action'),
                    dataType: 'json',
                    method: 'post',
                    data: data,
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            _this.find('#contactaddfullname').val('');
                            _this.find('#contactaddjobtitle').val('');
                            _this.find('#contactaddemail').val('');
                            _this.find('#contactaddphone').val('');

                            _show_added_contact(companyId);
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });
    };

    var _show_added_contact = function ($id) {
        var table_body = j('.contact-table-body');
        j.ajax({
            url: _url + "company/contacts",
            dataType: 'json',
            method: 'post',
            data: {'id': $id},
            success: function (e) {
                var _html = '';
                if (e != '') {
                    for (var i = 0; i < e.length; i++) {
                        _html += '<tr>';
                        _html += '<td>' + e[i].entity + '</td>';
                        _html += '<td>' + e[i].fullname + '</td>';
                        _html += '<td>' + e[i].jobtitle + '</td>';
                        _html += '<td>' + e[i].email + '</td>';
                        _html += '<td>' + e[i].phone + '</td>';
                        if (e[i].linkedin == null) {
                            _html += '<td>-</td>';
                        } else {
                            _html += '<td><a href="' + e[i].linkedin + '" target="_blank">' + e[i].linkedin + '</a></td>';
                        }
                        if (e[i].skype == null) {
                            _html += '<td>-</td>';
                        } else {
                            _html += '<td><a href="skype:' + e[i].skype + '?chat">' + e[i].skype + '</a></td>';
                        }
                        if (e[i].is_active == 1) {
                            _html += '<td><span class="badge badge-success">Active</span></td>';
                        } else {
                            _html += '<td><span class="badge badge-secondary">Inactive</span></td>';
                        }
                        _html += '<td class="text-center"><button class="btn btn-light btn-sm mr-1 edit-contact action-button-sm" data-id="' + e[i].id + '"><i class="fa fa-edit"></i></button><button class="btn btn-light btn-sm delete-contact action-button-sm" data-id="' + e[i].id + '"><i class="fa fa-trash"></i></button></td>';
                        _html += '</tr>';
                    }
                } else {
                    _html += '<tr>';
                    _html += '<td  class="text-center" colspan="9">No Data</td>';
                    _html += '<tr>';
                }
                table_body.html(_html);
                // _show_type_of_services_form();
                // delete_service();
            }
        });
    };

    var close_add_contact = function () {
        j(document).on("click", ".close-add-contact", function () {
            $('#collapseAddContact').collapse('hide');
        });
    };

    var edit_contact = function () {
        j(document).on("click", ".edit-contact", function () {
            var _this = j(this);
            var id = _this.attr('data-id');
            var form = j('.update-contact');

            $('#collapseEditContact').collapse('show');
            $('#collapseAddContact').collapse('hide');

            j.ajax({
                url: _url + 'contacts/show',
                dataType: 'json',
                method: 'post',
                data: {id: id},
                success: function (e) {
                    if (e.status === "ok") {
                        form.find('#contacteditid').val(e.data[0].id);
                        form.find('#contacteditfullname').val(e.data[0].fullname);
                        form.find('#contacteditjobtitle').val(e.data[0].jobtitle);
                        form.find('#contacteditemail').val(e.data[0].email);
                        form.find('#contacteditphone').val(e.data[0].phone);
                        form.find('#contacteditstatus').val(e.data[0].is_active);
                        form.find('#contacteditlinkedin').val(e.data[0].linkedin);
                        form.find('#contacteditskype').val(e.data[0].skype);
                        form.find('#contacteditentity').val(e.data[0].company_id);

                    } else {
                        new PNotify({
                            title: 'Notification!',
                            text: 'error',
                            type: 'error'
                        });
                    }
                }
            });
        });
    };

    var update_contact = function () {
        j('.update-contact').validate({
            rules: {
                contacteditid: {required: true},
                contacteditfullname: {required: true},
                contacteditjobtitle: {required: true},
                contacteditemail: {required: true},
                contacteditphone: {required: true},
                contacteditstatus: {required: true},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.update-contact')
                submit_btn.attr('disabled', 'disabled');

                var data = _this.serializeArray(); // convert form to array
                var companyId = j('#companyId').val();

                j.ajax({
                    url: _this.attr('action'),
                    dataType: 'json',
                    method: 'post',
                    data: data,
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            _show_added_contact(companyId);
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });
    };

    var close_edit_contact = function () {
        j(document).on("click", ".close-edit-contact", function () {
            $('#collapseEditContact').collapse('hide');
        });
    };

    var delete_contact = function () {
        j(document).on("click", ".delete-contact", function () {
            var _this = j(this);
            var companyId = j('#companyId').val();

            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this contact?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'contacts/destroy',
                                method: 'post',
                                data: {
                                    contact_id: _this.attr('data-id'),
                                },
                                success: function (e) {
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'success',
                                        type: 'success'
                                    });

                                    _show_added_contact(companyId);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });
    };

    var referral_settings = function () {
        var table_body = j('#referral-body');
        if (table_body.length > 0) {
            j.ajax({
                url: _url + 'settings/get_referrals',
                dataType: 'json',
                method: 'post',
                success: function (e) {
                    var _html = '';
                    if (e != '') {
                        for (var i = 0; i < e.length; i++) {
                            _html += '<tr class="form-edit-referral' + e[i].id + '">';
                            _html += '<td><input type="text" name="referral' + e[i].id + '" id="referral' + e[i].id + '" class="form-control" value="' + e[i].name + '"></td>';
                            _html += '<td><button class="btn btn-light btn-sm mr-1 update-referral" data-id="' + e[i].id + '"><i class="fa fa-edit"></i></button><button class="btn btn-light btn-sm delete-referral" data-id="' + e[i].id + '"><i class="fa fa-trash"></i></button></td>';
                            _html += '</tr>';
                        }
                    } else {
                        _html += '<tr>';
                        _html += '<td><i>No result</i></td><td></td>';
                        _html += '<tr>';
                    }
                    table_body.html(_html);
                }
            });
        }
    };

    var create_referral = function () {
        j(document).on("click", "#add-referral", function () {
            var _this = j(this);
            var referral = j('#referral').val();
            j.ajax({
                url: _url + 'settings/create_referral',
                dataType: 'json',
                method: 'post',
                data: {
                    name: referral
                },
                success: function (e) {
                    if (e.message == 'success') {
                        referral_settings();
                        j('#referral').val('');
                    }
                    new PNotify({
                        title: e.text + '!',
                        // text: e.message,
                        type: e.message
                    });
                }
            });
        });
    };

    var delete_referral = function () {
        j(document).on("click", ".delete-referral", function () {
            var _this = j(this);
            var id = _this.data('id');
            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this referral?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'settings/delete_referral',
                                method: 'post',
                                data: {
                                    id: id,
                                },
                                success: function (e) {
                                    referral_settings();
                                    new PNotify({
                                        title: 'Successfully deleted!',
                                        text: 'success',
                                        type: 'success'
                                    });
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });
    };

    var update_referral = function () {
        j(document).on("click", ".update-referral", function () {
            var _this = j(this);
            var id = _this.data('id');
            var name = j('#referral' + id).val();
            j.ajax({
                url: _url + 'settings/update_referral',
                dataType: 'json',
                method: 'post',
                data: {
                    id: id,
                    name: name
                },
                success: function (e) {
                    if (e.message == 'success') {
                        referral_settings();
                        j('#referral').val('');
                    }
                    new PNotify({
                        title: e.text + '!',
                        // text: e.message,
                        type: e.message
                    });
                }
            });
        });
    };

    var source_settings = function () {
        var table_body = j('#source-body');
        if (table_body.length > 0) {
            j.ajax({
                url: _url + 'settings/get_sources',
                dataType: 'json',
                method: 'post',
                success: function (e) {
                    var _html = '';
                    if (e != '') {
                        for (var i = 0; i < e.length; i++) {
                            _html += '<tr class="form-edit-source' + e[i].id + '">';
                            _html += '<td><input type="text" name="source' + e[i].id + '" id="source' + e[i].id + '" class="form-control" value="' + e[i].name + '"></td>';
                            _html += '<td><button class="btn btn-light btn-sm mr-1 update-source" data-id="' + e[i].id + '"><i class="fa fa-edit"></i></button><button class="btn btn-light btn-sm delete-source" data-id="' + e[i].id + '"><i class="fa fa-trash"></i></button></td>';
                            _html += '</tr>';
                        }
                    } else {
                        _html += '<tr>';
                        _html += '<td><i>No result</i></td><td></td>';
                        _html += '<tr>';
                    }
                    table_body.html(_html);
                }
            });
        }
    };

    var create_source = function () {
        j(document).on("click", "#add-source", function () {
            var _this = j(this);
            var source = j('#source').val();
            j.ajax({
                url: _url + 'settings/create_source',
                dataType: 'json',
                method: 'post',
                data: {
                    name: source
                },
                success: function (e) {
                    if (e.message == 'success') {
                        source_settings();
                        j('#source').val('');
                    }
                    new PNotify({
                        title: e.text + '!',
                        // text: e.message,
                        type: e.message
                    });
                }
            });
        });
    };

    var delete_source = function () {
        j(document).on("click", ".delete-source", function () {
            var _this = j(this);
            var id = _this.data('id');
            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this source?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'settings/delete_source',
                                method: 'post',
                                data: {
                                    id: id,
                                },
                                success: function (e) {
                                    source_settings();
                                    new PNotify({
                                        title: 'Successfully deleted!',
                                        text: 'success',
                                        type: 'success'
                                    });
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });
    };

    var update_source = function () {
        j(document).on("click", ".update-source", function () {
            var _this = j(this);
            var id = _this.data('id');
            var name = j('#source' + id).val();
            j.ajax({
                url: _url + 'settings/update_source',
                dataType: 'json',
                method: 'post',
                data: {
                    id: id,
                    name: name
                },
                success: function (e) {
                    if (e.message == 'success') {
                        source_settings();
                        j('#source').val('');
                    }
                    new PNotify({
                        title: e.text + '!',
                        // text: e.message,
                        type: e.message
                    });
                }
            });
        });
    };

    var terminationreason_settings = function () {
        var table_body = j('#terminationreason-body');
        if (table_body.length > 0) {
            j.ajax({
                url: _url + 'settings/get_terminationreasons',
                dataType: 'json',
                method: 'post',
                success: function (e) {
                    var _html = '';
                    if (e != '') {
                        for (var i = 0; i < e.length; i++) {
                            _html += '<tr class="form-edit-terminationreason' + e[i].id + '">';
                            _html += '<td><input type="text" name="terminationreason' + e[i].id + '" id="terminationreason' + e[i].id + '" class="form-control" value="' + e[i].name + '"></td>';
                            _html += '<td><button class="btn btn-light btn-sm mr-1 update-terminationreason" data-id="' + e[i].id + '"><i class="fa fa-edit"></i></button><button class="btn btn-light btn-sm delete-terminationreason" data-id="' + e[i].id + '"><i class="fa fa-trash"></i></button></td>';
                            _html += '</tr>';
                        }
                    } else {
                        _html += '<tr>';
                        _html += '<td><i>No result</i></td><td></td>';
                        _html += '<tr>';
                    }
                    table_body.html(_html);
                }
            });
        }
    };

    var create_terminationreason = function () {
        j(document).on("click", "#add-terminationreason", function () {
            var _this = j(this);
            var terminationreason = j('#terminationreason').val();
            j.ajax({
                url: _url + 'settings/create_terminationreason',
                dataType: 'json',
                method: 'post',
                data: {
                    name: terminationreason
                },
                success: function (e) {
                    if (e.message == 'success') {
                        terminationreason_settings();
                        j('#terminationreason').val('');
                    }
                    new PNotify({
                        title: e.text + '!',
                        // text: e.message,
                        type: e.message
                    });
                }
            });
        });
    };

    var delete_terminationreason = function () {
        j(document).on("click", ".delete-terminationreason", function () {
            var _this = j(this);
            var id = _this.data('id');
            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this terminationreason?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'settings/delete_terminationreason',
                                method: 'post',
                                data: {
                                    id: id,
                                },
                                success: function (e) {
                                    terminationreason_settings();
                                    new PNotify({
                                        title: 'Successfully deleted!',
                                        text: 'success',
                                        type: 'success'
                                    });
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });
    };

    var update_terminationreason = function () {
        j(document).on("click", ".update-terminationreason", function () {
            var _this = j(this);
            var id = _this.data('id');
            var name = j('#terminationreason' + id).val();
            j.ajax({
                url: _url + 'settings/update_terminationreason',
                dataType: 'json',
                method: 'post',
                data: {
                    id: id,
                    name: name
                },
                success: function (e) {
                    if (e.message == 'success') {
                        terminationreason_settings();
                        j('#terminationreason').val('');
                    }
                    new PNotify({
                        title: e.text + '!',
                        // text: e.message,
                        type: e.message
                    });
                }
            });
        });
    };


    var former_clients_datatables = function () {

        getleadstatus();
        getleadform();

        $(".leadstatus").click(function () {
            leadstatus = [];
            getleadstatus();
            former_clientstable.ajax.reload();
        });

        $(".leadform").click(function () {
            leadform = [];
            getleadform();
            former_clientstable.ajax.reload();
        });

        if (j('#cna-notes-former-client').length) {
            var cna_notes_former_client = CKEDITOR.inline('cna-notes-former-client', {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        }

        if (j('#cna-conversation-former-client').length) {
            var cna_conversation = CKEDITOR.inline('cna-conversation-former-client', {
                extraPlugins: 'link,uploadwidget,uploadimage',
                filebrowserUploadUrl: _url + "request/attached?type=image",
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        }


        var former_clientstable = j('#former-clients-table').DataTable({
            "dom": '<"row "<"col-lg-6"><"col-lg-6 d-flex justify-content-end" f<"dt-dropdown" <"dt-dropcontent" >>>>rt<"row mt-3"<"col-lg-6" l><"col-lg-6" p>><"clear">',
            // buttons: [
            //     {
            //         text: '<i class="fa fa-filter"></i> Filter',
            //         className: 'ml-2 d-sm-inline-block btn btn-sm btn-primary shadow-sm filter-btn',
            //         action: function (e, dt, node, config) {
            //             j(".dt-dropcontent").toggle("show");
            //         }
            //     }
            // ],
            "processing": true,
            "serverSide": true,
            "bInfo": false,
            "language": {
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "searchPlaceholder": "Search..."
            },
            "lengthMenu": [[10, 25, 50, 100], ["10 per page", "25 per page", "50 per page", "100 per page"]],
            "columnDefs": [
                {"orderable": false, "targets": 9},
                {"orderable": false, "targets": 4},
                {"orderable": false, "targets": 5}
            ],
            "order": [[6, "desc"]],
            "fnDrawCallback": function (oSettings) {
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                });
            },
            "ajax": {
                "url": _url + "Former_Clients/get_allcompanies",
                "dataType": "json",
                "type": "POST",
                "dataType": "json",
                "type": "POST",
                "data": function (d) {
                    d.leadstatus = leadstatus, d.leadform = leadform
                }
            },
            "columns": [
                {"data": "name"},
                {"data": "contact"},
                {"data": "leadform"},
                {"data": "industry"},
                {"data": "services"},
                {"data": "work_groups"},
                {"data": "lastupdate"},
                {"data": "termination_date"},
                {"data": "clientstatus"},
                {"data": "actions"}
            ]
        });

        $(document).on("click", ".delete-former-client", function () {
            var _this = j(this);
            var id = _this.data('id');
            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this client?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'company/destroy',
                                method: 'post',
                                data: {
                                    id: id,
                                },
                                success: function (e) {
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'success',
                                        type: 'success'
                                    });

                                    former_clientstable.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });

        // j('#filtercontent').appendTo(j('.dt-dropcontent'));

        // $(".close-filter").click(function () {
        //     $('#collapseFilter').collapse('toggle');
        // });

        $(document).on("click", ".open-former-client-viewmodal", function () {
            var _this = j(this);
            var cid = _this.data('id');
            var _modal = j('#viewModal');
            var companytext = _modal.find('.company');
            var contactfullnametext = _modal.find('.contactfullname');
            var jobtitletext = _modal.find('.jobtitle');
            var contactemailtext = _modal.find('.contactemail');
            var date_created = j('#viewModal').find('.date-created');
            var profile_tab_content = j('.profile-tab-content');
            var c_logo = j('.c-logo');
            c_logo.addClass('blur-image');
            j.ajax({
                url: _url + 'request/contact_info',
                dataType: 'json',
                method: 'post',
                data: {id: cid},
                success: function (e) {
                    c_logo.removeClass('blur-image');
                    companytext.text(e[0].account);
                    contactfullnametext.text(e[0].contactperson);
                    jobtitletext.text(e[0].contactjobtitle);
                    contactemailtext.text(e[0].contactemail);
                    cna_notes_former_client.setData(e[0].content);

                    profile_tab_content.find('#companyname').val(e[0].account);
                    profile_tab_content.find('#companywebsite').val(e[0].website);
                    profile_tab_content.find('#companyindustry').val(e[0].industry);
                    profile_tab_content.find('#companyaddress').val(e[0].full_address);
                    profile_tab_content.find('#companyphone').val(e[0].phone);
                    profile_tab_content.find('#companyemail').val(e[0].email);

                    profile_tab_content.find('#contactname').text(e[0].fullname);
                    profile_tab_content.find('#contactjobtitle').text(e[0].jobtitle);
                    profile_tab_content.find('#contactemail').text(e[0].email);
                    profile_tab_content.find('#contactphone').text(e[0].phone);

                    profile_tab_content.find('#companyreferral').val(e[0].referral);
                    profile_tab_content.find('#companysource').val(e[0].source);
                    profile_tab_content.find('#companyterminationreason').val(e[0].terminationreason);

                    profile_tab_content.find('#clientworkgroups').val(e[0].work_groups);
                    if (e[0].start_date) {
                        profile_tab_content.find('#companystartdate').val(moment(e[0].start_date).format("MM/DD/YYYY"));
                    }
                    if (e[0].termination_date) {
                        profile_tab_content.find('#companyterminationdate').val(moment(e[0].termination_date).format("MM/DD/YYYY"));
                    }

                    if (e[0].clientstatus_id == 1) {
                        j('.status-content').html('<span class="badge badge-success">Current</span>');
                    } else if (e[0].clientstatus_id == 2) {
                        j('.status-content').html('<span class="badge badge-primary">Past Due</span>');
                    } else if (e[0].clientstatus_id == 3) {
                        j('.status-content').html('<span class="badge badge-secondary">Offboarding</span>');
                    } else if (e[0].clientstatus_id == 4) {
                        j('.status-content').html('<span class="badge badge-info">Former Client</span>');
                    }

                    j('.assignedto-content').text(e[0].assignedto);
                    j('#companyId').val(e[0].ID);
                    j('.client_id').val(e[0].ID);



                    _show_added_contact(e[0].ID);
                    _show_added_type_of_services(e[0].ID);
                    _crm_type_of_service_upload_doc(e[0].ID);
                    _client_files_uploaded(e[0].ID);
                    _show_added_reminder(e[0].ID);
                    _show_added_conversation(e[0].ID);
                    _show_added_entity(e[0].ID);
                    // _revenue_select(e[0].ID);
                    // _ar_select(e[0].ID);
                    j('.billing_company').text(e[0].account);
                    j('.billing_company').val(e[0].account);

                    _show_transactions(e[0].ID);
                    j('.open-client-editmodal').attr('data-id', e[0].ID);
                    _get_client_entities(e[0].ID);
                }
            });
        });

        j(document).on("click", ".open-former-client-editmodal", function () {
            var _this = j(this);
            var cid = _this.data('id');
            var editmodal = j('#editModal');

            j.ajax({
                url: _url + 'company/show',
                dataType: 'json',
                method: 'post',
                data: {id: cid},
                success: function (e) {
                    editmodal.find('#editcompanyid').val(e.data[0].ID);
                    editmodal.find('#editname').val(e.data[0].account);
                    editmodal.find('#editwebsite').val(e.data[0].website);
                    editmodal.find('#editphone').val(e.data[0].phone);
                    editmodal.find('#editaddress').val(e.data[0].full_address);
                    editmodal.find('#editindustry').val(e.data[0].industry_id);
                    editmodal.find('#editstatus').val(e.data[0].status_id);
                    editmodal.find('#editclientstatus').val(e.data[0].clientstatus_id);
                    var test = [];
                    for (x = 0; x < e.data[0].users.length; x++) {
                        test.push(e.data[0].users[x].ID)
                    }
                    editmodal.find('#editassignedto').val(test).trigger("chosen:updated");

                    var groups = [];
                    for (x = 0; x < e.data[0].work_groups.length; x++) {
                        groups.push(e.data[0].work_groups[x].GID)
                    }
                    editmodal.find('#editworkgroups').val(groups).trigger("chosen:updated");

                    editmodal.find('#editreferralpartner').val(e.data[0].referral_partner_id).trigger("chosen:updated");
                    editmodal.find('#editleadsource').val(e.data[0].lead_source_id).trigger("chosen:updated");
                    editmodal.find('#editstartdate').val(moment(e.data[0].start_date).format('YYYY-MM-DD'));
                    editmodal.find('#editterminationdate').val(moment(e.data[0].termination_date).format('YYYY-MM-DD'));
                    editmodal.find('#editterminationreason').val(e.data[0].termination_reason_id).trigger("chosen:updated");
                }
            });
        });

        j('.edit-former-client-company').validate({
            rules: {
                editcompanyid: {required: true},
                editname: {required: true},
                editwebsite: {required: false},
                editphone: {required: false},
                editaddress: {required: false},
                editindustry: {required: false},
                editstatus: {required: false},
                editclientstatus: {required: false},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.update-former-client-company')
                submit_btn.attr('disabled', 'disabled');

                j.ajax({
                    url: _this.attr("action"),
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            former_clientstable.ajax.reload();
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });
    };
    var billing_select = function () {
        j(".chosen-selected").each(function () {
            var _this = j(this);
            _this.select2({
                theme: "bootstrap",
                placeholder: _this.attr('placeholder'),
            });
        });

    };



    var _delete_invoice = function () {
        j('.delete-ar-manual').unbind().bind('click', function () {
            var _this = j(this);
            _this.attr('data-id');
            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {

                            j.ajax({
                                url: _url + 'request/delete_ar_manual',
                                method: 'post',
                                data: {
                                    delete_ar_manual: _this.attr('data-id'),
                                },
                                success: function (e) {
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'AR has been deleted',
                                        type: 'success'
                                    });
                                    _this.parents('tr').remove();
                                }
                            });

                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });
    };

    var numberCommas = function (x) {
        x = Number(x);
        x = x.toFixed(2);
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    var manualRevenue = function () {
        j('.addmanualrevenue').validate({
            rules: {
                "invoiceno": {required: true},
                "invoiceday": {required: true},
                "itemnameselect": {required: true},
                "total": {required: true}

            }, showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.popover({content: value.message, trigger: 'hover', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.popover('dispose');
                });
            }
            , submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'request/addmanualrevenue',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        new PNotify({
                            title: 'Notification!',
                            text: 'This information has been added to your record',
                            type: 'success'
                        });
                        var _html = '';
//                        for (var i = 0; i < e.added.length; i++) {
//                            _html += '<tr>';
//                            _html += '<td class="count">1</td>';
//                            _html += '<td>' + e[i].invoice_no + '</td>';
//                            _html += '<td>' + e[i].invoice_date + '</td>';
//                            _html += '<td>' + e[i].customer_name + '</td>';
//                            _html += '<td>' + e[i].groupid + '</td>';
//                            _html += '<td class="text-right">' + e.total + '</td>';
//                            _html += '<td class="text-center">';
//                            _html += '<button class="btn btn-default delete-ar-manual" data-id="81726">';
//                            _html += '<i class="fa fa-trash"></i>';
//                            _html += '</button>';
//                            _html += '</td>';
//                            _html += '</tr>';
//                        }
                        _html += '<tr>';
                        _html += '<td class="count">1</td>';
                        _html += '<td>' + e.added[0].ID + '</td>';
                        _html += '<td>' + e.added[0].customer_name + '</td>';
                        _html += '<td>' + "<a href='" + e.added[0].invoice_no + "' data-bind='" + e.added[0].clientid + "' class='text-primary drill-down-invoice'>" + e.added[0].invoice_no + "</a>" + '</td>';
                        _html += '<td>' + e.added[0].invoice_date + '</td>';
                        _html += '<td>' + e.added[0].item_name + '</td>';
                        _html += '<td class="text-right">' + numberCommas(e.added[0].total) + '</td>';
                        _html += '<td class="text-center">';
                        _html += '<button type="button" class="btn btn-light action-button-sm delete-ar-manual" data-id="' + e.added[0].ID + '">';
                        _html += '<i class="fa fa-trash"></i>';
                        _html += '</button>';
                        _html += '</td>';
                        _html += '</tr>';

                        j('.added-revenue').prepend(_html);
                        j('.added-revenue .count').each(function (num) {
                            j(this).html(num + 1);
                        });
                        _delete_invoice();
                        _drill_down_invoice();
                    }
                });
                return false;

            }
        });
    };

    var manualAR = function () {
        j('.addmanualar').validate({
            rules: {
                "invoiceno": {required: true},
                "invoiceday": {required: true},
                "itemnameselect": {required: true},
                "total": {required: true}

            }, showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.popover({content: value.message, trigger: 'hover', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.popover('dispose');
                });
            }
            , submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'request/addmanualar',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        new PNotify({
                            title: 'Notification!',
                            text: 'This information has been added to your record',
                            type: 'success'
                        });
                        var _html = '';
//                        for (var i = 0; i < e.added.length; i++) {
//                            _html += '<tr>';
//                            _html += '<td class="count">1</td>';
//                            _html += '<td>' + e[i].invoice_no + '</td>';
//                            _html += '<td>' + e[i].invoice_date + '</td>';
//                            _html += '<td>' + e[i].customer_name + '</td>';
//                            _html += '<td>' + e[i].groupid + '</td>';
//                            _html += '<td class="text-right">' + e.total + '</td>';
//                            _html += '<td class="text-center">';
//                            _html += '<button class="btn btn-default delete-ar-manual" data-id="81726">';
//                            _html += '<i class="fa fa-trash"></i>';
//                            _html += '</button>';
//                            _html += '</td>';
//                            _html += '</tr>';
//                        }

                        _html += '<tr>';
                        _html += '<td class="count">1</td>';
                        _html += '<td>' + "<a href='" + e.added[0].invoice_no + "' data-bind='" + e.added[0].clientid + "' class='text-primary drill-down-invoice-ar'>" + e.added[0].invoice_no + "</a>" + '</td>';
                        // _html += '<td>' + e.added[0].invoice_no + '</td>';
                        _html += '<td>' + e.added[0].invoice_date + '</td>';
                        _html += '<td>' + e.added[0].customer_name + '</td>';
                        _html += '<td>' + e.added[0].group_name + '</td>';
                        _html += '<td class="text-right">' + numberCommas(e.added[0].amount_due) + '</td>';
                        _html += '<td class="text-center">';
                        _html += '<button type="button" class="btn btn-light  action-button-sm delete-ar-manual" data-id="' + e.added[0].ID + '">';
                        _html += '<i class="fa fa-trash"></i>';
                        _html += '</button>';
                        _html += '</td>';
                        _html += '</tr>';

                        j('.added-ar').prepend(_html);
                        j('.added-ar .count').each(function (num) {
                            j(this).html(num + 1);
                        });
                        _delete_invoice();
                        _drill_down_invoice_ar();
                    }
                });
                return false;

            }
        });
    };


    var revenue_drilldown_select = function (invoice, clientid) {

        var _html = '';
        _html += '<tr style="opacity:0.5">';
        _html += '<td class="count">1</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td class="text-right"></td>';
        _html += '<td class="text-center">';

        _html += '</td>';
        _html += '</tr>';
        j('.added-drill-revenue').html(_html);

        j.ajax({
            url: _url + 'request/revenue_drilldown_select',
            method: 'post',
            data: {"invoice": invoice, "clientid": clientid},
            success: function (e) {

                var _html = '';
                j('.added-drill-revenue').html(_html);

                var _html = '';
                var _total_invoice = 0;
                for (var i = 0; i < e.added.length; i++) {
                    _html += '<tr>';
                    _html += '<td class="count">1</td>';
                    _html += '<td>' + (e.added[i].invoice_no) + '</td>';
                    _html += '<td>' + e.added[i].invoice_date + '</td>';
                    _html += '<td>' + e.added[i].customer_name + '</td>';
                    _html += '<td>' + e.added[i].group_name + '</td>';
                    _html += '<td>' + e.added[i].item_name + '</td>';
                    _html += '<td class="text-right">' + numberCommas(e.added[i].total) + '</td>';
                    _html += '<td class="text-center">';
                    _html += '<button type="button" class="btn btn-default delete-ar-manual action-button-sm" data-id="' + e.added[i].ID + '">';
                    _html += '<i class="fa fa-trash"></i>';
                    _html += '</button>';
                    _html += '</td>';
                    _html += '</tr>';
                    _total_invoice += Number(e.added[i].total);
                }

                _html += '<tr>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td class="text-right">TOTAL</td>';
                _html += '<td class="text-right">' + numberCommas(_total_invoice) + '</td>';
                _html += '<td class="text-center">';

                _html += '</td>';
                _html += '</tr>';


                j('.added-drill-revenue').prepend(_html);
                j('.added-drill-revenue .count').each(function (num) {
                    j(this).html(num + 1);
                });
                _delete_invoice();

            }
        });
    };

    var ar_drilldown_select = function (invoice, clientid) {

        var _html = '';
        _html += '<tr style="opacity:0.5">';
        _html += '<td class="count">1</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '</tr>';
        j('.added-drill-ar').html(_html);

        j.ajax({
            url: _url + 'request/ar_drilldown_select',
            method: 'post',
            data: {"invoice": invoice, "clientid": clientid},
            success: function (e) {

                var _html = '';
                j('.added-drill-ar').html(_html);

                var _html = '';
                var _total_invoice = 0;
                for (var i = 0; i < e.added.length; i++) {
                    _html += '<tr>';
                    _html += '<td class="count">1</td>';
                    _html += '<td>' + (e.added[i].invoice_no) + '</td>';
                    _html += '<td>' + e.added[i].invoice_date + '</td>';
                    _html += '<td>' + e.added[i].customer_name + '</td>';
                    _html += '<td>' + e.added[i].group_name + '</td>';
                    _html += '<td>' + e.added[i].item_name + '</td>';
                    _html += '<td class="text-right">' + numberCommas(e.added[i].amount_due) + '</td>';
                    _html += '<td class="text-center">';
                    _html += '<button type="button" class="btn btn-default delete-ar-manual action-button-sm" data-id="' + e.added[i].ID + '">';
                    _html += '<i class="fa fa-trash"></i>';
                    _html += '</button>';
                    _html += '</td>';
                    _html += '</tr>';
                    _total_invoice += Number(e.added[i].amount_due);
                }

                _html += '<tr>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td class="text-right">TOTAL</td>';
                _html += '<td class="text-right">' + numberCommas(_total_invoice) + '</td>';
                _html += '<td class="text-center">';

                _html += '</td>';
                _html += '</tr>';


                j('.added-drill-ar').prepend(_html);
                j('.added-drill-ar .count').each(function (num) {
                    j(this).html(num + 1);
                });
                _delete_invoice();

            }
        });
    };

    var _drill_down_invoice = function () {
        j('.drill-down-invoice').unbind().bind('click', function (e) {
            e.preventDefault();
            var _modal = j('.modal-drill-down');
            var _full = j('.full-modal');
            var _this = j(this);
            _full.removeClass('show').addClass('hide');
            _modal.modal();
            _modal.find('.modal-title').html('INVOICE #' + _this.attr('href'));
            _modal.on('hidden.bs.modal', function () {
                _full.removeClass('hide').addClass('show');
            });
            revenue_drilldown_select(_this.attr('href'), _this.attr('data-bind'));
        });
    };

    var _drill_down_invoice_ar = function () {
        j('.drill-down-invoice-ar').unbind().bind('click', function (e) {
            e.preventDefault();
            var _modal = j('.modal-drill-down-ar');
            var _full = j('.full-modal');
            var _this = j(this);
            _full.removeClass('show').addClass('hide');
            _modal.modal();
            _modal.find('.modal-title').html('INVOICE #' + _this.attr('href'));
            _modal.on('hidden.bs.modal', function () {
                _full.removeClass('hide').addClass('show');
            });
            ar_drilldown_select(_this.attr('href'), _this.attr('data-bind'));
        });
    };
    var _revenue_select = function (id) {
        var _html = '';
        _html += '<tr style="opacity:0.5">';
        _html += '<td class="count">1</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '</tr>';
        j('.added-revenue').html(_html);

        j.ajax({
            url: _url + 'request/revenue_select',
            method: 'post',
            data: {"client_id": id},
            success: function (e) {
                console.log(e);
                var _html = '';
                j('.added-revenue').html(_html);

                var _html = '';
                var _total_invoice = 0;
                for (var i = 0; i < e.added.length; i++) {
                    _html += '<tr>';
                    _html += '<td class="count">1</td>';
                    // _html += '<td>' + e.added[i].invoice_date + '</td>';
                    _html += '<td>' + e.added[i].ID + '</td>';
                    _html += '<td>' + e.added[i].customer_name + '</td>';
                    _html += '<td>' + "<a href='" + e.added[i].invoice_no + "' data-bind='" + e.added[i].clientid + "' class='text-primary drill-down-invoice'>" + e.added[i].invoice_no + "</a>" + '</td>';
                    _html += '<td>' + e.added[i].invoice_date + '</td>';
                    _html += '<td>' + e.added[i].item_name + '</td>';
                    _html += '<td class="text-right">' + numberCommas(e.added[i].merge_invoice) + '</td>';
                    _html += '<td class="text-center width_150">';
                    _html += '<button type="button" class="btn btn-default delete-ar-manual action-button-sm" data-id="' + e.added[i].ID + '">';
                    _html += '<i class="fa fa-trash"></i>';
                    _html += '</button>';
                    _html += '</td>';
                    _html += '</tr>';
                    _total_invoice += Number(e.added[i].merge_invoice);
                }

                _html += '<tr>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td class="text-right">TOTAL</td>';
                _html += '<td class="text-right">' + numberCommas(_total_invoice) + '</td>';
                _html += '<td class="text-center">';

                _html += '</td>';
                _html += '</tr>';


                j('.added-revenue').prepend(_html);
                j('.added-revenue .count').each(function (num) {
                    j(this).html(num + 1);
                });
                _delete_invoice();
                revenue_drilldown_select();
                _drill_down_invoice();
            }
        });
    };

    var _ar_select = function (id) {
        var _html = '';
        _html += '<tr style="opacity:0.5">';
        _html += '<td class="count">1</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '<td>Loading....</td>';
        _html += '</tr>';
        j('.added-ar').html(_html);

        j.ajax({
            url: _url + 'request/ar_select',
            method: 'post',
            data: {"client_id": id},
            success: function (e) {
                console.log(e);
                var _html = '';
                j('.added-ar').html(_html);

                var _html = '';
                var _total_invoice = 0;
                for (var i = 0; i < e.added.length; i++) {
                    _html += '<tr>';
                    _html += '<td class="count">1</td>';
                    _html += '<td>' + "<a href='" + e.added[i].invoice_no + "' data-bind='" + e.added[i].clientid + "' class='text-primary drill-down-invoice-ar'>" + e.added[i].invoice_no + "</a>" + '</td>';
                    _html += '<td>' + e.added[i].invoice_date + '</td>';
                    _html += '<td>' + e.added[i].customer_name + '</td>';
                    _html += '<td>' + e.added[i].group_name + '</td>';
                    _html += '<td class="text-right">' + numberCommas(e.added[i].amount_due) + '</td>';
                    _html += '<td class="text-center">';
                    _html += '<button type="button" class="btn btn-default delete-ar-manual action-button-sm" data-id="' + e.added[i].ID + '">';
                    _html += '<i class="fa fa-trash"></i>';
                    _html += '</button>';
                    _html += '</td>';
                    _html += '</tr>';
                    _total_invoice += Number(e.added[i].amount_due);
                }

                _html += '<tr>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td class="text-right">TOTAL</td>';
                _html += '<td class="text-right">' + numberCommas(_total_invoice) + '</td>';
                _html += '<td class="text-center">';

                _html += '</td>';
                _html += '</tr>';


                j('.added-ar').prepend(_html);
                j('.added-ar .count').each(function (num) {
                    j(this).html(num + 1);
                });
                // _delete_invoice();
                _drill_down_invoice_ar();
            }
        });
    };

    var open_add_entity = function () {
        j(document).on("click", ".open-add-entity", function () {
            $('#collapseAddEntity').collapse('show');
            $('#collapseEditEntity').collapse('hide');
        });
    };

    var create_entity = function () {
        j('.create-entity').validate({
            rules: {
                entityname: {required: true},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.save-entity')
                submit_btn.attr('disabled', 'disabled');

                var data = _this.serializeArray(); // convert form to array
                var companyId = j('#companyId').val();
                data.push({name: 'companyId', value: companyId});
                j.ajax({
                    url: _this.attr('action'),
                    dataType: 'json',
                    method: 'post',
                    data: data,
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            _this.find('#entityname').val('');
                            _show_added_entity(companyId);
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });
    };

    var _client_experience_review = function () {
        j('.review-button').unbind().bind('click', function (e) {
            var _this = j(this);
            _this.attr('disabled', 'disabled');
            _this.addClass('loader-btn');
            _this.html();
            _this.html(_this.html() === 'Review' ? 'Reviewing...' : 'Canceling...');
            j.ajax({
                url: _url + 'Entity/UserExperienceReviewer',
                method: 'post',
                data: {
                    'uid': _this.attr('data-reviewer'),
                    'bind': _this.attr('data-bind')
                },
                success: function (e) {
                    _this.removeAttr('disabled');
                    _this.removeClass('loader-btn');
                    if (e.status === "success") {
                        _this.parents('tr').find('.review-timestamp').html(e.callback[0].reviewed_timestamp);
                        _this.parents('.update-experience').find('.client-experience-reviewer').html(e.callback[0].fullname);
                        _this.attr('data-bind', (e.callback[0].fullname ? '1' : '0'));
                        _this.html(e.callback[0].fullname ? 'Cancel Reviewed' : 'Review');
                    } else {
                        new PNotify({
                            title: 'Notification!',
                            text: e.message,
                            type: 'warning'
                        });
                    }
                }
            });
        });
    };

    var _client_experience_entry = function () {

        j('.ci-logs').on('change keypress keyup', function () {
            var _this = j(this);
            var editor_id = _this.parents('.editor-con').find('.update-logs-note-crm').attr('id');
            var _list = '<ol>';
            for (var i = 0; i < Number(_this.val()); i++) {
                _list += '<li>&nbsp;</li>';
            }
            _list += '</ol>';
            CKEDITOR.instances[editor_id].setData(_list);
        });

        j('.error-logs').on('change keypress keyup', function () {
            var _this = j(this);
            var editor_id = _this.parents('.editor-con').find('.update-error-notes-crm').attr('id');
            var _list = '<ol>';
            for (var i = 0; i < Number(_this.val()); i++) {
                _list += '<li>&nbsp;</li>';
            }
            _list += '</ol>';
            CKEDITOR.instances[editor_id].setData(_list);
        });

        j(".update-experience").each(function () {
            j(this).validate({
                rules: {
                    "Service_Continuum": {
                        required: true
                    }, "Calls_made": {
                        required: true
                    }, "QRC": {
                        required: true
                    }, "CI_log": {
                        required: true
                    }, "Errors": {
                        required: true
                    }, "Deadline_missed": {
                        required: true
                    }, "Audit_Ready": {
                        required: true
                    }

                }, showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.parent().addClass('has-error');
                        _elm.popover({content: value.message, trigger: 'hover', placement: 'top'});
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.parent().removeClass('has-error');
                        _elm.popover('dispose');
                    });
                },
                submitHandler: function (form) {
                    var _this = j(form);
                    _this.find('.btn').attr('disabled', 'disabled');

                    var concern = _this.find('.ongoing-concern-crm');
                    var value = CKEDITOR.instances[concern.attr('id')].getData();
                    concern.html(value);

                    var logsnote = _this.find('.update-logs-note-crm');
                    var value = CKEDITOR.instances[logsnote.attr('id')].getData();
                    logsnote.html(value);

                    var errornotes = _this.find('.update-error-notes-crm');
                    var value = CKEDITOR.instances[errornotes.attr('id')].getData();
                    errornotes.html(value);

                    var satifaction = _this.find('.client-satifaction-crm');
                    var value = CKEDITOR.instances[satifaction.attr('id')].getData();
                    satifaction.html(value);
                    
                    _this.find('.submit-button').html('Loading...');
                    j.ajax({
                        url: _this.attr('action'),
                        method: 'post',
                        data: _this.serialize(),
                        success: function (e) {
                            if (e.status === 'success') {
                                new PNotify({
                                    title: 'Notification!',
                                    text: e.message,
                                    type: 'success'
                                });
                                if (e.type === 'add') {
                                    _this.find('.entry_update').val('1');
                                    _this.find('.data-id').val(e.callback[0].UID);
                                    _this.find('.submit-button').html('Update');
                                    _this.find('.review-button').removeAttr('disabled').removeClass('hide').attr('data-reviewer', e.callback[0].UID);
                                    _this.parents('tr').animate({backgroundColor: '#28a745'}, 500, function () {
                                        var _anim = j(this);
                                        _anim.animate({backgroundColor: '#ffffff'}, 500, function () {
                                            _anim.removeAttr('style');
                                        });
                                    });
                                } else if (e.type === 'update') {
                                    _this.find('.submit-button').html('Update');
                                    _this.parents('tr').animate({backgroundColor: '#007bff'}, 500, function () {
                                        var _anim = j(this);
                                        _anim.animate({backgroundColor: '#ffffff'}, 500, function () {
                                            _anim.removeAttr('style');
                                        });
                                    });
                                }
                            } else {
                                new PNotify({
                                    title: 'Notification!',
                                    text: e.message,
                                    type: 'warning'
                                });
                            }
                            _this.find('.btn').removeAttr('disabled');
                            _client_experience_review();
                        }
                    });
                    return false;
                }
            });
        });
    };

    var _modal_ckeditr = function () {
        j('.update-logs-note-crm').each(function () {
            var _this = j(this);
            CKEDITOR.inline(_this.attr('id'), {
                extraPlugins: 'link,uploadwidget,uploadimage',
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        });
        j('.update-error-notes-crm').each(function () {
            var _this = j(this);
            CKEDITOR.inline(_this.attr('id'), {
                extraPlugins: 'link,uploadwidget,uploadimage',
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        });
        j('.ongoing-concern-crm').each(function () {
            var _this = j(this);
            CKEDITOR.inline(_this.attr('id'), {
                extraPlugins: 'link,uploadwidget,uploadimage',
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        });
        j('.client-satifaction-crm').each(function () {
            var _this = j(this);
            CKEDITOR.inline(_this.attr('id'), {
                extraPlugins: 'link,uploadwidget,uploadimage',
                uiColor: '#F8F8F8',
                toolbar: [
                    {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                ]
            });
        });
    };
    var _modal_cx = function () {
        j('.edit-cx').unbind().bind('click', function () {
            var _this = j(this);
            var _modal = j('.modal-cx');
            _modal.modal();
            j.ajax({
                url: _url + "entity/cx_entities",
                dataType: 'json',
                method: 'post',
                data: {'id': _this.attr('data-id')},
                success: function (e) {
                    console.log(e);
                    if (e.status === 'success') {
                        _modal.find('[name="QRC"] option').removeAttr('selected');
                        _modal.find('[name="Deadline_missed"] option').removeAttr('selected');
                        _modal.find('[name="Audit_Ready"] option').removeAttr('selected');

                        if (Number(e.update)) {
                            _modal.find('[name="data_id"]').val(e.data[0].UID);

                            _modal.find('[name="Service_Continuum"]').val(e.data[0].service_continum);

                            _modal.find('[name="Calls_made"]').val(e.data[0].calls_made);

                            _modal.find('[name="QRC"] [value="' + e.data[0].qrc + '"]').attr('selected', 'selected');

                            _modal.find('[name="Note"]').val(e.data[0].UID);

                            _modal.find('[name="ongoing_concern"]').html(e.data[0].ongoing_concern);
                            CKEDITOR.instances['ongoing-concern-crm'].setData(e.data[0].ongoing_concern); 
                            
                            _modal.find('[name="CI_log"]').val(e.data[0].ci_log);
                            _modal.find('[name="update-logs-note"]').html(e.data[0].ci_log_note);
                            CKEDITOR.instances['update-logs-note-crm'].setData(e.data[0].no_errors_note); 

                            _modal.find('[name="Errors"]').val(e.data[0].no_of_errors);
                            _modal.find('[name="error_note"]').html(e.data[0].no_errors_note);
                            CKEDITOR.instances['update-error-notes-crm'].setData(e.data[0].no_errors_note); 

                            _modal.find('[name="Client_satifaction"]').html(e.data[0].client_satifaction);
                            CKEDITOR.instances['client-satifaction-crm'].setData(e.data[0].client_satifaction); 
                            

                            _modal.find('[name="Deadline_missed"] [value="' + e.data[0].deadline_missed + '"]').attr('selected', 'selected');

                            _modal.find('[name="Audit_Ready"] [value="' + e.data[0].audit_ready + '"]').attr('selected', 'selected');


                            _modal.find('.client-experience-reviewer').html(e.reviewer);

                            _modal.find('.review-button').html((e.reviewer ? 'Reviewed' : "Review")).removeAttr('disabled').removeClass('hide').attr('data-reviewer', e.data[0].UID).attr("data-bind", (e.data[0].reviewed_by ? '1' : '0'));
                            _modal.find('.submit-button').html('Update');
                            _client_experience_review();
                        }
                        _modal.find('[name="client_id"]').val(_this.attr('data-id'));
                        _modal.find('[name="entry_update"]').val(e.update);
                        _modal.find('[name="group_id"]').val(_this.attr('data-groupid'));
                    } else {

                    }
                }
            });
        });
    };
    var _show_added_entity = function ($id) {
        var table_body = j('.entity-table-body');
        var _html = '';
        _html += '<tr>';
        _html += '<td colspan="2" class="text-center">Loading .....</td>';
        _html += '<tr>';
        table_body.html(_html);
        j.ajax({
            url: _url + "entity/client_entities",
            dataType: 'json',
            method: 'post',
            data: {'id': $id},
            success: function (e) {
                var _html = '';
                if (e.length) {
                    for (var i = 0; i < e.length; i++) {
                        _html += '<tr>';
                        _html += '<td>' + e[i].entity + '</td>';
                        _html += '<td class="text-center"><button class="btn btn-light action-button-sm mr-1 edit-entity" data-id="' + e[i].CID + '"><i class="fa fa-edit"></i></button> <button class="btn btn-light action-button-sm mr-1 edit-cx" data-id="' + e[i].CID + '" data-groupid="' + e[i].groupid + '"><i class="fa fa-users"></i></button> <button class="btn btn-light action-button-sm delete-entity action-button-sm" data-id="' + e[i].CID + '"><i class="fa fa-trash"></i></button></td>';
                        _html += '</tr>';
                    }
                } else {
                    _html += '<tr>';
                    _html += '<td  class="text-center" colspan="2">No Data</td>';
                    _html += '<tr>';
                }
                table_body.html(_html);
                _modal_cx();
            }
        });
    };

    var close_add_entity = function () {
        j(document).on("click", ".close-add-entity", function () {
            $('#collapseAddEntity').collapse('hide');
        });
    };

    var edit_entity = function () {
        j(document).on("click", ".edit-entity", function () {
            var _this = j(this);
            var id = _this.attr('data-id');
            var form = j('.update-entity');

            $('#collapseEditEntity').collapse('show');
            $('#collapseAddEntity').collapse('hide');

            j.ajax({
                url: _url + 'entity/show',
                dataType: 'json',
                method: 'post',
                data: {id: id},
                success: function (e) {
                    if (e.status === "ok") {
                        form.find('#entityeditid').val(e.data[0].ID);
                        form.find('#entityeditname').val(e.data[0].entity);
                    } else {
                        new PNotify({
                            title: 'Notification!',
                            text: 'error',
                            type: 'error'
                        });
                    }
                }
            });
        });
    };

    var update_entity = function () {
        j('.update-entity').validate({
            rules: {
                entityeditid: {required: true},
                entityeditname: {required: true},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.update-entity');
                submit_btn.attr('disabled', 'disabled');

                var data = _this.serializeArray(); // convert form to array
                var companyId = j('#companyId').val();

                j.ajax({
                    url: _this.attr('action'),
                    dataType: 'json',
                    method: 'post',
                    data: data,
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });

                            _show_added_entity(companyId);
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });
    };

    var close_edit_entity = function () {
        j(document).on("click", ".close-edit-entity", function () {
            $('#collapseEditEntity').collapse('hide');
        });
    };

    var delete_entity = function () {
        j(document).on("click", ".delete-entity", function () {
            var _this = j(this);
            var companyId = j('#companyId').val();

            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this entity?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'entity/destroy',
                                method: 'post',
                                data: {
                                    id: _this.attr('data-id'),
                                },
                                success: function (e) {
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'success',
                                        type: 'success'
                                    });

                                    _show_added_entity(companyId);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });
    };
    var toogle_add_revenue = function () {
        j('.btn-add-revenue').click(function () {
            var _toogle = j('.toogle-revenue');
            if (_toogle.hasClass('hide')) {
                _toogle.removeClass('hide');
            } else {
                _toogle.addClass('hide');
            }

        });
    };

    var toogle_add_ar = function () {
        j('.btn-add-ar').click(function () {
            var _toogle = j('.toogle-ar');
            if (_toogle.hasClass('hide')) {
                _toogle.removeClass('hide');
            } else {
                _toogle.addClass('hide');
            }

        });
    };

    var _show_transactions = function (id) {
        var _html = '';
        _html += '<tr style="opacity:0.5">';
        _html += '<td colspan="7" class="text-center">Loading....</td>';
        _html += '</tr>';
        j('.added-transactions').html(_html);

        j.ajax({
            url: _url + 'request/transactions',
            method: 'post',
            data: {"client_id": id},
            success: function (e) {
                console.log(e);
                var _html = '';
                j('.added-transactions').html(_html);

                var _html = '';
                var _total_invoice = 0;
                var _total_payments = 0;
                var _total_balance = 0;
                for (var i = 0; i < e.added.length; i++) {
                    _html += '<tr>';
                    _html += '<td class="count">1</td>';
                    _html += '<td>' + "<a href='" + e.added[i].invoice_number + "' data-bind='" + e.added[i].client_id + "' class='text-primary drill-down-transaction'>" + e.added[i].invoice_number + "</a>" + '</td>';
                    _html += '<td>' + moment(e.added[i].transaction_date).format("MM/DD/YYYY") + '</td>';
                    _html += '<td>' + e.added[i].entity + '</td>';
                    _html += '<td class="text-right">' + numberCommas(e.added[i].total) + '</td>';
                    _html += '<td class="text-right">' + numberCommas(e.added[i].payments) + '</td>';
                    _html += '<td class="text-right">' + numberCommas(e.added[i].balance) + '</td>';
                    _html += '</tr>';
                    _total_invoice += Number(e.added[i].total);
                    _total_payments += Number(e.added[i].payments);
                    _total_balance += Number(e.added[i].balance);
                }

                _html += '<tr>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td></td>';
                _html += '<td class="text-right">TOTAL</td>';
                _html += '<td class="text-right">' + numberCommas(_total_invoice) + '</td>';
                _html += '<td class="text-right">' + numberCommas(_total_payments) + '</td>';
                _html += '<td class="text-right">' + numberCommas(_total_balance) + '</td>';
                _html += '</tr>';

                if (e.added.length == 0) {
                    _html += '<tr>';
                    _html += '<td  class="text-center" colspan="7">No Data</td>';
                    _html += '</tr>';
                }

                j('.added-transactions').prepend(_html);
                j('.added-transactions .count').each(function (num) {
                    j(this).html(num + 1);
                });

                _drill_down_transaction();
            }
        });
    };

    var _drill_down_transaction = function () {
        j('.drill-down-transaction').unbind().bind('click', function (e) {
            e.preventDefault();
            var _modal = j('.modal-drill-down-transaction');
            var _full = j('.full-modal');
            var _this = j(this);
            _full.removeClass('show').addClass('hide');
            _modal.modal();
            _modal.find('.modal-title').html('INVOICE #' + _this.attr('href'));
            _modal.on('hidden.bs.modal', function () {
                _full.removeClass('hide').addClass('show');
            });
            _transactions_drilldown_select(_this.attr('href'), _this.attr('data-bind'));
        });
    };

    var _transactions_drilldown_select = function (invoice, clientid) {

        var _html = '';
        _html += '<tr style="opacity:0.5">';
        _html += '<td colspan="11" class="text-center">Loading....</td>';
        _html += '</tr>';
        j('.added-drill-transactions').html(_html);

        j.ajax({
            url: _url + 'request/transactions_drilldown_select',
            method: 'post',
            data: {"invoice": invoice, "clientid": clientid},
            success: function (e) {

                var _html = '';
                j('.added-drill-transactions').html(_html);

                var _html = '';
                var _total_invoice = 0;
                var _total_payments = 0;
                for (var i = 0; i < e.invoices.length; i++) {
                    _html += '<tr>';
                    _html += '<td>' + moment(e.invoices[i].created_at).format("MM/DD/YYYY") + '</td>';
                    _html += '<td>' + moment(e.invoices[i].updated_at).format("MM/DD/YYYY") + '</td>';
                    _html += '<td>' + e.invoices[i].entity + '</td>';
                    _html += '<td>' + (e.invoices[i].group_name == null ? '' : e.invoices[i].group_name) + '</td>';
                    _html += '<td>' + e.invoices[i].invoice_number + '</td>';
                    _html += '<td>' + moment(e.invoices[i].transaction_date).format("MM/DD/YYYY") + '</td>';
                    _html += '<td>' + (e.invoices[i].credit_amount == null ? '' : numberCommas(e.invoices[i].credit_amount)) + '</td>';
                    _html += '<td>' + (e.invoices[i].line_item_name == null ? '' : e.invoices[i].line_item_name) + '</td>';
                    if (e.invoices[i].transaction_type == 'payments') {
                        _total_payments += Number(e.invoices[i].line_item_amount);
                        _html += '<td class="text-right">-' + numberCommas(e.invoices[i].line_item_amount) + '</td>';
                    } else {
                        _total_invoice += Number(e.invoices[i].line_item_amount);
                        _html += '<td class="text-right">' + numberCommas(e.invoices[i].line_item_amount) + '</td>';
                    }
                    _html += '<td>' + e.invoices[i].total_line_items + '</td>';
                    _html += '<td class="text-capitalize">' + e.invoices[i].transaction_type + '</td>';
                    _html += '</tr>';
                }

                // _html += '<tr>';
                // _html += '<td></td>';
                // _html += '<td></td>';
                // _html += '<td></td>';
                // _html += '<td></td>';
                // _html += '<td></td>';
                // _html += '<td></td>';
                // _html += '<td></td>';
                // _html += '<td></td>';
                // _html += '<td></td>';
                // _html += '<td class="text-right">TOTAL</td>';
                // _html += '<td class="text-right">' + numberCommas(_total_invoice) + '</td>';
                // _html += '</tr>';

                j('#total_invoice').text(numberCommas(_total_invoice));
                j('#total_payments').text(numberCommas(_total_payments));
                j('#total_balance').text(numberCommas(_total_invoice - _total_payments));

                j('.added-drill-transactions').prepend(_html);
                j('.added-drill-transactions .count').each(function (num) {
                    j(this).html(num + 1);
                });
                _delete_invoice();

            }
        });
    };

    var _upload_image = function () {
        j('.company-logo-edit').change()
    };
    var _get_device_information_text = function (_device) {
        j.ajax({
            url: _url + 'CPU_usage/get_device_information',
            dataType: 'json',
            method: 'post',
            success: function (e) {
                var process = j('.process-list');
                var _html = '';

                for (var x = 0; x < e.process_list.length; x++) {
                    if (e.process_list[x].Info !== 'show full PROCESSLIST' && e.process_list[x].Info !== null) {
                        _html += '<tr>';
                        _html += '<td>' + e.process_list[x].User + '</td>';
                        _html += '<td>' + e.process_list[x].Command + '</td>';
                        _html += '<td>' + e.process_list[x].State + '</td>';
                        _html += '<td>' + e.process_list[x].Info + '</td>';
                        _html += '<td>' + e.process_list[x].Progress + '</td>';
                        _html += '</tr>';
                    }
                }
                process.html(_html);

                _device.find('.ram-total-text').html(e.random_access_memory.total);
                _device.find('.ram-used-text').html(e.random_access_memory.used);
                _device.find('.ram-remaining-text').html(e.random_access_memory.available);
                _device.find('.ram-used-html').html(e.random_access_memory.used_percent + '%').css({"width": e.random_access_memory.used_percent + '%'});
                _device.find('.ram-remaining-html').html(e.random_access_memory.available_percent + '%').css({"width": e.random_access_memory.available_percent + '%'});
                _device.find('.mmc-total-text').html(e.memory_card.total);
                _device.find('.mmc-used-text').html(e.memory_card.used);
                _device.find('.mmc-remaining-text').html(e.memory_card.remaining);
                _device.find('.mmc-used-html').html(e.memory_card.used_percent).css({"width": e.memory_card.used_percent});
                _device.find('.mmc-remaining-html').html(e.memory_card.remaining_percent).css({"width": e.memory_card.remaining_percent});
                _device.find('.proc-total-text').html(e.processor.total);
                _device.find('.proc-used-text').html(e.processor.used);
                _device.find('.proc-remaining-text').html(e.processor.remaining);
                _device.find('.proc-used-html').html(e.processor.used_percent).css({"width": e.processor.used_percent});
                _device.find('.proc-remaining-html').html(e.processor.remaining_percent).css({"width": e.processor.remaining_percent});
            }
        });
    };
    var _get_device_information = function () {
        var _device = j('.device-info');
        if (_device.length) {
            _get_device_information_text(_device);
            setInterval(function () {
                _get_device_information_text(_device);
            }, 1000);
        }
    };

    var _upload_logo = function () {
        var file_upload = j('.company-logo-edit');
        file_upload.find('.file-upload').change(function () {
            file_upload.submit();
        });
        file_upload.submit(function () {
            var _this = j(this);
            _this.ajaxSubmit({
                url: _this.attr('action'),
                beforeSend: function () {
                    _this.find('.progress').removeClass('hide');
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    _this.find('.progress').removeClass('hide');
                    _this.find('.progress-bar').css({width: percentComplete + '%'}).html(percentComplete + '%');
                },
                complete: function (e) {
                    var _e = (j.parseJSON(e.responseText));
                    if (_e.message === 'success') {
                        j('.c-logo').attr('src', _url + 'request/company_logo/' + _e.clientid);
                        _this[0].reset();
                    } else {
                        _this[0].reset();
                    }
                }
            });
            return false;
        });
    };

    var _get_client_entities = function ($id) {
        j.ajax({
            url: _url + "entity/client_all_entities",
            dataType: 'json',
            method: 'post',
            data: {'id': $id},
            success: function (e) {
                var _html = '';
                if (e.length) {
                    for (var i = 0; i < e.length; i++) {
                        if (e[i].ID == $id) {
                            _html += '<option value="' + e[i].ID + '" selected="selected">';
                        } else {
                            _html += '<option value="' + e[i].ID + '">';
                        }
                        _html += e[i].entity;
                        _html += '</option>';
                    }
                }
                j('#contactaddentity').html(_html);
                j('#contacteditentity').html(_html);
                j('#tosentity').html(_html);
                j('#edittosentity').html(_html);
            }
        });
    };

    var add_transaction = function () {
        j('.add_revenue').validate({
            rules: {
                client: {required: true},
                invoice_no: {required: true},
                // work_group: {required: true},
                line_item_amount: {required: true},
                line_item_name: {required: true},
                source: {required: true},
                transaction_date: {required: true},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.btn-submit');
                submit_btn.attr('disabled', 'disabled');
                submit_btn.html('Loading...');

                var data = _this.serializeArray(); // convert form to array

                j.ajax({
                    url: _this.attr('action'),
                    dataType: 'json',
                    method: 'post',
                    data: data,
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                                submit_btn.html('Add');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });
                            var html = '';
                            var tb_row = j('.revenue_row');

                            html += '<tr class="added-revenue">';
                            html += '<td>' + e.invoice.client_id + '</td>';
                            html += '<td>' + e.invoice.client_name + '</td>';
                            html += '<td>' + e.invoice.group_name + '</td>';
                            html += '<td>#' + e.invoice.invoice_number + '</td>';
                            html += '<td>' + e.invoice.line_item_name + '</td>';
                            html += '<td>' + e.invoice.transaction_date + '</td>';
                            html += '<td>' + e.invoice.line_item_amount + '</td>';
                            html += '<td>' + e.invoice.source + '</td>';
                            html += '<td class="text-center">';
                            html += '<button data-id="' + e.invoice.ID + '" data-toggle="modal" data-target="#editrevenueModal" class="btn btn-light btn-sm mr-1 edit-transaction"><i class="fa fa-edit"></i></button>';
                            html += '<button class="btn btn-light btn-sm delete-transaction" data-id="' + e.invoice.ID + '"><i class="fa fa-trash"></i></button>';
                            html += '</td>';
                            html += '</tr>';
                            tb_row.prepend(html);

                            j('.added-revenue').animate({backgroundColor: '#337ab7'}, 500, function () {
                                var _anim = j(this);
                                _anim.animate({backgroundColor: '#ffffff'}, 500, function () {
                                    _anim.removeClass('added-revenue');
                                });
                            });
                            // _this[0].reset();
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                                submit_btn.html('Add');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });
    };

    var edit_transaction = function () {
        j(document).on("click", ".edit-transaction", function () {
            var _this = j(this);
            var id = _this.attr('data-id');
            var form = j('.edit_revenue');

            j.ajax({
                url: _url + 'Invoices/getTransaction',
                dataType: 'json',
                method: 'post',
                data: {id: id},
                success: function (e) {
                    if (e.status === "ok") {
                        form.find('#edit_transaction_id').val(e.data.ID);
                        form.find('#edit_invoice_no').val(e.data.invoice_number);
                        form.find('#edit_line_item_amount').val(e.data.line_item_amount);
                        form.find('#edit_transaction_date').val(e.data.transaction_date);
                        form.find('#edit_client').val(e.data.client_id).trigger('chosen:updated');
                        form.find('#edit_work_group').val(e.data.group_id).trigger('chosen:updated');
                        form.find('#edit_line_item_name').val(e.data.line_item_name).trigger('chosen:updated');
                        form.find('#edit_source').val(e.data.source).trigger('chosen:updated');
                    } else {
                        new PNotify({
                            title: 'Notification!',
                            text: 'error',
                            type: 'error'
                        });
                    }
                }
            });
        });
    };

    var update_transaction = function () {
        j('.edit_revenue').validate({
            rules: {
                edit_transaction_id: {required: true},
                edit_client: {required: true},
                edit_invoice_no: {required: true},
                // edit_work_group: {required: true},
                edit_line_item_amount: {required: true},
                edit_line_item_name: {required: true},
                edit_source: {required: true},
                edit_transaction_date: {required: true},
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.addClass('is-invalid');
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.removeClass('is-invalid');
                });
            }, submitHandler: function (form) {
                var _this = j(form);
                var submit_btn = _this.find('.btn-submit');
                submit_btn.attr('disabled', 'disabled');
                submit_btn.html('Loading...');
                var t_id = _this.find('#edit_transaction_id').val();
                var transaction_item_row = j('.transaction_item_'+t_id);

                var data = _this.serializeArray(); // convert form to array

                j.ajax({
                    url: _this.attr('action'),
                    dataType: 'json',
                    method: 'post',
                    data: data,
                    success: function (e) {
                        if (e.status === "ok") {
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                                submit_btn.html('Update');
                            }, 1000);

                            new PNotify({
                                title: 'Notification!',
                                text: 'success',
                                type: 'success'
                            });
                            transaction_item_row.html('');                            
                            var html = '';
                            html += '<td>' + e.invoice.client_id + '</td>';
                            html += '<td>' + e.invoice.client_name + '</td>';
                            html += '<td>' + e.invoice.group_name + '</td>';
                            html += '<td>#' + e.invoice.invoice_number + '</td>';
                            html += '<td>' + e.invoice.line_item_name + '</td>';
                            html += '<td>' + e.invoice.transaction_date + '</td>';
                            html += '<td>' + e.invoice.line_item_amount + '</td>';
                            html += '<td>' + e.invoice.source + '</td>';
                            html += '<td class="text-center">';
                            html += '<button data-id="' + e.invoice.ID + '" data-toggle="modal" data-target="#editrevenueModal" class="btn btn-light btn-sm mr-1 edit-transaction"><i class="fa fa-edit"></i></button>';
                            html += '<button class="btn btn-light btn-sm delete-transaction" data-id="' + e.invoice.ID + '"><i class="fa fa-trash"></i></button>';
                            html += '</td>';
                            transaction_item_row.html(html);

                            j('.added-revenue').animate({backgroundColor: '#337ab7'}, 500, function () {
                                var _anim = j(this);
                                _anim.animate({backgroundColor: '#ffffff'}, 500, function () {
                                    _anim.removeClass('added-revenue');
                                });
                            });
                            $('#editrevenueModal').modal('toggle');
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                            setTimeout(function () {
                                submit_btn.removeAttr('disabled');
                                submit_btn.html('Update');
                            }, 1000);
                        }
                    }
                });
                return false;
            }
        });
    };

    var delete_transaction = function () {
        j(document).on("click", ".delete-transaction", function () {
            var _this = j(this);
            var t_id = _this.attr('data-id');
            var transaction_item_row = j('.transaction_item_'+t_id);

            j.confirm({
                title: "Confirmation",
                content: "Do you want to delete this transaction?",
                buttons: {
                    confirm: {
                        text: "Continue",
                        btnClass: 'btn-default',
                        action: function () {
                            j.ajax({
                                url: _url + 'Invoices/destroyTransaction',
                                method: 'post',
                                data: {
                                    id: _this.attr('data-id'),
                                },
                                success: function (e) {
                                    new PNotify({
                                        title: 'Notification!',
                                        text: 'success',
                                        type: 'success'
                                    });

                                    transaction_item_row.html('');
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default'
                    }
                }
            });
        });
    };

    var load_group_revenue_ar_filter_date = function () {
        if(j('#filter_date').length) {
            j(document).on("change", "#filter_date", function () {
                var month_year = j(this).val();
                var selected_group_id = j('#selected_group_id').val();
                if(selected_group_id != 0) {
                }
                load_group_revenue(selected_group_id, month_year);
                load_group_ar(selected_group_id, month_year);
                load_group_revenue_ar();
            });

            j(document).on("click", ".back_group_revenu_ar_table", function () {
                j('.revenue_table').addClass('hide');
                j('.ar_table').addClass('hide');
                j('.group_revenu_ar_table').removeClass('hide');

                j('.revenue_ar').removeClass('hide');
                j('.group_revenue').addClass('hide');
                j('.group_ar').addClass('hide');

                j('#selected_group_id').val(0);
            });

            j(document).on("click", ".viewrevenueModal", function () {
                var _this = j(this);
                var group_name = _this.data('groupname');
                var group_id = _this.data('id');
                j('#selected_group_id').val(group_id);

                var month_year = j('#filter_date').val();
                j('.revenue_table').removeClass('hide');
                j('.group_revenu_ar_table').addClass('hide');
                j('.group_name_text').html(group_name);

                j('.revenue_ar').addClass('hide');
                j('.group_revenue').removeClass('hide');

                load_group_revenue(group_id, month_year);

            });

            j(document).on("click", ".viewarModal", function () {
                var _this = j(this);
                var group_name = _this.data('groupname');
                var group_id = _this.data('id');
                j('#selected_group_id').val(group_id);

                var month_year = j('#filter_date').val();
                j('.ar_table').removeClass('hide');
                j('.group_revenu_ar_table').addClass('hide');
                j('.group_name_text').html(group_name);

                j('.revenue_ar').addClass('hide');
                j('.group_ar').removeClass('hide');

                load_group_ar(group_id, month_year);

            });
        };
    };

    var load_group_ar = function (group_id, month_year) {
        if(j('.group_revenu_ar_tbody').length) {
            j('.total_group_ar').text('0.00');
            _pre_html = '';
            _pre_html += '<tr>';
            _pre_html += '<td colspan="6">Loading...</td>';
            _pre_html += '<tr>';
            j('.ar_body').html(_pre_html);

            j.ajax({
                url: _url + 'Invoices/get_group_ar',
                method: 'post',
                dataType: 'json',
                data: {
                    month_year: month_year,
                    group_id: group_id
                },
                success: function (e) {
                    var _html = '';
                    if (e != '') {
                        for (var i = 0; i < e.data.length; i++) {
                            _html += '<tr>';
                            _html += '<td>' + (i+1) + '</td>';
                            _html += '<td>' + e.data[i].customer_name + '</td>';
                            _html += '<td>' + e.data[i].invoice_no + '</td>';
                            _html += '<td>' + moment(e.data[i].invoice_date).format("MM/DD/YYYY") + '</td>';
                            // _html += '<td>' + e.data[i].line_item_name + '</td>';
                            _html += '<td class="text-right">' + e.data[i].ar + '</td>';
                            _html += '</tr>';
                        }
                        _html += '<tr>';
                            _html += '<td></td>';
                            _html += '<td></td>';
                            _html += '<td></td>';
                            _html += '<td class="text-right"><strong>Total: </strong></td>';
                            _html += '<td class="text-right"><strong>' + e.ar_total + '</strong></td>';
                        _html += '</tr>';
                    } else {
                        _html += '<tr>';
                        _html += '<td colspan="5" class="text-center">No result</td>';
                        _html += '<tr>';
                    }
                    j('.ar_body').html(_html);
                    j('.total_group_ar').text(e.ar_total);

                }
            });
        }
    };

    var load_group_revenue = function (group_id, month_year) {
        if(j('.group_revenu_ar_tbody').length) {
            j('.total_group_revenue').text('0.00');
            _pre_html = '';
            _pre_html += '<tr>';
            _pre_html += '<td colspan="6">Loading...</td>';
            _pre_html += '<tr>';
            j('.revenue_body').html(_pre_html);

            j.ajax({
                url: _url + 'Invoices/get_group_revenue',
                method: 'post',
                dataType: 'json',
                data: {
                    month_year: month_year,
                    group_id: group_id
                },
                success: function (e) {
                    var _html = '';
                    if (e != '') {
                        for (var i = 0; i < e.data.length; i++) {
                            _html += '<tr>';
                            _html += '<td>' + (i+1) + '</td>';
                            _html += '<td>' + e.data[i].client_name + '</td>';
                            _html += '<td>' + e.data[i].invoice_number + '</td>';
                            _html += '<td>' + moment(e.data[i].transaction_date).format("MM/DD/YYYY") + '</td>';
                            // _html += '<td>' + e.data[i].line_item_name + '</td>';
                            _html += '<td class="text-right">' + e.data[i].revenue + '</td>';
                            _html += '</tr>';
                        }
                        _html += '<tr>';
                            _html += '<td></td>';
                            _html += '<td></td>';
                            _html += '<td></td>';
                            _html += '<td class="text-right"><strong>Total: </strong></td>';
                            _html += '<td class="text-right"><strong>' + e.revenue_total + '</strong></td>';
                        _html += '</tr>';
                    } else {
                        _html += '<tr>';
                        _html += '<td colspan="5" class="text-center">No result</td>';
                        _html += '<tr>';
                    }
                    j('.revenue_body').html(_html);
                    j('.total_group_revenue').text(e.revenue_total);

                }
            });
        }
    };

    var load_group_revenue_ar = function () {
        if(j('.group_revenu_ar_tbody').length) {
            var month_year = j('#filter_date').val();
            var table_body = j('.group_revenu_ar_tbody');
            _pre_html = '';
            _pre_html += '<tr>';
            _pre_html += '<td colspan="3">Loading...</td>';
            _pre_html += '<tr>';
            table_body.html(_pre_html);
            j('.total_revenue').text('0.00');
            j('.total_ar').text('0.00');

            j.ajax({
                url: _url + 'Invoices/get_group_revenue_ar',
                method: 'post',
                dataType: 'json',
                data: {
                    month_year: month_year,
                },
                success: function (e) {
                    var _html = '';
                    if (e != '') {
                        for (var i = 0; i < e.data.length; i++) {
                            _html += '<tr>';
                            _html += '<td>' + e.data[i].group_name + '</td>';
                            _html += '<td class="text-right"><a href="javascript:void(0);" data-id="' + e.data[i].GID + '" data-groupname="' + e.data[i].group_name + '" class="viewrevenueModal">' + e.data[i].revenue + '</a></td>';
                            _html += '<td class="text-right"><a href="javascript:void(0);" data-id="' + e.data[i].GID + '" data-groupname="' + e.data[i].group_name + '" class="viewarModal">' + e.data[i].ar + '</a></td>';
                            _html += '</tr>';
                        }
                        _html += '<tr>';
                            _html += '<td class="text-right"><strong>Total: </strong></td>';
                            _html += '<td class="text-right"><a href="javascript:void(0);" data-id="0" data-groupname="All" class="viewrevenueModal"><strong>' + e.revenue_total + '</strong></a></td>';
                            _html += '<td class="text-right"><a href="javascript:void(0);" data-id="0" data-groupname="All" class="viewarModal"><strong>' + e.ar_total + '</strong></a></td>';
                        _html += '</tr>';
                    } else {
                        _html += '<tr>';
                        _html += '<td colspan="9" class="text-center">No result</td>';
                        _html += '<tr>';
                    }
                    table_body.html(_html);
                    j('.total_revenue').text(e.revenue_total);
                    j('.total_ar').text(e.ar_total);
                }
            });
        }
    };

    var ar_date_filter = function () {
        j(document).on("change", "#filterDate", function () {
            var check = moment(j(this).val());
            var filterDate = check.format('Y-MM');
            var href = new URL(window.location.href);
            href.searchParams.set('filterDate', filterDate);
            window.location.href = href;
        });
    }

    var add_ar = function () {
        if(j('.add_ar').length) {
            j('.add_ar').validate({
                rules: {
                    invoice_no: {required: true},
                    invoice_date: {required: true},
                    customer_name: {required: true},
                    amount_due: {required: true},
                    groupid: {required: true},
                    item_name: {required: true},
                },
                showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.addClass('is-invalid');
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.removeClass('is-invalid');
                    });
                }, submitHandler: function (form) {
                    var _this = j(form);
                    var submit_btn = _this.find('.btn-submit');
                    submit_btn.attr('disabled', 'disabled');
                    submit_btn.html('Loading...');
    
                    var data = _this.serializeArray(); // convert form to array
                    var filterDate = j('#filterDate').val();
                    data.push({name: 'filterDate', value: filterDate});
    
                    j.ajax({
                        url: _this.attr('action'),
                        dataType: 'json',
                        method: 'post',
                        data: data,
                        success: function (e) {
                            if (e.status === "ok") {
                                setTimeout(function () {
                                    submit_btn.removeAttr('disabled');
                                    submit_btn.html('Add');
                                }, 1000);
    
                                new PNotify({
                                    title: 'Notification!',
                                    text: 'success',
                                    type: 'success'
                                });
                                var html = '';
                                var tb_row = j('.ar_row');
    
                                html += '<tr class="added-ar ar_item_' + e.invoice.ID + '">';
                                html += '<td>' + e.invoice.ID + '</td>';
                                html += '<td>' + e.invoice.month + '</td>';
                                html += '<td>' + e.invoice.year + '</td>';
                                html += '<td>' + e.invoice.invoice_no + '</td>';
                                html += '<td>' + e.invoice.invoice_date + '</td>';
                                html += '<td>' + e.invoice.customer_name + '</td>';
                                html += '<td>' + e.invoice.group_name + '</td>';
                                html += '<td>' + e.invoice.item_name + '</td>';
                                html += '<td>' + e.invoice.amount_due + '</td>';
                                html += '<td class="text-center">';
                                html += '<button data-id="' + e.invoice.ID + '" data-toggle="modal" data-target="#editarModal" class="btn btn-light btn-sm mr-1 edit-ar"><i class="fa fa-edit"></i></button>';
                                html += '<button class="btn btn-light btn-sm delete-ar" data-id="' + e.invoice.ID + '"><i class="fa fa-trash"></i></button>';
                                html += '</td>';
                                html += '</tr>';
                                tb_row.prepend(html);
    
                                j('.added-ar').animate({backgroundColor: '#337ab7'}, 500, function () {
                                    var _anim = j(this);
                                    _anim.animate({backgroundColor: '#ffffff'}, 500, function () {
                                        _anim.removeClass('added-ar');
                                    });
                                });
                                // _this[0].reset();
                            } else {
                                new PNotify({
                                    title: 'Notification!',
                                    text: 'error',
                                    type: 'error'
                                });
                                setTimeout(function () {
                                    submit_btn.removeAttr('disabled');
                                    submit_btn.html('Add');
                                }, 1000);
                            }
                        }
                    });
                    return false;
                }
            });
        }
    };

    var edit_ar = function () {
        if(j('.edit-ar').length) {
            j(document).on("click", ".edit-ar", function () {
                var _this = j(this);
                var id = _this.attr('data-id');
                var form = j('.edit_ar');
    
                j.ajax({
                    url: _url + 'Invoices/getAr',
                    dataType: 'json',
                    method: 'post',
                    data: {id: id},
                    success: function (e) {
                        if (e.status === "ok") {
                            form.find('#edit_ar_id').val(e.data.ID);
                            form.find('#edit_invoice_no').val(e.data.invoice_no);
                            form.find('#edit_invoice_date').val(e.data.invoice_date);
                            form.find('#edit_customer_name').val(e.data.customer_name).trigger('chosen:updated');
                            form.find('#edit_amount_due').val(e.data.amount_due);
                            form.find('#edit_groupid').val(e.data.groupid).trigger('chosen:updated');
                            form.find('#edit_item_name').val(e.data.item_name).trigger('chosen:updated');
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'error',
                                type: 'error'
                            });
                        }
                    }
                });
            });
        }
    };

    var update_ar = function () {
        if(j('.edit-ar').length) {
            j('.edit_ar').validate({
                rules: {
                    edit_ar_id: {required: true},
                    edit_invoice_no: {required: true},
                    edit_invoice_date: {required: true},
                    edit_customer_name: {required: true},
                    edit_amount_due: {required: true},
                    edit_groupid: {required: true},
                    edit_item_name: {required: true},
                },
                showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.addClass('is-invalid');
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.removeClass('is-invalid');
                    });
                }, submitHandler: function (form) {
                    var _this = j(form);
                    var submit_btn = _this.find('.btn-submit');
                    submit_btn.attr('disabled', 'disabled');
                    submit_btn.html('Loading...');
                    var t_id = _this.find('#edit_ar_id').val();
                    var transaction_item_row = j('.ar_item_'+t_id);
    
                    var data = _this.serializeArray(); // convert form to array
    
                    j.ajax({
                        url: _this.attr('action'),
                        dataType: 'json',
                        method: 'post',
                        data: data,
                        success: function (e) {
                            if (e.status === "ok") {
                                setTimeout(function () {
                                    submit_btn.removeAttr('disabled');
                                    submit_btn.html('Update');
                                }, 1000);
    
                                new PNotify({
                                    title: 'Notification!',
                                    text: 'success',
                                    type: 'success'
                                });
                                transaction_item_row.html('');                            
                                var html = '';
                                html += '<td>' + e.invoice.ID + '</td>';
                                html += '<td>' + e.invoice.month + '</td>';
                                html += '<td>' + e.invoice.year + '</td>';
                                html += '<td>' + e.invoice.invoice_no + '</td>';
                                html += '<td>' + e.invoice.invoice_date + '</td>';
                                html += '<td>' + e.invoice.customer_name + '</td>';
                                html += '<td>' + e.invoice.group_name + '</td>';
                                html += '<td>' + e.invoice.item_name + '</td>';
                                html += '<td>' + e.invoice.amount_due + '</td>';
                                html += '<td class="text-center">';
                                html += '<button data-id="' + e.invoice.ID + '" data-toggle="modal" data-target="#editarModal" class="btn btn-light btn-sm mr-1 edit-ar"><i class="fa fa-edit"></i></button>';
                                html += '<button class="btn btn-light btn-sm delete-ar" data-id="' + e.invoice.ID + '"><i class="fa fa-trash"></i></button>';
                                html += '</td>';
                                transaction_item_row.html(html);
    
                                transaction_item_row.animate({backgroundColor: '#337ab7'}, 500, function () {
                                    var _anim = j(this);
                                    _anim.animate({backgroundColor: '#ffffff'}, 500, function () {
                                        _anim.removeClass('added-ar');
                                    });
                                });
                                $('#editarModal').modal('toggle');
                            } else {
                                new PNotify({
                                    title: 'Notification!',
                                    text: 'error',
                                    type: 'error'
                                });
                                setTimeout(function () {
                                    submit_btn.removeAttr('disabled');
                                    submit_btn.html('Update');
                                }, 1000);
                            }
                        }
                    });
                    return false;
                }
            });
        }
    };

    var delete_ar = function () {
        if(j('.edit-ar').length) {
            j(document).on("click", ".delete-ar", function () {
                var _this = j(this);
                var t_id = _this.attr('data-id');
                var transaction_item_row = j('.ar_item_'+t_id);
    
                j.confirm({
                    title: "Confirmation",
                    content: "Do you want to delete this AR?",
                    buttons: {
                        confirm: {
                            text: "Continue",
                            btnClass: 'btn-default',
                            action: function () {
                                j.ajax({
                                    url: _url + 'Invoices/destroyAr',
                                    method: 'post',
                                    data: {
                                        id: _this.attr('data-id'),
                                    },
                                    success: function (e) {
                                        new PNotify({
                                            title: 'Notification!',
                                            text: 'success',
                                            type: 'success'
                                        });
    
                                        transaction_item_row.html('');
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: "Cancel",
                            btnClass: 'btn-default'
                        }
                    }
                });
            });
        }
    };


    return {
        init: init
    };
})(jQuery);

(function () {
    JA.javascript.init();
})(jQuery);
(function (j) {
    j(window).resize(function () {
        var t = j('.ui-dialog');
        var w = j(this);
        t.css({
            position: 'fixed',
            top: (w.height() / 2) - (t.height() / 2),
            left: (w.width() / 2) - (t.width() / 2)
        });
    });
    j.cnfrm = function (options, callback) {
        var _body = j('body');
        var settings = j.extend({
            title: "Confirm",
            text: "Add some text",
            cancel: function () {},
            accept: function () {}
        }, options);
        var _html = j('<div>');
        _html.addClass('confirm-dialog');
        if (_body.find('.confirm-dialog').length === 0) {
            _body.append(_html);
        }
        j('.confirm-dialog').html(settings.text);
        j('.confirm-dialog').dialog({
            title: settings.title,
            draggable: false,
            resizable: false,
            maxWidth: 400,
            modal: true,
            fluid: true,
            open: function (event, ui) {
                var t = j(this).parents('.ui-dialog');
                var w = j(window);
                t.css({
                    position: 'fixed',
                    top: (w.height() / 2) - (t.height() / 2),
                    left: (w.width() / 2) - (t.width() / 2)
                });
                j('.ui-dialog-buttonset .ui-button').addClass('btn btn-info btn-sm').blur();
            },
            close: function () {
                j(this).dialog("close");
                settings.cancel.call(this);
            },
            buttons: {
                "No": function () {
                    j(this).dialog("close");
                    settings.cancel.call(this);
                },
                "Yes": function () {
                    j(this).dialog("close");
                    settings.accept.call(this);
                }
            }
        });
    };
})(jQuery);


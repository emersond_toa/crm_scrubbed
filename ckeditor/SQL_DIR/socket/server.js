const io = require('socket.io')(3000);

const users = {};
io.on('connection', function(socket){
  socket.on('message', function(msg){
    io.emit('message', msg);
  });
  socket.on('camera', function(msg){
    io.emit('camera', msg);
  });
  socket.on('stime', function(msg){
    io.emit('stime', msg);
  });
  
  socket.on('chatuser', function(name){
    users[socket.id] = name;
    socket.broadcast.emit('chatuser', name);
  });
  socket.on('chat', function(msg){
    socket.broadcast.emit('chat', {message:msg,name: users[socket.id]});
  });
  
  socket.on('timetrack', function(msg){
    io.emit('timetrack', msg);
  });

  socket.on('raffle', function(msg){
    io.emit('raffle', msg);
  });


});

